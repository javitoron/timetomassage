package com.fingerhits.massageassistant.exportImport;

import android.app.DownloadManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 01/03/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class ExportImportPresenterTest {
    private ExportImportPresenter exportImportPresenter;

    @Mock ExportImportActivity view;
    @Mock ExportImportInteractor interactor;
    @Mock DownloadManager mManager;

    @Before
    public void setUp() throws Exception {
        exportImportPresenter = new ExportImportPresenter(view, interactor);
    }


    @Test
    public void initViewTest() {
        when(interactor.numberOfMassages()).thenReturn(10);
        when(interactor.filesToImportNames()).thenReturn(new String[1]);

        exportImportPresenter.initView();

        verify(interactor).numberOfMassages();
        verify(view).showExportOptions(10);

        verify(interactor).updateFilesToImportList();
        verify(view).showImportOptions( interactor.filesToImportNames() );
    }

    @Test
    public void doImportTest_ok()throws Exception{
        when(interactor.doImport(0)).thenReturn(10);

        exportImportPresenter.doImport(0);

        verify(interactor).doImport(0);
        verify(view).importFinishedOk(10);
    }

    @Test
    public void doImportTest_error() throws Exception{
//        doThrow(new Exception()).when(interactor).doImport(0);
        when(interactor.doImport(0)).thenThrow(new Exception());

        exportImportPresenter.doImport(0);

        verify(interactor).doImport(0);
        verify(view, never()).importFinishedOk(anyInt());

    }

    @Test
    public void doExportTest_ok() throws Exception{
        when(interactor.filesToImportNames()).thenReturn(new String[1]);

        exportImportPresenter.doExport();

        verify(interactor).doExport();
        verify(interactor).updateFilesToImportList();
        verify(interactor).filesToImportNames();

        verify(view).exportFinishedOk(any(String[].class));
    }


    @Test
    public void doExportTest_error() throws Exception{
        //TODO: Fix throw exception on void function
 /*       doThrow(new IOException()).when(interactor).doExport(mManager);

        exportImportPresenter.doExport(mManager);

        verify(interactor).doExport(mManager);
        //verify(interactor, never()).updateFilesToImportList();
        verify(interactor, never()).filesToImportNames();

        verify(view, never()).exportFinishedOk(any(String[].class));*/

    }


}
