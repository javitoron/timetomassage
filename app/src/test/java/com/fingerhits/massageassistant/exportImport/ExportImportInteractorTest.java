package com.fingerhits.massageassistant.exportImport;

import android.app.DownloadManager;
import android.content.Context;
import android.content.res.Resources;

import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 27/02/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class ExportImportInteractorTest {
    private ExportImportInteractor exportImportInteractor;

    @Mock MassageAssistantDBHelper databaseHelper;
    @Mock Context mContext;
    @Mock
    ExportedFilesManager mExportedFilesManager;
    @Mock DataXmlImporter importer;
    @Mock DataXmlExporter exporter;
    @Mock DownloadManager mManager;
    @Mock Resources mRes;


    @Before
    public void setUp() throws Exception {
        exportImportInteractor = new ExportImportInteractor(databaseHelper, mContext,
                mExportedFilesManager, importer, exporter);

        exportImportInteractor.importer.inject(mContext);
        exportImportInteractor.exporter.inject(mContext);
    }


    @Test
    public void numberOfMassagesTest(){
        when(databaseHelper.numberOfMassages()).thenReturn(1);
        exportImportInteractor.numberOfMassages();

        verify(databaseHelper).numberOfMassages();
    }

    @Test
    public void updateFilesToImportListTest() throws Exception {
        //when(mExportedFilesManager.getFilesList()).thenReturn(new File[0] );
        exportImportInteractor.updateFilesToImportList();

        verify(mExportedFilesManager).loadFileList();
        verify(mExportedFilesManager).getFilesList();
        verify(mExportedFilesManager).getFileNamesList();
    }

    @Test
    public void doImportTest() throws Exception{
        exportImportInteractor.setFileList(new File[1]);
        when(importer.doImport(any(File.class))).thenReturn(0);
        exportImportInteractor.doImport(0);
        verify(importer).doImport(any(File.class));
    }

    @Test
    public void doExportTest() throws Exception{
        when(mContext.getResources()).thenReturn(mRes);
        when(mRes.getString(anyInt())).thenReturn("");
        exportImportInteractor.doExport();
        verify(exporter).export();
        /*verify(mManager).addCompletedDownload(anyString(), anyString(),
                eq(false), eq("text/xml"), anyString(), anyInt(), eq(true));
*/
    }

}
