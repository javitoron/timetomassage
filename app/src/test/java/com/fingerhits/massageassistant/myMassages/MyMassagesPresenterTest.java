package com.fingerhits.massageassistant.myMassages;

import android.database.Cursor;
import android.util.Log;

import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.utils.MiscUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Date;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;

/**
 * Created by Javier Torón on 03/03/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class MyMassagesPresenterTest {

    private MyMassagesPresenter presenter;

    @Mock MyMassagesActivity view;
    @Mock MyMassagesInteractor interactor;

    @Before
    public void setUp() throws Exception {
        presenter = new MyMassagesPresenter(view, interactor);

        PowerMockito.mockStatic(Log.class);
    }


    @Test
    public void deleteMassageTest() {
        when(interactor.getMassage(1L)).thenReturn(new MassageRecord(
                MiscUtils.currentLocalUser(),
                "pepe", new Date(), MassageRecord.MODE_ADVANCED));
        presenter.deleteMassage(1L);
        verify(interactor).getMassage(1L);

        verify(interactor).deleteMassage(1L);

        verify(interactor).getAllMassageRecords_LastToFirst();
        verify(view).updateList(any(Cursor.class));
        verify(view).sendDeleteTracker(
                MassageRecord.modeToAbbr(MassageRecord.MODE_ADVANCED));
    }

    @Test
    public void savePatientTest() {
        presenter.savePatient(1L, "pepe");

        verify(interactor).savePatient(1L, "pepe");
    }

   @Test
    public void saveCommentsTest() {
       presenter.saveComments(1L, "esguince");

       verify(interactor).saveComments(1L, "esguince");
    }

    @Test
    public void updateMassageOrListTest_withMassages() {

        Cursor massageData = mock(Cursor.class);

        when(massageData.moveToFirst()).thenReturn(true);
        PowerMockito.when(interactor.getMassageCursor(1L)).thenReturn(massageData);

        presenter.updateMassageOrList(1L);

        verify(interactor).getMassageCursor(1L);

        verify(view).updateMassage(massageData);
    }


    @Test
    public void updateMassageOrListTest_withNoMoreMassages() throws Exception{

        Cursor massageData = mock(Cursor.class);

        when(massageData.moveToFirst()).thenReturn(false);
        PowerMockito.when(interactor.getMassageCursor(1L)).thenReturn(massageData);

        presenter.updateMassageOrList(1L);

        verify(interactor).getAllMassageRecords_LastToFirst();
        verify(view).updateList(any(Cursor.class));
    }



    @Test
    public void updateListTest() {
        presenter.updateList();

        verify(interactor).getAllMassageRecords_LastToFirst();
        verify(view).updateList(any(Cursor.class));
    }


    @Test
    public void sendMassageSummaryTest_massageNotNull(){

        when(interactor.getMassage(0L)).thenReturn(new MassageRecord(
                MiscUtils.currentLocalUser(),
                "pepe", new Date(), MassageRecord.MODE_ADVANCED));

        presenter.sendMassageSummary(0L);

        verify(interactor).getMassage(0L);

        verify(interactor).getTreatedZones(0L);

        verify(view).sendEmail(any(MassageRecord.class), any(Cursor.class));

    }

    @Test
    public void sendMassageSummaryTest_massageNull(){

        when(interactor.getMassage(1L)).thenReturn(null);

        presenter.sendMassageSummary(1L);

        verify(interactor).getMassage(1L);

        verify(interactor, never()).getTreatedZones(1L);

        verify(view, never()).sendEmail(
                any(MassageRecord.class), any(Cursor.class));

    }



}
