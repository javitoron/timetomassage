package com.fingerhits.massageassistant.myMassages;

import android.content.Context;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Created by Javier Torón on 03/03/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class MyMassagesInteractorTest {

    private MyMassagesInteractor interactor;

    @Mock
    MassageAssistantDBHelper mDatabaseHelper;
    @Mock Context mContext;
    @Mock MySharedPreferences mPrefs;

    @Before
    public void setUp() throws Exception {
        interactor = new MyMassagesInteractor(mDatabaseHelper, mContext, mPrefs);
    }

    @Test
    public void deleteMassageTest(){
        interactor.deleteMassage(1L);
        verify(mDatabaseHelper).deleteMassageRecord(1L);
    }

    @Test
    public void getAllMassageRecords_LastToFirstTest() {
        interactor.getAllMassageRecords_LastToFirst();
        verify(mDatabaseHelper)
                .getAllMassageRecords(
                        MassageAssistantDBHelper.SORT_LAST_TO_FIRST);
    }

    @Test
    public void getMassageTest(){
        interactor.getMassage(1L);
        verify(mDatabaseHelper).getMassageRecord(1L);
    }

    @Test
    public void getMassageCursorTest() {
        interactor.getMassageCursor(1L);
        verify(mDatabaseHelper).getMassageCursor(1L);
    }


    @Test
    public void getTreatedZonesTest(){
        interactor.getTreatedZones(1L);
        verify(mDatabaseHelper).getTreatedZonesInMassage(1L);
    }


    @Test
    public void savePatientTest() {
        interactor.savePatient(1L, "pepe");
        verify(mDatabaseHelper).savePatientToMassageRecord(1L, "pepe");
    }

    @Test
    public void saveCommentsTest() {
        interactor.saveComments(1L, "esguince");
        verify(mDatabaseHelper).saveCommentsToMassageRecord(1L, "esguince");
    }


}
