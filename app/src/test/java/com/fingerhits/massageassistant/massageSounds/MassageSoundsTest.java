package com.fingerhits.massageassistant.massageSounds;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.media.SoundPool;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.utils.MiscUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 04/04/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({MiscUtils.class})
public class MassageSoundsTest {
    private MassageSounds mSounds;

    @Mock private Context context;
    @Mock private MySharedPreferences prefs;
    @Mock private SoundPool soundPool;
    @Mock private Resources res;

    private String[] soundLabels = {"sound0", "sound1", "sound2", "sound3"};
    private TypedArray soundIds;


    @Before
    public void setup() throws Exception {
        when(context.getResources()).thenReturn(res);
        when(res.getStringArray(R.array.sounds_array)).thenReturn(soundLabels);
        when(res.obtainTypedArray(R.array.soundIds_array)).thenReturn(soundIds);

        mSounds = MassageSounds.getInstance(context, prefs);

        mSounds.setDependencies(context, prefs, soundPool);
    }


    //Tested on each reload method
    public void reloadAllTest() throws Exception {
    }

    @Test
    public void reloadDingTimeTest() throws Exception {
        when(prefs.getInt(MySharedPreferences.PREFS_DINGTIMESOUND_KEY))
                .thenReturn(1);

        //Call
        mSounds.reloadDingTime();

        //Assertion
        verify(soundPool).load( any(Context.class), eq(1), eq(0));
    }

    @Test
    public void reloadHalfMassageTest() throws Exception {
        when(prefs.getInt(MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY))
                .thenReturn(2);

        //Call
        mSounds.reloadHalfMassage();

        //Assertion
        verify(soundPool).load(any(Context.class), eq(2), eq(0));
    }

    @Test
    public void reloadFullMassageTest() throws Exception {
        when(prefs.getInt(MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY))
                .thenReturn(3);

        //Call
        mSounds.reloadFullMassage();

        //Assertion
        verify(soundPool).load(any(Context.class), eq(3), eq(0));
    }

    @Test
    public void playDingTimeTest() throws Exception {
        mSounds.setIdDingTime(10);
        //Call
        mSounds.playDingTime();

        //Assertion
        verify(soundPool).play(10, 1, 1, 1, 0, 1);
    }

    @Test
    public void playHalfMassageTest() throws Exception {
        mSounds.setIdHalfMassage(20);
        //Call
        mSounds.playHalfMassage();

        //Assertion
        verify(soundPool).play(20, 1, 1, 1, 0, 1);
    }

    @Test
    public void playFullMassageTest() throws Exception {
        mSounds.setIdFullMassage(30);
        //Call
        mSounds.playFullMassage();

        //Assertion
        verify(soundPool).play(30, 1, 1, 1, 0, 1);
    }

    //To test soundLabels
//    public void dingTimeLabelTest()
//    public void halfMassageLabelTest()

    @Test
    public void fullMassageLabelTest() throws Exception {
        when(prefs.getInt(MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY))
                .thenReturn(3);

        PowerMockito.mockStatic(MiscUtils.class);
        when(MiscUtils.findInTypedArray(any(TypedArray.class), eq(3)))
                .thenReturn(3);

        //Call & Assertion
        assertEquals("sound3", mSounds.fullMassageLabel());
    }

/*        int soundIndex = MiscUtils.findInTypedArray(soundIds, soundId);

        if(soundIndex == -1){
            resetSoundValue(soundTypeLabel);
            soundIndex = MiscUtils.findInTypedArray(soundIds,
                    mPrefs.getInt(soundTypeLabel));
        }

        if(soundIndex < 0 || soundIndex >= soundLabels.length ) {
            return soundLabels[0];
        }
        else {
            return soundLabels[soundIndex];
        }
    }*/


}
