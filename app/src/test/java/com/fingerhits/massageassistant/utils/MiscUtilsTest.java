package com.fingerhits.massageassistant.utils;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import android.content.res.Resources;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.options.MySharedPreferences;

/**
 * Created by javi on 29/01/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class MiscUtilsTest{


    @SuppressWarnings("SpellCheckingInspection")
    private static final String FAKE_IDS =
            "9e4b53a2-6a4b-47fe-967b-d50f12467557,5d01ebbf-0713-48d8-ba4d-0dfeabea691f,f65c66f6-8154-4b0f-8fd7-38cbba4c68e7,d2579cd0-d1b5-441d-bde7-fb284be978f8";

    @SuppressWarnings("SpellCheckingInspection")
    private static final String SUPERUSER_ID =
            "9e4b53a2-6a4b-47fe-967b-d50f12467557";

    @SuppressWarnings("SpellCheckingInspection")
    private static final String NORMALUSER_ID =
            "fffhhhas-6a4b-47fe-967b-d50f12467557";

    @Mock
    MySharedPreferences mMockPrefs;

    @Mock
    Resources mMockRes;


    @Test
    public void extractTitleTest() {
        assertEquals("Error extracting correct title",
                MiscUtils.extractTitle("<h1>titulo</h1><p>todo bien</p>"),
                "titulo");

        assertEquals("Error extracting empty title",
                MiscUtils.extractTitle("<h2>titulo</h2><p>todo bien</p>"),
                "");
    }

}
