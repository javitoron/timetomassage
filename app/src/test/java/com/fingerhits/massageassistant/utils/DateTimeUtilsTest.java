package com.fingerhits.massageassistant.utils;

import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.fingerhits.massageassistant.utils.DateTimeUtils;


/**
 * Created by javi on 28/01/2016.
 */
public class DateTimeUtilsTest {
    @Test
    public void durationToStringTest(){
        assertEquals("Fails conversion from 0 to 00:00", "00:00",
                DateTimeUtils.durationToString(0) );
        assertEquals("Fails conversion less than 1000 to 00:00", "00:00",
                DateTimeUtils.durationToString(999) );
        assertEquals("Fails conversion 1000 to 00:01", "00:01",
                DateTimeUtils.durationToString(1000) );
        assertEquals("Fails conversion from milliseconds to mm:ss", "01:30",
                DateTimeUtils.durationToString(90000) );
        assertEquals("Fails conversion from milliseconds to hh:mm:ss", "02:30:00",
                DateTimeUtils.durationToString(9000000) );
    }

    @Test
    public void durationToZoneStringTest(){
        assertEquals("Fails conversion from 0 to 00 00", "00 00",
                DateTimeUtils.durationToZoneString(0) );
        assertEquals("Fails conversion less than 1000 to 00 00", "00 00",
                DateTimeUtils.durationToZoneString(999) );
        assertEquals("Fails conversion 1000 to 00 01", "00 01",
                DateTimeUtils.durationToZoneString(1000) );
        assertEquals("Fails conversion from milliseconds to mm ss", "01 30",
                DateTimeUtils.durationToZoneString(90000) );
        assertEquals("Fails conversion from milliseconds to hh mm ss", "02 30 00",
                DateTimeUtils.durationToZoneString(9000000) );
    }


    @Test
    public void durationStringToMillisecondsTest() {
        assertEquals("Fails conversion from 00:00 to 0", 0,
                DateTimeUtils.durationStringToMilliseconds("00:00"));
        assertEquals("Fails conversion 00:01 to 1000", 1000,
                DateTimeUtils.durationStringToMilliseconds("00:01"));
        assertEquals("Fails conversion mm:ss to milliseconds", 90000,
                DateTimeUtils.durationStringToMilliseconds("01:30"));
        assertEquals("Fails conversion hh:mm:ss to milliseconds", 9000000,
                DateTimeUtils.durationStringToMilliseconds("02:30:00"));
    }


    @Test
    public void durationZoneStringToMilliseconds() {
        assertEquals("Fails conversion from 00 00 to 0", 0,
                DateTimeUtils.durationZoneStringToMilliseconds("00 00"));
        assertEquals("Fails conversion 00 01 to 1000", 1000,
                DateTimeUtils.durationZoneStringToMilliseconds("00 01"));
        assertEquals("Fails conversion mm ss to milliseconds", 90000,
                DateTimeUtils.durationZoneStringToMilliseconds("01 30"));
        assertEquals("Fails conversion hh mm ss to milliseconds", 9000000,
                DateTimeUtils.durationZoneStringToMilliseconds("02 30 00"));
    }

    @Test
    public void sameDayTest() {
        assertTrue("Says false for same dates and time",
                DateTimeUtils.sameDay(
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-03-30 00:00:00",
                                DateTimeUtils.SQLLiteFormat()),
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-03-30 00:00:00",
                                DateTimeUtils.SQLLiteFormat())));

        assertTrue("Says false for same dates and different time",
                DateTimeUtils.sameDay(
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-03-30 00:00:00",
                                DateTimeUtils.SQLLiteFormat()),
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-03-30 12:00:00",
                                DateTimeUtils.SQLLiteFormat())));

        assertFalse("Says true for dates with different year",
                DateTimeUtils.sameDay(
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-03-30 00:00:00",
                                DateTimeUtils.SQLLiteFormat()),
                        DateTimeUtils.FormattedStringToDateTime(
                                "2016-03-30 00:00:00",
                                DateTimeUtils.SQLLiteFormat())));
        assertFalse("Says true for dates with different month",
                DateTimeUtils.sameDay(
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-03-30 00:00:00",
                                DateTimeUtils.SQLLiteFormat()),
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-04-30 00:00:00",
                                DateTimeUtils.SQLLiteFormat())));
        assertFalse("Says true for dates with different day",
                DateTimeUtils.sameDay(
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-03-30 00:00:00",
                                DateTimeUtils.SQLLiteFormat()),
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-03-29 00:00:00",
                                DateTimeUtils.SQLLiteFormat())));

        assertFalse("Says true when passing a null date",
                DateTimeUtils.sameDay( null,
                        DateTimeUtils.FormattedStringToDateTime(
                                "2015-03-29 00:00:00",
                                DateTimeUtils.SQLLiteFormat())));
    }

}
