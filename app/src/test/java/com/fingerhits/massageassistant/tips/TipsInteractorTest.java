package com.fingerhits.massageassistant.tips;

import android.content.res.Resources;
import android.util.Log;

import com.fingerhits.massageassistant.options.MySharedPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 23/02/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class TipsInteractorTest {
    @Mock private MySharedPreferences mPrefs;
    @Mock private Resources mRes;

    TipsInteractor tipsInteractor;

    private final int tipsMaxIndex = TipsInteractor.getStaticCount() - 1;
    private final String twoTipsShown =
            TipsInteractor.tipNames[0] + "|" + TipsInteractor.tipNames[1];
    private final String allTipsShown = allTips();

    @Before
    public void setUp() throws Exception {
        tipsInteractor = new TipsInteractor();

        tipsInteractor.mPrefs = mPrefs;
        tipsInteractor.mRes = mRes;

        PowerMockito.mockStatic(Log.class);
    }

    @Test
    public void setUpFromMainTest_noTipsShown() throws Exception {
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn("");
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn("");

        tipsInteractor.setUp(0, false);

        assertEquals("Should show first tip", 0, tipsInteractor.getCurrentTip());
    }

    @Test
    public void setUpFromMainTest_twoTipsShown() throws Exception {
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn(twoTipsShown);
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn(twoTipsShown);
        tipsInteractor.setUp(0, false);

        assertEquals("Should show third tip", 2,
                tipsInteractor.getCurrentTip());
    }

    @Test
    public void setUpFromMainTest_allTipsShown() throws Exception {
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn(allTipsShown);
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn(allTipsShown);
        tipsInteractor.setUp(0, false);

        assertEquals("Should return last tip", tipsMaxIndex,
                tipsInteractor.getCurrentTip());
    }

    @Test
    public void setUpFromTipsTest_noTipsShown() throws Exception {
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn("");
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn("");

        tipsInteractor.setUp(0, true);

        assertEquals("Should show first tip", 0,
                tipsInteractor.getCurrentTip());
    }

    @Test
    public void setUpFromTipsTest_privacyTipShown() throws Exception {
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn(twoTipsShown);
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn(twoTipsShown);
        tipsInteractor.setUp(2, true);

        assertEquals("Should return third tip", 2,
                tipsInteractor.getCurrentTip());
    }

    @Test
    public void setUpFromTipsTest_allTipsShown() throws Exception {
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn(allTipsShown);
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn(allTipsShown);
        tipsInteractor.setUp(10, true);

        assertEquals("Should return last tip", tipsMaxIndex,
                tipsInteractor.getCurrentTip());
    }


    @Test
    public void firstUnseenTip() throws Exception {
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn("");
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn("");

        assertEquals("Should return first tip", 0,
                tipsInteractor.firstUnseenTip());

        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn(twoTipsShown);
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn(twoTipsShown);
        assertEquals("Should return third tip", 2,
                tipsInteractor.firstUnseenTip());

        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn(allTipsShown);
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn(allTipsShown);
        assertEquals("Should return last tip", tipsMaxIndex,
                tipsInteractor.firstUnseenTip());
    }


    @Test
    public void goPrevTest() throws Exception {
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn("");
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn("");

        tipsInteractor.setUp(0, true);
        tipsInteractor.goPrev();

        assertEquals("Should be on first tip", 0,
                tipsInteractor.getCurrentTip());

        tipsInteractor.setUp(5, true);
        tipsInteractor.goPrev();

        assertEquals("Should be on fifth tip", 4,
                tipsInteractor.getCurrentTip());

        tipsInteractor.setUp(tipsMaxIndex + 10, true);
        tipsInteractor.goPrev();

        assertEquals("Should be on one prev to last tip", tipsMaxIndex - 1,
                tipsInteractor.getCurrentTip());
    }

    @Test
    public void goNextTest() throws Exception {
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn("");
        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN))
                .thenReturn("");

        tipsInteractor.setUp(0, true);
        tipsInteractor.goNext();

        assertEquals("Should be on second tip", 1,
                tipsInteractor.getCurrentTip());

        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn(twoTipsShown);
        tipsInteractor.setUp(4, true);
        tipsInteractor.goNext();

        assertEquals("Should be on fifth tip", 5,
                tipsInteractor.getCurrentTip());

        when( mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN))
                .thenReturn(allTipsShown);
        tipsInteractor.goNext();

        assertEquals("Should be on last tip", tipsMaxIndex,
                tipsInteractor.getCurrentTip());
    }


    /* PRIVATE METHODS */
    private static String allTips(){
        int numberOfTips = TipsInteractor.getStaticCount();
        int i;
        String s_tips = "";
        for(i = 0; i < numberOfTips - 1; i++ ){
            s_tips += TipsInteractor.tipNames[i] + "|";
        }

        return s_tips;
    }

}
