package com.fingerhits.massageassistant.tips;

import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by javit on 23/02/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class TipsPresenterTest {
    private TipsPresenter tipsPresenter;

    @Mock private TipsActivity tipsActivity;
    @Mock private TipsInteractor tipsInteractor;

    @Before
    public void setup(){
        tipsPresenter =
                new TipsPresenter(tipsActivity, tipsInteractor);

        PowerMockito.mockStatic(Log.class);
    }


    @Test
    public void initViewTest_noTipSelected(){
        tipsPresenter.initView(0);

        verify(tipsInteractor).firstUnseenTip();
        verify(tipsInteractor).setUp(anyInt(), eq(true));
    }

    @Test
    public void initViewTest_tipSelected(){
        tipsPresenter.initView(4);

        verify(tipsInteractor).setUp(eq(4), eq(true));
    }

    @Test
    public void prevTipTest(){
        tipsPresenter.prevTip();

        verify(tipsInteractor).goPrev();
    }

    @Test
    public void nextTipTest(){
        tipsPresenter.nextTip();

        verify(tipsInteractor).goNext();
    }

}
