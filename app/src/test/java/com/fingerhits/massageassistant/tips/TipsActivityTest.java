package com.fingerhits.massageassistant.tips;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Created by Javier Torón on 24/05/2016.
 TODO: TipsActivity unit tests
 */

public class TipsActivityTest {

 /*   ActivityController activityController;
    TipsActivity activity;

    @Before
    public void setup(){
    }

    @Test
    public void prevTipTest() {
    }


    @Test
    public void nextTipTest(){
     }

    @Test
    public void showTipTest() {
    }

    @Test
    public void showPrevBtnTest() {
    }

    @Test
    public void showNextBtnTest(){
    }

    @Test
    public void showTipsNumberTest(){
    }

    /******* Helper methods *********/
}
