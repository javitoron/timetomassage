package com.fingerhits.massageassistant.main;


import android.util.Log;

import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.tips.TipsInteractor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 24/05/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class MainPresenterTest {


    private MainPresenter mMainPresenter;

    @Mock private MainActivity mMainView;

    @Mock private MainInteractor mMainInteractor;
    @Mock private TipsInteractor mTipsInteractor;


    @Before
    public void setup(){
        mMainPresenter =
                new MainPresenter(mMainView, mMainInteractor, mTipsInteractor);

        PowerMockito.mockStatic(Log.class);
    }

    @Test
    public void initViewTest(){
        mMainPresenter.initView();

        verify(mMainInteractor).fixMinimalZoneDurationOldValue();
        verify(mMainInteractor).fixWarningSchemaFrom1To2();

        //verify(mMainPresenter).manageMassagesHistory();

    }

    @Test
    public void showMassagesInfoTest_noMassages() {
        when(mMainInteractor.getNumberOfMassages()).thenReturn(0);
        mMainPresenter.showMassagesInfo();

        verify(mMainView).showMassagesList(0);
        verify(mMainView).noLastMassage();
    }

    @Test
    public void showMassagesInfoTest(){
        setup1Massage();
        mMainPresenter.showMassagesInfo();

        verify(mMainView).showMassagesList(1);
        verify(mMainView).showLastMassage(any(MassageRecord.class));
    }

    @Test
    public void showTipTest_allTipsShown(){
        when(mTipsInteractor.allTipsShown()).thenReturn(true);
        mMainPresenter.showTip();

        verify(mTipsInteractor, never()).getTipString();
        verify(mMainView, never()).showTip(anyInt(), anyString());
    }

    @Test
    public void showTipTest_tipsPending(){
        when(mTipsInteractor.allTipsShown()).thenReturn(false);
        mMainPresenter.showTip();

        verify(mTipsInteractor).getTipString();
        verify(mMainView).showTip(anyInt(), anyString());
    }

    @Test
    public void saveMassagePatientTest(){
        setup1Massage();
        mMainPresenter.showMassagesInfo();
        mMainPresenter.saveMassagePatient("pepe");

        verify(mMainInteractor).savePatientIntoMassage(1L, "pepe");
    }

    @Test
    public void saveMassageCommentsTest(){
        setup1Massage();
        mMainPresenter.showMassagesInfo();
        mMainPresenter.saveMassageComments("esguince");
        verify(mMainInteractor).saveCommentsIntoMassage(1L, "esguince");

    }

    /******* Helper methods *********/
    private void setup1Massage(){
        when(mMainInteractor.getNumberOfMassages()).thenReturn(1);
        when(mMainInteractor.getLastMassage())
                .thenReturn(new MassageRecord(1L, "paco", 1L, null, null, 0,
                        MassageRecord.MODE_SIMPLE, "", "", "", ""));
    }
}
