package com.fingerhits.massageassistant.main;

import android.content.Context;
import android.util.Log;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 19/02/2017.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class MainInteractorTest {

    @Mock private MySharedPreferences mPrefs;
    @Mock private MassageAssistantDBHelper mDatabaseHelper;
    @Mock private Context mContext;

    MainInteractor mainInteractor;


    @Before
    public void setUp() throws Exception {
        mainInteractor = new MainInteractor();

        mainInteractor.mPrefs = mPrefs;
        mainInteractor.mDatabaseHelper = mDatabaseHelper;
        mainInteractor.mContext = mContext;

        PowerMockito.mockStatic(Log.class);

    }


    @Test
    public void getNumberOfMassages() throws Exception {
        mainInteractor.getNumberOfMassages();

        verify(mDatabaseHelper).cleanUpMassages();
        verify(mDatabaseHelper).numberOfMassages();
    }

    @Test
    public void getLastMassageId() throws Exception {
        mainInteractor.getLastMassageId();

        verify(mDatabaseHelper).getLastInsertedMassageId();

    }

    @Test
    public void getLastMassage_noMassage() throws Exception {
        when(mDatabaseHelper.getLastInsertedMassageId()).thenReturn(0L);

        MassageRecord lastMassage = mainInteractor.getLastMassage();

        assertNull(lastMassage);

    }

    @Test
    public void getLastMassage() throws Exception {
        when(mDatabaseHelper.getLastInsertedMassageId()).thenReturn(1L);
        when(mDatabaseHelper.getMassageRecord(1L))
                .thenReturn(new MassageRecord(1L, "paco", 1L, null, null, 0,
                        MassageRecord.MODE_SIMPLE, "", "", "", ""));

        MassageRecord lastMassage = mainInteractor.getLastMassage();

        assertEquals("Wrong lastMassage ID", 1L, lastMassage.getId());
        assertEquals("Wrong lastMassage patient", "paco", lastMassage.getPatient());
    }

    @Test
    public void savePatientIntoMassage() throws Exception {
        mainInteractor.savePatientIntoMassage(1L, "pepe");

        verify(mDatabaseHelper).savePatientToMassageRecord(1L, "pepe");
    }

    @Test
    public void saveCommentsIntoMassage() throws Exception {
        mainInteractor.saveCommentsIntoMassage(1L, "esguince");

        verify(mDatabaseHelper).saveCommentsToMassageRecord(1L, "esguince");
    }

    @Test
    public void fixMinimalZoneDurationOldValue_fix() throws Exception {
        when(mPrefs.getBool(
                MySharedPreferences.PREFS_MINIMALZONEDURATION_FIXED_KEY))
                .thenReturn(false);
        when(mPrefs.getInt(
                MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY))
                .thenReturn(60);

        mainInteractor.fixMinimalZoneDurationOldValue();

        verify(mPrefs).saveBool(
                MySharedPreferences.PREFS_MINIMALZONEDURATION_FIXED_KEY, true);
        verify(mPrefs).saveInt(
                MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY, 0);
    }

    @Test
    public void fixMinimalZoneDurationOldValue_leave_unchanged() throws Exception {
        when(mPrefs.getBool(
                MySharedPreferences.PREFS_MINIMALZONEDURATION_FIXED_KEY))
                .thenReturn(false);
        when(mPrefs.getInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY))
                .thenReturn(40);

        mainInteractor.fixMinimalZoneDurationOldValue();

        verify(mPrefs).saveBool(
                MySharedPreferences.PREFS_MINIMALZONEDURATION_FIXED_KEY, true);
        verify(mPrefs, never())
                .saveInt(eq(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY),
                        anyInt());
    }

    @Test
    public void fixWarningSchemaFrom1_2() throws Exception {
        when(mPrefs.getBool(
                MySharedPreferences.PREFS_WARNINGS_SCHEMA_1_2_FIXED_KEY))
                .thenReturn(false);
        when(mPrefs.contains(MySharedPreferences.PREFS_SOUNDENABLED_KEY))
                .thenReturn(true);

        mainInteractor.fixWarningSchemaFrom1To2();

        verify(mPrefs).saveBool(
                MySharedPreferences.PREFS_WARNINGS_SCHEMA_1_2_FIXED_KEY, true);
        verify(mPrefs).saveBool(
                eq(MySharedPreferences.PREFS_VISUALWARNINGSENABLED_KEY),
                anyBoolean());
    }

}