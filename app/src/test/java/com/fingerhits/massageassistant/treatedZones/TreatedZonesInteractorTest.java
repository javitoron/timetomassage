package com.fingerhits.massageassistant.treatedZones;

import android.content.Context;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Created by Javier Torón on 03/03/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class TreatedZonesInteractorTest {

    private TreatedZonesInteractor interactor;

    @Mock
    MassageAssistantDBHelper mDatabaseHelper;
    @Mock Context mContext;
    @Mock MySharedPreferences mPrefs;

    @Before
    public void setUp() throws Exception {
        interactor =
                new TreatedZonesInteractor(mDatabaseHelper, mContext, mPrefs);
    }


    @Test
    public void getMassageTest(){
        interactor.getMassage(1L);
        verify(mDatabaseHelper).getMassageRecord(1L);
    }

    @Test
    public void getTreatedZoneTest(){
        interactor.getTreatedZone(1L);
        verify(mDatabaseHelper).getTreatedZoneRecord(1L);
    }

    @Test
    public void getTreatedZonesTest(){
        interactor.getTreatedZones(1L);
        verify(mDatabaseHelper).getTreatedZonesInMassage(1L);
    }

    @Test
    public void saveNewCodZoneTest(){
        interactor.saveNewCodZone(1L, TreatedZoneRecord.CODZONE_ABDOMEN);
        verify(mDatabaseHelper).saveCodZoneInTreatedZoneRecord(1L,
                TreatedZoneRecord.CODZONE_ABDOMEN);
    }



    @Test
    public void deleteTreatedZoneTest(){
        interactor.deleteTreatedZone(1L, 2L);
        verify(mDatabaseHelper).deleteTreatedZoneRecord(1L, 2L);
    }


    @Test
    public void deleteMassageTest(){
        interactor.deleteMassage(1L);
        verify(mDatabaseHelper).deleteMassageRecord(1L);
    }
}
