package com.fingerhits.massageassistant.treatedZones;

import android.database.Cursor;
import android.util.Log;

import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.MiscUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Date;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 03/03/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class TreatedZonesPresenterTest {
    private TreatedZonesPresenter presenter;

    @Mock TreatedZonesActivity view;
    @Mock TreatedZonesInteractor interactor;

    @Before
    public void setUp() throws Exception {
        presenter = new TreatedZonesPresenter(view, interactor);

        PowerMockito.mockStatic(Log.class);
    }


    @Test
    public void initViewTest_massageFinished(){
        when(interactor.getMassage(1L)).thenReturn(new MassageRecord(
                MiscUtils.currentLocalUser(),
                "pepe", new Date(), MassageRecord.MODE_ADVANCED));

        presenter.initView(1L, true);

        verify(interactor).getMassage(1L);
        verify(view).showMassage(any(MassageRecord.class));

        verify(view).showTreatedZones(true);
    }


    @Test
    public void initViewTest_massageNotFinished(){
        presenter.initView(1L, false);

        verify(view).hideMassage();

        verify(view).showTreatedZones(false);
    }


    @Test
    public void saveNewCodZoneTest() {
        Long duration = 10000L;
        Date start = new Date();
        Date end = new Date(start.getTime() + ( duration * 1000 ) );

        when(interactor.getTreatedZone(1L)).thenReturn(new TreatedZoneRecord(
                1L, 2L, TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                Divisions.CODDIVISION_DEFAULT, TreatedZoneRecord.CODZONE_HEADNECK,
                start, end, duration, "comments" ));

        presenter.saveNewCodZone(1L, 3 ); //TreatedZoneRecord.CODZONE_TORSO);

        verify(interactor).getTreatedZone(1L);
        verify(interactor).saveNewCodZone( 1L, TreatedZoneRecord.CODZONE_TORSO);
    }

    @Test
    public void deleteTreatedZoneTest(){
        Long duration = 10000L;
        Date start = new Date();
        Date end = new Date(start.getTime() + ( duration * 1000 ) );

        when(interactor.getTreatedZone(1L)).thenReturn(new TreatedZoneRecord(
                1L, 2L, TreatedZoneRecord.CODPOSITION_RIGHTSIDE,
                Divisions.CODDIVISION_DEFAULT, TreatedZoneRecord.CODZONE_HEAD,
                start, end, duration, "comments" ));

        presenter.deleteTreatedZone(1L, true);

        verify(interactor).getTreatedZone(1L);

        verify(interactor).deleteTreatedZone( 2L, 1L);

        verify(interactor).getMassage(2L);

        verify(view).removeTreatedZone( any(MassageRecord.class),
                any(TreatedZoneRecord.class), eq(true));
    }


    @Test
    public void deleteMassageTest() {
        when(interactor.getMassage(1L)).thenReturn(new MassageRecord(
                MiscUtils.currentLocalUser(),
                "pepe", new Date(), MassageRecord.MODE_ADVANCED));
        presenter.deleteMassage(1L);
        verify(interactor).getMassage(1L);

        verify(interactor).deleteMassage(1L);

        verify(view).removeMassage(
                MassageRecord.modeToAbbr(MassageRecord.MODE_ADVANCED));
    }


    @Test
    public void sendMassageSummaryTest_massageNotNull(){

        when(interactor.getMassage(0L)).thenReturn(new MassageRecord(
                MiscUtils.currentLocalUser(),
                "pepe", new Date(), MassageRecord.MODE_ADVANCED));

        presenter.sendMassageSummary(0L);

        verify(interactor).getMassage(0L);

        verify(interactor).getTreatedZones(0L);

        verify(view).sendEmail(any(MassageRecord.class), any(Cursor.class));

    }


    @Test
    public void sendMassageSummaryTest_massageNull(){

        when(interactor.getMassage(1L)).thenReturn(null);

        presenter.sendMassageSummary(1L);

        verify(interactor).getMassage(1L);

        verify(interactor, never()).getTreatedZones(1L);

        verify(view, never()).sendEmail(
                any(MassageRecord.class), any(Cursor.class));

    }

}
