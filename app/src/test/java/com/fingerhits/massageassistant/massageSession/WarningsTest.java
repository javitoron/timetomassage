package com.fingerhits.massageassistant.massageSession;

import android.content.Context;
import android.os.Vibrator;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.massageSounds.MassageSounds;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.chronos.Warnings;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.utils.GraphicUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 03/04/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({MassageSounds.class, MySharedPreferences.class, GraphicUtils.class})
public class WarningsTest {
    private Warnings warnings;

    @Mock private MassageSessionActivity mActivity;
    @Mock private MassageSounds mSounds;
    @Mock private MySharedPreferences mPrefs;
    @Mock private Context context;
    @Mock private Vibrator mVibrator;


    @Before
    public void setup() throws Exception{
        PowerMockito.mockStatic(GraphicUtils.class);

        when(mActivity.getSystemService(Context.VIBRATOR_SERVICE))
                .thenReturn(mVibrator);

        when(mPrefs.getInt(
                MySharedPreferences.PREFS_MASSAGEDURATION_KEY)).thenReturn(20);

        when(mPrefs.getInt(
                MySharedPreferences.PREFS_DINGTIME_KEY )).thenReturn(5);

        when(mPrefs.getBool(
                MySharedPreferences.PREFS_DINGTIMEWARNINGSENABLED_KEY))
                    .thenReturn(true);

        when(mPrefs.getBool(
                MySharedPreferences.PREFS_HALFMASSAGEDURATIONWARNINGENABLED_KEY))
                    .thenReturn(true);

        when(mPrefs.getBool(
                MySharedPreferences.PREFS_FULLMASSAGEDURATIONWARNINGENABLED_KEY))
                    .thenReturn(true);

        when(mPrefs.getBool(
                MySharedPreferences.PREFS_VISUALWARNINGSENABLED_KEY))
                    .thenReturn(true);

        when(mPrefs.getBool(
                MySharedPreferences.PREFS_VIBRATIONWARNINGSENABLED_KEY))
                    .thenReturn(true);

        when(mPrefs.getBool(
                MySharedPreferences.PREFS_SOUNDWARNINGSENABLED_KEY))
                    .thenReturn(true);

        warnings = Warnings.getInstance(mActivity, mSounds, mPrefs);
        warnings.setDependencies(mActivity, mSounds, mPrefs, mVibrator);
    }

    @Test
    public void reloadPrefsTest(){
        //Call
        warnings.reloadPrefs();

        //Assertions
        assertEquals(1200, warnings.getMassageDurationInSeconds());
        assertEquals(300, warnings.getDingTimeInSeconds()) ;
        assertTrue( warnings.getDingTimeWarningsEnabled());
        assertTrue( warnings.getHalfMassageWarningEnabled());
        assertTrue( warnings.getFullMassageWarningEnabled());
        assertTrue( warnings.getVisualWarningEnabled());
        assertTrue( warnings.getVibrationWarningEnabled());
        assertTrue( warnings.getSoundWarningsEnabled());

        assertTrue( warnings.getSomeWarningEnabled());
    }


    @Test
    public void tickTest_0() {
        //Call
        warnings.tick(0L);

        //Assertions
        verify(mVibrator, never()).vibrate(500);
        verify(mSounds, never()).playDingTime();

        PowerMockito.verifyStatic(never());
        GraphicUtils.blinkScreen(any(LinearLayout.class), eq(1));
    }


    @Test
    public void tickTest_full() {
        //Call
        warnings.tick(1200L);

        //Assertions
        //Full
        verify(mVibrator).vibrate(Mockito.any(long[].class), eq(-1));
        verify(mSounds).playFullMassage();

        PowerMockito.verifyStatic(times(1));
        GraphicUtils.blinkScreen(any(LinearLayout.class), eq(3));

        //Not ding
        verify(mVibrator, never()).vibrate(500L);
        verify(mSounds, never()).playDingTime();

        PowerMockito.verifyStatic(never());
        GraphicUtils.blinkScreen(any(LinearLayout.class), eq(1));
    }

    @Test
    public void tickTest_half() {
        //Call
        warnings.tick(600L);

        //Assertions
        //Full
        verify(mVibrator).vibrate(Mockito.any(long[].class), eq(-1));
        verify(mSounds).playHalfMassage();

        PowerMockito.verifyStatic(times(1));
        GraphicUtils.blinkScreen(any(LinearLayout.class), eq(2));

        //Not ding
        verify(mVibrator, never()).vibrate(500L);
        verify(mSounds, never()).playDingTime();

        PowerMockito.verifyStatic(never());
        GraphicUtils.blinkScreen(any(LinearLayout.class), eq(1));
    }

    @Test
    public void tickTest_ding() {
        //Call
        warnings.tick(300L);

        //Assertions
        verify(mVibrator).vibrate(500L);
        verify(mSounds).playDingTime();

        PowerMockito.verifyStatic(times(1));
        GraphicUtils.blinkScreen(any(LinearLayout.class), eq(1));
    }

}
