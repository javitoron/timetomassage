package com.fingerhits.massageassistant.massageSession;

import android.content.ContentResolver;
import android.content.Context;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.fingerhits.massageassistant.massagesession.MassageSessionInteractor;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

/**
 * Created by Javier Torón on 23/03/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class, TreatedZoneRecord.class})
public class MassageSessionInteractorTest {

    @Mock private MySharedPreferences mPrefs;
    @Mock private MassageAssistantDBHelper mDatabaseHelper;
    @Mock private Context mContext;

    MassageSessionInteractor massageSessionInteractor;

    @Before
    public void setUp() throws Exception {
        massageSessionInteractor = new MassageSessionInteractor(mDatabaseHelper, mContext,
                mPrefs);

        PowerMockito.mockStatic(Log.class);

    }

    @Test
    public void getMassageTest() {
        MassageRecord massage = massageSessionInteractor.getMassage(1L);

        verify(mDatabaseHelper).getMassageRecord(1L);
    }

    @Test
    public void getDurationInLastPositionTest() {
        Long durationInLastPosition =
                massageSessionInteractor.getDurationInLastPosition(1L);
        verify(mDatabaseHelper).getDurationInLastPosition(1L);
    }


    @Test
    public void getLastTreatedZone_returnsNull_Test() {
        when(mDatabaseHelper.getLastTreatedZoneInMassage(1L)).thenReturn(
                new Cursor() {
                    @Override
                    public int getCount() {
                        return 0;
                    }

                    @Override
                    public int getPosition() {
                        return 0;
                    }

                    @Override
                    public boolean move(int offset) {
                        return false;
                    }

                    @Override
                    public boolean moveToPosition(int position) {
                        return false;
                    }

                    @Override
                    public boolean moveToFirst() {
                        return false;
                    }

                    @Override
                    public boolean moveToLast() {
                        return false;
                    }

                    @Override
                    public boolean moveToNext() {
                        return false;
                    }

                    @Override
                    public boolean moveToPrevious() {
                        return false;
                    }

                    @Override
                    public boolean isFirst() {
                        return false;
                    }

                    @Override
                    public boolean isLast() {
                        return false;
                    }

                    @Override
                    public boolean isBeforeFirst() {
                        return false;
                    }

                    @Override
                    public boolean isAfterLast() {
                        return false;
                    }

                    @Override
                    public int getColumnIndex(String columnName) {
                        return 0;
                    }

                    @Override
                    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
                        return 0;
                    }

                    @Override
                    public String getColumnName(int columnIndex) {
                        return null;
                    }

                    @Override
                    public String[] getColumnNames() {
                        return new String[0];
                    }

                    @Override
                    public int getColumnCount() {
                        return 0;
                    }

                    @Override
                    public byte[] getBlob(int columnIndex) {
                        return new byte[0];
                    }

                    @Override
                    public String getString(int columnIndex) {
                        return null;
                    }

                    @Override
                    public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {

                    }

                    @Override
                    public short getShort(int columnIndex) {
                        return 0;
                    }

                    @Override
                    public int getInt(int columnIndex) {
                        return 0;
                    }

                    @Override
                    public long getLong(int columnIndex) {
                        return 0;
                    }

                    @Override
                    public float getFloat(int columnIndex) {
                        return 0;
                    }

                    @Override
                    public double getDouble(int columnIndex) {
                        return 0;
                    }

                    @Override
                    public int getType(int columnIndex) {
                        return 0;
                    }

                    @Override
                    public boolean isNull(int columnIndex) {
                        return false;
                    }

                    @Override
                    public void deactivate() {

                    }

                    @Override
                    public boolean requery() {
                        return false;
                    }

                    @Override
                    public void close() {

                    }

                    @Override
                    public boolean isClosed() {
                        return false;
                    }

                    @Override
                    public void registerContentObserver(ContentObserver observer) {

                    }

                    @Override
                    public void unregisterContentObserver(ContentObserver observer) {

                    }

                    @Override
                    public void registerDataSetObserver(DataSetObserver observer) {

                    }

                    @Override
                    public void unregisterDataSetObserver(DataSetObserver observer) {

                    }

                    @Override
                    public void setNotificationUri(ContentResolver cr, Uri uri) {

                    }

                    @Override
                    public Uri getNotificationUri() {
                        return null;
                    }

                    @Override
                    public boolean getWantsAllOnMoveCalls() {
                        return false;
                    }

                    @Override
                    public void setExtras(Bundle extras) {

                    }

                    @Override
                    public Bundle getExtras() {
                        return null;
                    }

                    @Override
                    public Bundle respond(Bundle extras) {
                        return null;
                    }
                }
        );

        TreatedZoneRecord tz = massageSessionInteractor.getLastTreatedZone(1L);

        assertEquals(null, tz);

    }

   // @Test
    public void getLastTreatedZone_returnsTreatedZone_Test() throws Exception{
        Cursor mockedTreatedZones = mock(Cursor.class);

        when(mDatabaseHelper.getLastTreatedZoneInMassage(1L))
                .thenReturn(mockedTreatedZones);

        when(mockedTreatedZones.moveToFirst()).thenReturn(true);

        whenNew(TreatedZoneRecord.class).withArguments(mockedTreatedZones)
                .thenReturn(new TreatedZoneRecord( 0, 1L,
                        TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                        Divisions.CODDIVISION_LEFTRIGHT,
                        TreatedZoneRecord.CODZONE_RIGHTSIDE, new Date(),
                        new Date(), 10L, "comments" ) );

        TreatedZoneRecord tz = massageSessionInteractor.getLastTreatedZone(1L);

        assertEquals(tz.getIdMassage(), 1L);
    }


    @Test
    public void createMassage_Test() throws Exception{

        MassageRecord massage =
                massageSessionInteractor.createMassage(MassageRecord.MODE_ADVANCED);

        verify(mDatabaseHelper).insertMassageRecord(any(MassageRecord.class));
        verify(mDatabaseHelper).getMassageRecord(anyLong());
    }

    @Test
    public void insertTreatedZone_Test() throws Exception{
        Date start = new Date();
        massageSessionInteractor.insertTreatedZone(1L,
                TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                Divisions.CODDIVISION_LEFTRIGHT,
                TreatedZoneRecord.CODZONE_RIGHTSIDE, start, 10L);

        verify(mDatabaseHelper).insertTreatedZoneRecord(any(TreatedZoneRecord.class));
    }

    @Test
    public void finishMassage_Test() throws Exception{
        massageSessionInteractor.finishMassage(1L);

        verify(mDatabaseHelper).finishMassageRecord(1L);
    }

}
