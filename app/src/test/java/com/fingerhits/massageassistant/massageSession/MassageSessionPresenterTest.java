package com.fingerhits.massageassistant.massageSession;

import android.util.Log;
import android.widget.Button;

import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.MassageSessionInteractor;
import com.fingerhits.massageassistant.massagesession.MassageSessionPresenter;
import com.fingerhits.massageassistant.massagesession.chronos.MassageSessionChronosPresenter;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.positions.Position;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 23/03/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class, Position.class})
public class MassageSessionPresenterTest {
    private MassageSessionPresenter presenter;

    @Mock private MassageSessionActivity view;
    @Mock private MassageSessionInteractor interactor;
    @Mock private MassageSessionChronosPresenter chronosPresenter;
    @Mock private Position position;
    @Mock private MassageRecord massage;
    @Mock private TreatedZoneRecord tz;
    @Mock private Button pressedBtn;


    @Before
    public void setup(){
        presenter = new MassageSessionPresenter(view, chronosPresenter,
                interactor);

        PowerMockito.mockStatic(Log.class);
    }

    @Test
    public void initViewTest(){
        //Call
        presenter.initView();

        //Assertions
        verify(interactor).getScreenLock();
        verify(view).manageScreenLock(anyBoolean());
    }


    @Test
    public void setUpMenuTest(){
        //Call
        presenter.setUpMenu();

        //Assertions
        verify(interactor).getSoundWarningsEnabled();
        verify(view).toggleVolume(anyBoolean());
    }


    @Test
    public void goToOptionsTest(){
        presenter.setState(MassageSessionPresenter.STATE_NOT_STARTED);

        //Call
        presenter.goToOptions();

        //Assertions
        verify(view).goToOptionsActivity();
    }


    @Test
    public void showMassageDetailsTest(){
        presenter.setState(MassageSessionPresenter.STATE_NOT_STARTED);

        //Call
        presenter.showMassageDetails();

        //Assertions
        verify(view).goToTreatedZones(anyLong());
    }

    @Test
    public void muteClickTest_soundOn() {
        when(interactor.getSoundWarningsEnabled()).thenReturn(false);

        //Call
        presenter.muteClick();

        //Assertions
        verify(interactor).setSoundWarningsEnabled(true);
        verify(view).toggleVolume(true);
        verify(chronosPresenter).optionsChanged();

    }

    @Test
    public void muteClickTest_soundOff() {
        when(interactor.getSoundWarningsEnabled()).thenReturn(true);

        //Call
        presenter.muteClick();

        //Assertions
        verify(interactor).setSoundWarningsEnabled(false);
        verify(view).toggleVolume(false);
        verify(chronosPresenter).optionsChanged();
    }


    @Test
    public void pauseMassageTest(){
        when(position.lastPressed()).thenReturn(null);

        presenter.setPosition(position);

        presenter.setState(MassageSessionPresenter.STATE_RUNNING);

        //Call
        presenter.pauseMassage();

        //Assertions
        verify(interactor).getAnimationsDisabled();
        verify(view).pause(false);
        verify(chronosPresenter).pauseChronometers();
        verify(view).disableMassageControllers(anyInt());

        assertEquals(MassageSessionPresenter.STATE_PAUSED,
                presenter.getState());
    }


    //TODO: Test for advanced mode
    @Test
    public void resumeMassageTest_notAdvanced() {
        presenter.setState(MassageSessionPresenter.STATE_PAUSED);
        presenter.setMode(MassageRecord.MODE_SIMPLE);

        //Call
        presenter.resumeMassage();

        //Assertions
        verify(view).resume();

        //savePauseDuration(view.getPausedTime());

        verify(chronosPresenter).resumeChronometers();

        verify(view).enableMassageControllers(MassageRecord.MODE_SIMPLE);

        assertEquals(MassageSessionPresenter.STATE_RUNNING,
                presenter.getState());
    }


    @Test
    public void endMassageTest_notStarted() {
        presenter.setState(MassageSessionPresenter.STATE_NOT_STARTED);

        //Call
        presenter.endMassage();

        //Assertions
        verify(view).showEndNotSaving();
    }


    @Test
    public void endMassageTest_running() {

        presenter.setState(MassageSessionPresenter.STATE_RUNNING);
        presenter.setMode(MassageRecord.MODE_SIMPLE);

        when(massage.getModeAbbr())
                .thenReturn(MassageRecord.modeToAbbr(
                        MassageRecord.MODE_SIMPLE));
        when(massage.getId()).thenReturn(1L);
        presenter.setMassage(massage);

        when(interactor.finishMassage(1L)).thenReturn(30L);

        //Call
        presenter.endMassage();

        //Assertions
        verify(interactor).finishMassage(1L);
        verify(view).showEndSaving();

        verify(view).logEndMassage(30L,
                MassageRecord.modeToAbbr(MassageRecord.MODE_SIMPLE));
    }

    @Test
    public void changePositionTest() {
        presenter.setState(MassageSessionPresenter.STATE_WAITING_TO_SELECT_ZONE);
        presenter.setMode(MassageRecord.MODE_INTERMEDIATE);

        when(position.getPositionName()).thenReturn( "Lying face down" );
        when(view.loadIntermediatePosition(anyString(),anyString()))
                .thenReturn(position);
        presenter.setPosition(position);

        when(massage.getModeAbbr())
                .thenReturn(MassageRecord.modeToAbbr(
                        MassageRecord.MODE_INTERMEDIATE));
        presenter.setMassage(massage);

        //Call
        presenter.changePosition(TreatedZoneRecord.CODPOSITION_FACEDOWN);

        //Assertions
        verify(view).logChangePosition("Lying face down",
                MassageRecord.modeToAbbr(MassageRecord.MODE_INTERMEDIATE));
    }



    //  @Test
    public void changeDivisionTest_started_notSinglePosition() {
        presenter.setState(MassageSessionPresenter.STATE_WAITING_TO_SELECT_ZONE);
        presenter.setMode(MassageRecord.MODE_ADVANCED);

        when(position.getCodPosition()).thenReturn( TreatedZoneRecord.CODPOSITION_FACEUP );
        presenter.setPosition(position);

        //Call
        presenter.changeDivision(Divisions.CODDIVISION_DEFAULT);

        //Assertions
        verify(position).loadDivision(Divisions.CODDIVISION_DEFAULT);

        //Assertions for DivisionLoaded(false)
        verify(view).changeDisabledDivision(TreatedZoneRecord.CODPOSITION_FACEUP,
                Divisions.CODDIVISION_DEFAULT);
        verify(chronosPresenter).divisionChangedTo(Divisions.CODDIVISION_DEFAULT);

        verify(chronosPresenter).blinkZoneChrono();

        assertEquals(MassageSessionPresenter.STATE_WAITING_TO_SELECT_ZONE,
                presenter.getState());
    }

    //  @Test
    public void changeDivisionTest_started_singlePosition() {
        presenter.setState(MassageSessionPresenter.STATE_WAITING_TO_SELECT_ZONE);
        presenter.setMode(MassageRecord.MODE_ADVANCED);

        when(position.getCodPosition()).thenReturn( TreatedZoneRecord.CODPOSITION_FACEUP );
        presenter.setPosition(position);

        //Call
        presenter.changeDivision(Divisions.CODDIVISION_SINGLE);

        //Assertions
        verify(position).loadDivision(Divisions.CODDIVISION_SINGLE);

        //Assertions for DivisionLoaded(false)
        verify(view).changeDisabledDivision(TreatedZoneRecord.CODPOSITION_FACEUP,
                Divisions.CODDIVISION_SINGLE);
        verify(chronosPresenter).divisionChangedTo(Divisions.CODDIVISION_SINGLE);

        verify(view).getFullBodyBtn();

        assertEquals(MassageSessionPresenter.STATE_RUNNING,
                presenter.getState());
    }


    //  @Test
    public void startZoneMassageTest_waiting() {
        presenter.setState(MassageSessionPresenter.STATE_WAITING_TO_SELECT_ZONE);

        presenter.setPosition(position);

        //Call
        presenter.startZoneMassage(pressedBtn);

        //Assertions
        //saveUndefinedZone
        verify(chronosPresenter).setZoneChronoNormalColor();

        verify(position).pressButtonInDivision(pressedBtn);

        verify(chronosPresenter).startZoneChrono(pressedBtn,
                MassageSessionPresenter.STATE_WAITING_TO_SELECT_ZONE);

        assertEquals(MassageSessionPresenter.STATE_RUNNING,
                presenter.getState());
    }

    //  @Test
    public void startZoneMassageTest_running_single() {
        presenter.setState(MassageSessionPresenter.STATE_RUNNING);

        presenter.setPosition(position);
        presenter.setCurrentCodDivision(Divisions.CODDIVISION_SINGLE);

        //Call
        presenter.startZoneMassage(pressedBtn);

        //Assertions
        verify(position).pressButtonInDivision(pressedBtn);

        verify(chronosPresenter).startZoneChrono(pressedBtn,
                MassageSessionPresenter.STATE_WAITING_TO_SELECT_ZONE);

        assertEquals(MassageSessionPresenter.STATE_RUNNING,
                presenter.getState());
    }


    @Test
    public void startMassageTest_simple() {
        presenter.setMode(MassageRecord.MODE_SIMPLE);

        when(massage.getMode()).thenReturn(MassageRecord.MODE_SIMPLE);
        when(massage.getModeAbbr()).thenReturn("SIMPLE");
        when(massage.getId()).thenReturn(1L);
        when(massage.getDuration()).thenReturn(600L);
        when(massage.getDurationInMilliseconds()).thenReturn(600000L);

        when(interactor.createMassage(MassageRecord.MODE_SIMPLE)).thenReturn(massage);
        when(interactor.getMassageDurationInMinutes()).thenReturn(6000000);

        //Call
        presenter.startMassage(massage.getMode(), "");

        //Assertions
        verify(interactor).createMassage(MassageRecord.MODE_SIMPLE);

        //Assertions preparePresenter(mMassage)
        verify(view).hideSummaryMenu();
        assertEquals(MassageRecord.MODE_SIMPLE, presenter.getMode());
        assertEquals((Long) 1L, presenter.getIdMassage());

        verify(chronosPresenter).prepareMode(MassageRecord.MODE_SIMPLE, 600000L);

        //Assertions prepareForSimpleMode
        verify(view).prepareScreenForSimpleMode();

        //Assertions startMassage
        verify(view).logNewMassage(1L, "SIMPLE", 6000000);

        assertEquals(MassageSessionPresenter.STATE_RUNNING,
                presenter.getState());
    }


    @Test
    public void startMassageTest_intermediate() {
        presenter.setMode(MassageRecord.MODE_INTERMEDIATE);

        when(massage.getMode()).thenReturn(MassageRecord.MODE_INTERMEDIATE);
        when(massage.getModeAbbr()).thenReturn("INTERMEDIATE");
        when(massage.getId()).thenReturn(1L);
        when(massage.getDuration()).thenReturn(600L);
        when(massage.getDurationInMilliseconds()).thenReturn(600000L);

        when(interactor.createMassage(MassageRecord.MODE_INTERMEDIATE)).thenReturn(massage);
        when(interactor.getMassageDurationInMinutes()).thenReturn(6000000);

        when(view.getFullBodyBtnLabel()).thenReturn("Full body");

        //Call
        presenter.startMassage(massage.getMode(),
                TreatedZoneRecord.CODPOSITION_RIGHTSIDE);

        //Assertions
        verify(interactor).createMassage(MassageRecord.MODE_INTERMEDIATE);

        //Assertions preparePresenter(mMassage)
        assertEquals(MassageRecord.MODE_INTERMEDIATE, presenter.getMode());
        assertEquals((Long) 1L, presenter.getIdMassage());

        verify(chronosPresenter).prepareMode(MassageRecord.MODE_INTERMEDIATE,
                600000L);

        //Assertions prepareForIntermediateMode
        verify(view).prepareScreenForIntermediateMode(
                TreatedZoneRecord.CODPOSITION_RIGHTSIDE);

        //Assertions setPositionForIntermediateMode
        verify(view).loadIntermediatePosition("",
                TreatedZoneRecord.CODPOSITION_RIGHTSIDE);

        verify(view).getFullBodyBtnLabel();
        verify(chronosPresenter).loadIntermediatePosition(
                TreatedZoneRecord.CODPOSITION_RIGHTSIDE, "Full body", 0L);

        //Assertions startMassage
        verify(view).logNewMassage(1L, "INTERMEDIATE", 6000000);

        assertEquals(MassageSessionPresenter.STATE_RUNNING,
                presenter.getState());
    }


    @Test
    public void startMassageTest_advanced() {
        presenter.setMode(MassageRecord.MODE_ADVANCED);

        when(massage.getMode()).thenReturn(MassageRecord.MODE_ADVANCED);
        when(massage.getModeAbbr()).thenReturn("ADVANCED");
        when(massage.getId()).thenReturn(1L);
        when(massage.getDuration()).thenReturn(600L);
        when(massage.getDurationInMilliseconds()).thenReturn(600000L);

        when(interactor.createMassage(MassageRecord.MODE_ADVANCED)).thenReturn(massage);
        when(interactor.getMassageDurationInMinutes()).thenReturn(6000000);

        when(position.getCodPosition())
                .thenReturn(TreatedZoneRecord.CODPOSITION_SITTINGFRONT);

        PowerMockito.mockStatic(Position.class);
        when(Position.getPosition(
                        Matchers.any(MassageSessionActivity.class),
                        anyString()))
                .thenReturn(position);


        //Call
        presenter.startMassage(massage.getMode(),
                TreatedZoneRecord.CODPOSITION_SITTINGFRONT);

        //Assertions
        verify(interactor).createMassage(MassageRecord.MODE_ADVANCED);
        verify(view).logNewMassage(1L, "ADVANCED", 6000000);
        assertEquals(MassageSessionPresenter.STATE_RUNNING,
                presenter.getState());

        //Assertions preparePresenter(mMassage)
        assertEquals(MassageRecord.MODE_ADVANCED, presenter.getMode());
        assertEquals((Long) 1L, presenter.getIdMassage());
        verify(chronosPresenter).prepareMode(MassageRecord.MODE_ADVANCED,
                600000L);

        //Assertions prepareForAdvancedMode
        verify(view).prepareScreenForAdvancedMode(
                TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                Divisions.CODDIVISION_SINGLE);


        //Assertions setPositionForAdvancedMode
        assertEquals( Divisions.CODDIVISION_SINGLE,
                presenter.getCurrentCodDivision());
        verify(chronosPresenter).startFullChronoIn(600000L);
        verify(position).loadDivision(Divisions.CODDIVISION_SINGLE);
        chronosPresenter.changePositionChronoText(
                TreatedZoneRecord.CODPOSITION_SITTINGFRONT);

        //Assertions startPositionChronoForAdvancedMode( 0L );
        verify(chronosPresenter).startPositionChronoIn(0L);

        //Assertions divisionLoaded(true);
        verify(view).changeDisabledDivision(
                TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                Divisions.CODDIVISION_SINGLE);
        verify(chronosPresenter).divisionChangedTo(Divisions.CODDIVISION_SINGLE);
        verify(view).getFullBodyBtn();

    }


    @Test
    public void restartMassageTest_noIdMassage() {

        //Call
        presenter.restartMassage(-1L);

        //Assertions
        verify(view).errorResumingMassage();
    }


    @Test
    public void restartMassageTest_invalidIdMassage() {

        when(interactor.getMassage(1L)).thenReturn(null);

        //Call
        presenter.restartMassage(1L);

        //Assertions
        verify(interactor).getMassage(1L);
        verify(view).errorResumingMassage();
    }


    @Test
    public void restartMassageTest_simple() {
        presenter.setMode(MassageRecord.MODE_SIMPLE);

        when(massage.getMode()).thenReturn(MassageRecord.MODE_SIMPLE);
        when(massage.getModeAbbr()).thenReturn("SIMPLE");
        when(massage.getId()).thenReturn(1L);
        when(massage.getDuration()).thenReturn(600L);
        when(massage.getDurationInMilliseconds()).thenReturn(600000L);

        when(interactor.getMassage(1L)).thenReturn(massage);

        //Call
        presenter.restartMassage(1L);

        //Assertions
        verify(interactor).getMassage(1L);

        //Assertions preparePresenter(mMassage) Made in startMassageTest_simple
        //Assertions prepareForSimpleMode() Made in startMassageTest_simple
        //Assertions startMassage
        verify(view).logResumeMassage(1L, "SIMPLE");
        assertEquals(MassageSessionPresenter.STATE_RUNNING,
                presenter.getState());
    }


    @Test
    public void restartMassageTest_intermediate() {
        presenter.setMode(MassageRecord.MODE_INTERMEDIATE);

        when(massage.getMode()).thenReturn(MassageRecord.MODE_INTERMEDIATE);
        when(massage.getModeAbbr()).thenReturn("INTERMEDIATE");
        when(massage.getId()).thenReturn(1L);
        when(massage.getDuration()).thenReturn(600L);
        when(massage.getDurationInMilliseconds()).thenReturn(600000L);

        when(tz.getCodPosition())
                .thenReturn(TreatedZoneRecord.CODPOSITION_SITTINGBACK);

        when(interactor.getMassage(1L)).thenReturn(massage);
        when(interactor.getLastTreatedZone(1L)).thenReturn(tz);
        when(interactor.getDurationInLastPosition(1L)).thenReturn(300L);

        when(view.getFullBodyBtnLabel()).thenReturn("Full body");

        //Call
        presenter.restartMassage(1L);

        //Assertions
        verify(interactor).getMassage(1L);
        verify(interactor).getLastTreatedZone(1L);

        //Assertions preparePresenter(mMassage)
        //  Made in startMassageTest_intermediate
        //Assertions prepareForIntermediateMode()
        //  Made in startMassageTest_intermediate

        //Assertions setPositionForIntermediateMode. Only for duration.
        //  Rest made in startMassageTest_intermediate
        verify(chronosPresenter).loadIntermediatePosition(
                TreatedZoneRecord.CODPOSITION_SITTINGBACK, "Full body", 300000L);

        //Assertions startMassage
        verify(view).logResumeMassage(1L, "INTERMEDIATE");
        assertEquals(MassageSessionPresenter.STATE_RUNNING,
                presenter.getState());
    }


    @Test
    public void restartMassageTest_advanced() {
        presenter.setMode(MassageRecord.MODE_ADVANCED);

        when(massage.getMode()).thenReturn(MassageRecord.MODE_ADVANCED);
        when(massage.getModeAbbr()).thenReturn("ADVANCED");
        when(massage.getId()).thenReturn(1L);
        when(massage.getDuration()).thenReturn(600L);
        when(massage.getDurationInMilliseconds()).thenReturn(600000L);

        when(tz.getCodPosition())
                .thenReturn(TreatedZoneRecord.CODPOSITION_SITTINGBACK);
        when(tz.getCodDivision()).thenReturn(Divisions.CODDIVISION_UPPERLOWER);

        PowerMockito.mockStatic(Position.class);
        when(Position.getPosition(
                Matchers.any(MassageSessionActivity.class),
                anyString()))
                .thenReturn(position);

        when(interactor.getMassage(1L)).thenReturn(massage);
        when(interactor.getLastTreatedZone(1L)).thenReturn(tz);
        when(interactor.getDurationInLastPosition(1L)).thenReturn(300L);

        when(view.getFullBodyBtnLabel()).thenReturn("Full body");

        //Call
        presenter.restartMassage(1L);

        //Assertions
        verify(interactor).getMassage(1L);
        verify(interactor).getLastTreatedZone(1L);

        //Assertions preparePresenter(mMassage)
        //  Made in startMassageTest_advanced
        //Assertions prepareForAdvancedMode()
        //  Made in startMassageTest_advanced

        //Assertions setPositionForAdvancedMode. Only for duration.
        //  Rest made in startMassageTest_advanced
        verify(chronosPresenter).startPositionChronoIn(300000L);

        //Rest of assertions made in startMassageTest_advanced
    }


    @Test
    public void optionsChanged() {
        presenter.setState(MassageSessionPresenter.STATE_PAUSED);

        //Call
        presenter.optionsChanged();

        //Assertions
        verify(chronosPresenter).optionsChanged();
    }


    @Test
    public void resumeIfPausedTest_paused(){
        presenter.setState(MassageSessionPresenter.STATE_PAUSED);

        //Call
        presenter.resumeIfPaused();

        //Assertions
        verify(chronosPresenter).resumeChronometers();
    }


    @Test
    public void resumeIfPausedTest_notPaused(){
        presenter.setState(MassageSessionPresenter.STATE_RUNNING);

        //Call
        presenter.resumeIfPaused();

        //Assertions
        verify(chronosPresenter, never()).resumeChronometers();
    }

}