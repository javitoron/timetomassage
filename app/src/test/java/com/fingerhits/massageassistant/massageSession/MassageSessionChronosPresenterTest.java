package com.fingerhits.massageassistant.massageSession;

import android.widget.Button;
import android.widget.Chronometer;

import com.fingerhits.massageassistant.massagesession.MassageSessionInteractor;
import com.fingerhits.massageassistant.massagesession.MassageSessionPresenter;
import com.fingerhits.massageassistant.massagesession.chronos.MassageSessionChronosFragment;
import com.fingerhits.massageassistant.massagesession.chronos.MassageSessionChronosPresenter;
import com.fingerhits.massageassistant.massagesession.chronos.Warnings;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 03/04/2017.
 */

@RunWith(PowerMockRunner.class)
public class MassageSessionChronosPresenterTest {
    private MassageSessionChronosPresenter chronosPresenter;

    @Mock MassageSessionPresenter massageSessionPresenter;
    @Mock private MassageSessionInteractor interactor;
    @Mock private MassageSessionChronosFragment view;
    @Mock private Warnings warnings;
    @Mock private MassageRecord massage;
    @Mock private Chronometer chrono;
    @Mock private Button pressedBtn;


    @Before
    public void setup(){
        chronosPresenter = new MassageSessionChronosPresenter(view, interactor, warnings);
    }


    @Test
    public void setViewTest(){
        when(interactor.getMassageDurationInMilliseconds()).thenReturn(60000L);

        //Call
        chronosPresenter.setView();

        //Assertions
        verify(interactor).getMassageDurationInMilliseconds();
        verify(view).initChronos(60000L);
        verify(warnings).reloadPrefs();
    }


    @Test
    public void prepareModeTest_simple(){
        when(interactor.getMassageDurationInMilliseconds()).thenReturn(60000L);

        //Call
        chronosPresenter.prepareMode(MassageRecord.MODE_SIMPLE, 30000L);

        //Assertions
        verify(view).hideZoneChrono();
        verify(view).hidePositionChrono();
        verify(view).setVisibleChrono("full");
        verify(view).enableFullChrono(false);

        //Assertions startFullChronoIn(lastMassageDurationInMilliseconds);
        verify(view).startFullChronoIn(30000L, false);
    }

    @Test
    public void prepareModeTest_notSimple(){
        when(interactor.getMassageDurationInMilliseconds()).thenReturn(60000L);

        //Call
        chronosPresenter.prepareMode(MassageRecord.MODE_INTERMEDIATE, 90000L);

        //Assertions
        verify(view).setVisibleChrono( "mPosition" );
        verify(view).hideZoneChrono();
        //Assertions startFullChronoIn(lastMassageDurationInMilliseconds);
        verify(view).startFullChronoIn(90000L, true);
    }


    @Test
    public void optionsChangedTest(){
        when(interactor.getMassageDurationInSeconds()).thenReturn(600L);
        when(view.getFullElapsedTime(MassageSessionPresenter.STATE_RUNNING))
                .thenReturn(900000L);

        //Call
        chronosPresenter.optionsChanged();

        //Assertions
        verify(warnings).reloadPrefs();
        verify(view).getFullElapsedTime(MassageSessionPresenter.STATE_RUNNING);
        verify(interactor).getMassageDurationInSeconds();

        verify(view).enableCurrentChrono(true);
    }


    @Test
    public void loadIntermediatePositionTest(){
        //Call
        chronosPresenter.loadIntermediatePosition("Sit back", "Full body",
                900000L);

        //Assertions
        verify(view).startPositionChronoIn(900000L);
        verify(view).changePositionChronoText("Sit back");
        verify(view).startZoneChronoIn("Full body", 0L);
    }


    @Test
    public void divisionChangedToTest_single() {
        //Call
        chronosPresenter.divisionChangedTo(Divisions.CODDIVISION_SINGLE);

        //Assertions
        verify(view).showHideZoneChrono(false);
    }


    @Test
    public void divisionChangedToTest_notSingle() {
        //Call
        chronosPresenter.divisionChangedTo(Divisions.CODDIVISION_DEFAULT);

        //Assertions
        verify(view).showHideZoneChrono(true);
        verify(view).stopZoneChronoInZero();
    }


    @Test
    public void tickFullTest(){
        when(interactor.getMassageDurationInMilliseconds()).thenReturn(600000L);
        when(interactor.getMassageDurationInSeconds()).thenReturn(600L);
        when(view.getFullElapsedTime(MassageSessionPresenter.STATE_RUNNING))
                .thenReturn(600000L);

        chronosPresenter.setTimeExceeded(false);

        //Call
        chronosPresenter.tickFull(chrono);

        //Assertions
        verify(view).getFullElapsedTime(MassageSessionPresenter.STATE_RUNNING);

        verify(view).tickFullAndRemaining( 600000L, 600000L );

        verify(warnings).tick(600);

        verify(interactor).getMassageDurationInSeconds();

        verify(view).enableCurrentChrono(true);
    }

    @Test
    public void pauseChronometersTest() {
        //Call
        chronosPresenter.pauseChronometers();

        //Assertions
        verify(view).pauseChronometers();
    }


    @Test
    public void resumeChronometersTest_simple() {
        chronosPresenter.setMode(MassageRecord.MODE_SIMPLE);
        //Call
        chronosPresenter.resumeChronometers();

        //Assertions
        verify(view).startZoneChronoIn("", 0L);
        verify(view, never()).resumePositionChrono();
    }


    @Test
    public void resumeChronometersTest_intermediate() {
        chronosPresenter.setMode(MassageRecord.MODE_INTERMEDIATE);
        //Call
        chronosPresenter.resumeChronometers();

        //Assertions
        verify(view).startZoneChronoIn("", 0L);
        verify(view).resumePositionChrono();
    }


    @Test
    public void resumeChronometersTest_advanced() {
        chronosPresenter.setMode(MassageRecord.MODE_ADVANCED);
        //Call
        chronosPresenter.resumeChronometers();

        //Assertions
        verify(view, never()).startZoneChronoIn("", 0L);
        verify(view).resumePositionChrono();
    }

    //startFullChronoIn tested in prepareModeTest


    @Test
    public void startPositionChronoInTest() {
        //Call
        chronosPresenter.startPositionChronoIn(60000L);

        //Assertions
        verify(view).startPositionChronoIn(60000L);
    }


    @Test
    public void changePositionChronoTextTest() {
        //Call
        chronosPresenter.changePositionChronoText(
                TreatedZoneRecord.CODPOSITION_RIGHTSIDE);

        //Assertions
        verify(view).changePositionChronoText(
                TreatedZoneRecord.CODPOSITION_RIGHTSIDE);
    }

    @Test
    public void setZoneChronoNormalColorTest() {
        //Call
        chronosPresenter.setZoneChronoNormalColor();

        //Assertions
        verify(view).putZoneBackgroundNormalColor();
    }


    @Test
    public void startZoneChronoInTest() {
        //Call
        chronosPresenter.startZoneChronoIn("Head/Neck", 10000L);

        //Assertions
        verify(view).startZoneChronoIn("Head/Neck", 10000L);
    }


    @Test
    public void startZoneChronoTest() {
        //Call
        chronosPresenter.startZoneChrono(pressedBtn,
                MassageSessionPresenter.STATE_RUNNING);

        //Assertions
        verify(view).zoneStarted(pressedBtn,
                MassageSessionPresenter.STATE_RUNNING);
    }


    @Test
    public void blinkZoneChronoTest() {
        //Call
        chronosPresenter.blinkZoneChrono();

        //Assertions
        verify(view).blinkZone();
    }

    @Test
    public void getZoneChronoElapsedTimeTest() {
        //Call
        chronosPresenter.getZoneChronoElapsedTime(
                MassageSessionPresenter.STATE_RUNNING);

        //Assertions
        verify(view).getZoneChronoElapsedTime(
                MassageSessionPresenter.STATE_RUNNING);
    }

}
