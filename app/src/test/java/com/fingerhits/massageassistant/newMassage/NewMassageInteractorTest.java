package com.fingerhits.massageassistant.newMassage;

import android.content.Context;
import android.util.Log;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Javier Torón on 08/03/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class NewMassageInteractorTest {
    @Mock
    private MySharedPreferences mPrefs;
    @Mock private MassageAssistantDBHelper mDatabaseHelper;
    @Mock private Context mContext;

    NewMassageInteractor interactor;


    @Before
    public void setUp() throws Exception {
        interactor = new NewMassageInteractor();

        interactor.mPrefs = mPrefs;
        interactor.mContext = mContext;

        PowerMockito.mockStatic(Log.class);

    }


    @Test
    public void getDurationTest(){
        when(mPrefs.getInt(
                MySharedPreferences.PREFS_MASSAGEDURATION_KEY))
                .thenReturn(60);

        assertEquals( 60, interactor.getDuration() );
    }

    @Test
    public void setDurationTest() {
        interactor.setDuration(600);

        verify(mPrefs).saveInt(MySharedPreferences.PREFS_MASSAGEDURATION_KEY, 600);
    }

    @Test
    public void getModeTest(){
        when(mPrefs.getInt(
                MySharedPreferences.PREFS_MODE_KEY))
                .thenReturn(2);

        assertEquals( 2, interactor.getMode() );
    }

    @Test
    public void setModeTest() {
        interactor.setMode(2);

        verify(mPrefs).saveInt(MySharedPreferences.PREFS_MODE_KEY, 2);
    }


}
