package com.fingerhits.massageassistant.newMassage;

import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;

/**
 * Created by Javier Torón on 08/03/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class NewMassagePresenterTest {
    private NewMassagePresenter presenter;

    @Mock private NewMassageActivity view;
    @Mock private NewMassageInteractor interactor;


    @Before
    public void setup(){
        presenter =
                new NewMassagePresenter(view, interactor);

        PowerMockito.mockStatic(Log.class);
    }

    @Test
    public void initViewTest(){
        presenter.initView();

        verify(interactor).getDuration();
        verify(interactor).getMode();

        verify(view).setUpMassageDuration(anyInt());
        verify(view).setUpMode(anyInt());
    }


    @Test
    public void savePreferencesTest() {
        presenter.savePreferences(100, 2);

        verify(interactor).setDuration(100);
        verify(interactor).setMode(2);
    }

}
