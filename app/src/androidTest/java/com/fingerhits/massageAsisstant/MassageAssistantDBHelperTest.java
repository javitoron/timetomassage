package com.fingerhits.massageAsisstant;

import android.content.Context;
import android.database.Cursor;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.myMassages.MyMassagesActivity;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

/**
 * Created by Javier Torón on 24/05/2016.
 */
@RunWith(AndroidJUnit4.class)
public class MassageAssistantDBHelperTest {

    static Context mContext;
    static MassageAssistantDBHelper mDatabaseHelper;
    static DataUtils dataUtils;

    private long duration_for_test = 10000;

    private long pepeID, joseID, pacoID, marID;

    @Rule
    public ActivityTestRule<MyMassagesActivity> mActivityRule =
            new ActivityTestRule(MyMassagesActivity.class, true, false);


    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);

        dataUtils = new DataUtils(mDatabaseHelper, mContext);
    }


    @Before
    public void beforeEachTest() {
    }


    @Test
    public void dateTimeSQLLiteConversionsTest() {
        Date dateToTest = new Date();
        try {
            dateToTest = new SimpleDateFormat("dd/MM/yyyy").parse("30/03/1971");
        }
        catch (java.text.ParseException e)
        {
            fail("Cannot create date");
        }

        String dateInSQLFormat =
                MassageAssistantDBHelper.convertDateTimeForSQLLite(
                        dateToTest);

        assertEquals(dateInSQLFormat, "1971-03-30 00:00:00");

        Date dateFromSQL =
                MassageAssistantDBHelper.getDateTimeFromSQLLite(
                        dateInSQLFormat);

        assertEquals(dateFromSQL, dateToTest);
    }


    /*****************/
    /* MASSAGE TESTS */
    /*****************/
    @Test
    public void numberOfMassagesTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call & Assertion
        assertEquals(4, mDatabaseHelper.numberOfMassages());
    }


    @Test
    public void getAllMassageRecordsTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call: Read last to first
        Cursor massages = mDatabaseHelper.getAllMassageRecords(
                MassageAssistantDBHelper.SORT_LAST_TO_FIRST);

        if (massages.moveToFirst()) {
            MassageRecord massage = new MassageRecord(massages);

            //Assertion
            assertEquals("mar", massage.getPatient());
        }
        else{
            fail("Failed reading massages - last to first");
        }

        //Call: Read first to last
        massages = mDatabaseHelper.getAllMassageRecords(
                MassageAssistantDBHelper.SORT_FIRST_TO_LAST);

        if (massages.moveToFirst()) {
            MassageRecord massage = new MassageRecord(massages);

            //Assertion
            assertEquals("pepe", massage.getPatient());
        }
        else{
            fail("Failed reading massages - first to last");
        }
    }


    @Test
    public void getMassageCursorTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        Cursor massageData = mDatabaseHelper.getMassageCursor(joseID);

        if (massageData.moveToFirst()) {
            MassageRecord massage = new MassageRecord(massageData);

            //Assertion
            assertEquals("jose", massage.getPatient());
        }
        else{
            fail("Failed reading massage by id");
        }

    }

    @Test
    public void getMassageRecordTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        MassageRecord massage = mDatabaseHelper.getMassageRecord(pacoID);

        //Assertion
        assertEquals("paco", massage.getPatient());
    }


    @Test
    public void getLastInsertedMassageIdTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        long lastID = mDatabaseHelper.getLastInsertedMassageId();

        //Assertion
        assertEquals(marID, lastID);
    }


    @Test
    public void savePatientToMassageRecordTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        mDatabaseHelper.savePatientToMassageRecord(joseID, "benito");

        //Assertion
        MassageRecord massage = mDatabaseHelper.getMassageRecord(joseID);
        assertEquals("benito", massage.getPatient());

    }


    @Test
    public void saveCommentsToMassageRecordTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        mDatabaseHelper.saveCommentsToMassageRecord(joseID, "good massage");

        //Assertion
        MassageRecord massage = mDatabaseHelper.getMassageRecord(joseID);
        assertEquals("good massage", massage.getComments());
    }


    @Test
    public void deleteMassageRecordTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        mDatabaseHelper.deleteMassageRecord(joseID);

        //Assertion
        assertEquals(3, mDatabaseHelper.numberOfMassages());
    }


    @Test
    public void cleanUpMassagesTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        mDatabaseHelper.deleteMassageRecord(pepeID);

        //this massage has no treatedZones
        dataUtils.insertMassageRecord_pepe_simple();

        assertEquals(4, mDatabaseHelper.numberOfMassages());

        //Call
        mDatabaseHelper.cleanUpMassages();

        //Assertion
        assertEquals(3, mDatabaseHelper.numberOfMassages());
    }


    /**********************/
    /* TREATEDZONES TESTS */
    /**********************/
    @Test
    public void getAllTreatedZonesTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        Cursor treatedZones = mDatabaseHelper.getAllTreatedZones();

        //Assertion
        assertEquals(7, treatedZones.getCount());
    }


    @Test
    public void getTreatedZonesInMassageTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        Cursor treatedZones = mDatabaseHelper.getTreatedZonesInMassage(marID);

        //Assertion
        assertEquals(3, treatedZones.getCount());
    }


    @Test
    public void getLastTreatedZoneInMassageTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        Cursor lastTreatedZone = mDatabaseHelper.getLastTreatedZoneInMassage(marID);

        if (lastTreatedZone.moveToFirst()) {
            TreatedZoneRecord tz =
                    new TreatedZoneRecord(lastTreatedZone);

            //Assertion
            assertEquals(TreatedZoneRecord.CODZONE_RIGHTSIDE, tz.getCodZone());
        }
        else{
            fail("Failed reading last treated zone");
        }
    }


    @Test
    public void getTreatedZoneCursorTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        Cursor treatedZonesForPepe =
                mDatabaseHelper.getTreatedZonesInMassage(pepeID);

        if (treatedZonesForPepe.moveToFirst()) {
            TreatedZoneRecord treatedZoneForPepe =
                    new TreatedZoneRecord(treatedZonesForPepe);
            Long pepeTreatedZoneID = treatedZoneForPepe.getId();

            //Call
            Cursor treatedZoneData =
                    mDatabaseHelper.getTreatedZoneCursor(pepeTreatedZoneID);

            if (treatedZoneData.moveToFirst()) {
                TreatedZoneRecord treatedZone =
                        new TreatedZoneRecord(treatedZoneData);

                //Assertion
                assertEquals(TreatedZoneRecord.CODZONE_MASSAGE,
                        treatedZone.getCodZone());

                assertEquals(pepeID, treatedZone.getIdMassage());
            }
            else{
                fail("Failed reading pepe's first treated zone");
            }
        }
        else{
            fail("Failed reading pepe's treated zones");
        }

    }

    @Test
    public void getTreatedZoneRecordTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        Cursor treatedZonesForPepe =
                mDatabaseHelper.getTreatedZonesInMassage(pepeID);

        if (treatedZonesForPepe.moveToFirst()) {
            TreatedZoneRecord treatedZoneForPepe =
                    new TreatedZoneRecord(treatedZonesForPepe);
            Long pepeTreatedZoneID = treatedZoneForPepe.getId();

            //Call
            TreatedZoneRecord treatedZone =
                    mDatabaseHelper.getTreatedZoneRecord(pepeTreatedZoneID);

            //Assertions
            assertEquals(TreatedZoneRecord.CODZONE_MASSAGE,
                    treatedZone.getCodZone() );

            assertEquals(pepeID, treatedZone.getIdMassage() );
        }
        else{
            fail("Failed reading pepe's treated zones");
        }


    }


    @Test
    public void getDurationInLastPositionTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        long lastDuration = mDatabaseHelper.getDurationInLastPosition(marID);

        //Assertion
        assertEquals(duration_for_test * 3, lastDuration);
    }



    @Test
    public void getTreatedZonesDurationsInMassagePositionAndDivision(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        Cursor treatedZones =
                mDatabaseHelper
                        .getTreatedZonesDurationsInMassagePositionAndDivision(
                                marID, TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                                Divisions.CODDIVISION_LEFTRIGHT);

        if (treatedZones.moveToFirst()) {
            long duration = treatedZones.getLong(1);
            //Assertion
            assertEquals(duration_for_test * 3, duration);
        }
        else{
            fail("Failed reading zones cursor for a given massage, mPosition, mDivision");
        }
    }


    @Test
    public void getTreatedZonesDurationInMassageTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Call
        long fullDuration = mDatabaseHelper.getTreatedZonesDurationInMassage(marID);

        //Assertion
        assertEquals(duration_for_test * 6, fullDuration);
    }


    public void saveCodZoneInTreatedZoneRecordTest() {
        TreatedZoneRecord tz;
        long tz_id;
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        Cursor treatedZones = mDatabaseHelper.getTreatedZonesInMassage(marID);
        if (treatedZones.moveToFirst()) {
            tz = new TreatedZoneRecord(treatedZones);
            tz_id = tz.getId();

            //Call
            mDatabaseHelper.saveCodZoneInTreatedZoneRecord(tz_id,
                    TreatedZoneRecord.CODZONE_LEFTFLANK);

            //Assertion
            tz = mDatabaseHelper.getTreatedZoneRecord(tz_id);
            assertEquals(TreatedZoneRecord.CODZONE_LEFTFLANK, tz.getCodZone());
        }
        else{
            fail("Failed reading treated zones for saving codZone test");
        }

    }

    public void deleteTreatedZoneRecordTest() {
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        Cursor treatedZones = mDatabaseHelper.getTreatedZonesInMassage(marID);
        if (treatedZones.moveToFirst()) {
            TreatedZoneRecord tz = new TreatedZoneRecord(treatedZones);

            //Call
            mDatabaseHelper.deleteTreatedZoneRecord(marID, tz.getId());

            //Assertion
            treatedZones = mDatabaseHelper.getTreatedZonesInMassage(marID);
            assertEquals(2, treatedZones.getCount());
        }
        else{
            fail("Failed reading treated zones for deleting test");
        }

    }

    /******* Helper methods *********/
    private void insertMassageTestRecords(){
        pepeID = dataUtils.insert_valid_massage_pepe_simple(duration_for_test);
        joseID = dataUtils.insert_valid_massage_full_jose(duration_for_test);
        pacoID = dataUtils.insert_valid_massage_full_paco(duration_for_test);
        marID = dataUtils.insert_valid_massage_advanced_mar(duration_for_test);
    }

}
