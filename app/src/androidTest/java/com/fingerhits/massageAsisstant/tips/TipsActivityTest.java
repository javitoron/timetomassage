package com.fingerhits.massageAsisstant.tips;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.tips.TipsActivity;
import com.fingerhits.massageassistant.tips.TipsInteractor;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.StringContains.containsString;

/**
 * Created by Javier Torón on 23/02/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class TipsActivityTest {

    static Context mContext;
    static MySharedPreferences mPrefs;
    TipsActivity tipsActivity;
    private Intent intent;
    private Bundle bundle;

    @Rule
    public IntentsTestRule<TipsActivity> mActivityRule =
            new IntentsTestRule(TipsActivity.class, true, false);

    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mPrefs = new MySharedPreferences(mContext);
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "");
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, "");

        PrefsUtils.setPreferences(mPrefs, 5, false, true, 1, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 0);
    }

    @Before
    public void beforeEachTest() {
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "");
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, "");
    }

    @Test
    public void initialStateTest() {
        intent = new Intent();
        intent.putExtras(getBundle(0));
        mActivityRule.launchActivity(intent);

        onView(withId(R.id.prevTipBtn))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.tipTxt))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("IMPORTANT"))));

        onView(withId(R.id.nextTipBtn))
                .check(matches(isDisplayed()));

        onView(withId(R.id.pageNumberText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("1/"))));

        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "");
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, "");
    }

    @Test
    public void goToSecondTest() {
        intent = new Intent();
        intent.putExtras(getBundle(0));
        mActivityRule.launchActivity(intent);

        onView(withId(R.id.prevTipBtn))
                .check(matches(not(isDisplayed())));
        //Se estaban borrando antes de entrar en el test
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "tip_privacy_txt");
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, "tip_privacy_txt");

        onView(withId(R.id.nextTipBtn))
                .perform(click());

        onView(withId(R.id.tipTxt))
                .check(matches(withText(containsString("After every"))));


        onView(withId(R.id.prevTipBtn))
                .check(matches(isDisplayed()));

        onView(withId(R.id.pageNumberText))
                .check(matches(withText(containsString("2/"))));

        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "");
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, "");
    }

    @Test
    public void fromSecondToFirstTest() {
        intent = new Intent();
        intent.putExtras(getBundle(0));
        mActivityRule.launchActivity(intent);

        onView(withId(R.id.prevTipBtn))
                .check(matches(not(isDisplayed())));
        //Se estaban borrando antes de entrar en el test
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "tip_privacy_txt");
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, "tip_privacy_txt");

        onView(withId(R.id.nextTipBtn))
                .perform(click());

        onView(withId(R.id.prevTipBtn))
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.pageNumberText))
                .check(matches(withText(containsString("1/"))));
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "");
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, "");
    }

    @Test
    public void goToLastTest() {
        intent = new Intent();
        intent.putExtras(getBundle(0));
        mActivityRule.launchActivity(intent);

        int numberOfTips = TipsInteractor.getStaticCount();

        onView(withId(R.id.prevTipBtn))
                .check(matches(not(isDisplayed())));
        //Se estaban borrando antes de entrar en el test
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "tip_privacy_txt");
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, "tip_privacy_txt");


        int i;
        for(i = 0; i < numberOfTips - 1; i++ ){
            onView(withId(R.id.nextTipBtn))
                    .perform(click());
        }

        i++;

        onView(withId(R.id.pageNumberText))
                .check(matches(withText(containsString(i + "/" + i))));
        onView(withId(R.id.nextTipBtn))
                .check(matches(not(isDisplayed())));

        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "");
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, "");
    }


    private Bundle getBundle(int tipIndex){
        bundle = new Bundle();
        bundle.putLong(TipsActivity.PARAMETER_TIPINDEX, tipIndex);

        return bundle;
    }}
