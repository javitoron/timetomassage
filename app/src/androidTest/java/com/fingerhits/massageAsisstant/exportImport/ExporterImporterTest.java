package com.fingerhits.massageAsisstant.exportImport;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.fingerhits.massageAsisstant.DataUtils;
import com.fingerhits.massageAsisstant.MySharedPreferencesUtils;
import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.exportImport.DataXmlExporter;
import com.fingerhits.massageassistant.exportImport.DataXmlImporter;
import com.fingerhits.massageassistant.exportImport.ExportedFilesManager;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.MiscUtils;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.fail;

/**
 * Created by Javier Torón on 02/03/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ExporterImporterTest {
    static Context mContext;
    static Resources mRes;
    static MassageAssistantDBHelper mDatabaseHelper;
    static DataUtils dataUtils;
    static MySharedPreferences mPrefs;
    static ExportedFilesManager exportedFilesManager;
    static DataXmlExporter exporter;
    static DataXmlImporter importer;

    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mRes = mContext.getResources();
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);
        mPrefs = new MySharedPreferences(mContext);

        PrefsUtils.setPreferences(mPrefs, 5, false, true, 1, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 0);


        dataUtils = new DataUtils(mDatabaseHelper, mContext);
        exportedFilesManager = new ExportedFilesManager();
        exportedFilesManager.setDownloadManager(mContext);
        exporter = new DataXmlExporter();
        exporter.inject(mContext);
        importer = new DataXmlImporter();
        importer.inject(mContext);
    }

    @Before
    public void beforeEachTest() {
        mDatabaseHelper.deleteAllMassages();

        MySharedPreferencesUtils.setPreferences(mPrefs, 1, 5, false, false,
                false, false, false, 1, R.raw.flute_shorter, false,
                R.raw.gong_metal, false, R.raw.harp_dream, 0, "", "");
    }


    @Test
    public void noMassagesOnlyPreferencesTest() throws Exception {

        exporter.export();

        MySharedPreferencesUtils.setPreferences(mPrefs, 2, 120, true, true,
                true, true, true, 30, R.raw.bell_high, true, R.raw.bird_high,
                true, R.raw.bird_quiet, 300, "tipsInMain", "tips" );

        importer.doImport(new File(exporter.filePath, exporter.fileName));

        //Assert 0 massages imported
        assertEquals(mDatabaseHelper.numberOfMassages(), 0);
        assertEquals(importer.massagesImported, 0);

        //Assert preferences back to min values
        assertEquals(mPrefs.getInt(MySharedPreferences.PREFS_MODE_KEY), 1);
        assertEquals(mPrefs.getInt(MySharedPreferences.PREFS_MASSAGEDURATION_KEY), 5);

        assertFalse(mPrefs.getBool(MySharedPreferences.PREFS_DISABLESCREENLOCK_KEY));
        assertFalse(mPrefs.getBool(MySharedPreferences.PREFS_VISUALWARNINGENABLED_KEY));
        assertFalse(mPrefs.getBool(MySharedPreferences.PREFS_VIBRATIONWARNINGENABLED_KEY));
        assertFalse(mPrefs.getBool(MySharedPreferences.PREFS_SOUNDENABLED_KEY));

        assertFalse(mPrefs.getBool(MySharedPreferences.PREFS_DINGTIMESOUNDENABLED_KEY));
        assertEquals(mPrefs.getInt(MySharedPreferences.PREFS_DINGTIME_KEY), 1);
        assertEquals(mPrefs.getInt(MySharedPreferences.PREFS_DINGTIMESOUND_KEY),
                MySharedPreferences.DEFAULT_DINGTIMESOUND_KEY);

        assertFalse(mPrefs.getBool(
                MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUNDENABLED_KEY));
        assertEquals(mPrefs.getInt(MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY),
                MySharedPreferences.DEFAULT_HALFMASSAGEDURATIONSOUND_KEY);

        assertFalse(mPrefs.getBool(
                MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUNDENABLED_KEY));
        assertEquals(mPrefs.getInt(MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY),
                MySharedPreferences.DEFAULT_FULLMASSAGEDURATIONSOUND_KEY);

        assertEquals(mPrefs.getInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY), 0);

    }


    @Test
    public void oneSimpleMassageTest() throws IOException {
        long duration_for_test = 10000;
        dataUtils.insert_valid_massage_pepe_simple(duration_for_test);

        try {
            exporter.export();

            mDatabaseHelper.deleteAllMassages();

            importer.doImport(new File(exporter.filePath, exporter.fileName));

            //Assert 1 massage imported
            assertEquals(mDatabaseHelper.numberOfMassages(), 1);
            assertEquals(importer.massagesImported, 1);

            Cursor massages = mDatabaseHelper.getAllMassageRecords(
                    MassageAssistantDBHelper.SORT_FIRST_TO_LAST);
            assertEquals(massages.getCount(), 1);

            if(massages.moveToFirst()){
                MassageRecord massage = new MassageRecord(massages);

                assertEquals(massage.getPatient(), "pepe");
                assertEquals(massage.getDuration(), duration_for_test);
                assertEquals(massage.getIdMasseur(), MiscUtils.currentLocalUser());
                assertEquals(massage.getMode(), MassageRecord.MODE_SIMPLE);

                Cursor treatedZones =
                        mDatabaseHelper.getTreatedZonesInMassage(
                                massage.getId());
                assertEquals(treatedZones.getCount(), 1);

                if(treatedZones.moveToFirst()) {
                    TreatedZoneRecord treatedZone =
                            new TreatedZoneRecord(treatedZones);
                    assertEquals(treatedZone.getCodPosition(), TreatedZoneRecord.CODPOSITION_UNDEFINED);
                    assertEquals(treatedZone.getCodDivision(), Divisions.CODDIVISION_SINGLE);
                    assertEquals(treatedZone.getCodZone(), TreatedZoneRecord.CODZONE_MASSAGE);
                    assertEquals(treatedZone.getDuration(), duration_for_test);
                    assertEquals( treatedZone.getEndDateTime().getTime()
                                - treatedZone.getStartDateTime().getTime(), duration_for_test * 1000);
                }
                else{
                    fail("Massage had no treated zones");
                }
            }
            else{
                fail("Couldn't read any massage");
            }


        } catch (IOException e) {
            Log.d("ExporterImporterTest", "IO Error testing oneSimpleMassageTest");
        }
    }


    @Test
    public void twoIntermediateMassagesTest() throws IOException{
        long duration_for_test = 5000;
        mDatabaseHelper.deleteAllMassages();

        dataUtils.insert_valid_massage_full_jose(duration_for_test);
        dataUtils.insert_valid_massage_full_paco(duration_for_test);

        try {
            exporter.export();

            mDatabaseHelper.deleteAllMassages();

            importer.doImport(new File(exporter.filePath, exporter.fileName));

            //Assert 2 massages imported
            assertEquals(mDatabaseHelper.numberOfMassages(), 2);
            assertEquals(importer.massagesImported, 2);

            Cursor massages = mDatabaseHelper.getAllMassageRecords(
                    MassageAssistantDBHelper.SORT_FIRST_TO_LAST );
            assertEquals(massages.getCount(), 2);

            if(massages.moveToFirst()){
                MassageRecord massage = new MassageRecord(massages);

                assertEquals(massage.getPatient(), "jose");
                assertEquals(massage.getDuration(), duration_for_test);
                assertEquals(massage.getComments(), "comments");
                assertEquals(massage.getObjective(), "objective");
                assertEquals(massage.getUsedMaterials(), "usedMaterials");
                assertEquals(massage.getType(), "type");
                assertEquals(massage.getMode(), MassageRecord.MODE_INTERMEDIATE);

                Cursor treatedZones =
                        mDatabaseHelper.getTreatedZonesInMassage(
                                massage.getId());
                assertEquals(treatedZones.getCount(), 1);

                if(treatedZones.moveToFirst()) {
                    TreatedZoneRecord treatedZone =
                            new TreatedZoneRecord(treatedZones);
                    assertEquals(treatedZone.getCodPosition(), TreatedZoneRecord.CODPOSITION_FACEDOWN);
                    assertEquals(treatedZone.getCodDivision(), Divisions.CODDIVISION_SINGLE);
                    assertEquals(treatedZone.getCodZone(), TreatedZoneRecord.CODZONE_FULLBODY);
                }
                else{
                    fail("Massage1 had no treated zones");
                }


                /*MASSAGE 2*/
                massages.moveToNext();
                massage = new MassageRecord(massages);

                assertEquals(massage.getPatient(), "paco");
                assertEquals(massage.getDuration(), duration_for_test * 2);

                treatedZones = mDatabaseHelper.getTreatedZonesInMassage(
                        massage.getId());
                assertEquals(treatedZones.getCount(), 2);

                if(treatedZones.moveToFirst()) {
                    TreatedZoneRecord treatedZone =
                            new TreatedZoneRecord(treatedZones);
                    assertEquals(treatedZone.getCodPosition(), TreatedZoneRecord.CODPOSITION_FACEUP);

                    treatedZones.moveToNext();
                    treatedZone = new TreatedZoneRecord(treatedZones);
                    assertEquals(treatedZone.getCodPosition(), TreatedZoneRecord.CODPOSITION_LEFTSIDE);
                }
                else{
                    fail("Massage2 had no treated zones");
                }
            }
            else{
                fail("Couldn't read any massage");
            }


        } catch (IOException e) {
            Log.d("ExporterImporterTest", "IO Error testing twoIntermediateMassagesTest");
        }
    }


    @Test
    public void oneAdvancedMassagesTest() throws IOException{
        long duration_for_test = 300;
        mDatabaseHelper.deleteAllMassages();

        dataUtils.insert_valid_massage_advanced_mar(duration_for_test);

        try {
            exporter.export();

            mDatabaseHelper.deleteAllMassages();

            importer.doImport(new File(exporter.filePath, exporter.fileName));

            //Assert 1 massage imported
            assertEquals(mDatabaseHelper.numberOfMassages(), 1);
            assertEquals(importer.massagesImported, 1);

            Cursor massages = mDatabaseHelper.getAllMassageRecords(
                    MassageAssistantDBHelper.SORT_FIRST_TO_LAST);
            assertEquals(massages.getCount(), 1);

            if(massages.moveToFirst()){
                MassageRecord massage = new MassageRecord(massages);

                assertEquals(massage.getMode(), MassageRecord.MODE_ADVANCED);

                Cursor treatedZones =
                        mDatabaseHelper.getTreatedZonesInMassage(
                                massage.getId());
                assertEquals(treatedZones.getCount(), 3);

                if(treatedZones.moveToFirst()) {
                    TreatedZoneRecord treatedZone =
                            new TreatedZoneRecord(treatedZones);
                    assertEquals(treatedZone.getCodPosition(), TreatedZoneRecord.CODPOSITION_RIGHTSIDE);
                    assertEquals(treatedZone.getCodDivision(), Divisions.CODDIVISION_DEFAULT);
                    assertEquals(treatedZone.getCodZone(), TreatedZoneRecord.CODZONE_HEAD);

                    treatedZones.moveToNext();
                    treatedZone = new TreatedZoneRecord(treatedZones);
                    assertEquals(treatedZone.getCodPosition(), TreatedZoneRecord.CODPOSITION_SITTINGBACK);
                    assertEquals(treatedZone.getCodDivision(), Divisions.CODDIVISION_DETAILED);
                    assertEquals(treatedZone.getCodZone(), TreatedZoneRecord.CODZONE_LEFTFOREARM);

                    treatedZones.moveToNext();
                    treatedZone = new TreatedZoneRecord(treatedZones);
                    assertEquals(treatedZone.getCodPosition(), TreatedZoneRecord.CODPOSITION_SITTINGFRONT);
                    assertEquals(treatedZone.getCodDivision(), Divisions.CODDIVISION_LEFTRIGHT);
                    assertEquals(treatedZone.getCodZone(), TreatedZoneRecord.CODZONE_RIGHTSIDE);
                }
                else{
                    fail("Massage had no treated zones");
                }
            }
            else{
                fail("Couldn't read any massage");
            }


        } catch (IOException e) {
            Log.d("ExporterImporterTest", "IO Error testing oneAdvancedMassagesTest");
        }
    }

}
