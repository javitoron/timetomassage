package com.fingerhits.massageAsisstant.exportImport;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.fingerhits.massageAsisstant.DataUtils;
import com.fingerhits.massageAsisstant.MySharedPreferencesUtils;
import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.exportImport.DataXmlExporter;
import com.fingerhits.massageassistant.exportImport.DataXmlImporter;
import com.fingerhits.massageassistant.exportImport.ExportImportActivity;
import com.fingerhits.massageassistant.exportImport.ExportedFilesManager;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;


/**
 * Created by Javier Torón on 24/05/2016.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ExportImportActivityTest {

    static Context mContext;
    static Resources mRes;
    static MassageAssistantDBHelper mDatabaseHelper;
    static DataUtils dataUtils;
    static MySharedPreferences mPrefs;
    static ExportedFilesManager exportedFilesManager;
    static DataXmlImporter importer;
    static DataXmlExporter exporter;

    int duration_for_test = 1000;

    @Rule
    public IntentsTestRule<ExportImportActivity> mActivityRule =
            new IntentsTestRule(ExportImportActivity.class, true, false);

    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mRes = mContext.getResources();
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);
        mPrefs = new MySharedPreferences(mContext);

        PrefsUtils.setPreferences(mPrefs, 5, false, true, 1, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 0);

        dataUtils = new DataUtils(mDatabaseHelper, mContext);
        exportedFilesManager = new ExportedFilesManager();
        exportedFilesManager.setDownloadManager(mContext);
        exporter = new DataXmlExporter();
        exporter.inject(mContext);
        importer = new DataXmlImporter();
        importer.inject(mContext);
    }


    @Before
    public void beforeEachTest() {
        mDatabaseHelper.deleteAllMassages();
        exportedFilesManager.deleteAll();

        MySharedPreferencesUtils.setPreferences(mPrefs, 1, 5, false, false,
                false, false, false, 1, 0, false, 0, false, 0, 0, "", "");
    }

    @Test
    public void initialStateTest_withoutMassagesAndFiles() throws Exception {
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.export_noMassagesTxt))
                .check(matches(isDisplayed()));

        onView(withId(R.id.exportBtn))
                .check(matches(isDisplayed()));

        onView(withId(R.id.noImportFilesLabel))
                .check(matches(isDisplayed()));

        onView(withId(R.id.importLayout))
                .check(matches(not(isDisplayed())));
    }


    @Test
    public void initialStateTest_withMassagesAndFiles() throws Exception {
        dataUtils.insert_valid_massage_pepe_simple(duration_for_test);
        exporter.export();

        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.export_noMassagesTxt))
                .check(matches(not( isDisplayed())));

        onView(withId(R.id.exportBtn))
                .check(matches(isDisplayed()));

        onView(withId(R.id.noImportFilesLabel))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.importLayout))
                .check(matches(isDisplayed()));

        onView(withId(R.id.importFileSelectLabel))
                .check(matches(isDisplayed()));

        onView(withId(R.id.importFileSelectSpinner))
                .check(matches(isDisplayed()));

        onView(withId(R.id.importBtn))
                .check(matches(isDisplayed()));
    }


    @Test
    public void noImportFiles_export() throws Exception {
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.noImportFilesLabel))
                .check(matches(isDisplayed()));

        onView(withId(R.id.importLayout))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.exportBtn))
                .perform(click());

        onView(withId(R.id.noImportFilesLabel))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.importLayout))
                .check(matches(isDisplayed()));

        onView(withId(R.id.importFileSelectSpinner))
                .check(matches(isDisplayed()));

        onView(withId(R.id.importBtn))
                .check(matches(isDisplayed()));

    }


}
