package com.fingerhits.massageAsisstant;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.options.OptionsInteractor;

/**
 * Created by Javier Torón on 24/03/2017.
 */

public class PrefsUtils {
    //    /******* Helper methods *********/
    public static void setPreferences(MySharedPreferences mPrefs,
                                      int MassageDuration,
                                      boolean DisableScreenLock,
                                      boolean DingTimeWarningEnabled,
                                      int DingTime,
                                      boolean HalfMassageDurationWarningEnabled,
                                      boolean FullMassageDurationWarningEnabled,
                                      boolean VisualWarningsEnabled,
                                      boolean VibrationWarningsEnabled,
                                      boolean SoundWarningsEnabled,
                                      int DingTimeSound,
                                      int HalfMassageDurationSound,
                                      int FullMassageDurationSound,
                                      int MinimalZoneDuration){
        OptionsInteractor interactor = new OptionsInteractor();
        interactor.mPrefs = mPrefs;

        interactor.saveMassageDuration(MassageDuration);

        interactor.saveDisableScreenLock(DisableScreenLock);

        interactor.saveDingTimeWarnings(DingTimeWarningEnabled);
        interactor.saveDingTime(DingTime);
        interactor.saveHalfDurationWarning(HalfMassageDurationWarningEnabled);
        interactor.saveFullDurationWarning(FullMassageDurationWarningEnabled);

        interactor.saveVisualWarnings(VisualWarningsEnabled);
        interactor.saveVibrationWarnings(VibrationWarningsEnabled);
        interactor.saveSoundWarnings(SoundWarningsEnabled);

        interactor.saveDingTimeSound(DingTimeSound);
        interactor.saveHalfMassageSound(HalfMassageDurationSound);
        interactor.saveFullMassageSound(FullMassageDurationSound);

        interactor.saveMinimalZoneDuration(MinimalZoneDuration);

    }

}
