package com.fingerhits.massageAsisstant.newMassage;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.fingerhits.massageAsisstant.EspressoHelpers;
import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.newMassage.NewMassageActivity;
import com.fingerhits.massageassistant.newMassage.NewMassageInteractor;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageRecord;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.StringContains.containsString;

/**
 * Created by Javier Torón on 08/03/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class NewMassageActivityTest {
    static Context mContext;
    static MySharedPreferences mPrefs;

    @Rule
    public IntentsTestRule<NewMassageActivity> mActivityRule =
            new IntentsTestRule(NewMassageActivity.class, true, false);

    NewMassageActivity activity;

    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mPrefs = new MySharedPreferences(mContext);

        PrefsUtils.setPreferences(mPrefs, 5, false, true, 1, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 0);
    }

    @Before
    public void beforeEachTest() {
    }


    @Test
    public void simpleMinInitTest() {
        setPreferences(5, MassageRecord.MODE_SIMPLE);
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.massageDurationLabel))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("This massage will be 5 minutes long"))));

        onView(withId(R.id.massageDurationSeekBar))
                .check(matches(isDisplayed()))
                .check(matches(EspressoHelpers.withProgress(0)));

        onView(withId(R.id.massageModeRadio_simple))
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));

        onView(withId(R.id.massageModeRadio_intermediate))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.massageModeRadio_advanced))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.newMassageBtn))
                .check(matches(isDisplayed()));

        onView(withId(R.id.positionSelectorLayout))
                .check(matches(not(isDisplayed())));

    }


    @Test
    public void intermediateInitTest() {
        setPreferences(105, MassageRecord.MODE_INTERMEDIATE);
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.massageDurationLabel))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("This massage will be 105 minutes long"))));

        onView(withId(R.id.massageDurationSeekBar))
                .check(matches(isDisplayed()))
                .check(matches(EspressoHelpers.withProgress(20)));

        onView(withId(R.id.massageModeRadio_simple))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.massageModeRadio_intermediate))
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));

        onView(withId(R.id.massageModeRadio_advanced))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.newMassageBtn))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.positionSelectorLayout))
                .check(matches(isDisplayed()));

    }


    @Test
    public void advancedMaxInitTest() {
        setPreferences(120, MassageRecord.MODE_ADVANCED);
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.massageDurationLabel))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("This massage will be 120 minutes long"))));

        onView(withId(R.id.massageDurationSeekBar))
                .check(matches(isDisplayed()))
                .check(matches(EspressoHelpers.withProgress(23)));

        onView(withId(R.id.massageModeRadio_simple))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.massageModeRadio_intermediate))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.massageModeRadio_advanced))
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));

        onView(withId(R.id.newMassageBtn))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.positionSelectorLayout))
                .check(matches(isDisplayed()));

    }


    @Test
    public void simpleToIntermediateTest() {
        setPreferences(5, MassageRecord.MODE_SIMPLE);
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.massageModeRadio_intermediate))
                .perform(click());

        onView(withId(R.id.newMassageBtn))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.positionSelectorLayout))
                .check(matches(isDisplayed()));
    }


    @Test
    public void simpleToAdvancedTest() {
        setPreferences(5, MassageRecord.MODE_SIMPLE);
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.massageModeRadio_advanced))
                .perform(click());

        onView(withId(R.id.newMassageBtn))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.positionSelectorLayout))
                .check(matches(isDisplayed()));
    }


    @Test
    public void advancedToSimpleTest() {
        setPreferences(5, MassageRecord.MODE_ADVANCED);
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.massageModeRadio_simple))
                .perform(click());

        onView(withId(R.id.newMassageBtn))
                .check(matches(isDisplayed()));

        onView(withId(R.id.positionSelectorLayout))
                .check(matches(not(isDisplayed())));
    }




    //    /******* Helper methods *********/
    private void setPreferences(int duration, int mode) {
        NewMassageInteractor interactor = new NewMassageInteractor();
        interactor.mPrefs = mPrefs;

        interactor.setDuration(duration);
        interactor.setMode(mode);
    }
}
