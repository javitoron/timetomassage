package com.fingerhits.massageAsisstant;

import android.content.Context;
import android.database.Cursor;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.myMassages.MyMassagesActivity;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.fail;

/**
 * Created by Javier Torón on 24/05/2016.
 */

public class MassageRecordTest {

    static Context mContext;
    static MassageAssistantDBHelper mDatabaseHelper;
    static DataUtils dataUtils;

    private long duration_for_test = 10000;

    private long pepeID, joseID, pacoID, marID;


    @Rule
    public ActivityTestRule<MyMassagesActivity> mActivityRule =
            new ActivityTestRule(MyMassagesActivity.class, true, false);


    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);

        dataUtils = new DataUtils(mDatabaseHelper, mContext);
    }


    @Before
    public void beforeEachTest() {
    }


    @Test
    public void constructorAllParametersTest() {
        Date start = new Date();
        Date end = new Date();
        try {
            start = new SimpleDateFormat("dd/MM/yyyy").parse("30/03/2016");
            end = new SimpleDateFormat("dd/MM/yyyy").parse("31/03/2016");
        }
        catch (java.text.ParseException e)
        {
            fail("Cannot create date");
        }

        MassageRecord massage = new MassageRecord( 0, "patient", 1, start, end,
                duration_for_test, MassageRecord.MODE_INTERMEDIATE, "type",
                "objective", "usedMaterials", "comments" );

        assertEquals(0L, massage.getId() );
        assertEquals("patient", massage.getPatient() );
        assertEquals(1, massage.getIdMasseur() );
        assertEquals("2016-03-30 00:00:00",
                MassageAssistantDBHelper.convertDateTimeForSQLLite(
                        massage.getStartDateTime() ) );
        assertEquals("2016-03-31 00:00:00",
                MassageAssistantDBHelper.convertDateTimeForSQLLite(
                        massage.getEndDateTime() ) );
        assertEquals(duration_for_test, massage.getDuration() );
        assertEquals(MassageRecord.MODE_INTERMEDIATE, massage.getMode());
        assertEquals("type", massage.getType() );
        assertEquals("objective", massage.getObjective() );
        assertEquals("usedMaterials", massage.getUsedMaterials() );
        assertEquals("comments", massage.getComments() );

    }


    @Test
    public void constructorForNewMassageTest() {
        Date start = new Date();
        try {
            start = new SimpleDateFormat("dd/MM/yyyy").parse("30/03/2016");
        }
        catch (java.text.ParseException e)
        {
            fail("Cannot create date");
        }

        MassageRecord massage = new MassageRecord( 1, "patient", start,
                MassageRecord.MODE_SIMPLE );

        assertEquals(0L, massage.getId());
        assertEquals("patient", massage.getPatient() );
        assertEquals(1, massage.getIdMasseur());
        assertEquals("2016-03-30 00:00:00",
                MassageAssistantDBHelper.convertDateTimeForSQLLite(
                        massage.getStartDateTime() ) );
        assertEquals("2016-03-30 00:00:00",
                MassageAssistantDBHelper.convertDateTimeForSQLLite(
                        massage.getEndDateTime()) );
        assertEquals(0, massage.getDuration());
        assertEquals(MassageRecord.MODE_SIMPLE, massage.getMode());
        assertEquals("", massage.getType() );
        assertEquals("", massage.getObjective());
        assertEquals("", massage.getUsedMaterials());
        assertEquals("", massage.getComments());

    }


    @Test
    public void constructorFromCursorTest(){
        mDatabaseHelper.deleteAllMassages();

        Date start = new Date();
        Date end = new Date();
        try {
            start = new SimpleDateFormat("dd/MM/yyyy").parse("30/03/2016");
            end = new SimpleDateFormat("dd/MM/yyyy").parse("31/03/2016");
        }
        catch (java.text.ParseException e)
        {
            fail("Cannot create date");
        }

        MassageRecord massage = new MassageRecord( 0, "patient", 1, start, end,
                duration_for_test, MassageRecord.MODE_SIMPLE, "type",
                "objective", "usedMaterials", "comments" );

        long patientID = mDatabaseHelper.insertMassageRecord(massage);


        Cursor massageCursor = mDatabaseHelper.getMassageCursor(patientID);

        if (massageCursor.moveToFirst()) {
            MassageRecord massageFromCursor = new MassageRecord(massageCursor);

            assertNotSame(0L, massageFromCursor.getId());
            assertEquals("patient", massageFromCursor.getPatient());
            assertEquals(1, massageFromCursor.getIdMasseur());
            assertEquals("2016-03-30 00:00:00",
                    MassageAssistantDBHelper.convertDateTimeForSQLLite(
                            massageFromCursor.getStartDateTime() ));
            assertEquals("2016-03-31 00:00:00",
                    MassageAssistantDBHelper.convertDateTimeForSQLLite(
                            massageFromCursor.getEndDateTime() ));
            assertEquals(duration_for_test, massageFromCursor.getDuration());
            assertEquals(MassageRecord.MODE_SIMPLE, massageFromCursor.getMode());
            assertEquals("type", massageFromCursor.getType());
            assertEquals("objective", massageFromCursor.getObjective());
            assertEquals("usedMaterials", massageFromCursor.getUsedMaterials() );
            assertEquals("comments", massageFromCursor.getComments() );
        }
        else{
            fail("Failed reading massage by id");
        }

    }


    /*****************/
    /* MASSAGE TESTS */
    /*****************/
    @Test
    public void numberOfMassagesTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        assertEquals(mDatabaseHelper.numberOfMassages(), 4);
    }


    @Test
    public void getAllMassageRecordsTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        //Read last to first
        Cursor massages = mDatabaseHelper.getAllMassageRecords(
                MassageAssistantDBHelper.SORT_LAST_TO_FIRST);

        if (massages.moveToFirst()) {
            MassageRecord massage = new MassageRecord(massages);

            assertEquals(massage.getPatient(), "mar");
        }
        else{
            fail("Failed reading massages - last to first");
        }

        //Read first to last
        massages = mDatabaseHelper.getAllMassageRecords(
                MassageAssistantDBHelper.SORT_FIRST_TO_LAST);

        if (massages.moveToFirst()) {
            MassageRecord massage = new MassageRecord(massages);

            assertEquals(massage.getPatient(), "pepe");
        }
        else{
            fail("Failed reading massages - first to last");
        }
    }


    @Test
    public void getMassageRecordTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        MassageRecord massage = mDatabaseHelper.getMassageRecord(pacoID);

        assertEquals(massage.getPatient(), "paco");

    }


    @Test
    public void getLastInsertedMassageIdTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        long lastID = mDatabaseHelper.getLastInsertedMassageId();

        assertEquals(lastID, marID);

    }


    @Test
    public void savePatientToMassageRecordTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        mDatabaseHelper.savePatientToMassageRecord(joseID, "benito");

        MassageRecord massage = mDatabaseHelper.getMassageRecord(joseID);

        assertEquals(massage.getPatient(), "benito");

    }


    @Test
    public void saveCommentsToMassageRecordTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        mDatabaseHelper.saveCommentsToMassageRecord(joseID, "good massage");

        MassageRecord massage = mDatabaseHelper.getMassageRecord(joseID);

        assertEquals(massage.getComments(), "good massage");

    }


    @Test
    public void deleteMassageRecordTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        mDatabaseHelper.deleteMassageRecord(joseID);

        assertEquals(mDatabaseHelper.numberOfMassages(), 3);
    }


    @Test
    public void cleanUpMassagesTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        mDatabaseHelper.deleteMassageRecord(pepeID);

        //this massage has no treatedZones
        dataUtils.insertMassageRecord_pepe_simple();

        assertEquals(mDatabaseHelper.numberOfMassages(), 4);

        mDatabaseHelper.cleanUpMassages();
        assertEquals(mDatabaseHelper.numberOfMassages(), 3);
    }


    /**********************/
    /* TREATEDZONES TESTS */
    /**********************/
    @Test
    public void getAllTreatedZonesTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        Cursor treatedZones = mDatabaseHelper.getAllTreatedZones();
        assertEquals(treatedZones.getCount(), 7);
    }


    @Test
    public void getTreatedZonesInMassageTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        Cursor treatedZones = mDatabaseHelper.getTreatedZonesInMassage(marID);
        assertEquals(treatedZones.getCount(), 3);
    }


    @Test
    public void getLastTreatedZoneInMassageTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        Cursor lastTreatedZone = mDatabaseHelper.getLastTreatedZoneInMassage(marID);

        if (lastTreatedZone.moveToFirst()) {
            TreatedZoneRecord tz =
                    new TreatedZoneRecord(lastTreatedZone);

            assertEquals(tz.getCodZone(), TreatedZoneRecord.CODZONE_RIGHTSIDE);
        }
        else{
            fail("Failed reading last treated zone");
        }
    }


    @Test
    public void getDurationInLastPositionTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        long lastDuration = mDatabaseHelper.getDurationInLastPosition(marID);

        assertEquals(lastDuration, duration_for_test * 3);
    }



    @Test
    public void getTreatedZonesDurationsInMassagePositionAndDivision(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        Cursor treatedZones =
                mDatabaseHelper
                        .getTreatedZonesDurationsInMassagePositionAndDivision(
                                marID, TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                                Divisions.CODDIVISION_LEFTRIGHT);

        if (treatedZones.moveToFirst()) {
            long duration = treatedZones.getLong(1);
            assertEquals(duration, duration_for_test * 3);
        }
        else{
            fail("Failed reading zones cursor for a given massage, mPosition, mDivision");
        }
    }


    @Test
    public void getTreatedZonesDurationInMassageTest(){
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        long fullDuration = mDatabaseHelper.getTreatedZonesDurationInMassage(marID);

        assertEquals(fullDuration, duration_for_test * 6);
    }


    public void deleteTreatedZoneRecordTest() {
        mDatabaseHelper.deleteAllMassages();

        insertMassageTestRecords();

        Cursor treatedZones = mDatabaseHelper.getTreatedZonesInMassage(marID);
        if (treatedZones.moveToFirst()) {
            TreatedZoneRecord tz = new TreatedZoneRecord(treatedZones);

            mDatabaseHelper.deleteTreatedZoneRecord(marID, tz.getId());
        }
        else{
            fail("Failed reading treated zones for deleting test");
        }

        treatedZones = mDatabaseHelper.getTreatedZonesInMassage(marID);
        assertEquals(treatedZones.getCount(), 2);
    }

    /******* Helper methods *********/
    private void insertMassageTestRecords(){
        pepeID = dataUtils.insert_valid_massage_pepe_simple(duration_for_test);
        joseID = dataUtils.insert_valid_massage_full_jose(duration_for_test);
        pacoID = dataUtils.insert_valid_massage_full_paco(duration_for_test);
        marID = dataUtils.insert_valid_massage_advanced_mar(duration_for_test);

    }

}
