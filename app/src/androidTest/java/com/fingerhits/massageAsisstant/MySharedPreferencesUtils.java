package com.fingerhits.massageAsisstant;

import com.fingerhits.massageassistant.options.MySharedPreferences;

/**
 * Created by Javier Torón on 02/03/2017.
 */

public class MySharedPreferencesUtils {

    public static void setPreferences(MySharedPreferences mPrefs, int mode,
                                      int MassageDuration,
                                      boolean DisableScreenLock,
                                      boolean VisualWarningEnabled,
                                      boolean VibrationWarningEnabled,
                                      boolean SoundEnabled,
                                      boolean DingTimeSoundEnabled, int DingTime,
                                      int DingTimeSound,
                                      boolean HalfMassageDurationSoundEnabled,
                                      int HalfMassageDurationSound,
                                      boolean FullMassageDurationSoundEnabled,
                                      int FullMassageDurationSound,
                                      int MinimalZoneDuration,
                                      String tipsShownInMain, String tipsShown){

        mPrefs.saveInt(MySharedPreferences.PREFS_MODE_KEY, mode);
        mPrefs.saveInt(MySharedPreferences.PREFS_MASSAGEDURATION_KEY, MassageDuration);

        mPrefs.saveBool(MySharedPreferences.PREFS_DISABLESCREENLOCK_KEY, DisableScreenLock);
        mPrefs.saveBool(MySharedPreferences.PREFS_VISUALWARNINGENABLED_KEY,
                VisualWarningEnabled);
        mPrefs.saveBool(MySharedPreferences.PREFS_VIBRATIONWARNINGENABLED_KEY,
                VibrationWarningEnabled);
        mPrefs.saveBool(MySharedPreferences.PREFS_SOUNDENABLED_KEY, SoundEnabled);

        mPrefs.saveBool(MySharedPreferences.PREFS_DINGTIMESOUNDENABLED_KEY,
                DingTimeSoundEnabled);
        mPrefs.saveInt(MySharedPreferences.PREFS_DINGTIME_KEY, DingTime);
        if(DingTimeSound != 0) {
            mPrefs.saveInt(MySharedPreferences.PREFS_DINGTIMESOUND_KEY, DingTimeSound);
        }

        mPrefs.saveBool(MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUNDENABLED_KEY,
                HalfMassageDurationSoundEnabled);
        if(HalfMassageDurationSound != 0) {
            mPrefs.saveInt(MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY,
                    HalfMassageDurationSound);
        }

        mPrefs.saveBool(MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUNDENABLED_KEY,
                FullMassageDurationSoundEnabled);
        if(FullMassageDurationSound != 0) {
            mPrefs.saveInt(MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY,
                    FullMassageDurationSound);
        }

        mPrefs.saveInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY,
                MinimalZoneDuration);

        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN, tipsShownInMain);
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, tipsShown);
    }

}
