package com.fingerhits.massageAsisstant.main;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.fingerhits.massageAsisstant.DataUtils;
import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.main.MainActivity;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.myMassages.MyMassagesActivity;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.StringContains.containsString;


/**
 * Created by Javier Torón on 20/02/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {
    static MySharedPreferences mPrefs;
    static MassageAssistantDBHelper mDatabaseHelper;

    @Rule
    public IntentsTestRule<MainActivity> mActivityRule =
            new IntentsTestRule(MainActivity.class, true, false);


    @BeforeClass
    public static void intentWith4Massages() {

        long duration_for_test = 10000;

        Context mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mPrefs = new MySharedPreferences(mContext);
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);

        PrefsUtils.setPreferences(mPrefs, 5, false, true, 1, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 0);

        DataUtils dataUtils = new DataUtils(mDatabaseHelper, mContext);

        mDatabaseHelper.deleteAllMassages();

        dataUtils.insert_valid_massage_pepe_simple(duration_for_test);
        dataUtils.insert_valid_massage_full_jose(duration_for_test);
        dataUtils.insert_valid_massage_advanced_mar(duration_for_test);
        dataUtils.insert_valid_massage_full_paco(duration_for_test);
    }

    @Before
    public void beforeEachTest() {

            }

    @Test
    public void initialStateWithMassagesTest() {
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.newMassageBtn)).check(matches(isDisplayed()));

        onView(withId(R.id.lastMassageLayout))
                .check(matches(isDisplayed()));
        onView(withId(R.id.lastMassageText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("Last massage started"))));
        onView(withId(R.id.patientText))
                .check(matches(isDisplayed()))
                .check(matches(withText("paco")));
        onView(withId(R.id.commentsText))
                .check(matches(isDisplayed()))
                .check(matches(withText("comments")));
        onView(withId(R.id.resumeLastMassageBtn))
                .check(matches(isDisplayed()));

        onView(withId(R.id.massagesCountText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("You have done"))));
        onView(withId(R.id.myMassagesBtn))
                .check(matches(isDisplayed()));

        onView(withId(R.id.playTutorialLayout))
                .check(matches(not( isDisplayed())));
        onView(withId(R.id.importBtn))
                .check(matches(not( isDisplayed())));

        onView(withId(R.id.tipLayout)).perform(scrollTo())
                .check(matches(isDisplayed()));
        onView(withId(R.id.bannerLayout))
                .check(matches(isDisplayed()));
    }


    @Test
    public void clickingMyMassages_shouldStartMyMassagesActivity() {
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.myMassagesBtn)).perform(click());
        intended(hasComponent(MyMassagesActivity.class.getName()));
    }

    @Test
    public void clickingResume_shouldStartMassageSessionActivity() {
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.resumeLastMassageBtn)).perform(click());
        intended(hasComponent(MassageSessionActivity.class.getName()));
    }


    @Test
    public void savePatientTest(){
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.patientText))
                .perform(click())
                .perform(typeText("2"));
        onView(withId(R.id.savePatientButton))
                .perform(click());

        onView(withId(R.id.patientText))
                .check(matches(withText("paco2")));

        //undo the change for the initialStateTest
        onView(withId(R.id.patientText))
                .perform(click())
                .perform(clearText())
                .perform(typeText("paco"));
        onView(withId(R.id.savePatientButton))
                .perform(click());
    }



    @Test
    public void saveCommentsTest(){
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.commentsText))
                .perform(click())
                .perform(typeText("2"));
        onView(withId(R.id.saveCommentsButton))
                .perform(click());

        onView(withId(R.id.commentsText))
                .check(matches(withText("comments2")));

        //undo the change for the initialStateTest
        onView(withId(R.id.commentsText))
                .perform(click())
                .perform(clearText())
                .perform(typeText("comments"));
        onView(withId(R.id.saveCommentsButton))
                .perform(click());
    }


    /******* Helper methods *********/
}