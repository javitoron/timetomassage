package com.fingerhits.massageAsisstant.main;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.exportImport.ExportImportActivity;
import com.fingerhits.massageassistant.main.MainActivity;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.newMassage.NewMassageActivity;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.options.OptionsActivity;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.init;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.release;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;


/**
 * Created by Javier Torón on 20/02/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityEmptyTest {
    static MySharedPreferences mPrefs;
    static MassageAssistantDBHelper mDatabaseHelper;

    @Rule
    public IntentsTestRule<MainActivity> mActivityRule =
            new IntentsTestRule(MainActivity.class, true, false);

    @BeforeClass
    public static void intentWithNoMassages() {
        Context mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mPrefs = new MySharedPreferences(mContext);
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);

        PrefsUtils.setPreferences(mPrefs, 5, false, true, 1, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 0);

    }


    @Before
    public void beforeEachTest() {
        mDatabaseHelper.deleteAllMassages();
    }

    @Test
    public void initialStateWithNoMassagesTest() {
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.newMassageBtn)).check(matches(isDisplayed()));
        onView(withId(R.id.lastMassageLayout)).check(matches(not(isDisplayed())));

        onView(withId(R.id.massagesCountText)).
                check(matches(withText(R.string.main_massages_count_zero)));

        onView(withId(R.id.myMassagesBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.playTutorialLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.importBtn)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.tipLayout)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.bannerLayout)).check(matches(isDisplayed()));
    }


    @Test
    public void clickingImport_shouldStartExportImportActivity() {

        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.importBtn)).perform(click());
        intended(hasComponent(ExportImportActivity.class.getName()));

        //TODO: Test menu option for export/import
        //openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        //onView(withText( mContext.getString(R.string.menu_exportImport))).perform(click());
        //onView(withId( R.id.exportImport_menu)).perform(click());
        //intended(hasComponent(ExportImportActivity.class.getName()));
    }

    @Test
    public void clickingNewMassage_shouldStartNewMassageActivity() {
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.newMassageBtn)).perform(click());
        intended(hasComponent(NewMassageActivity.class.getName()));
    }

    @Test
    public void clickingPlayTutorial_shouldStartMassageSessionActivity() {
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.playTutorialBtn)).perform(click());
        intended(hasAction(equalTo(Intent.ACTION_VIEW)));
        //TODO: Could test that URL is the Tutorial URL
        /*String tutorial_url = Uri.parse(mContext.getString(R.string.tutorial_url)).toString();
        intended(allOf(
                hasAction(equalTo(Intent.ACTION_VIEW)),
                hasData(hasHost(equalTo(tutorial_url)))));*/

        //TODO: Test menu option for View tutorial
        //openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        //onView(withId( R.id.open_tutorial_video_menu)).perform(click());
        //intended(hasAction(equalTo(Intent.ACTION_VIEW)));
    }

    @Test
    public void clickingOptionsMenu_shouldStartOptionsActivity() {
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.options_menu)).perform(click());
        intended(hasComponent(OptionsActivity.class.getName()));
    }

    //TODO: Test menu option for Go to tiemetomassage.com
    //TODO: Test menu option for info@fingerhits.com


    @Test
    public void completingAndResumingAMassageTest() {
        mPrefs.saveInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY, 0);
        mPrefs.saveInt(MySharedPreferences.PREFS_MASSAGEDURATION_KEY, 5);
        mPrefs.saveInt(MySharedPreferences.PREFS_MODE_KEY,
                MassageRecord.MODE_SIMPLE);
        mActivityRule.launchActivity(new Intent());

        //MainActivity
        onView(withId(R.id.newMassageBtn)).perform(click());

        //NewMassageActivity
        onView(withId(R.id.newMassageBtn)).perform(click());

        SystemClock.sleep(5000);

        onView(withId(R.id.endMassageBtn)).perform(click());

        //Back to MainActivity
        onView(withId(R.id.lastMassageText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("lasted 00:05"))));

        //Delete previous intents record
        release();
        init();

        onView(withId(R.id.resumeLastMassageBtn))
                .check(matches(isDisplayed()))
                .perform(click());

        intended(hasComponent(MassageSessionActivity.class.getName()));
    }

    @Test
    public void discardedZoneAndMassageNotSavedTest() {
        mPrefs.saveInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY, 10);
        mPrefs.saveInt(MySharedPreferences.PREFS_MASSAGEDURATION_KEY, 5);
        mPrefs.saveInt(MySharedPreferences.PREFS_MODE_KEY,
                MassageRecord.MODE_SIMPLE);
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.newMassageBtn)).perform(click());

        onView(withId(R.id.newMassageBtn)).perform(click());

        SystemClock.sleep(5000);

        onView(withId(R.id.endMassageBtn)).perform(click());

        //Back to MainActivity
        onView(withId(R.id.lastMassageLayout)).check(matches(not(isDisplayed())));

        mPrefs.saveInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY, 0);
    }

}