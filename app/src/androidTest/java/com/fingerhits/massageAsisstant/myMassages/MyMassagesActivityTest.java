package com.fingerhits.massageAsisstant.myMassages;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.fingerhits.massageAsisstant.DataUtils;
import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.myMassages.MyMassagesActivity;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.treatedZones.TreatedZonesActivity;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.CursorMatchers.withRowString;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by Javier Torón on 04/03/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MyMassagesActivityTest {
    static Context mContext;
    static Resources mRes;
    static MassageAssistantDBHelper mDatabaseHelper;
    static DataUtils dataUtils;

    @Rule
    public IntentsTestRule<MyMassagesActivity> mActivityRule =
            new IntentsTestRule(MyMassagesActivity.class, true, false);

    MyMassagesActivity activity;
    int duration_for_test = 1000;


    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mRes = mContext.getResources();

        mDatabaseHelper = new MassageAssistantDBHelper(mContext);

        PrefsUtils.setPreferences(new MySharedPreferences(mContext), 5, false,
                true, 1, true, true, true, true, true, R.raw.harp_double,
                R.raw.bell_high, R.raw.birds_park, 0);

        dataUtils = new DataUtils(mDatabaseHelper, mContext);
    }

    @Before
    public void beforeEachTest() {
        mDatabaseHelper.deleteAllMassages();
    }

    @Test
    public void initialStateTest_withoutMassages() throws Exception {
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.myMassagesList))
                .check(matches(isDisplayed()))
                .check(matches(not(hasDescendant(any(View.class)))));
    }

    @Test
    public void deleteTheOnlyMassageTest() throws Exception {
        dataUtils.insert_valid_massage_pepe_simple(duration_for_test);
//        dataUtils.insert_valid_massage_full_jose(duration_for_test);
        //      dataUtils.insert_valid_massage_advanced_mar(duration_for_test);

        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.myMassagesList))
                .check(matches(isDisplayed()));


        onData(withRowString(MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT, "pepe"))
                .inAdapterView(withId(R.id.myMassagesList))
                .onChildView(withId(R.id.massagesDeleteButton))
                .perform(click());

/*        onData(anything())
                .inAdapterView(withId(R.id.myMassagesList)).atPosition(0)
                .perform(click());*/

        onView(withText(R.string.myMassages_delete_massage_confirmation_question))
                .check(matches(isDisplayed()));

        onView(withText(android.R.string.yes)).perform(click());

        //This is because there was only one massage
        assertTrue(mActivityRule.getActivity().isFinishing());

    }


    @Test
    public void viewDetailsTest() throws Exception {
        dataUtils.insert_valid_massage_pepe_simple(duration_for_test);
        dataUtils.insert_valid_massage_full_jose(duration_for_test);
        dataUtils.insert_valid_massage_advanced_mar(duration_for_test);

        mActivityRule.launchActivity(new Intent());

        onData(withRowString(MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT, "jose"))
                .inAdapterView(withId(R.id.myMassagesList))
                .onChildView(withId(R.id.massagesViewButton))
                .perform(click());

        intended(hasComponent(TreatedZonesActivity.class.getName()));

    }

    @Test
    public void sendEmailDetailsTest() throws Exception {
        dataUtils.insert_valid_massage_pepe_simple(duration_for_test);
        dataUtils.insert_valid_massage_full_jose(duration_for_test);
        dataUtils.insert_valid_massage_advanced_mar(duration_for_test);

        mActivityRule.launchActivity(new Intent());

        intending(hasAction(Intent.ACTION_SEND))
                .respondWith(new Instrumentation.ActivityResult(
                        Activity.RESULT_OK, new Intent()));

        onData(withRowString(MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT, "mar"))
                .inAdapterView(withId(R.id.myMassagesList))
                .onChildView(withId(R.id.massagesSendButton))
                .perform(click());

        intended(hasAction(Intent.ACTION_SEND));

    }

    @Test
    public void savePatientTest() throws Exception {
        dataUtils.insert_valid_massage_pepe_simple(duration_for_test);
        dataUtils.insert_valid_massage_full_jose(duration_for_test);
        dataUtils.insert_valid_massage_advanced_mar(duration_for_test);

        mActivityRule.launchActivity(new Intent());

        onData(withRowString(MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT, "jose"))
                .inAdapterView(withId(R.id.myMassagesList))
                .onChildView(withId(R.id.patientText))
                .perform(click())
                .perform(typeText("2"));

        onData(withRowString(MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT, "jose"))
                .inAdapterView(withId(R.id.myMassagesList))
                .onChildView(withId(R.id.savePatientButton))
                .perform(click());

        onData(withRowString(MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT, "jose2"))
                .inAdapterView(withId(R.id.myMassagesList))
                .check(matches(isDisplayed()));

    }

    @Test
    public void saveCommentsTest() throws Exception {
        dataUtils.insert_valid_massage_pepe_simple(duration_for_test);
        dataUtils.insert_valid_massage_full_jose(duration_for_test);
        dataUtils.insert_valid_massage_advanced_mar(duration_for_test);

        mActivityRule.launchActivity(new Intent());

        onData(withRowString(MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT, "mar"))
                .inAdapterView(withId(R.id.myMassagesList))
                .onChildView(withId(R.id.commentsText))
                .perform(click())
                .perform(typeText("new comment"));

        onData(withRowString(MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT, "mar"))
                .inAdapterView(withId(R.id.myMassagesList))
                .onChildView(withId(R.id.saveCommentsButton))
                .perform(click());

        onData(withRowString(MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT, "mar"))
                .inAdapterView(withId(R.id.myMassagesList))
                .onChildView(withId(R.id.commentsText))
                .check(matches(withText(containsString("new comment"))));

    }

}
