package com.fingerhits.massageAsisstant;

import android.content.Context;

import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecordInvalidException;
import com.fingerhits.massageassistant.utils.MiscUtils;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Javier Torón on 30/06/2016.
 */
public class DataUtils {
    MassageAssistantDBHelper mDatabaseHelper;
    Context mContext;
    int massageRecordIndex = 0;
    int treatedZoneRecordIndex = 0;

    public DataUtils(MassageAssistantDBHelper db, Context c){
        mDatabaseHelper = db;
        mContext = c;
    }


    public long insertMassageRecord_pepe_simple(){
        Date start = getMassageStartDate();
        MassageRecord mMassageRecord = new MassageRecord(
                MiscUtils.currentLocalUser(),
                "pepe", start, MassageRecord.MODE_SIMPLE);

        return mDatabaseHelper.insertMassageRecord(mMassageRecord);
        //mMassageRecord.setId(mIdMassage);

    }


    public long insert_valid_massage_pepe_simple(long duration_for_test) {
        long mIdMassage = insertMassageRecord_pepe_simple();
        insertTreatedZone_for_testing(mIdMassage,
                TreatedZoneRecord.CODPOSITION_UNDEFINED,
                Divisions.CODDIVISION_SINGLE, TreatedZoneRecord.CODZONE_MASSAGE,
                duration_for_test);
        mDatabaseHelper.finishMassageRecord(mIdMassage);

        return mIdMassage;
    }


    public long insert_valid_massage_full_jose(long duration_for_test) {
        long mIdMassage = insertMassageRecord_full_intermediate("jose");
        insertTreatedZone_for_testing(mIdMassage,
                TreatedZoneRecord.CODPOSITION_FACEDOWN,
                Divisions.CODDIVISION_SINGLE, TreatedZoneRecord.CODZONE_FULLBODY,
                duration_for_test);
        mDatabaseHelper.finishMassageRecord(mIdMassage);

        return mIdMassage;
    }


    public long insert_valid_massage_full_paco(long duration_for_test) {

        long mIdMassage = insertMassageRecord_full_intermediate("paco");
        insertTreatedZone_for_testing(mIdMassage,
                TreatedZoneRecord.CODPOSITION_FACEUP,
                Divisions.CODDIVISION_SINGLE, TreatedZoneRecord.CODZONE_FULLBODY,
                duration_for_test);
        insertTreatedZone_for_testing(mIdMassage,
                TreatedZoneRecord.CODPOSITION_LEFTSIDE,
                Divisions.CODDIVISION_SINGLE, TreatedZoneRecord.CODZONE_FULLBODY,
                duration_for_test);
        mDatabaseHelper.finishMassageRecord(mIdMassage);

        return mIdMassage;
    }



    public long insertMassageRecord_full_intermediate( String patient){
        Date start = getMassageStartDate();
        Date end = new Date(start.getTime() + TimeUnit.HOURS.toMillis(2) );
        MassageRecord mMassageRecord = new MassageRecord( 0, patient,
                MiscUtils.currentLocalUser(), start, end,
                0L, MassageRecord.MODE_INTERMEDIATE, "type", "objective",
                "usedMaterials", "comments" );

        return mDatabaseHelper.insertMassageRecord(mMassageRecord);
        //mMassageRecord.setId(mIdMassage);

    }


    public long insert_valid_massage_advanced_mar(long duration_for_test) {
        long mIdMassage = insertMassageRecord_advanced("mar");
        insertTreatedZone_for_testing(mIdMassage,
                TreatedZoneRecord.CODPOSITION_RIGHTSIDE,
                Divisions.CODDIVISION_DEFAULT, TreatedZoneRecord.CODZONE_HEAD,
                duration_for_test);
        insertTreatedZone_for_testing(mIdMassage,
                TreatedZoneRecord.CODPOSITION_SITTINGBACK,
                Divisions.CODDIVISION_DETAILED,
                TreatedZoneRecord.CODZONE_LEFTFOREARM, duration_for_test * 2);
        insertTreatedZone_for_testing(mIdMassage,
                TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                Divisions.CODDIVISION_LEFTRIGHT,
                TreatedZoneRecord.CODZONE_RIGHTSIDE,
                duration_for_test * 3);
        mDatabaseHelper.finishMassageRecord(mIdMassage);

        return mIdMassage;
    }

    public long insert_valid_massage_advanced_mar_10minutesAgo(long duration_for_test) {
        long mIdMassage = insertMassageRecord_advanced_10minutesAgo("mar");
        insertTreatedZone_for_testing_10minutesAgo(mIdMassage,
                TreatedZoneRecord.CODPOSITION_RIGHTSIDE,
                Divisions.CODDIVISION_DEFAULT, TreatedZoneRecord.CODZONE_HEAD,
                duration_for_test);
        insertTreatedZone_for_testing_10minutesAgo(mIdMassage,
                TreatedZoneRecord.CODPOSITION_SITTINGBACK,
                Divisions.CODDIVISION_DETAILED,
                TreatedZoneRecord.CODZONE_LEFTFOREARM, duration_for_test * 2);
        insertTreatedZone_for_testing_10minutesAgo(mIdMassage,
                TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                Divisions.CODDIVISION_LEFTRIGHT,
                TreatedZoneRecord.CODZONE_RIGHTSIDE,
                duration_for_test * 3);
        mDatabaseHelper.finishMassageRecord(mIdMassage);

        return mIdMassage;
    }

    public long insertMassageRecord_advanced(String patient){
        Date start = getMassageStartDate();
        MassageRecord mMassageRecord = new MassageRecord(
                MiscUtils.currentLocalUser(),
                patient, start, MassageRecord.MODE_ADVANCED);

        return mDatabaseHelper.insertMassageRecord(mMassageRecord);
        //mMassageRecord.setId(mIdMassage);
    }

    public long insertMassageRecord_advanced_10minutesAgo(String patient){
        Date start = getMassageStartDate_10minutesAgo();
        MassageRecord mMassageRecord = new MassageRecord(
                MiscUtils.currentLocalUser(),
                patient, start, MassageRecord.MODE_ADVANCED);

        return mDatabaseHelper.insertMassageRecord(mMassageRecord);
        //mMassageRecord.setId(mIdMassage);
    }



    public long insertTreatedZone_for_testing(long mIdMassage,
                                              String codPosition,
                                              String codDivision,
                                              String codZone, long duration ){
        Date start = getTreatedZoneStartDate();
        Date end = new Date(start.getTime() + ( duration * 1000 ) );
        //Meter treatedZones
        TreatedZoneRecord treatedZoneRecord = new TreatedZoneRecord( 0,
                mIdMassage, codPosition, codDivision, codZone, start, end,
                duration, "comments" );

        try {
            return mDatabaseHelper.insertTreatedZoneRecord(treatedZoneRecord);
        } catch (TreatedZoneRecordInvalidException e) {
            e.printStackTrace();
        }
        return -1;
    }


    public long insertTreatedZone_for_testing_10minutesAgo(long mIdMassage,
                                              String codPosition,
                                              String codDivision,
                                              String codZone, long duration ){
        Date start = getTreatedZoneStartDate_10minutesAgo();
        Date end = new Date(start.getTime() + ( duration * 1000 ) );
        //Meter treatedZones
        TreatedZoneRecord treatedZoneRecord = new TreatedZoneRecord( 0,
                mIdMassage, codPosition, codDivision, codZone, start, end,
                duration, "comments" );

        try {
            return mDatabaseHelper.insertTreatedZoneRecord(treatedZoneRecord);
        } catch (TreatedZoneRecordInvalidException e) {
            e.printStackTrace();
        }
        return -1;
    }


    /* PRIVATE METHODS */
    private Date getMassageStartDate(){
        Date base = new Date();
        return new Date(base.getTime() +
                TimeUnit.MINUTES.toMillis(10 * ( massageRecordIndex++ ) ) );

    }

    private Date getMassageStartDate_10minutesAgo(){
        Date base = new Date();
        return new Date(base.getTime() - TimeUnit.MINUTES.toMillis( 10 ) );

    }



    private Date getTreatedZoneStartDate(){
        Date base = new Date();
        return new Date(base.getTime() +
                TimeUnit.MINUTES.toMillis(10 * ( treatedZoneRecordIndex++ ) ) );

    }

    private Date getTreatedZoneStartDate_10minutesAgo(){
        Date base = new Date();
        return new Date(base.getTime() - TimeUnit.MINUTES.toMillis( 10 ) +
                TimeUnit.MINUTES.toMillis(1 * ( treatedZoneRecordIndex++ ) ) );

    }

}
