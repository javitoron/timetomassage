package com.fingerhits.massageAsisstant.treatedZones;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.fingerhits.massageAsisstant.DataUtils;
import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.treatedZones.TreatedZonesActivity;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.matcher.CursorMatchers.withRowString;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.fingerhits.massageAsisstant.EspressoHelpers.withAdaptedData;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by Javier Torón on 04/03/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class TreatedZonesActivityTest {
    static Context mContext;
    static Resources mRes;
    static MassageAssistantDBHelper mDatabaseHelper;
    static DataUtils dataUtils;
    private Intent intent;
    private Bundle bundle;
    private Long mIdMassage;

    @Rule
    public IntentsTestRule<TreatedZonesActivity> mActivityRule =
            new IntentsTestRule(TreatedZonesActivity.class, true, false);

    TreatedZonesActivity activity;
    static int duration_for_test = 1000;


    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mRes = mContext.getResources();
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);
        dataUtils = new DataUtils(mDatabaseHelper, mContext);


        PrefsUtils.setPreferences(new MySharedPreferences(mContext), 5, false,
                true, 1, true, true, true, true, true, R.raw.harp_double,
                R.raw.bell_high, R.raw.birds_park, 0);
    }

    @Before
    public void beforeEachTest() {
        mDatabaseHelper.deleteAllMassages();
        mIdMassage = dataUtils.insert_valid_massage_advanced_mar(duration_for_test);
        dataUtils.insertTreatedZone_for_testing(mIdMassage,
                TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                Divisions.CODDIVISION_LEFTRIGHT,
                TreatedZoneRecord.CODZONE_LEFTSIDE,
                duration_for_test * 4);
        dataUtils.insertTreatedZone_for_testing(mIdMassage,
                TreatedZoneRecord.CODPOSITION_SITTINGFRONT,
                Divisions.CODDIVISION_LEFTRIGHT,
                TreatedZoneRecord.CODZONE_HEADNECK,
                duration_for_test * 5);

    }

    @Test
    public void initialStateTest_fromMyMassages() throws Exception {
        intent = new Intent();
        intent.putExtras(getBundle(true));
        mActivityRule.launchActivity(intent);

        //MASSAGE HEADER
        onView(withId(R.id.massageRecordLayout))
                .check(matches(isDisplayed()));
        onView(withId(R.id.massagesDeleteButton))
                .check(matches(isDisplayed()));
        onView(withId(R.id.massagesSendButton))
                .check(matches(isDisplayed()));
        onView(withId(R.id.patientText))
                .check(matches(isDisplayed()));
        onView(withId(R.id.startDateText))
                .check(matches(isDisplayed()));
        onView(withId(R.id.durationText))
                .check(matches(isDisplayed()));
        onView(withId(R.id.startEndTimeText))
                .check(matches(isDisplayed()));
        onView(withId(R.id.modeText))
                .check(matches(isDisplayed()));


        //LIST
        onView(withId(R.id.treatedZonesList))
                .check(matches(isDisplayed()));

        //First position
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_RIGHTSIDE))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.positionText))
                .check(matches(withText(containsString("Lying on right side"))));

        //First position. First zone
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_HEAD))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(withText(containsString("Head"))));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_HEAD))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneSelectSpinner))
                .check(matches(not(isDisplayed())));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_HEAD))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.saveTreatedZoneButton))
                .check(matches(not(isDisplayed())));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_HEAD))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneDurationText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("16:40"))));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_HEAD))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZonesDeleteButton))
                .check(matches(isDisplayed()));




        //Second position
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_SITTINGBACK))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.positionText))
                .check(matches(withText(containsString("Sitting back"))));

        //Second position. First zone
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(withText(containsString("Left forearm"))));

        //Third position
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_SITTINGFRONT))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.positionText))
                .atPosition(0)
                .check(matches(withText(containsString("Sitting front"))));

        //Third position. First zone
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_RIGHTSIDE))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(withText(containsString("Right side"))));

        //No position
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_SITTINGFRONT))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.positionText))
                .atPosition(1)
                .check(matches(not(isDisplayed())));

        //Third position. Second zone
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTSIDE))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(withText(containsString("Left side"))));


        //No position
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_SITTINGFRONT))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.positionText))
                .atPosition(2)
                .check(matches(not(isDisplayed())));

        //Third position. Third zone
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_HEADNECK))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(withText(containsString("Head/Neck"))));
    }


    @Test
    public void saveCodZoneTest() throws Exception {
        intent = new Intent();
        intent.putExtras(getBundle(true));
        mActivityRule.launchActivity(intent);


        //Click to show select
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(isDisplayed()))
                .perform(click());

        //Select 6 Right biceps
        onData(allOf(is(instanceOf(String.class)), is("Right biceps")))
                .perform(click());

        onData(allOf(is(instanceOf(Cursor.class)),
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM)))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneSelectSpinner))
                .check(matches(isDisplayed()));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(not(isDisplayed())));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.saveTreatedZoneButton))
                .check(matches(isDisplayed()));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneDurationText))
                .check(matches(isDisplayed()));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZonesDeleteButton))
                .check(matches(not(isDisplayed())));

        //Save
        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.saveTreatedZoneButton))
                .perform(click());


        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(withText(containsString("Right biceps"))))
                .check(matches(isDisplayed()));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneSelectSpinner))
                .check(matches(not(isDisplayed())));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.saveTreatedZoneButton))
                .check(matches(not(isDisplayed())));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneDurationText))
                .check(matches(isDisplayed()));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTFOREARM))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZonesDeleteButton))
                .check(matches(isDisplayed()));

    }


    @Test
    public void initialStateTest_fromMassageSession() throws Exception {
        intent = new Intent();
        intent.putExtras(getBundle(false));
        mActivityRule.launchActivity(intent);

        onView(withId(R.id.massageRecordLayout))
                .check(matches(not(isDisplayed())));
        onView(withId(R.id.treatedZonesList))
                .check(matches(isDisplayed()));

    }

    @Test
    public void deleteMassageTest() throws Exception {
        intent = new Intent();
        intent.putExtras(getBundle(true));
        mActivityRule.launchActivity(intent);

        onView(withId(R.id.massagesDeleteButton))
                .perform(click());

        onView(withText(R.string.myMassages_delete_massage_confirmation_question))
                .check(matches(isDisplayed()));

        onView(withText(android.R.string.yes)).perform(click());

        //This is because there was only one massage
        assertTrue(mActivityRule.getActivity().isFinishing());

    }


    @Test
    public void sendEmailDetailsTest() throws Exception {
        intent = new Intent();
        intent.putExtras(getBundle(true));
        mActivityRule.launchActivity(intent);

        intending(hasAction(Intent.ACTION_SEND))
                .respondWith(new Instrumentation.ActivityResult(
                        Activity.RESULT_OK, new Intent()));

        onView(withId(R.id.massagesSendButton))
                .perform(click());

        intended(hasAction(Intent.ACTION_SEND));
    }


    @Test
    public void delete_onlyTreatedZoneInPosition_Test() throws Exception {

        intent = new Intent();
        intent.putExtras(getBundle(true));
        mActivityRule.launchActivity(intent);

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_HEAD))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZonesDeleteButton))
                .perform(click());

        onView(withText(R.string.treatedZones_delete_confirmation_question))
                .check(matches(isDisplayed()));

        onView(withText(android.R.string.yes)).perform(click());



        onView(withId(R.id.treatedZonesList))
                .check(matches(not(withAdaptedData(withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_RIGHTSIDE)))));

    }

    @Test
    public void delete_secondTreatedZoneInPosition_Test() throws Exception {

        intent = new Intent();
        intent.putExtras(getBundle(true));
        mActivityRule.launchActivity(intent);

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTSIDE))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZonesDeleteButton))
                .perform(click());

        onView(withText(R.string.treatedZones_delete_confirmation_question))
                .check(matches(isDisplayed()));

        onView(withText(android.R.string.yes)).perform(click());



        onView(withId(R.id.treatedZonesList))
                .check(matches(not(withAdaptedData(withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_LEFTSIDE)))));


        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_SITTINGFRONT))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.positionText))
                .atPosition(0)
                .check(matches(isDisplayed()));
    }


    @Test
    public void delete_firstTreatedZoneInPosition_Test() throws Exception {

        intent = new Intent();
        intent.putExtras(getBundle(true));
        mActivityRule.launchActivity(intent);

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_RIGHTSIDE))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZonesDeleteButton))
                .perform(click());

        onView(withText(R.string.treatedZones_delete_confirmation_question))
                .check(matches(isDisplayed()));

        onView(withText(android.R.string.yes)).perform(click());



        onView(withId(R.id.treatedZonesList))
                .check(matches(not(withAdaptedData(withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODZONE_RIGHTSIDE)))));


        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_SITTINGFRONT))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.positionText))
                .atPosition(0)
                .check(matches(isDisplayed()));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE,
                        TreatedZoneRecord.CODZONE_LEFTSIDE))
                .inAdapterView(withId(R.id.treatedZonesList))
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(withText(containsString("Left side"))));

        onData(
                withRowString(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION,
                        TreatedZoneRecord.CODPOSITION_SITTINGFRONT))
                .inAdapterView(withId(R.id.treatedZonesList))
                .atPosition(0)
                .onChildView(withId(R.id.treatedZoneText))
                .check(matches(withText(containsString("Left side"))));
    }



    private Bundle getBundle(boolean massageFinished){
        bundle = new Bundle();
        bundle.putLong(TreatedZonesActivity.PARAMETER_IDMASSAGE, mIdMassage);
        bundle.putBoolean(TreatedZonesActivity.PARAMETER_MASSAGE_FINISHED, massageFinished);

        return bundle;
    }
}
