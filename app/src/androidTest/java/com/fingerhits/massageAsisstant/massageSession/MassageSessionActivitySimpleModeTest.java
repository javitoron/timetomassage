package com.fingerhits.massageAsisstant.massageSession;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.fingerhits.massageAsisstant.DataUtils;
import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.MassageSessionInteractor;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.options.OptionsActivity;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.app.Activity.RESULT_OK;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.fingerhits.massageAsisstant.EspressoHelpers.withBackgroundColor;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.not;

/**
 * Created by Javier Torón on 08/03/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MassageSessionActivitySimpleModeTest {
    static Context mContext;
    static MySharedPreferences mPrefs;
    static Resources mRes;
    static MassageSessionInteractor interactor;
    static MassageAssistantDBHelper mDatabaseHelper;

    @Rule
    public IntentsTestRule<MassageSessionActivity> mActivityRule =
            new IntentsTestRule(MassageSessionActivity.class, true, false);

    //MassageSessionActivity activity;

    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mPrefs = new MySharedPreferences(mContext);
        mRes = mContext.getResources();

        PrefsUtils.setPreferences(mPrefs, 5, false, true, 1, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 0);

        mDatabaseHelper = new MassageAssistantDBHelper(mContext);
        interactor = new MassageSessionInteractor(mDatabaseHelper, mContext, mPrefs);
    }

    @Before
    public void beforeEachTest() {
        mDatabaseHelper.deleteAllMassages();
    }


    @Test
    public void fromNewMassageIntentTest() {
        MassageSessionUtils.setPreferences(mPrefs, 5, MassageRecord.MODE_SIMPLE);
        mActivityRule.launchActivity(new Intent());

        //Goes automatically to NewMassageActivity
        onView(withId(R.id.newMassageBtn))
                .check(matches(isDisplayed()))
                .perform(click());

        //Returns to MassageSessionActivity with RESULT_OK and mode MODE_SIMPLE
        MassageSessionActivity act = mActivityRule.getActivity();

        assertEquals( RESULT_OK, act.resultCode);
        assertEquals( MassageRecord.MODE_SIMPLE,
                act.resultData.getIntExtra(
                        MassageSessionActivity.PARAMETER_MODE, 0 ));

    }

    @Test
    public void newSimpleMassageInitialStateTest() {
        startSimpleNewMassage();

        onView(withId(R.id.mute_menu)).check(matches(isDisplayed()));
        onView(withId(R.id.summary_menu)).check(doesNotExist());
        onView(withId(R.id.options_menu)).check(matches(isDisplayed()));
        onView(withId(R.id.zoneChronoLayout)).check(matches(not(isDisplayed())));
        onView(withId(R.id.positionChronoLayout)).check(matches(not(isDisplayed())));
        onView(withId(R.id.fullChronoLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.fullChrono)).check(matches(isDisplayed()));
        onView(withId(R.id.remainingChronoLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.remainingChronoTxt)).check(matches(not(isDisplayed())));
        onView(withId(R.id.pauseMassageBtn)).check(matches(isDisplayed()));
        onView(withId(R.id.endMassageBtn)).check(matches(isDisplayed()));
        onView(withId(R.id.massagingTxt)).check(matches(isDisplayed()));

        onView(withId(R.id.sittingBackBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.sittingFrontBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.lyingFaceDownBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.positionLayout)).check(matches(not(isDisplayed())));
        onView(withId(R.id.divisionsLayout)).check(matches(not(isDisplayed())));

        onView(withId(R.id.pausedMassageLayout)).check(matches(not(isDisplayed())));

    }

    @Test
    public void muteSoundTest() {
        interactor.setSoundWarningsEnabled(true);

        startSimpleNewMassage();

        //TODO: Check drawable sound_on
        onView(withId(R.id.mute_menu))
                .check(matches(isDisplayed()))
                .perform(click());

        assertFalse( interactor.getSoundWarningsEnabled());

        //TODO: Check drawable sound_off
        onView(withId(R.id.mute_menu)).perform(click());
        assertTrue( interactor.getSoundWarningsEnabled());
    }


    @Test
    public void clickingOptionsMenu_shouldStartOptionsActivity() {
        interactor.setSoundWarningsEnabled(true);
        startSimpleNewMassage();

        onView(withId(R.id.options_menu)).perform(click());
        intended(hasComponent(OptionsActivity.class.getName()));

        //Set soundWarnings false
        onView(withId(R.id.soundWarningsCheck))
                .perform(scrollTo())
                .perform(click());


        onView(withId(android.R.id.home))
                .perform(click());

        //Set soundWarnings true again
        onView(withId(R.id.mute_menu)).perform(click());
        assertTrue( interactor.getSoundWarningsEnabled());



        //TODO: Test changes on another options (warnings, duration, etc)

    }


    @Test
    public void clickingChronosTest() {
        startSimpleNewMassage();

        onView(withId(R.id.remainingChronoLayout)).perform(click());

        onView(withId(R.id.fullChrono)).check(matches(not(isDisplayed())));
        onView(withId(R.id.remainingChronoTxt)).check(matches(isDisplayed()));

        onView(withId(R.id.fullChronoLayout)).perform(click());

        onView(withId(R.id.fullChrono)).check(matches(isDisplayed()));
        onView(withId(R.id.remainingChronoTxt)).check(matches(not(isDisplayed())));
    }


    @Test
    public void clickingPauseTest() {
        mPrefs.saveBool(MySharedPreferences.PREFS_ANIMATIONS_DISABLED_FOR_TESTING, true);
        startSimpleNewMassage();

        onView(withId(R.id.pauseMassageBtn)).perform(click());

        onView(withId(R.id.pauseMassageBtn)).check(matches(not(isEnabled())));
        onView(withId(R.id.endMassageBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.massagingTxt)).check(matches(not(isDisplayed())));

        onView(withId(R.id.pausedMassageLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.pauseChrono)).check(matches(isDisplayed()));
        onView(withId(R.id.resumeMassageBtn)).check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.pausedMassageLayout)).check(matches(not(isDisplayed())));
        onView(withId(R.id.pauseChrono)).check(matches(not(isDisplayed())));
        onView(withId(R.id.resumeMassageBtn)).check(matches(not(isDisplayed())));

        onView(withId(R.id.pauseMassageBtn)).check(matches(isEnabled()));
        onView(withId(R.id.endMassageBtn)).check(matches(isDisplayed()));
        onView(withId(R.id.massagingTxt)).check(matches(isDisplayed()));
        mPrefs.saveBool(MySharedPreferences.PREFS_ANIMATIONS_DISABLED_FOR_TESTING, false);
    }


    @Test
    public void clickingEndTest() {
        startSimpleNewMassage();

        onView(withId(R.id.endMassageBtn)).perform(click());

        assertTrue(mActivityRule.getActivity().isFinishing());
    }


    @Test
    public void savingMassageWithAPauseTest() {
        mPrefs.saveBool(MySharedPreferences.PREFS_ANIMATIONS_DISABLED_FOR_TESTING, true);
        startSimpleNewMassage();

        SystemClock.sleep(3000);

        onView(withId(R.id.pauseMassageBtn)).perform(click());

        SystemClock.sleep(15000);

        onView(withId(R.id.resumeMassageBtn)).perform(click());

        SystemClock.sleep(4000);

        onView(withId(R.id.endMassageBtn)).perform(click());

        MassageRecord massage = mDatabaseHelper.getMassageRecord(
                mDatabaseHelper.getLastInsertedMassageId());

        assertTrue("Massage duration is " + massage.getDuration(),
                massage.getDuration()>= 7);
        assertTrue("Massage duration is " + massage.getDuration(),
                massage.getDuration()<= 8);

        Cursor treatedZones = mDatabaseHelper.getTreatedZonesInMassage(
                massage.getId());

        treatedZones.moveToFirst();

        TreatedZoneRecord tz = new TreatedZoneRecord(treatedZones);

        assertTrue("Treated zone duration is " + tz.getDuration(),
                tz.getDuration()>= 3);
        assertTrue("Treated zone duration is " + tz.getDuration(),
                tz.getDuration()<= 4);
        assertEquals(TreatedZoneRecord.CODPOSITION_UNDEFINED, tz.getCodPosition());
        assertEquals(Divisions.CODDIVISION_SINGLE, tz.getCodDivision());
        assertEquals(TreatedZoneRecord.CODZONE_MASSAGE, tz.getCodZone());

        treatedZones.moveToNext();

        tz = new TreatedZoneRecord(treatedZones);

        assertEquals(15, tz.getDuration());
        assertEquals(TreatedZoneRecord.CODPOSITION_UNDEFINED, tz.getCodPosition());
        assertEquals(Divisions.CODDIVISION_SINGLE, tz.getCodDivision());
        assertEquals(TreatedZoneRecord.CODZONE_PAUSE, tz.getCodZone());

        treatedZones.moveToNext();

        tz = new TreatedZoneRecord(treatedZones);

        assertEquals(4, tz.getDuration());
        assertEquals(TreatedZoneRecord.CODPOSITION_UNDEFINED, tz.getCodPosition());
        assertEquals(Divisions.CODDIVISION_SINGLE, tz.getCodDivision());
        assertEquals(TreatedZoneRecord.CODZONE_MASSAGE, tz.getCodZone());

        assertFalse(treatedZones.moveToNext());
        mPrefs.saveBool(MySharedPreferences.PREFS_ANIMATIONS_DISABLED_FOR_TESTING, false);

    }


    @Test
    public void timeExceededChangesChronosStyleTest() {
        restartSimpleMassage(598);

        onView(withId(R.id.fullChronoLayout))
                .check(matches(withBackgroundColor(
                        GraphicUtils.getColor(mRes,
                                R.color.chrono_enabled_under_duration))));

        SystemClock.sleep(3000);

        onView(withId(R.id.fullChronoLayout))
                .check(matches(withBackgroundColor(GraphicUtils.getColor(mRes,
                        R.color.chrono_enabled_over_duration))));
    }


    @Test
    public void resumeMassageTest() {
        restartSimpleMassage(50);

        SystemClock.sleep(5000);

        onView(withId(R.id.endMassageBtn)).perform(click());

        MassageRecord massage = mDatabaseHelper.getMassageRecord(
                mDatabaseHelper.getLastInsertedMassageId());

        assertTrue(massage.getDuration()>= 55);
        assertTrue(massage.getDuration()<= 56);

        Cursor treatedZones = mDatabaseHelper.getTreatedZonesInMassage(
                massage.getId());

        treatedZones.moveToFirst();

        TreatedZoneRecord tz = new TreatedZoneRecord(treatedZones);

        assertEquals(50, tz.getDuration());
        assertEquals(TreatedZoneRecord.CODPOSITION_UNDEFINED, tz.getCodPosition());
        assertEquals(Divisions.CODDIVISION_SINGLE, tz.getCodDivision());
        assertEquals(TreatedZoneRecord.CODZONE_MASSAGE, tz.getCodZone());

        treatedZones.moveToNext();

        tz = new TreatedZoneRecord(treatedZones);

        assertTrue(tz.getDuration()>= 5);
        assertTrue(tz.getDuration()<= 6);
        assertEquals(TreatedZoneRecord.CODPOSITION_UNDEFINED, tz.getCodPosition());
        assertEquals(Divisions.CODDIVISION_SINGLE, tz.getCodDivision());
        assertEquals(TreatedZoneRecord.CODZONE_MASSAGE, tz.getCodZone());

        assertFalse(treatedZones.moveToNext());
    }





    //    /******* Helper methods *********/
    private void startSimpleNewMassage() {
        MassageSessionUtils.setPreferences(mPrefs, 5, MassageRecord.MODE_SIMPLE);
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.newMassageBtn)).perform(click());
    }


    private void restartSimpleMassage(long duration) {
        mDatabaseHelper.deleteAllMassages();

        DataUtils dataUtils = new DataUtils(mDatabaseHelper, mContext);

        Long mMassageId = dataUtils.insert_valid_massage_pepe_simple(duration);

        MassageSessionUtils.setPreferences(mPrefs, 10, MassageRecord.MODE_SIMPLE);

        Intent intent = new Intent();

        Bundle b = new Bundle();
        b.putLong(MassageSessionActivity.PARAMETER_IDMASSAGE, mMassageId);
        intent.putExtras(b);

        mActivityRule.launchActivity(intent);
    }
}
