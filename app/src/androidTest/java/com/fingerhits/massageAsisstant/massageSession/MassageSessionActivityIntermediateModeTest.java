package com.fingerhits.massageAsisstant.massageSession;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.fingerhits.massageAsisstant.DataUtils;
import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.app.Activity.RESULT_OK;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.not;

/**
 * Created by Javier Torón on 08/03/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MassageSessionActivityIntermediateModeTest {
    static Context mContext;
    static MySharedPreferences mPrefs;
    static MassageAssistantDBHelper mDatabaseHelper;

    @Rule
    public IntentsTestRule<MassageSessionActivity> mActivityRule =
            new IntentsTestRule(MassageSessionActivity.class, true, false);

    MassageSessionActivity activity;

    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mPrefs = new MySharedPreferences(mContext);
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);

        PrefsUtils.setPreferences(mPrefs, 5, false, true, 1, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 0);
    }

    @Before
    public void beforeEachTest() {
    }


    @Test
    public void startingIntermediateTest() {
        startIntermediateNewMassage();

        //Returns to MassageSessionActivity with RESULT_OK and mode MODE_SIMPLE
        MassageSessionActivity act = mActivityRule.getActivity();

        assertEquals( RESULT_OK, act.resultCode);
        assertEquals( MassageRecord.MODE_INTERMEDIATE,
                act.resultData.getIntExtra( MassageSessionActivity.PARAMETER_MODE, 0 ));
        assertEquals( TreatedZoneRecord.CODPOSITION_FACEUP,
                act.resultData.getStringExtra( MassageSessionActivity.PARAMETER_CODPOSITION ));
    }


    @Test
    public void newIntermediateMassageInitialStateTest() {
        startIntermediateNewMassage();

        onView(withId(R.id.mute_menu)).check(matches(isDisplayed()));
        onView(withId(R.id.summary_menu)).check(matches(isDisplayed()));
        onView(withId(R.id.options_menu)).check(matches(isDisplayed()));
        onView(withId(R.id.zoneChronoLayout)).check(matches(not(isDisplayed())));
        onView(withId(R.id.positionChronoLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.positionChrono)).check(matches(isDisplayed()));
        onView(withId(R.id.fullChronoLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.fullChrono)).check(matches(not(isDisplayed())));
        onView(withId(R.id.remainingChronoLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.remainingChronoTxt)).check(matches(not(isDisplayed())));
        onView(withId(R.id.pauseMassageBtn)).check(matches(isDisplayed()));
        onView(withId(R.id.endMassageBtn)).check(matches(isDisplayed()));

        onView(withId(R.id.massagingTxt)).check(matches(not(isDisplayed())));

        onView(withId(R.id.sittingBackBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.sittingFrontBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isDisplayed()))
                .check(matches(not(isEnabled())));
        onView(withId(R.id.lyingFaceDownBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));

        onView(withId(R.id.positionLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.fullBodyLayoutId)).check(matches(isDisplayed()));
        onView(withId(R.id.fullBodyMassageBtnId)).check(matches(isDisplayed()));

        onView(withId(R.id.divisionsLayout)).check(matches(not(isDisplayed())));

        onView(withId(R.id.pausedMassageLayout)).check(matches(not(isDisplayed())));

    }

    @Test
    public void clickingPositionsTest() {
        startIntermediateNewMassage();

        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isDisplayed()))
                .check(matches(not(isEnabled())));
        onView(withId(R.id.lyingFaceDownBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.sittingBackBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.sittingFrontBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));


        onView(withId(R.id.lyingFaceDownBtn)).perform(click());

        onView(withId(R.id.lyingFaceDownBtn)).check(matches(not(isEnabled())));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingBackBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingFrontBtn)).check(matches(isEnabled()));

        onView(withId(R.id.lyingFaceUpBtn)).perform(click());

        onView(withId(R.id.lyingFaceDownBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(not(isEnabled())));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingBackBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingFrontBtn)).check(matches(isEnabled()));

        onView(withId(R.id.lyingLeftSideBtn)).perform(click());

        onView(withId(R.id.lyingFaceDownBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(not(isEnabled())));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingBackBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingFrontBtn)).check(matches(isEnabled()));

        onView(withId(R.id.lyingRightSideBtn)).perform(click());

        onView(withId(R.id.lyingFaceDownBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(not(isEnabled())));
        onView(withId(R.id.sittingBackBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingFrontBtn)).check(matches(isEnabled()));

        onView(withId(R.id.sittingBackBtn)).perform(click());

        onView(withId(R.id.lyingFaceDownBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingBackBtn)).check(matches(not(isEnabled())));
        onView(withId(R.id.sittingFrontBtn)).check(matches(isEnabled()));

        onView(withId(R.id.sittingFrontBtn)).perform(click());

        onView(withId(R.id.lyingFaceDownBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingBackBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingFrontBtn)).check(matches(not(isEnabled())));

        onView(withId(R.id.lyingFaceDownBtn)).perform(click());

        onView(withId(R.id.lyingFaceDownBtn)).check(matches(not(isEnabled())));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingBackBtn)).check(matches(isEnabled()));
        onView(withId(R.id.sittingFrontBtn)).check(matches(isEnabled()));
    }


    @Test
    public void clickingChronosTest() {
        startIntermediateNewMassage();

        onView(withId(R.id.remainingChronoLayout)).perform(click());

        onView(withId(R.id.positionChrono)).check(matches(not(isDisplayed())));
        onView(withId(R.id.fullChrono)).check(matches(not(isDisplayed())));
        onView(withId(R.id.remainingChronoTxt)).check(matches(isDisplayed()));

        onView(withId(R.id.fullChronoLayout)).perform(click());

        onView(withId(R.id.positionChrono)).check(matches(not(isDisplayed())));
        onView(withId(R.id.fullChrono)).check(matches(isDisplayed()));
        onView(withId(R.id.remainingChronoTxt)).check(matches(not(isDisplayed())));

        onView(withId(R.id.positionChronoLayout)).perform(click());

        onView(withId(R.id.positionChrono)).check(matches(isDisplayed()));
        onView(withId(R.id.fullChrono)).check(matches(not(isDisplayed())));
        onView(withId(R.id.remainingChronoTxt)).check(matches(not(isDisplayed())));
    }


    @Test
    public void clickingPauseTest() {
        mPrefs.saveBool(MySharedPreferences.PREFS_ANIMATIONS_DISABLED_FOR_TESTING, true);
        startIntermediateNewMassage();

        onView(withId(R.id.pauseMassageBtn)).perform(click());

        onView(withId(R.id.pauseMassageBtn)).check(matches(not(isEnabled())));
        onView(withId(R.id.endMassageBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.sittingBackBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.sittingFrontBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.lyingFaceDownBtn)).check(matches(not(isDisplayed())));

        onView(withId(R.id.pausedMassageLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.pauseChrono)).check(matches(isDisplayed()));
        onView(withId(R.id.resumeMassageBtn)).check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.pausedMassageLayout)).check(matches(not(isDisplayed())));
        onView(withId(R.id.pauseChrono)).check(matches(not(isDisplayed())));
        onView(withId(R.id.resumeMassageBtn)).check(matches(not(isDisplayed())));

        onView(withId(R.id.pauseMassageBtn)).check(matches(isEnabled()));
        onView(withId(R.id.endMassageBtn)).check(matches(isDisplayed()));
        onView(withId(R.id.sittingBackBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.sittingFrontBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.lyingRightSideBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isDisplayed()))
                .check(matches(not(isEnabled())));
        onView(withId(R.id.lyingFaceDownBtn)).check(matches(isDisplayed()))
                .check(matches(isEnabled()));

        mPrefs.saveBool(MySharedPreferences.PREFS_ANIMATIONS_DISABLED_FOR_TESTING, false);
    }


    @Test
    public void savingMassageWith2PositionsAnd1DiscardedTest() {
        mPrefs.saveInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY, 2);
        startIntermediateNewMassage();

        SystemClock.sleep(3000);

        onView(withId(R.id.sittingFrontBtn)).perform(click());
        onView(withId(R.id.sittingFrontBtn)).check(matches(not(isEnabled())));
        onView(withId(R.id.lyingFaceUpBtn)).check(matches(isEnabled()));

        //This has to be discarded
        SystemClock.sleep(1000);

        onView(withId(R.id.lyingLeftSideBtn)).perform(click());
        onView(withId(R.id.sittingFrontBtn)).check(matches(isEnabled()));
        onView(withId(R.id.lyingLeftSideBtn)).check(matches(not(isEnabled())));

        SystemClock.sleep(5000);

        onView(withId(R.id.endMassageBtn)).perform(click());

        MassageRecord massage = mDatabaseHelper.getMassageRecord(
                mDatabaseHelper.getLastInsertedMassageId());

        assertTrue("Massage duration is " + massage.getDuration(),
                massage.getDuration()>= 8);
        assertTrue("Massage duration is " + massage.getDuration(),
                massage.getDuration()<= 9);

        Cursor treatedZones = mDatabaseHelper.getTreatedZonesInMassage(
                massage.getId());

        treatedZones.moveToFirst();

        TreatedZoneRecord tz = new TreatedZoneRecord(treatedZones);

        assertTrue("Treated zone duration is " + tz.getDuration(),
                tz.getDuration()>= 3);
        assertTrue("Treated zone duration is " + tz.getDuration(),
                tz.getDuration()<= 4);
        assertEquals(TreatedZoneRecord.CODPOSITION_FACEUP, tz.getCodPosition());
        assertEquals(Divisions.CODDIVISION_SINGLE, tz.getCodDivision());
        assertEquals(TreatedZoneRecord.CODZONE_FULLBODY, tz.getCodZone());

        treatedZones.moveToNext();

        tz = new TreatedZoneRecord(treatedZones);

        assertEquals(5, tz.getDuration());
        assertEquals(TreatedZoneRecord.CODPOSITION_LEFTSIDE, tz.getCodPosition());
        assertEquals(Divisions.CODDIVISION_SINGLE, tz.getCodDivision());
        assertEquals(TreatedZoneRecord.CODZONE_FULLBODY, tz.getCodZone());

        assertFalse(treatedZones.moveToNext());
        mPrefs.saveInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY, 0);
    }


    @Test
    public void resumeMassageTest() {
        restartIntermediateMassage(50);

        SystemClock.sleep(400);
        onView(withId(R.id.sittingBackBtn)).perform(click());

        SystemClock.sleep(5000);

        onView(withId(R.id.endMassageBtn)).perform(click());


        MassageRecord massage = mDatabaseHelper.getMassageRecord(
                mDatabaseHelper.getLastInsertedMassageId());

        assertTrue("Massage duration is " + massage.getDuration(),
                massage.getDuration() >= 55);
        assertTrue("Massage duration is " + massage.getDuration(),
                massage.getDuration() <= 56);

        Cursor treatedZones = mDatabaseHelper.getTreatedZonesInMassage(
                massage.getId());

        treatedZones.moveToFirst();

        TreatedZoneRecord tz = new TreatedZoneRecord(treatedZones);

        assertEquals(50, tz.getDuration());
        assertEquals(TreatedZoneRecord.CODPOSITION_FACEDOWN, tz.getCodPosition());
        assertEquals(Divisions.CODDIVISION_SINGLE, tz.getCodDivision());
        assertEquals(TreatedZoneRecord.CODZONE_FULLBODY, tz.getCodZone());

        treatedZones.moveToNext();

        tz = new TreatedZoneRecord(treatedZones);

        assertTrue("Treated zone duration is " + tz.getDuration(),
                tz.getDuration() >= 5);
        assertTrue("Treated zone duration is " + tz.getDuration(),
                tz.getDuration() <= 6);
        assertEquals(TreatedZoneRecord.CODPOSITION_SITTINGBACK, tz.getCodPosition());
        assertEquals(Divisions.CODDIVISION_SINGLE, tz.getCodDivision());
        assertEquals(TreatedZoneRecord.CODZONE_FULLBODY, tz.getCodZone());

        assertFalse(treatedZones.moveToNext());
    }



        //    /******* Helper methods *********/
    private void startIntermediateNewMassage() {
        MassageSessionUtils.setPreferences(mPrefs, 5,
                MassageRecord.MODE_INTERMEDIATE);
        mActivityRule.launchActivity(new Intent());

        //Goes automatically to NewMassageActivity
        onView(withId(R.id.lyingFaceUpBtn)).perform(click());
    }

    private void restartIntermediateMassage(long duration) {
        mDatabaseHelper.deleteAllMassages();

        DataUtils dataUtils = new DataUtils(mDatabaseHelper, mContext);

        Long mMassageId = dataUtils.insert_valid_massage_full_jose(duration);

        MassageSessionUtils.setPreferences(mPrefs, 10,
                MassageRecord.MODE_INTERMEDIATE);

        Intent intent = new Intent();

        Bundle b = new Bundle();
        b.putLong(MassageSessionActivity.PARAMETER_IDMASSAGE, mMassageId);
        intent.putExtras(b);

        mActivityRule.launchActivity(intent);
    }


}
