package com.fingerhits.massageAsisstant.massageSession;

import com.fingerhits.massageassistant.options.MySharedPreferences;

/**
 * Created by Javier Torón on 18/03/2017.
 */

public class MassageSessionUtils {
    public static void setPreferences(MySharedPreferences mPrefs,
                                      int duration, int mode) {
        mPrefs.saveInt(MySharedPreferences.PREFS_MASSAGEDURATION_KEY, duration);
        mPrefs.saveInt(MySharedPreferences.PREFS_MODE_KEY, mode);
    }

}
