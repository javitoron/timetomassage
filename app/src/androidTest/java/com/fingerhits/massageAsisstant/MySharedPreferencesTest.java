package com.fingerhits.massageAsisstant;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.test.suitebuilder.annotation.SmallTest;

import android.content.Context;

import com.fingerhits.massageassistant.options.MySharedPreferences;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


@RunWith(AndroidJUnit4.class)
@SmallTest
public class MySharedPreferencesTest {

    public MySharedPreferencesTest(){}

    private MySharedPreferences mPrefs;

    @Before
	public void SetUp() {
        Context mContext = InstrumentationRegistry.getContext();

		mPrefs = new MySharedPreferences(mContext);

		mPrefs.removeValue(mPrefs.PREFS_INT_TEST_KEY);
		mPrefs.removeValue(mPrefs.PREFS_STRING_TEST_KEY);
		mPrefs.removeValue(mPrefs.PREFS_BOOLEAN_TEST_KEY);
	}

    @Test
	public void IntKey() {
		assertEquals(0, mPrefs.getInt(mPrefs.PREFS_INT_TEST_KEY));

		mPrefs.saveInt(mPrefs.PREFS_INT_TEST_KEY, 10);
		assertEquals(10, mPrefs.getInt(mPrefs.PREFS_INT_TEST_KEY));

		mPrefs.removeValue(mPrefs.PREFS_INT_TEST_KEY);
        assertEquals(0, mPrefs.getInt(mPrefs.PREFS_INT_TEST_KEY));
	}

    @Test
	public void StringKey() {

		assertEquals("", mPrefs.getString(mPrefs.PREFS_STRING_TEST_KEY));

		mPrefs.saveString(mPrefs.PREFS_STRING_TEST_KEY, "testing");
		assertEquals("testing", mPrefs.getString(mPrefs.PREFS_STRING_TEST_KEY));

		mPrefs.removeValue(mPrefs.PREFS_STRING_TEST_KEY);
        assertEquals("", mPrefs.getString(mPrefs.PREFS_STRING_TEST_KEY));
	}

    @Test
	public void BooleanKey() {

		assertFalse(mPrefs.getBool(mPrefs.PREFS_BOOLEAN_TEST_KEY));

		mPrefs.saveBool(mPrefs.PREFS_BOOLEAN_TEST_KEY, true);
		assertTrue(mPrefs.getBool(mPrefs.PREFS_BOOLEAN_TEST_KEY));

		mPrefs.removeValue(mPrefs.PREFS_BOOLEAN_TEST_KEY);
        assertFalse(mPrefs.getBool(mPrefs.PREFS_BOOLEAN_TEST_KEY));
	}

}
