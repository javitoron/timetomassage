package com.fingerhits.massageAsisstant.options;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.fingerhits.massageAsisstant.EspressoHelpers;
import com.fingerhits.massageAsisstant.PrefsUtils;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.options.OptionsActivity;
import com.fingerhits.massageassistant.options.OptionsInteractor;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringContains.containsString;

/**
 * Created by Javier Torón on 24/05/2016.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class OptionsActivityTest {

    static Context mContext;
    static MySharedPreferences mPrefs;

    @Rule
    public IntentsTestRule<OptionsActivity> mActivityRule =
            new IntentsTestRule(OptionsActivity.class, true, false);

    OptionsActivity activity;

    @BeforeClass
    public static void setUpTest() {

        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mPrefs = new MySharedPreferences(mContext);
    }

    @Before
    public void beforeEachTest() {
    }

    @Test
    public void allMinDisabledInitTest() {
        PrefsUtils.setPreferences(mPrefs, 5, false, false, 0, false, false,
                false, false, false, 1, 0, 0, 0);
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.massageDurationLabel))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("Sessions are 5 minutes long"))));

        onView(withId(R.id.massageDurationSeekBar))
                .check(matches(isDisplayed()))
                .check(matches(EspressoHelpers.withProgress(0)));

        onView(withId(R.id.disableScreenLockCheck))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

         onView(withId(R.id.dingTimeWarningsCheck))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.dingTimeWarningsText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("Every"))));

        onView(withId(R.id.dingTimeSeekBar))
                .check(matches(not( isDisplayed())));

        onView(withId(R.id.halfDurationWarningText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("In the middle: 02:30"))));

        onView(withId(R.id.halfDurationWarningCheck))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.fullDurationWarningText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("At the end: 05:00"))));

        onView(withId(R.id.fullDurationWarningCheck))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.visualWarningsCheck))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())))
                .perform(scrollTo());
        onView(withId(R.id.vibrationWarningsCheck))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));
        onView(withId(R.id.soundWarningsCheck))
                .check(matches(isDisplayed()))
                .check(matches(not(isChecked())));

        onView(withId(R.id.soundWarningsLayout))
                .check(matches(not( isDisplayed())));

        onView(withId(R.id.minimalZoneDurationSeekBar))
                .perform(scrollTo())
                .check(matches(isDisplayed()))
                .check(matches(EspressoHelpers.withProgress(0)));

        onView(withId(R.id.minimalZoneDurationLabel))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("No discarded zones"))));
    }


    @Test
    public void allMaxEnabledInitTest() {
        PrefsUtils.setPreferences(mPrefs, 120, true, true, 30, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 300);
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.massageDurationLabel))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("Sessions are 120 minutes long"))));

        onView(withId(R.id.massageDurationSeekBar))
                .check(matches(isDisplayed()))
                .check(matches(EspressoHelpers.withProgress(23)));

        onView(withId(R.id.disableScreenLockCheck))
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));

        onView(withId(R.id.dingTimeWarningsCheck))
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));

        onView(withId(R.id.dingTimeWarningsText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("Every 30 minutes"))));

        onView(withId(R.id.dingTimeSeekBar))
                .check(matches(isDisplayed()))
                .check(matches(EspressoHelpers.withProgress(29)));

        onView(withId(R.id.halfDurationWarningText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("In the middle: 01:00:00"))));

        onView(withId(R.id.halfDurationWarningCheck))
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));

        onView(withId(R.id.fullDurationWarningText))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString("At the end: 02:00:00"))));

        onView(withId(R.id.fullDurationWarningCheck))
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));

        onView(withId(R.id.visualWarningsCheck)).perform(scrollTo())
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));
        onView(withId(R.id.vibrationWarningsCheck)).perform(scrollTo())
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));
        onView(withId(R.id.soundWarningsCheck)).perform(scrollTo())
                .check(matches(isDisplayed()))
                .check(matches(isChecked()));

        onView(withId(R.id.soundWarningsLayout)).perform(scrollTo())
                .check(matches(isDisplayed()));


        onView(withId(R.id.dingTimeSoundText))
                .check(matches(isDisplayed()));
        onView(withId(R.id.dingTimeSoundLayout))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.halfDurationSoundText))
                .check(matches(isDisplayed()));
        onView(withId(R.id.halfDurationSoundSettingsLayout))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.fullDurationSoundText))
                .perform(scrollTo())
                .check(matches(isDisplayed()));
        onView(withId(R.id.fullDurationSoundSettingsLayout))
                .check(matches(not(isDisplayed())));

        onView(withId(R.id.minimalZoneDurationSeekBar))
                .perform(scrollTo())
                .check(matches(isDisplayed()))
                .check(matches(EspressoHelpers.withProgress(60)));

        onView(withId(R.id.minimalZoneDurationLabel))
                .check(matches(isDisplayed()))
                .check(matches(withText(containsString(
                        "Discard zones shorter than 300 seconds"))));
    }


    @Test
    public void fromMinToMaxTest() {
        //Check layouts from hide to show and values saved
        PrefsUtils.setPreferences(mPrefs, 5, false, false, 0, false, false,
                false, false, false, 1, 0, 0, 0);
        mActivityRule.launchActivity(new Intent());

        OptionsInteractor interactor = new OptionsInteractor();
        interactor.mPrefs = mPrefs;


        onView(withId(R.id.massageDurationSeekBar))
                .perform(EspressoHelpers.setProgress(20));

        onView(withId(R.id.massageDurationLabel))
                .check(matches(withText(containsString(
                        "Sessions are 105 minutes long"))));
        assertEquals(105, interactor.massageDuration() );



        onView(withId(R.id.disableScreenLockCheck))
                .perform(click());

        assertEquals(true, interactor.disableScreenLock() );



        onView(withId(R.id.dingTimeWarningsCheck))
                .perform(click());

        assertEquals(true, interactor.dingTimeWarningsEnabled() );
        onView(withId(R.id.dingTimeSeekBar))
                .check(matches(isDisplayed()))
                .perform(EspressoHelpers.setProgress(15));
        assertEquals(16, interactor.dingTime() );



        onView(withId(R.id.halfDurationWarningCheck))
                .perform(click());

        assertEquals(true, interactor.halfMassageDurationWarningEnabled() );



        onView(withId(R.id.fullDurationWarningCheck))
                .perform(click());

        assertEquals(true, interactor.fullMassageDurationWarningEnabled() );



        onView(withId(R.id.visualWarningsCheck))
                .perform(scrollTo())
                .perform(click());

        assertEquals(true, interactor.visualWarningsEnabled() );



        onView(withId(R.id.vibrationWarningsCheck)).perform(scrollTo())
                .perform(click());

        assertEquals(true, interactor.vibrationWarningsEnabled() );



        onView(withId(R.id.soundWarningsCheck))
                .perform(scrollTo())
                .perform(click());

        assertEquals(true, interactor.soundWarningsEnabled() );
        onView(withId(R.id.soundWarningsLayout)).perform(scrollTo())
                .check(matches(isDisplayed()));

        onView(withId(R.id.fullDurationSoundText))
                .perform(scrollTo())
                .check(matches(isDisplayed()));
        onView(withId(R.id.dingTimeSoundText))
                .check(matches(isDisplayed()));
        onView(withId(R.id.halfDurationSoundText))
                .check(matches(isDisplayed()));


        onView(withId(R.id.minimalZoneDurationSeekBar))
                .perform(scrollTo())
                .perform(EspressoHelpers.setProgress(20));

        onView(withId(R.id.minimalZoneDurationLabel))
                .check(matches(withText(containsString(
                        "Discard zones shorter than 100 seconds"))));
        assertEquals(100, interactor.minimalZoneDuration() );

    }


    @Test
    public void fromMaxToMinTest() {
        //Check layouts from show to hide and values saved
        PrefsUtils.setPreferences(mPrefs, 120, true, true, 30, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 300);

        mActivityRule.launchActivity(new Intent());

        OptionsInteractor interactor = new OptionsInteractor();
        interactor.mPrefs = mPrefs;


        onView(withId(R.id.massageDurationSeekBar))
                .perform(EspressoHelpers.setProgress(0));

        onView(withId(R.id.massageDurationLabel))
                .check(matches(withText(containsString(
                        "Sessions are 5 minutes long"))));
        assertEquals(5, interactor.massageDuration() );



        onView(withId(R.id.disableScreenLockCheck))
                .perform(click());

        assertEquals(false, interactor.disableScreenLock() );



        onView(withId(R.id.dingTimeWarningsCheck))
                .perform(click());

        assertEquals(false, interactor.dingTimeWarningsEnabled() );
        onView(withId(R.id.dingTimeSeekBar))
                .check(matches(not(isDisplayed())));



        onView(withId(R.id.halfDurationWarningCheck))
                .perform(click());

        assertEquals(false, interactor.halfMassageDurationWarningEnabled() );



        onView(withId(R.id.fullDurationWarningCheck))
                .perform(click());

        assertEquals(false, interactor.fullMassageDurationWarningEnabled() );



        onView(withId(R.id.visualWarningsCheck))
                .perform(scrollTo())
                .perform(click());

        assertEquals(false, interactor.visualWarningsEnabled() );



        onView(withId(R.id.vibrationWarningsCheck))
                .perform(click());

        assertEquals(false, interactor.vibrationWarningsEnabled() );



        onView(withId(R.id.soundWarningsCheck))
                .perform(scrollTo())
                .perform(click());

        assertEquals(false, interactor.soundWarningsEnabled() );
        onView(withId(R.id.soundWarningsLayout))
                .check(matches(not(isDisplayed())));



        onView(withId(R.id.minimalZoneDurationSeekBar))
                .perform(scrollTo())
                .perform(EspressoHelpers.setProgress(0));

        onView(withId(R.id.minimalZoneDurationLabel))
                .check(matches(withText(containsString(
                        "No discarded zones"))));
        assertEquals(0, interactor.minimalZoneDuration() );

    }


    @Test
    public void changeSoundsTest() {
        //Check layouts from show to hide and values saved
        PrefsUtils.setPreferences(mPrefs, 120, true, true, 30, true, true, true,
                true, true, R.raw.harp_double, R.raw.bell_high,
                R.raw.birds_park, 300);

        mActivityRule.launchActivity(new Intent());

        OptionsInteractor interactor = new OptionsInteractor();
        interactor.mPrefs = mPrefs;

        onView(withId(R.id.minimalZoneDurationSeekBar))
                .perform(scrollTo());

        onView(withId(R.id.dingTimeSoundText))
                .perform(click());

        onView(withId(R.id.dingTimeSoundLayout))
                .check(matches(isDisplayed()));
        onView(withId(R.id.playDingTimeBtn))
                .check(matches(isDisplayed()));
        onView(withId(R.id.dingTimeSoundSelectSpinner))
                .check(matches(isDisplayed()));

        onView(withId(R.id.dingTimeSoundSelectSpinner))
                .perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Horn deep"))).perform(click());

        assertEquals(R.raw.horn_deep, interactor.dingTimeSound() );



        onView(withId(R.id.minimalZoneDurationSeekBar))
                .perform(scrollTo());

        onView(withId(R.id.halfDurationSoundText))
                .perform(click());

        onView(withId(R.id.halfDurationSoundSettingsLayout))
                .check(matches(isDisplayed()));
        onView(withId(R.id.playHalfDurationBtn))
                .check(matches(isDisplayed()));
        onView(withId(R.id.halfDurationSoundSelectSpinner))
                .check(matches(isDisplayed()));

        onView(withId(R.id.halfDurationSoundSelectSpinner))
                .perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Bird quiet"))).perform(click());

        assertEquals(R.raw.bird_quiet, interactor.halfMassageSound() );



        onView(withId(R.id.minimalZoneDurationSeekBar))
                .perform(scrollTo());

        onView(withId(R.id.fullDurationSoundText))
                .perform(click());

        onView(withId(R.id.fullDurationSoundSettingsLayout))
                .check(matches(isDisplayed()));
        onView(withId(R.id.playFullDurationBtn))
                .check(matches(isDisplayed()));
        onView(withId(R.id.fullDurationSoundSelectSpinner))
                .check(matches(isDisplayed()));

        onView(withId(R.id.fullDurationSoundSelectSpinner))
                .perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Flute long"))).perform(click());

        assertEquals(R.raw.flute_long, interactor.fullMassageSound() );

    }

    //    /******* Helper methods *********/
}