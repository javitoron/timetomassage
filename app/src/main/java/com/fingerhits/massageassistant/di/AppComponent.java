package com.fingerhits.massageassistant.di;

import android.app.Application;

import com.fingerhits.massageassistant.exportImport.ExportImportActivityComponent;
import com.fingerhits.massageassistant.exportImport.ExportImportActivityModule;
import com.fingerhits.massageassistant.main.MainActivityComponent;
import com.fingerhits.massageassistant.main.MainActivityModule;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivityComponent;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivityModule;
import com.fingerhits.massageassistant.myMassages.MyMassagesActivityComponent;
import com.fingerhits.massageassistant.myMassages.MyMassagesActivityModule;
import com.fingerhits.massageassistant.newMassage.NewMassageActivityComponent;
import com.fingerhits.massageassistant.newMassage.NewMassageActivityModule;
import com.fingerhits.massageassistant.options.OptionsActivityComponent;
import com.fingerhits.massageassistant.options.OptionsActivityModule;
import com.fingerhits.massageassistant.tips.TipsActivityComponent;
import com.fingerhits.massageassistant.tips.TipsActivityModule;
import com.fingerhits.massageassistant.treatedZones.TreatedZonesActivityComponent;
import com.fingerhits.massageassistant.treatedZones.TreatedZonesActivityModule;

import javax.inject.Singleton;

import dagger.Component;
/**
 * Created by Javier Torón on 18/02/2017.
 */

@Singleton
@Component(modules = {AppModule.class, CommonModule.class})
public interface AppComponent {

    MainActivityComponent plus(MainActivityModule module);
    TipsActivityComponent plus(TipsActivityModule module);
    OptionsActivityComponent plus(OptionsActivityModule module);
    ExportImportActivityComponent plus(ExportImportActivityModule module);
    MyMassagesActivityComponent plus(MyMassagesActivityModule module);
    TreatedZonesActivityComponent plus(TreatedZonesActivityModule module);
    NewMassageActivityComponent plus(NewMassageActivityModule module);
    MassageSessionActivityComponent plus(MassageSessionActivityModule module);
    Application application();
}