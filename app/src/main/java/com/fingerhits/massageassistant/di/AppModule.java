package com.fingerhits.massageassistant.di;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.fingerhits.massageassistant.massageSounds.MassageSounds;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Javier Torón on 17/02/2017.
 */
@Module
public class AppModule {

    static MainApp app;

    public AppModule(MainApp application) {
        app = application;
    }


    @Provides
    @Singleton
    public static Resources provideResources() {
        return app.getResources();
    }

    @Provides
    public static Context provideContext() {
        return app.getApplicationContext();
    }

    @Provides
    public static Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton
    public static MySharedPreferences provideMySharedPreferences() {
        return new MySharedPreferences(provideContext());
    }

    @Provides
    @Singleton
    public static MassageAssistantDBHelper provideMassageAssistantDBHelper(){
        return new MassageAssistantDBHelper(app.getApplicationContext());
    }


    @Provides
    @Singleton
    public static MassageSounds provideMassageSounds() {
        return MassageSounds.getInstance(app.getApplicationContext(),
                provideMySharedPreferences());
    }


}
