package com.fingerhits.massageassistant.di;

import android.app.Application;
import android.content.Context;

import com.fingerhits.massageassistant.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Javier Torón on 16/02/2017.
 */

public class MainApp extends Application {
    private static AppComponent appComponent;
    private static MainApp instance;

    private Tracker mTracker;

    @Override
    public void onCreate(){
        super.onCreate();
        instance = this;
        initAppComponents();
    }

    public static MainApp get(Context context) {
        return (MainApp) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }


    public void initAppComponents() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }


    /**
     * Visible only for testing purposes.
     */
    public void setTestComponent(AppComponent testingComponent) {
        appComponent = testingComponent;
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }
}
