package com.fingerhits.massageassistant.massagesession.chronos;

import android.content.Context;
import android.os.Vibrator;

import com.fingerhits.massageassistant.massageSounds.MassageSounds;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.utils.GraphicUtils;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 17/03/2017.
 */

public class Warnings {
    private static Warnings instance;

    @Inject MassageSessionActivity mActivity;
    @Inject MassageSounds mSounds;
    @Inject MySharedPreferences mPrefs;

    private Vibrator mVibrator;
    private long[] mTwoVibrationPattern = {0, 500, 500, 500 };
    private long[] mThreeVibrationPattern = {0, 500, 500, 500, 500, 500 };

    private boolean mSomeWarningEnabled;
    private long mMassageDurationInSeconds;
    private int mDingTimeInSeconds;
    private boolean mDingTimeWarningsEnabled;
    private boolean mHalfMassageWarningEnabled;
    private boolean mFullMassageWarningEnabled;
    private boolean mVisualWarningEnabled;
    private boolean mVibrationWarningEnabled;
    private boolean mSoundWarningsEnabled;


    @Inject
    protected Warnings(MassageSessionActivity activity, MassageSounds sounds,
                       MySharedPreferences prefs) {

        mActivity = activity;
        mSounds = sounds;
        mPrefs = prefs;

        mVibrator = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);

        reloadPrefs();
    }


    //For testing purposes: to inject mock objects in a singleton like this
    public void setDependencies(MassageSessionActivity activity,
                                MassageSounds sounds,
                                MySharedPreferences prefs, Vibrator vibrator) {

        mActivity = activity;
        mSounds = sounds;
        mPrefs = prefs;
        mVibrator = vibrator;

    }

    public static Warnings getInstance(MassageSessionActivity activity,
                                       MassageSounds sounds,
                                       MySharedPreferences prefs){
        if(instance == null){
            instance = new Warnings(activity, sounds, prefs);
        }
        return instance;
    }


    public void tick(long elapsedTimeInSeconds){
        boolean warningTriggered = false;
        if( getSomeWarningEnabled() ) {

            if (elapsedTimeInSeconds == mMassageDurationInSeconds) {
                if (mFullMassageWarningEnabled) {
                    warningTriggered = true;
                    if (mVisualWarningEnabled) {
                        blinkScreen3();
                    }

                    if (mSoundWarningsEnabled) {
                        playFullMassage();
                    }

                    if (mVibrationWarningEnabled) {
                        vibrate3();
                    }
                }
            }

            if (mHalfMassageWarningEnabled && !warningTriggered ) {
                long mHalfMassageDurationInSeconds =
                        mMassageDurationInSeconds / 2;
                if (elapsedTimeInSeconds == mHalfMassageDurationInSeconds) {
                    warningTriggered = true;
                    if (mVisualWarningEnabled) {
                        blinkScreen2();
                    }

                    if (mSoundWarningsEnabled) {
                        playHalfMassage();
                    }

                    if (mVibrationWarningEnabled) {
                        vibrate2();
                    }
                }
            }
            if(mDingTimeWarningsEnabled && elapsedTimeInSeconds != 0L
                    && !warningTriggered ) {
                if (elapsedTimeInSeconds % mDingTimeInSeconds == 0) {
                    if (mVisualWarningEnabled) {
                        blinkScreen1();
                    }

                    if (mSoundWarningsEnabled) {
                        playDingTime();
                    }

                    if (mVibrationWarningEnabled) {
                        vibrate1();
                    }
                }
            }
        }

    }


    public void reloadPrefs(){
        mMassageDurationInSeconds = getMassageDurationInSeconds();
        mDingTimeInSeconds = getDingTimeInSeconds() ;
        mDingTimeWarningsEnabled = getDingTimeWarningsEnabled();
        mHalfMassageWarningEnabled = getHalfMassageWarningEnabled();
        mFullMassageWarningEnabled = getFullMassageWarningEnabled();
        mVisualWarningEnabled = getVisualWarningEnabled();
        mVibrationWarningEnabled = getVibrationWarningEnabled();
        mSoundWarningsEnabled = getSoundWarningsEnabled();

        mSomeWarningEnabled = getSomeWarningEnabled();
    }



    public boolean getSomeWarningEnabled(){
        return ( ( mDingTimeWarningsEnabled
                || mHalfMassageWarningEnabled
                || mFullMassageWarningEnabled )
                && (mVisualWarningEnabled
                || mVibrationWarningEnabled
                || mSoundWarningsEnabled) );
    }


    public long getMassageDurationInSeconds(){
        return mPrefs.getInt(
                MySharedPreferences.PREFS_MASSAGEDURATION_KEY) * 60;
    }
    public int getDingTimeInSeconds() {
        return mPrefs.getInt(
                MySharedPreferences.PREFS_DINGTIME_KEY ) * 60;
    }

    public boolean getDingTimeWarningsEnabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_DINGTIMEWARNINGSENABLED_KEY);
    }

    public boolean getHalfMassageWarningEnabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_HALFMASSAGEDURATIONWARNINGENABLED_KEY);
    }

    public boolean getFullMassageWarningEnabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_FULLMASSAGEDURATIONWARNINGENABLED_KEY);
    }

    public boolean getVisualWarningEnabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_VISUALWARNINGSENABLED_KEY);
    }

    public boolean getVibrationWarningEnabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_VIBRATIONWARNINGSENABLED_KEY);
    }

    public boolean getSoundWarningsEnabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_SOUNDWARNINGSENABLED_KEY);
    }


    //*******************
    // PRIVATE METHODS
    //*******************
    private void blinkScreen1(){
        blinkScreen(1);
    }
    private void blinkScreen2(){
        blinkScreen(2);
    }
    private void blinkScreen3(){
        blinkScreen(3);
    }

    private void vibrate1(){
        mVibrator.vibrate(500L);
    }
    private void vibrate2(){
        mVibrator.vibrate(mTwoVibrationPattern, -1);
    }
    private void vibrate3(){
        mVibrator.vibrate(mThreeVibrationPattern, -1);
    }

    private void playDingTime() {
        mSounds.playDingTime();
    }
    private void playHalfMassage() {
        mSounds.playHalfMassage();
    }
    private void playFullMassage() {
        mSounds.playFullMassage();
    }


    private void blinkScreen(int timesToBlink) {
/*        if(!GraphicUtils.isScreenOn(mActivity)){

        }*/
        GraphicUtils.blinkScreen(mActivity.currentOpLayout, timesToBlink);
    }

}