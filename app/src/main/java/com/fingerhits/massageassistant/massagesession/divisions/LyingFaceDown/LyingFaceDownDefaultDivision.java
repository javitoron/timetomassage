package com.fingerhits.massageassistant.massagesession.divisions.LyingFaceDown;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * 
 * Controls LyingFaceDown Default Division
 * 
 * @author Javier Torón
 * 
 * It has buttons for HeadNeck, Back, ArmLeft, ArmRight, LegLeft and LegRight.
 */
public class LyingFaceDownDefaultDivision extends LyingFaceDownDivisions {

	private Button mHeadNeck, mBack, mArmLeft, mArmRight, mLegLeft,
		mLegRight;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingFaceDownDefaultDivision(MassageSessionActivity act,
                                        LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_DEFAULT;
	}

    /*Static methods*/
    public static String[] getCodZonesArray(){
        return new String[] {TreatedZoneRecord.CODZONE_ARMLEFT,
                TreatedZoneRecord.CODZONE_ARMRIGHT,
                TreatedZoneRecord.CODZONE_BACK,
                TreatedZoneRecord.CODZONE_HEADNECK,
                TreatedZoneRecord.CODZONE_LEGLEFT,
                TreatedZoneRecord.CODZONE_LEGRIGHT};
    }

    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones( View v ){
        super.showFixLayout((LinearLayout) v, R.id.upperFixLayoutId, 1);
        showHeadNeckZones((LinearLayout) v);
        showArmsBackZones((LinearLayout) v);
        showLegsZones((LinearLayout) v);
    }


    public Divisions prevDivision(){
        clearZones();

        LyingFaceDownDivisions newDivision =
                new LyingFaceDownUpperLowerDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    public Divisions nextDivision(){

        clearZones();

        LyingFaceDownDivisions newDivision =
                new LyingFaceDownDetailedDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }

    /* OVERRIDES */
    @Override
    public String zoneTitle( Button btn ){
        if (btn == mHeadNeck ){
            return res.getString(R.string.headNeck_button);
        }
        if (btn == mBack ){
            return res.getString(R.string.back_button);
        }
        if (btn == mArmLeft ){
            return res.getString(R.string.armLeft_button);
        }
        if (btn == mArmRight ){
            return res.getString(R.string.armRight_button);
        }
        if (btn == mLegLeft ){
            return res.getString(R.string.legLeft_button);
        }
        if (btn == mLegRight ){
            return res.getString(R.string.legRight_button);
        }

        return super.zoneTitle(btn);
    }


    @Override
    public String codZone( Button btn ){
        if (btn == mHeadNeck ){
            return TreatedZoneRecord.CODZONE_HEADNECK;
        }
        if (btn == mBack ){
            return TreatedZoneRecord.CODZONE_BACK;
        }
        if (btn == mArmLeft ){
            return TreatedZoneRecord.CODZONE_ARMLEFT;
        }
        if (btn == mArmRight ){
            return TreatedZoneRecord.CODZONE_ARMRIGHT;
        }
        if (btn == mLegLeft ){
            return TreatedZoneRecord.CODZONE_LEGLEFT;
        }
        if (btn == mLegRight ){
            return TreatedZoneRecord.CODZONE_LEGRIGHT;
        }

        return super.codZone(btn);
    }


    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_HEADNECK ) ){
            return mHeadNeck;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_BACK ) ){
            return mBack;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_ARMLEFT ) ){
            return mArmLeft;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_ARMRIGHT ) ){
            return mArmRight;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEGLEFT ) ){
            return mLegLeft;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEGRIGHT ) ){
            return mLegRight;
        }

        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mHeadNeck, enabled);
        ZonesUtils.enableButton(res, mBack, enabled);
        ZonesUtils.enableButton(res, mArmLeft, enabled);
        ZonesUtils.enableButton(res, mArmRight, enabled);
        ZonesUtils.enableButton(res, mLegLeft, enabled);
        ZonesUtils.enableButton(res, mLegRight, enabled);
	}

    /* PRIVATES */
    /**
     * Shows buttons in HeadNeckLayout
     */
    private void showHeadNeckZones( LinearLayout mainLayout ){
        mHeadNeck = super.setHeadNeckZones(mainLayout, 9);
    }

    /**
     * Shows buttons in ArmsBackLayout
     */
    private void showArmsBackZones( LinearLayout mainLayout ){
        LinearLayout.LayoutParams armsBackLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 14);
        armsBackLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout armsBackLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        armsBackLayout.setId(R.id.armsBackLayoutId);

        super.showFixViewWeight(armsBackLayout, 1);

        mArmLeft = setZoneBtn( R.id.armLeftMassageBtnId, armsBackLayout,
                2,R.string.armLeft_button );

        mBack = setZoneBtn( R.id.backMassageBtnId, armsBackLayout,
                2,R.string.back_button );

        mArmRight = setZoneBtn( R.id.armRightMassageBtnId, armsBackLayout,
                2,R.string.armRight_button );

        super.showFixViewWeight(armsBackLayout, 1);

        mainLayout.addView(armsBackLayout, armsBackLayoutParameters);

    }

    /**
     * Shows buttons in LegsLayout
     */
    private void showLegsZones( LinearLayout mainLayout ){
        LinearLayout legsLayout = super.setLegsLayout();

        super.showFixViewWeight(legsLayout, 2);

        mLegLeft = setZoneBtn( R.id.legLeftMassageBtnId, legsLayout,
                3,R.string.legLeft_button );

        mLegRight = setZoneBtn( R.id.legRightMassageBtnId, legsLayout,
                3,R.string.legRight_button );

        super.showFixViewWeight(legsLayout, 2);

        mainLayout.addView(legsLayout);
    }

}
