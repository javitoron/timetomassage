package com.fingerhits.massageassistant.massagesession;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.fingerhits.massageassistant.massagesession.chronos.MassageSessionChronosFragment;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.divisions.MassageSessionDivisionSelector;
import com.fingerhits.massageassistant.massagesession.pauseResume.PauseResumeController;
import com.fingerhits.massageassistant.massagesession.positions.MassageSessionPositionSelector;
import com.fingerhits.massageassistant.massagesession.positions.Position;
import com.fingerhits.massageassistant.massagesession.utils.SquareRelativeLayout;
import com.fingerhits.massageassistant.newMassage.NewMassageActivity;
import com.fingerhits.massageassistant.options.OptionsActivity;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.treatedZones.TreatedZonesActivity;
import com.fingerhits.massageassistant.utils.GraphicUtils;
import com.fingerhits.massageassistant.utils.MiscUtils;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 
 * Controls the total duration and zone by zone duration for the massage session
 * 
 * @author Javier Torón
 * 
 * It contains a body zone containerLayout, a list of durations and buttons to pause
 * and stop (quit, always enabled)
 * 
 * Can be in 4 states:
 * * NotStarted: Only when starting activity while preparing the screen
 *
 * * WaitingToSelectZone: Disable pauseBtn. Only on advanced mode:
 * * * When changed to a mDivision != SINGLE
 * * * When changed to a mPosition which returned a mDivision != SINGLE
 * * * When loaded a massage and start in a mDivision != SINGLE
 *
 * * Running: Body zone containerLayout enabled, pause button enabled,
 * *	chronometers running, pausedText not visible
 *
 * * Paused: Body zone containerLayout disabled, pause button (resume button) enabled,
 * *	chronometers paused, pausedText visible and blinking
 */
public class MassageSessionActivity extends Activity implements IMassageSessionView{

    /*Constants*/
    private static final int OPTIONS_REQUEST_CODE = 1;
    private static final int NEW_MASSAGE_REQUEST_CODE = 2;
    private static final int TREATED_ZONES_REQUEST_CODE = 3;

    public static final String PARAMETER_IDMASSAGE = "idMassage";
    public static final String PARAMETER_CODPOSITION = "codPosition";
    public static final String PARAMETER_MODE = "mode";

    /*Public variables*/
    @Inject public IMassageSessionPresenter presenter;
    @Inject public Context context;
    @Inject public Resources res;

    PowerManager.WakeLock wl;

    /*Private variables*/
    /*Components*/
    private PauseResumeController mPauseResume;
    private MassageSessionPositionSelector mPositionSelector;
    private MassageSessionDivisionSelector mDivisionSelector;

    private Menu mMenu;

    /*Working mode*/
    public int resultCode;
    public Intent resultData;


    /*Views*/
    @BindView(R.id.positionLayout) public LinearLayout positionLayout;
    @BindView(R.id.currentOpLayout) public LinearLayout currentOpLayout;
    @BindView(R.id.divisionsLayout) LinearLayout mDivisionsLayout;
    @BindView(R.id.positionContainerLayout)
    RelativeLayout mPositionContainerLayout;

    @BindView(R.id.massagingTxt) TextView mMassagingTxt;
    @BindView(R.id.endMassageBtn) ImageButton mEndMassageBtn;

    @BindView(R.id.pausedMassageLayout) SquareRelativeLayout mPausedMassageLayout;
    @BindView(R.id.pauseMassageBtn) ImageButton mPauseMassageBtn;
    @BindView(R.id.resumeMassageBtn) ImageButton mResumeMassageBtn;
    @BindView(R.id.pauseChrono) Chronometer mPauseChronometer;


    @BindString(R.string.massageSessionActivity_resume_massage_error_toast)
    String massageSessionActivity_resume_massage_error_toast;
    @BindString(R.string.massageSessionActivity_end_no_save_msg)
    String massageSessionActivity_end_no_save_msg;
    @BindString(R.string.massageSessionActivity_end_save_msg)
    String massageSessionActivity_end_save_msg;
    @BindString(R.string.fullBody_button) String fullBody_button_label;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_massage_session);

        ButterKnife.bind(this);

        MassageSessionChronosFragment chronosFragment =
                (MassageSessionChronosFragment)
                        getFragmentManager()
                                .findFragmentById(R.id.fragment_chronos);

        MainApp.get(this)
                .getAppComponent()
                .plus(new MassageSessionActivityModule(this, chronosFragment))
                .inject(this);


        mPauseResume = new PauseResumeController(
                mPausedMassageLayout, mPauseMassageBtn, mResumeMassageBtn,
                mPauseChronometer);
        mPositionSelector = new MassageSessionPositionSelector(this);
        mDivisionSelector = new MassageSessionDivisionSelector(this);

        Bundle b = getIntent().getExtras();

        presenter.initView();

        if (b == null) {
            gotoNewMassageActivity();
        } else {
            presenter.restartMassage(b.getLong(PARAMETER_IDMASSAGE));
        }

        MiscUtils.prepareActionBar(this);
        MiscUtils.logActivityStarting(this, NAME);
    }


    /* ****** */
	/* EVENTS */
	/* ****** */
    /**
     * Manages return from optionsActivity
     *
     * @param requestCode To identify activity is coming from
     * @param resultCode  OK or error
     * @param data        Returned from activity
     */
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        /*For testing purposes*/
        this.resultCode = resultCode;
        resultData = data;
        if (requestCode == NEW_MASSAGE_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                presenter.startMassage(
                        data.getIntExtra(PARAMETER_MODE,
                                /*default value, in case there's not PARAMETER_MODE value*/
                                MassageRecord.MODE_SIMPLE),
                        data.getStringExtra(PARAMETER_CODPOSITION));
            }

            if (resultCode == RESULT_CANCELED) {
                finish();
            }

        }

        if (requestCode == OPTIONS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                presenter.optionsChanged();
            }
        }

        if (requestCode == TREATED_ZONES_REQUEST_CODE) {
            presenter.resumeIfPaused();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(wl != null ) wl.release();
    }

    /**
     * Call endMassage before exit
     */
    @Override
    public void onBackPressed() {
        endMassage();
    }


    /**
     * Create menu options (options activity)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_massage_session_actions, menu);

        mMenu = menu;
        presenter.setUpMenu();
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Handle presses on the action bar items (options or home/back)
     *
     * @param item Action bar item pressed
     * @return True if menu processing was successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.options_menu:
                presenter.goToOptions();

                return true;

            case R.id.summary_menu:
                presenter.showMassageDetails();
                return true;

            case R.id.mute_menu:
                presenter.muteClick();
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // **************
	// ONCLICK VIEW *
	// **************

    /**
     * Pause massage session.
     * <p/>
     * Changes pauseMassageBtn image and function call.
     * Pause chronometers and disable body zone buttons.
     * Shows a "Paused massage" blinking animation
     */
    @OnClick( R.id.pauseMassageBtn)
    public void pauseMassage() {
        presenter.pauseMassage();
    }


    /**
     * Resume massage session.
     * <p/>
     * Changes pauseMassageBtn image and function call.
     * Resume chronometers and enable body zone buttons (except last pressed).
     * Stops "Paused massage" blinking animation and hides text
     */
    @OnClick(R.id.resumeMassageBtn)
    public void resumeMassage() {
        presenter.resumeMassage();
    }


    /**
     * End massage session. Quit screen and return to MainActivity
     */
    @OnClick(R.id.endMassageBtn)
    public void endMassage() {
        presenter.endMassage();

        Intent intent = getIntent();

        this.setResult(RESULT_OK, intent);

        finish();
    }


    @OnClick({R.id.lyingFaceDownBtn, R.id.lyingFaceUpBtn,
            R.id.lyingRightSideBtn, R.id.lyingLeftSideBtn, R.id.sittingBackBtn,
            R.id.sittingFrontBtn})
    public void selectNewPosition(View view) {
        mPositionSelector.enableAll();
        GraphicUtils.disableSelectionBtn(view);

        switch (view.getId()) {
            case R.id.lyingFaceDownBtn:
                presenter.changePosition(TreatedZoneRecord.CODPOSITION_FACEDOWN);
                break;

            case R.id.lyingFaceUpBtn:
                presenter.changePosition(TreatedZoneRecord.CODPOSITION_FACEUP);
                break;

            case R.id.lyingRightSideBtn:
                presenter.changePosition(TreatedZoneRecord.CODPOSITION_RIGHTSIDE);
                break;

            case R.id.lyingLeftSideBtn:
                presenter.changePosition(TreatedZoneRecord.CODPOSITION_LEFTSIDE);
                break;

            case R.id.sittingBackBtn:
                presenter.changePosition(TreatedZoneRecord.CODPOSITION_SITTINGBACK);
                break;

            case R.id.sittingFrontBtn:
                presenter.changePosition(TreatedZoneRecord.CODPOSITION_SITTINGFRONT);
                break;

            default:
                presenter.changePosition("");
                break;
        }

    }

    @OnClick({R.id.divisionSingleBtn, R.id.divisionLeftRightBtn,
            R.id.divisionUpperLowerBtn, R.id.divisionDefaultBtn,
            R.id.divisionDetailedBtn})
    public void selectNewDivision(View view) {
        String currentCodDivision = "";

        switch (view.getId()) {
            case R.id.divisionSingleBtn:
                currentCodDivision = Divisions.CODDIVISION_SINGLE;

                break;

            case R.id.divisionLeftRightBtn:
                currentCodDivision = Divisions.CODDIVISION_LEFTRIGHT;
                break;

            case R.id.divisionUpperLowerBtn:
                currentCodDivision = Divisions.CODDIVISION_UPPERLOWER;
                break;

            case R.id.divisionDefaultBtn:
                currentCodDivision = Divisions.CODDIVISION_DEFAULT;
                break;

            case R.id.divisionDetailedBtn:
                currentCodDivision = Divisions.CODDIVISION_DETAILED;
                break;
        }

        presenter.changeDivision(currentCodDivision);

    }


    /**
     * Manages clicks on body zone button
     * <p/>
     * Save current time and pressed button, Disable new pressed button and
     * starts partial chrono
     * First run: show start time and starts full time chrono
     *
     * @param view - Pressed body zone button
     */
    public void startZoneMassage(View view) {
        presenter.startZoneMassage(view);
    }


    // ******************
    // Setup views (visibility...)
    // ******************
    public Position loadIntermediatePosition(String currentCodDivision,
                                             String codPosition) {
        Position position = Position.getPosition(this, codPosition);

        if(position != null ) {
            position.loadDivision(currentCodDivision);

            Button fullBodyBtn = (Button) findViewById(R.id.fullBodyMassageBtnId);

            position.pressButtonInDivision(fullBodyBtn);
        }
        return position;
    }

    public void prepareScreenForSimpleMode(){
        mDivisionsLayout.setVisibility(View.GONE);
        positionLayout.setVisibility(View.GONE);

        LinearLayout.LayoutParams positionContainerLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 3);
        mPositionContainerLayout.setLayoutParams(
                positionContainerLayoutParameters);

        mPositionSelector.showHideAll(false);
    }


    public void prepareScreenForIntermediateMode(String firstCodPosition){
        mMassagingTxt.setVisibility(View.GONE);
        disablePosition(firstCodPosition);
        hideDivisions();
    }

    public void prepareScreenForAdvancedMode(String firstCodPosition,
                                             String firstCodDivision){
        mMassagingTxt.setVisibility(View.GONE);
        disablePosition(firstCodPosition);
        disableDivision(firstCodDivision);
    }


    public void enableMassageControllers(int mode) {
        mEndMassageBtn.setVisibility(View.VISIBLE);

        if(mode == MassageRecord.MODE_SIMPLE){
            mMassagingTxt.setVisibility(View.VISIBLE);
        }
        else{
            mPositionSelector.showHideAll(true);
        }

        if(mode == MassageRecord.MODE_ADVANCED){
            mDivisionSelector.showHideAll(true);
        }
    }

    public void disableMassageControllers(int mode) {
        mEndMassageBtn.setVisibility(View.INVISIBLE);

        if(mode == MassageRecord.MODE_SIMPLE){
            mMassagingTxt.setVisibility(View.GONE);
        }
        else{
            mPositionSelector.showHideAll(false);
        }

        if(mode == MassageRecord.MODE_ADVANCED) {
            mDivisionSelector.showHideAll(false);
        }
    }


    public void disablePosition(String firstCodPosition){
        mPositionSelector.disablePositionBtn(firstCodPosition);
    }

    public void disableDivision(String firstCodDivision) {
        mDivisionSelector.disableDivisionBtn(firstCodDivision);
    }


    public void changeDisabledDivision(String codPosition, String codDivision){
        mDivisionSelector.enableAll(codPosition);
        mDivisionSelector.disableDivisionBtn(codDivision);
    }


    public void hideDivisions(){
        mDivisionsLayout.setVisibility(View.GONE);
    }

    public View getFullBodyBtn(){
        return findViewById(R.id.fullBodyMassageBtnId);
    }

    public String getFullBodyBtnLabel(){
        return fullBody_button_label;
    }


    // ******************
    // Start/pause/resume
    // ******************
    public long getPausedTime() {
        return mPauseResume.getPausedTime();
    }


    public void pause(boolean animationsDisabled) {
        mPauseResume.pause(animationsDisabled);
    }

    public void resume() {
        mPauseResume.resume();
    }

    public void enablePause(boolean enable) {
        mPauseResume.enablePauseButton(enable);
    }

    public void stopPause() {
        mPauseResume.stopPauseChrono();
        mPauseResume.enablePauseButton(true);
    }


    public void startPause() {
        mPauseResume.startPauseChrono();
        mPauseResume.enablePauseButton(false);
    }


    public void resetPausedTime() {
        mPauseResume.resetPausedTime();
    }



    // ***************
	// Navigation
	// ***************
    public void goToTreatedZones(Long idMassage){

        Intent intent = new Intent(this, TreatedZonesActivity.class);

        Bundle b = new Bundle();
        b.putLong(TreatedZonesActivity.PARAMETER_IDMASSAGE, idMassage);
        b.putBoolean(TreatedZonesActivity.PARAMETER_MASSAGE_FINISHED, false);
        intent.putExtras(b);

        startActivityForResult(intent, TREATED_ZONES_REQUEST_CODE);
    }

    public void goToOptionsActivity() {
        Intent intent = new Intent(this, OptionsActivity.class);
        startActivityForResult(intent, OPTIONS_REQUEST_CODE);
    }


    // ***************
    // Options
    // ***************
    public void manageScreenLock(boolean screenLock) {
        if (screenLock) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            if(wl != null) wl.release();

        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "KeepRunningTag");
            wl.acquire();
        }
    }

    public void hideSummaryMenu() {
        if (mMenu != null) {
            mMenu.findItem(R.id.summary_menu).setVisible(false);
        }
    }

    public void toggleVolume(boolean isOn) {
        Drawable volumeIcon;

        if (isOn) {
            volumeIcon = ResourcesCompat.getDrawable(res,
                    R.drawable.sound_on, null);
        } else {
            volumeIcon = ResourcesCompat.getDrawable(res,
                    R.drawable.sound_off, null);
        }

        mMenu.findItem(R.id.mute_menu).setIcon(volumeIcon);
    }

    // ***************
    // Messages, exceptions
    // ***************
    public void showEndNotSaving(){
        MiscUtils.showToast(getApplicationContext(),
                massageSessionActivity_end_no_save_msg);
    }

    public void showEndSaving(){
        MiscUtils.showToast(getApplicationContext(),
                massageSessionActivity_end_save_msg);
    }

    public void errorResumingMassage(){
        MiscUtils.showToast(context,
                massageSessionActivity_resume_massage_error_toast);

        this.setResult(RESULT_CANCELED);
        finish();
    }

    public void sendExceptionTracker(Exception e) {
        MiscUtils.sendExceptionTracker(this, NAME, e.toString(), true);
    }


    // ******************
    // Log operations
    // ******************
    public void logResumeMassage(Long idMassage, String mode) {

        if (BuildConfig.DEBUG) {
            Log.d(NAME,
                    "Massage -> Restart " + String.valueOf(idMassage));
        }
        MiscUtils.sendEventTracker(this, NAME, "Massage", "Restart", mode, -1L);
    }


    public void logEndMassage(long duration, String mode) {

        if (BuildConfig.DEBUG) {
            Log.d(NAME,
                    "Massage -> End " + mode + ". Duration: " + duration);
        }

        MiscUtils.sendEventTracker(this, NAME, "Massage", "End", mode, duration);
    }


    public void logNewMassage(Long idMassage, String mode, int durationInMinutes){
        if (BuildConfig.DEBUG) {
            Log.d(NAME,
                    "Massage -> New " + mode + " " + String.valueOf(idMassage));
        }

        MiscUtils.sendEventTracker(this, NAME, "Massage", "New", mode,
                (long) durationInMinutes);

    }

    public void logChangePosition(String positionName, String mode){
        if (BuildConfig.DEBUG) {
            Log.d(NAME,
                    "Massage -> ChangePosition to " + positionName);
        }
        MiscUtils.sendEventTracker(this, NAME, "Massage", "ChangePosition",
                mode, -1L);
    }

    public void logDiscardZone(Long idMassage, String discarded_zone,
                               String mode){
        Log.d(NAME, "Massage -> Discard zone " + String.valueOf(idMassage) +
                ". Zone: " + discarded_zone);

        MiscUtils.sendEventTracker(this, NAME, "Massage", "Discard zone",
                mode, -1L);

    }



    /* ***************** */
	/* PRIVATE FUNCTIONS */
	/* ***************** */
    private void gotoNewMassageActivity() {
        //NEW MASSAGE
        Intent intent = new Intent(this, NewMassageActivity.class);

        Bundle defineMassageBundle = new Bundle();

        defineMassageBundle.putBoolean(
                NewMassageActivity.PARAMETER_DEFINE_MASSAGE, true);
        intent.putExtras(defineMassageBundle);

        startActivityForResult(intent, NEW_MASSAGE_REQUEST_CODE);
    }
}