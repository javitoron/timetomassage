package com.fingerhits.massageassistant.massagesession;

import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecordInvalidException;

import java.util.Date;

/**
 * Created by Javier Torón on 13/03/2017.
 */

public interface IMassageSessionInteractor {
    boolean getScreenLock();

    boolean getSoundWarningsEnabled();
    void setSoundWarningsEnabled(boolean enable );
    int getMassageDurationInMinutes();
    long getMassageDurationInSeconds();
    long getMassageDurationInMilliseconds();
    int getMinimalDurationInSeconds();

    boolean getAnimationsDisabled();

    MassageRecord getMassage(Long idMassage);
    Long getDurationInLastPosition(Long idMassage);
    TreatedZoneRecord getLastTreatedZone(Long idMassage);

    MassageRecord createMassage(int mode);
    void insertTreatedZone(Long idMassage, String codPosition,
                                  String codDivision, String codZone,
                                  Date zoneStartDateTime,
                                  long elapsedTimeInSeconds)
            throws TreatedZoneRecordInvalidException;
    Long finishMassage(Long idMassage);
    }

