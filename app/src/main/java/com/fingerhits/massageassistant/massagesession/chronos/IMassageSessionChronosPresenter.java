package com.fingerhits.massageassistant.massagesession.chronos;

import android.view.View;
import android.widget.Chronometer;

/**
 * Created by Javier Torón on 15/03/2017.
 */

public interface IMassageSessionChronosPresenter {
    void setView();
    void prepareMode(int mode, long massageDurationInMilliseconds);
    void optionsChanged();

    void loadIntermediatePosition(String position_abbr,
                                  String fullBodyLabel,
                                  Long positionDurationInMilliseconds);
    void divisionChangedTo(String codDivision);

    void tickFull( Chronometer chrono );
    void pauseChronometers();
    void resumeChronometers();

    //public void initChronos(Long massageDuration);
    void startFullChronoIn(Long startingTimeInMilliseconds);
    void startPositionChronoIn(Long startingTimeInMilliseconds);
    void startZoneChronoIn(String label, Long startingTimeInMilliseconds);

    void setZoneChronoNormalColor();
    void startZoneChrono(View pressedBtn, String state);
    Long getZoneChronoElapsedTime(String state);
    void blinkZoneChrono();

    void changePositionChronoText(String position_abbr);
}