package com.fingerhits.massageassistant.massagesession.divisions;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.LyingFaceDown.LyingFaceDownDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.LyingFaceUp.LyingFaceUpDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.LyingLeftSide.LyingLeftSideDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.LyingRightSide.LyingRightSideDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.SittingBack.SittingBackDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.SittingFront.SittingFrontDivisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * Common behavior on pressing buttons relative to each body area in any mPosition
 * 
 * @author Javier Torón
 * 
 * Buttons for each body zone can be enabled and disabled and return 
 * text labels corresponding to each one.
 * 
 */
abstract public class Divisions {
    public static final String CODDIVISION_SINGLE = "Single";
    public static final String CODDIVISION_LEFTRIGHT = "LeftRight";
    public static final String CODDIVISION_UPPERLOWER = "UpperLower";
    public static final String CODDIVISION_DEFAULT = "Default";
    public static final String CODDIVISION_DETAILED = "Detailed";

    protected final LinearLayout containerLayout;
    protected final Resources res;
    protected final MassageSessionActivity activity;
    protected final Context context;

    public String codDivision;
	/**
	 * Button corresponding to zone currently being massaged
	 * 
	 * It has mButtonSelected
	 */
    public Button lastPressed;

    /**
     * Common buttons to all positions
     */
//    public Button transitionsBtn, fullBodyBtn;


    /**
     * Divisions constructor
     * @param p_containerLayout Divisions container
     */
	protected Divisions(MassageSessionActivity act,
                        LinearLayout p_containerLayout) {
        containerLayout = p_containerLayout;
		res = containerLayout.getResources();
        activity = act;
        context = activity.getApplicationContext();

    }

    /*Static methods*/
    public static String[] getCodZonesArray(String codPosition, String codDivision){
        switch(codPosition) {
            case TreatedZoneRecord.CODPOSITION_FACEDOWN:
                return LyingFaceDownDivisions.getCodZonesArray(codDivision);

            case TreatedZoneRecord.CODPOSITION_FACEUP:
                return LyingFaceUpDivisions.getCodZonesArray(codDivision);

            case TreatedZoneRecord.CODPOSITION_LEFTSIDE:
                return LyingLeftSideDivisions.getCodZonesArray(codDivision);

            case TreatedZoneRecord.CODPOSITION_RIGHTSIDE:
                return LyingRightSideDivisions.getCodZonesArray(codDivision);

            case TreatedZoneRecord.CODPOSITION_SITTINGBACK:
                return SittingBackDivisions.getCodZonesArray(codDivision);

            case TreatedZoneRecord.CODPOSITION_SITTINGFRONT:
                return SittingFrontDivisions.getCodZonesArray(codDivision);

            default:
                return new String[] {};
        }

    }


	/* COMMON METHODS */
	/**
	 * Enable previous lastPressed, disable btn and updates lastPressed
	 * 
	 * @param btn Button pressed
	 */
    public void pressButton( Button btn ){
        if(btn != null) {
            if (lastPressed != null) {
                ZonesUtils.enableButton(res, lastPressed, true);
            }

            ZonesUtils.enableButton(res, btn, false);

            lastPressed = btn;
        }
    }


    /**
	 * Recover state pre pause
	 */
	public void reenable() {
		enableAll(true);

        ZonesUtils.enableButton(res, lastPressed, false);
	}



	/* METHODS IMPLEMENTED IN EACH SUBCLASS */
	/* Manage transitions and fullBody buttons for all the positions */

    /**
     * Shows button for each zone
     */
    public abstract void showZones(View v);

    /**
     * Clear all buttons for this mDivision
     */
    public void clearZones(){
        containerLayout.removeAllViews();
    }

    public void load(){
        clearZones();

        showZones(activity.positionLayout);

    }

        /**
         * Returns title for the button
         *
         * @param btn
         *
         * To be implemented in each subclass
         */
    public String zoneTitle( Button btn ){
        return "";
    }

    /**
     * Returns codZone for the button
     *
     * @param btn
     *
     * To be implemented in each subclass
     */
    public String codZone( Button btn ){
        return "";
    }


    /**
     * Returns button corresponding to the codZone
     *
     * @param codZone
     *
     * To be implemented in each subclass
     */
    public Button buttonForCodZone( String codZone ){
        return null;
    }


    /**
     * Enable or disable all buttons for the mPosition
     *
     * @param enabled (true - or disabled false)
     *
     * To be implemented in each subclass
     */
    public void enableAll(Boolean enabled) {
    }



    /* PROTECTED METHODS */
    /**
     * Shows zone button
     */
    protected Button setZoneBtn( int buttonId, LinearLayout layout,
                                 float weight, int textId ){
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams( 0,
                LinearLayout.LayoutParams.MATCH_PARENT, weight);

        Button button = (Button) activity.getLayoutInflater().inflate(
                R.layout.template_button_zone, null);
        button.setId(buttonId);
        button.setText(res.getString(textId));
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                activity.startZoneMassage(v);
            }
        });
        layout.addView(button, lp);

        return button;
    }

    /**
     * Shows zone button (vertical)
     */
    protected Button setZoneBtnVertical(int buttonId, LinearLayout layout,
                                        float weight, int textId ){
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 0, weight);

        Button button = (Button) activity.getLayoutInflater().inflate(
                R.layout.template_button_zone_vertical, null);
        button.setId(buttonId);
        button.setText(res.getString(textId));
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                activity.startZoneMassage(v);
            }
        });
        layout.addView(button, lp);

        return button;
    }



    /**
     * Shows fixView
     */
    protected void showFixViewWeight( LinearLayout layout, float weight ){

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams( 0,
                GraphicUtils.dpToPixels(res, 1), weight);

        View fixView = new View(context);
        layout.addView(fixView, lp);

    }

    /**
     * Shows fixTopLayout
     */
    protected void showFixLayout(LinearLayout mainLayout, int id, float weight){

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 0, weight);

        LinearLayout fixLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        fixLayout.setId(id);

        showFixViewWeight(fixLayout, 1);

        mainLayout.addView(fixLayout, lp);
    }

    /**
     * Creates HeadNeckLayout
     */
    protected LinearLayout setHeadNeckLayout(float weight){
        return setHeadLayouts(R.id.headNeckLayoutId, weight);
    }

    /**
     * Creates HeadLayout
     */
    protected LinearLayout setHeadLayout(float weight){
        return setHeadLayouts(R.id.headLayoutId, weight);
    }

    /**
     * Creates any head type layout
     */
    protected LinearLayout setHeadLayouts(int id, float weight){

        LinearLayout.LayoutParams headLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, weight);
        headLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout headLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        headLayout.setId(id);
        headLayout.setLayoutParams(headLayoutParameters);

        return headLayout;
    }



    /**
     * Creates LegsLayout
     */
    protected LinearLayout setLegsLayout(){

        LinearLayout.LayoutParams legsLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 34);
        legsLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 5));

        LinearLayout legsLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        legsLayout.setId(R.id.legsLayoutId);
        legsLayout.setLayoutParams(legsLayoutParameters);

        return legsLayout;
    }


    /**
     * Creates leftRightLayout
     */
    protected LinearLayout setLeftRightLayout(float weight) {
        LinearLayout.LayoutParams leftRightLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, weight);
        leftRightLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout leftRightLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        leftRightLayout.setId(R.id.leftRightLayoutId);
        leftRightLayout.setLayoutParams(leftRightLayoutParameters);

        return leftRightLayout;
    }

        /**
         * Shows buttons in HeadNeckLayout and return headNeckBtn
         */
    protected Button setHeadNeckZones( LinearLayout mainLayout, float weight ){

        LinearLayout headNeckLayout = setHeadNeckLayout(weight);

        showFixViewWeight(headNeckLayout, 4);
//        showTransitionsBtn(headNeckLayout, 3);
//        showFixViewWeight(headNeckLayout, 1);

        Button headNeck = setZoneBtn( R.id.headNeckMassageBtnId, headNeckLayout,
                4,R.string.headNeck_button );

        showFixViewWeight(headNeckLayout, 4);
//        showFullBodyBtn(headNeckLayout, 3);
//        showFixViewWeight(headNeckLayout, 1);

        mainLayout.addView(headNeckLayout);

        return headNeck;
    }

    /**
     * Shows buttons in HeadLayout and return headBtn
     */
    protected Button setHeadZones( LinearLayout mainLayout, float weight ){

        LinearLayout headLayout = setHeadLayout(weight);

        showFixViewWeight(headLayout, 4);
//        showTransitionsBtn(headLayout, 3);
//        showFixViewWeight(headLayout, 1);

        Button headNeck = setZoneBtn( R.id.headMassageBtnId, headLayout,
                4, R.string.head_button );

        showFixViewWeight(headLayout, 4);
//        showFullBodyBtn(headLayout, 3);
//        showFixViewWeight(headLayout, 1);

        mainLayout.addView(headLayout);

        return headNeck;
    }

    /**
     * Shows buttons in upperBodyLayout and return upperBodyBtn
     */
    protected Button setUpperBodyZones( LinearLayout mainLayout, float weight ){
        LinearLayout.LayoutParams upperBodyLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, weight);
        upperBodyLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout upperBodyLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        upperBodyLayout.setId(R.id.upperBodyLayoutId);

        showFixViewWeight(upperBodyLayout, 1);

        Button upperBodyBtn = setZoneBtn( R.id.upperMassageBtnId, upperBodyLayout,
                2, R.string.upperBody_button );

        showFixViewWeight(upperBodyLayout, 1);

        mainLayout.addView(upperBodyLayout, upperBodyLayoutParameters);

        return upperBodyBtn;

    }

    /**
     * Shows buttons in lowerBodyLayout and return lowerBodyBtn
     */
    protected Button setLowerBodyZones( LinearLayout mainLayout, float weight,
                                        float fixWeights ){
        LinearLayout.LayoutParams lowerBodyLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, weight);
        lowerBodyLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout lowerBodyLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        lowerBodyLayout.setId(R.id.lowerBodyLayoutId);

        showFixViewWeight(lowerBodyLayout, fixWeights);

        Button lowerBodyBtn = setZoneBtn( R.id.lowerBodyLayoutId, lowerBodyLayout,
                2, R.string.lowerBody_button);

        showFixViewWeight(lowerBodyLayout, fixWeights);

        mainLayout.addView(lowerBodyLayout, lowerBodyLayoutParameters);

        return lowerBodyBtn;
    }



    /* PRIVATE METHODS */


}