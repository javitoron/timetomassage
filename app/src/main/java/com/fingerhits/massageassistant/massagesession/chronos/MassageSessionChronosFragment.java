package com.fingerhits.massageassistant.massagesession.chronos;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.fingerhits.massageassistant.massagesession.IMassageSessionView;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivityModule;
import com.fingerhits.massageassistant.massagesession.MassageSessionPresenter;
import com.fingerhits.massageassistant.massagesession.positions.Position;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.fingerhits.massageassistant.utils.DateTimeUtils.durationToString;

/**
 * Created by Javier Torón on 14/03/2017.
 */

public class MassageSessionChronosFragment extends Fragment
        implements IMassageSessionChronosView {

    @Inject IMassageSessionChronosPresenter presenter;

    @BindView(R.id.chronosLayout) LinearLayout mChronosLayout;

    @BindView(R.id.zoneChrono) Chronometer mZoneChrono;
    @BindView(R.id.zoneChronoLayout) LinearLayout mZoneChronoLayout;
    @BindView(R.id.zoneChronoTxt) TextView mZoneChronoTxt;


    @BindView(R.id.positionChrono) Chronometer mPositionChrono;
    @BindView(R.id.positionChronoLayout) LinearLayout mPositionChronoLayout;
    @BindView(R.id.positionChronoTxt) TextView mPositionChronoTxt;


    @BindView(R.id.fullChrono) Chronometer mFullChrono;
    @BindView(R.id.fullChronoLayout) LinearLayout mFullChronoLayout;
    @BindView(R.id.fullChronoTxt) TextView mFullChronoTxt;


    @BindView(R.id.remainingChronoTxt) TextView mRemainingChronoTxt;
    @BindView(R.id.remainingChronoLayout) LinearLayout mRemainingChronoLayout;
    @BindView(R.id.remainingLabelTxt) TextView mRemainingLabelTxt;


    @BindString(R.string.zone_not_selected) String zone_not_selected;


    @BindColor(R.color.whiteBackground ) int colorFrom;
    @BindColor(R.color.accentLight ) int colorTo;
    @BindColor(R.color.primaryLight ) int primaryLight;
    @BindColor(R.color.whiteBackground ) int whiteBackground;

    public MassageSessionChronometer zoneChrono;
    public MassageSessionChronometer positionChrono;
    public MassageSessionChronometer fullChrono;
    public MassageSessionChronometer remainingChrono;


    private MassageSessionChronometer mVisibleChrono;
    private ValueAnimator mZoneColorAnimation;

    private MassageSessionActivity activity;
    private Context mContext;

    private boolean mTimeExceeded = false;

    public MassageSessionChronosFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chronometers, container, false);
        ButterKnife.bind(this, view);

        MainApp.get(this.getActivity().getApplicationContext())
                .getAppComponent()
                .plus(
                        new MassageSessionActivityModule(
                                (IMassageSessionView) this.getActivity(),
                        this))
                .inject(this);

        activity = (MassageSessionActivity) this.getActivity();
        mContext = activity.getApplicationContext();
        presenter.setView();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
    }


    public void initChronos(long massageDurationInMilliseconds) {
        zoneChrono = new MassageSessionChronometer( activity, mZoneChrono,
                mZoneChronoLayout, mZoneChronoTxt);

        positionChrono = new MassageSessionChronometer(activity, mPositionChrono,
                mPositionChronoLayout, mPositionChronoTxt);

        fullChrono = new MassageSessionChronometer(activity, mFullChrono,
                mFullChronoLayout, mFullChronoTxt);
        fullChrono.chrono.setOnChronometerTickListener(
                new Chronometer.OnChronometerTickListener() {
                    @Override
                    public void onChronometerTick(Chronometer chronometer) {
                        presenter.tickFull(chronometer);
                    }
                });

        remainingChrono = new MassageSessionChronometer(activity,
                mRemainingChronoTxt, mRemainingChronoLayout, mRemainingLabelTxt);
        remainingChrono.chronoTxt.setText(durationToString( massageDurationInMilliseconds));
    }


    /**
     * Pauses full, mPosition and current zone Chronometers
     */
    public void pauseChronometers(){
        zoneChrono.pause();

        positionChrono.pause();

        fullChrono.pause();
    }



    public void setVisibleChrono(String chronoAbbr ) {
        switch (chronoAbbr ){
            case MassageSessionChronosPresenter.CHRONO_FULL:
                mVisibleChrono = fullChrono;
                break;

            case MassageSessionChronosPresenter.CHRONO_REMAINING:
                mVisibleChrono = remainingChrono;
                break;

            case MassageSessionChronosPresenter.CHRONO_POSITION:
                mVisibleChrono = positionChrono;
                break;

            case MassageSessionChronosPresenter.CHRONO_ZONE:
                mVisibleChrono = zoneChrono;
                break;
        }

    }
    /**
     * Shows a new chrono and hides the current
     *
     * @param view Pressed on chrono to change
     */
    @OnClick({R.id.zoneChronoLayout, R.id.positionChronoLayout,
            R.id.fullChronoLayout, R.id.remainingChronoLayout})
    public void changeChrono(View view) {
        LinearLayout chronoLayout = (LinearLayout) view;

        if(chronoLayout.getId() == R.id.zoneChronoLayout ){
            change(MassageSessionChronosPresenter.CHRONO_ZONE, mTimeExceeded);
        }

        if(chronoLayout.getId() == R.id.positionChronoLayout ){
            change(
                    MassageSessionChronosPresenter.CHRONO_POSITION, mTimeExceeded);
        }

        if(chronoLayout.getId() == R.id.fullChronoLayout ){
            change(
                    MassageSessionChronosPresenter.CHRONO_FULL, mTimeExceeded);
        }

        if(chronoLayout.getId() == R.id.remainingChronoLayout ){
            change(MassageSessionChronosPresenter.CHRONO_REMAINING, mTimeExceeded);
        }
    }


    public void change( String newChrono, boolean timeExceeded ) {
        mTimeExceeded = timeExceeded;
        if(!newChrono.equals("")) {
            if (mVisibleChrono != null) mVisibleChrono.disable();
            setVisibleChrono(newChrono);
            enableCurrentChrono(timeExceeded);
        }
    }


    public void enableCurrentChrono( boolean timeExceeded ) {
        mTimeExceeded = timeExceeded;
        mVisibleChrono.enable(timeExceeded);
    }



    //*******************************
    // FULL CHRONO METHODS
    //*******************************
    public void enableFullChrono(boolean timeExceeded) {
        mTimeExceeded = timeExceeded;
        fullChrono.enable(timeExceeded);
    }


    public void startFullChronoIn(long startingTimeInMilliseconds,
                                  boolean timeExceeded) {
        mTimeExceeded = timeExceeded;
        fullChrono.startIn(startingTimeInMilliseconds);

        if(startingTimeInMilliseconds!= 0L){
            mVisibleChrono.setEnabledBackground(timeExceeded);
        }
    }

    public void resumeFullChrono() {
        fullChrono.resume();
    }


    public long getFullElapsedTime(String state ) {
        return fullChrono.getElapsedTime(state);
    }


    public void tickFullAndRemaining( long elapsedTime, long massageDuration ) {
        long elapsedTimeInSeconds = elapsedTime / 1000;
        if (elapsedTimeInSeconds > fullChrono.getLastTick()) {
            fullChrono.setLastTick(elapsedTimeInSeconds);
            remainingChrono.setChronoText(
                    durationToString(massageDuration - elapsedTime));
        }
    }



    //*******************************
    //** POSITION CHRONO METHODS
    //*******************************
    public void startPositionChronoIn(long startingTimeInMilliseconds) {
        positionChrono.startIn(startingTimeInMilliseconds);
    }


    /**
     * Changes label for mPosition chrono
     * @param codPosition New mPosition code
     */
    public void changePositionChronoText( String codPosition ){
        positionChrono.setText(
                Position.codPositionToString(mContext, codPosition));
    }

    public void hidePositionChrono() {
        positionChrono.hide();
    }

    public void resumePositionChrono() {
        positionChrono.resume();
    }



    //*******************************
    //** ZONE CHRONO METHODS *
    //*******************************
    public long getZoneChronoElapsedTime(String state ) {
        return zoneChrono.getElapsedTime(state);
    }

    public void stopZoneChronoInZero(){
        zoneChrono.stopInZero();
    }

    /**
     * Set chrono to 0 or accumulated time and start zone chrono
     */
    public void startZoneChronoIn(String label, long startingTimeInMilliseconds ){
        if(!label.equals("")) zoneChrono.setText(label);

        zoneChrono.startIn(startingTimeInMilliseconds);
    }


    public void showHideZoneChrono(boolean show){
        if(show) {
            zoneChrono.show();
            zoneChrono.setText(zone_not_selected);
        }
        else{
            if(mVisibleChrono == zoneChrono){
                changeChrono(positionChrono.chronoTxt);
            }
            zoneChrono.hide();
        }
    }

    public void blinkZone(){

        mZoneColorAnimation =
                ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        mZoneColorAnimation.setDuration(3000); // milliseconds
        mZoneColorAnimation.addUpdateListener(
                new ValueAnimator.AnimatorUpdateListener() {

                    @Override
                    public void onAnimationUpdate(ValueAnimator animator) {
                        zoneChrono.setBackgroundColor(
                                (int) animator.getAnimatedValue());
                    }

                });
        mZoneColorAnimation.start();
    }


    public void putZoneBackgroundNormalColor() {

        if(mZoneColorAnimation.isRunning()){
            mZoneColorAnimation.cancel();
        }

        if(mVisibleChrono == zoneChrono) {
            zoneChrono.setBackgroundColor(primaryLight);
        }
        else{
            zoneChrono.setBackgroundColor(whiteBackground);
        }
    }


    public void hideZoneChrono() {
        zoneChrono.hide();
    }


    public void zoneStarted( View view, String state){
        if (state.equals(MassageSessionPresenter.STATE_WAITING_TO_SELECT_ZONE)) {
            positionChrono.resume();
            /* We reset this starting time in order to avoid chronos
            * counting time elapsed between starting activity and clicking
            * the first zone */
            //fullChrono.startIn(startingTime);
        }

        startZoneChronoIn(((Button) view).getText().toString(), 0L);
    }

}