package com.fingerhits.massageassistant.massagesession.positions;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.divisions.LyingFaceDown.LyingFaceDownDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.LyingFaceUp.LyingFaceUpDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.LyingLeftSide.LyingLeftSideDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.LyingRightSide.LyingRightSideDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.SittingBack.SittingBackDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.SittingFront.SittingFrontDivisions;
import com.fingerhits.massageassistant.massagesession.divisions.UndefinedSingleDivision;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

/**
 * Shows buttons for a mPosition (base class) and detects swipe
 * 
 * @author Javier Torón
 * 
 */
abstract public class Position {
	
	protected Resources mRes;
    protected MassageSessionActivity activity;

    private Divisions mDivision;

    private String mCodPosition;
	private String mPositionName;
    private String mPositionAbbr;

    /**
     * Initializes the containerLayout
     * @param act
     *
     * Children have to set mDivision, mPosition and codPosition
     */
	public Position(MassageSessionActivity act) {
		activity = act;
        mRes = activity.res;
	}

    /********************************
     * PROPERTIES**** ***************
     *******************************/
    public String getCodPosition(){return mCodPosition;}
    public void setCodPosition(String codPosition){mCodPosition = codPosition;}

    public String getPositionName(){return mPositionName;}
    public void setPositionName(String positionName){mPositionName = positionName;}

    public String getPositionAbbr(){return mPositionAbbr;}
    public void setPositionAbbr(String positionAbbr){mPositionAbbr = positionAbbr;}

    public Divisions getDivision(){return mDivision;}
    public Divisions getDivision(String p_codDivision){
        switch ( mCodPosition ) {
            case TreatedZoneRecord.CODPOSITION_UNDEFINED:
                mDivision = new UndefinedSingleDivision(activity,
                        activity.positionLayout);
                break;

            case TreatedZoneRecord.CODPOSITION_FACEDOWN:
                mDivision = LyingFaceDownDivisions.getDivision(activity,
                        activity.positionLayout, p_codDivision);
                break;

            case TreatedZoneRecord.CODPOSITION_FACEUP:
                mDivision = LyingFaceUpDivisions.getDivision(activity,
                        activity.positionLayout, p_codDivision);
                break;

            case TreatedZoneRecord.CODPOSITION_RIGHTSIDE:
                mDivision = LyingRightSideDivisions.getDivision(activity,
                        activity.positionLayout, p_codDivision);
                break;

            case TreatedZoneRecord.CODPOSITION_LEFTSIDE:
                mDivision = LyingLeftSideDivisions.getDivision(activity,
                        activity.positionLayout, p_codDivision);
                break;

            case TreatedZoneRecord.CODPOSITION_SITTINGBACK:
                mDivision = SittingBackDivisions.getDivision(activity,
                        activity.positionLayout, p_codDivision);
                break;

            case TreatedZoneRecord.CODPOSITION_SITTINGFRONT:
                mDivision = SittingFrontDivisions.getDivision(activity,
                        activity.positionLayout, p_codDivision);
                break;

        }
        return mDivision;
    }

    public void setDivision(Divisions division){mDivision = division;}
    public void setDivision(String codDivision){
        mDivision = getDivision(codDivision);
    }

    public String getCodDivision(){
        return mDivision.codDivision;
    }

    /********************************
     * STATIC METHODS ***************
     *******************************/

    /**
     * Returns full and translated name of the codPosition
     * @param context From activity. In order to read the resources
     * @param codPosition Code of mPosition to translate
     * @return Full translated name of the mPosition
     */
    public static String codPositionToString(Context context,
                                             String codPosition ){
        switch ( codPosition ) {
            case TreatedZoneRecord.CODPOSITION_FACEDOWN:
                return context.getString(R.string.lyingFaceDownPosition);

            case TreatedZoneRecord.CODPOSITION_FACEUP:
                return context.getString(R.string.lyingFaceUpPosition);

            case TreatedZoneRecord.CODPOSITION_RIGHTSIDE:
                return context.getString(R.string.lyingRightSidePosition);

            case TreatedZoneRecord.CODPOSITION_LEFTSIDE:
                return context.getString(R.string.lyingLeftSidePosition);

            case TreatedZoneRecord.CODPOSITION_SITTINGBACK:
                return context.getString(R.string.sittingBackPosition);

            case TreatedZoneRecord.CODPOSITION_SITTINGFRONT:
                return context.getString(R.string.sittingFrontPosition);

            default:
                return "";
        }
    }


    /**
     * Returns abbreviated and translated name of the codPosition
     * @param context From activity. In order to read the resources
     * @param codPosition Code of mPosition to translate
     * @return Abbreviated translated name of the mPosition
     */
    public static String codPositionToAbbrString(Context context,
                                                 String codPosition ){
        switch ( codPosition ) {
            case TreatedZoneRecord.CODPOSITION_FACEDOWN:
                return context.getString(R.string.lyingFaceDownPositionAbbr);

            case TreatedZoneRecord.CODPOSITION_FACEUP:
                return context.getString(R.string.lyingFaceUpPositionAbbr);

            case TreatedZoneRecord.CODPOSITION_RIGHTSIDE:
                return context.getString(R.string.lyingRightSidePositionAbbr);

            case TreatedZoneRecord.CODPOSITION_LEFTSIDE:
                return context.getString(R.string.lyingLeftSidePositionAbbr);

            case TreatedZoneRecord.CODPOSITION_SITTINGBACK:
                return context.getString(R.string.sittingBackPositionAbbr);

            case TreatedZoneRecord.CODPOSITION_SITTINGFRONT:
                return context.getString(R.string.sittingFrontPositionAbbr);

            default:
                return "";
        }
    }

    /**
     * Returns containerLayout corresponding to codPosition
     * @param codPosition - String TreatedZoneRecord.XXX
     * @return XXXZoneFragment
     */
    public static Position getPosition(MassageSessionActivity act,
                                       String codPosition){
        switch ( codPosition ) {
            case TreatedZoneRecord.CODPOSITION_UNDEFINED:
                return new UndefinedPosition(act);

            case TreatedZoneRecord.CODPOSITION_FACEDOWN:
                return new LyingFaceDownPosition(act);

            case TreatedZoneRecord.CODPOSITION_FACEUP:
                return new LyingFaceUpPosition(act);

            case TreatedZoneRecord.CODPOSITION_RIGHTSIDE:
                return new LyingRightSidePosition(act);

            case TreatedZoneRecord.CODPOSITION_LEFTSIDE:
                return new LyingLeftSidePosition(act);

            case TreatedZoneRecord.CODPOSITION_SITTINGBACK:
                return new SittingBackPosition(act);

            case TreatedZoneRecord.CODPOSITION_SITTINGFRONT:
                return new SittingFrontPosition(act);

            default:
                return null;
        }
    }

    /********************************
     * DIVISION METHODS ***************
     *******************************/

    public void loadDivision(String p_codDivision ) {
        activity.positionLayout.setBackground(background());

        mDivision = getDivision(p_codDivision);

        mDivision.load();
    }


    /**
	 * Manages clicks on body zone button
	 * 
	 * Calls mDivision.pressButton
	 *  
	 * @param view - Pressed body zone button
	 * */
	public void pressButtonInDivision(View view ) {
        mDivision.pressButton( (Button) view );
	}

    /********************************
     * ZONE METHODS ***************
     *******************************/

    /**
     * Shortcut for mDivision.codZone
     * @param btn Zone Button
     * @return CodZone corresponding to button
     */
    public String codZone( Button btn ){
        return mDivision.codZone( btn );
    }

	/**
	 * Enables or disables all buttons.
	 *
	 * @param enabled - True or false
	 * */
	public void enableAll(@SuppressWarnings("SameParameterValue") boolean enabled) {
        mDivision.enableAll(enabled);
	}


	/**
	 * Returns last pressed button from the mDivision
	 */
	public Button lastPressed() {
	    return mDivision.lastPressed;
	}


	/* METHODS IMPLEMENTED IN EACH SUBCLASS */
    public abstract Drawable background();
    public abstract boolean hasDivision(String p_codDivision);

    /* PRIVATE METHODS */

}
