package com.fingerhits.massageassistant.massagesession.divisions;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * 
 * Controls Undefined Single Division
 * 
 * @author Javier Torón
 * 
 * Only has 1 button: Massage
 */
public class UndefinedSingleDivision extends Divisions {

	private Button mMassage;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public UndefinedSingleDivision(MassageSessionActivity act,
                                   LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_SINGLE;
	}


    public Divisions prevDivision(){
        return this;
    }


    public Divisions nextDivision(){
        return this;
    }

    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones( View v ){
        showMassageZone((LinearLayout) v);
    }



    /* OVERRIDES */
    @Override
    public String zoneTitle( Button btn ){
        if (btn == mMassage){
            return res.getString(R.string.simple_massage);
        }

        return super.zoneTitle(btn);
    }


    @Override
    public String codZone( Button btn ){
        if (btn == mMassage){
            return TreatedZoneRecord.CODZONE_MASSAGE;
        }
        return super.codZone(btn);
    }


    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_MASSAGE ) ){
            return mMassage;
        }

        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mMassage, enabled);
	}

    /* PRIVATES */
    /**
     * Shows buttons in FullBodyLayout
     */
    private void showMassageZone( LinearLayout mainLayout ){
        LinearLayout.LayoutParams massageLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 1);
        massageLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout massageLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        massageLayout.setId(R.id.fullBodyLayoutId);

        super.showFixViewWeight(massageLayout, 1);

        mMassage = setZoneBtn( R.id.fullBodyMassageBtnId, massageLayout,
                3, R.string.simple_massage );

        super.showFixViewWeight(massageLayout, 1);

        mainLayout.addView(massageLayout, massageLayoutParameters);

    }

}
