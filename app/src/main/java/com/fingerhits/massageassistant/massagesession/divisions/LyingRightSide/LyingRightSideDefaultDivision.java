package com.fingerhits.massageassistant.massagesession.divisions.LyingRightSide;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * 
 * Controls LyingRightSide Default Division
 * 
 * @author Javier Torón
 * 
 * It has buttons for HeadNeck, LeftFlank, ArmLeft, LegLeft and LegRight.
 */
public class LyingRightSideDefaultDivision extends LyingRightSideDivisions {
	private Button mHeadNeck, mLeftFlank, mArmLeft, mLegLeft, mLegRight;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingRightSideDefaultDivision(MassageSessionActivity act,
                                         LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_DEFAULT;

    }

    /*Static methods*/
    public static String[] getCodZonesArray(){
        return new String[] { TreatedZoneRecord.CODZONE_ARMLEFT,
                TreatedZoneRecord.CODZONE_HEADNECK,
                TreatedZoneRecord.CODZONE_LEFTFLANK,
                TreatedZoneRecord.CODZONE_LEGLEFT,
                TreatedZoneRecord.CODZONE_LEGRIGHT };
    }




    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones(View v) {
        super.showFixLayout((LinearLayout) v, R.id.upperFixLayoutId, 1);
        showHeadNeckZones((LinearLayout) v);
        showArmFlankZones((LinearLayout) v);
        showLegsZones((LinearLayout) v);

    }


    public Divisions prevDivision(){

        clearZones();

        LyingRightSideDivisions newDivision =
                new LyingRightSideUpperLowerDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    public Divisions nextDivision(){

        clearZones();

        LyingRightSideDivisions newDivision =
                new LyingRightSideDetailedDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    /* OVERRIDES */
	@Override
	public String zoneTitle( Button btn ){
		if (btn == mHeadNeck ){
			return res.getString(R.string.headNeck_button);
		}
		if (btn == mLeftFlank ){
			return res.getString(R.string.leftFlank_button);
		}
		if (btn == mArmLeft ){
			return res.getString(R.string.armLeft_button);
		}
		if (btn == mLegLeft ){
			return res.getString(R.string.legLeft_button);
		}
		if (btn == mLegRight ){
			return res.getString(R.string.legRight_button);
		}

        return super.zoneTitle(btn);
	}


    @Override
    public String codZone( Button btn ){
        if (btn == mHeadNeck ){
            return TreatedZoneRecord.CODZONE_HEADNECK;
        }
        if (btn == mLeftFlank ){
            return TreatedZoneRecord.CODZONE_LEFTFLANK;
        }
        if (btn == mArmLeft ){
            return TreatedZoneRecord.CODZONE_ARMLEFT;
        }
        if (btn == mLegLeft ){
            return TreatedZoneRecord.CODZONE_LEGLEFT;
        }
        if (btn == mLegRight ){
            return TreatedZoneRecord.CODZONE_LEGRIGHT;
        }

        return super.codZone(btn);
    }

    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_HEADNECK ) ){
            return mHeadNeck;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTFLANK ) ){
            return mLeftFlank;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_ARMLEFT ) ){
            return mArmLeft;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEGLEFT ) ){
            return mLegLeft;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEGRIGHT ) ){
            return mLegRight;
        }

        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mHeadNeck, enabled);
        ZonesUtils.enableButton(res, mLeftFlank, enabled);
        ZonesUtils.enableButton(res, mArmLeft, enabled);
        ZonesUtils.enableButton(res, mLegLeft, enabled);
        ZonesUtils.enableButton(res, mLegRight, enabled);
	}

        /* PRIVATES */
    /**
     * Shows buttons in HeadNeckLayout
     */
    private void showHeadNeckZones( LinearLayout mainLayout ){
        mHeadNeck = super.setHeadNeckZones(mainLayout, 9);
    }

    /**
     * Shows buttons in ArmFlankLayout
     */
    private void showArmFlankZones( LinearLayout mainLayout ){
        LinearLayout.LayoutParams armFlankLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 14);
        armFlankLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout armFlankLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        armFlankLayout.setId(R.id.armFlankLayoutId);

        super.showFixViewWeight(armFlankLayout, 1);

        mArmLeft = setZoneBtn( R.id.armLeftMassageBtnId, armFlankLayout,
                2,R.string.armLeft_button );

        mLeftFlank = setZoneBtn( R.id.leftFlankMassageBtnId, armFlankLayout,
                2,R.string.leftFlank_button );

        super.showFixViewWeight(armFlankLayout, 1);

        mainLayout.addView(armFlankLayout, armFlankLayoutParameters);
    }

    /**
     * Shows buttons in LegsLayout
     */
    private void showLegsZones( LinearLayout mainLayout ){
        LinearLayout legsLayout = super.setLegsLayout();

        super.showFixViewWeight(legsLayout, 2);

        mLegLeft = setZoneBtn( R.id.legLeftMassageBtnId, legsLayout,
                3,R.string.legLeft_button );

        mLegRight = setZoneBtn( R.id.legRightMassageBtnId, legsLayout,
                3,R.string.legRight_button );

        super.showFixViewWeight(legsLayout, 2);

        mainLayout.addView(legsLayout);
    }

}
