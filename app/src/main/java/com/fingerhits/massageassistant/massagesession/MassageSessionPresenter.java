package com.fingerhits.massageassistant.massagesession;

import android.util.Log;
import android.view.View;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.massagesession.chronos.IMassageSessionChronosPresenter;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.positions.Position;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecordInvalidException;

import java.util.Date;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 13/03/2017.
 */

public class MassageSessionPresenter implements IMassageSessionPresenter {
    public static final Long MINIMAL_HARDCODED_ZONE_DURATION = 2L;
    public static final Long MINIMAL_HARDCODED_PAUSE_DURATION = 10L;

    public static final String DEFAULT_SIMPLE_MODE_POSITION =
            TreatedZoneRecord.CODPOSITION_UNDEFINED;
    public static final String DEFAULT_SIMPLE_MODE_DIVISION =
            Divisions.CODDIVISION_SINGLE;
    public static final String DEFAULT_SIMPLE_MODE_ZONE =
            TreatedZoneRecord.CODZONE_MASSAGE;

    public static final String STATE_NOT_STARTED = "NotStarted";
    public static final String STATE_WAITING_TO_SELECT_ZONE =
            "WaitingToSelectZone";
    public static final String STATE_RUNNING = "Running";
    public static final String STATE_PAUSED = "Paused";

    private IMassageSessionView view;
    private IMassageSessionChronosPresenter chronosPresenter;
    private IMassageSessionInteractor interactor;

    /*Screen status*/
    private String mState; //NotStarted, Running, Paused, WaitingToSelectZone
    private Position mPosition;
    private String mCurrentCodDivision = "";

    /*Massage data*/
    private MassageRecord mMassage;
    private long mIdMassage = -1;

    private Date mZoneStartDateTime;

    private int mMode;
    private boolean mModeSimple = false;
    private boolean mModeIntermediate = false;
    private boolean mModeAdvanced = false;


    @Inject public MassageSessionPresenter(IMassageSessionView view,
                               IMassageSessionChronosPresenter chronosPresenter,
                               IMassageSessionInteractor interactor) {
        this.view = view;
        this.chronosPresenter = chronosPresenter;
        this.interactor = interactor;
    }


    // ******************
    // PROPERTIES
    // ******************
    public String getState(){return mState;}
    public void setState(String pState){mState = pState;}

    public String getCurrentCodDivision(){return mCurrentCodDivision;}
    public void setCurrentCodDivision(String pCurrentCodDivision){mCurrentCodDivision = pCurrentCodDivision;}

    public Position getPosition(){return mPosition;}
    public void setPosition(Position position){mPosition = position;}

    public MassageRecord getMassage(){return mMassage;}
    public void setMassage(MassageRecord massage){
        mMassage = massage;

        if(massage == null) mIdMassage = -1L;
        else mIdMassage = massage.getId();
    }

    public Long getIdMassage(){return mIdMassage;}
    public void setIdMassage(Long idMassage) {
        mIdMassage = idMassage;
    }

    public int getMode(){return mMode;}
    public void setMode(int mode) {
        mMode = mode;
        switch (mode){
            case MassageRecord.MODE_SIMPLE:
                mModeSimple = true;
                mModeIntermediate = false;
                mModeAdvanced = false;
                break;

            case MassageRecord.MODE_INTERMEDIATE:
                mModeSimple = false;
                mModeIntermediate = true;
                mModeAdvanced = false;
                break;

            case MassageRecord.MODE_ADVANCED:
                mModeSimple = false;
                mModeIntermediate = false;
                mModeAdvanced = true;
                break;
        }
    }

    // ******************
    // Setup views (visibility...)
    // ******************
    public void initView() {
        view.manageScreenLock(interactor.getScreenLock());
    }


    public void setUpMenu() {
        view.toggleVolume(interactor.getSoundWarningsEnabled());
    }



    // **************
    // ONCLICK MENU *
    // **************
    public void goToOptions() {
        pauseMassage();
        view.goToOptionsActivity();
    }


    public void showMassageDetails(){
        pauseMassage();
        view.goToTreatedZones(mIdMassage);
    }


    public void muteClick() {
        if (interactor.getSoundWarningsEnabled()) {
            interactor.setSoundWarningsEnabled(false);
            view.toggleVolume(false);
        } else {
            interactor.setSoundWarningsEnabled(true);
            view.toggleVolume(true);
        }
        chronosPresenter.optionsChanged();
    }

    // **************
    // ONCLICK VIEW *
    // **************
    public void pauseMassage() {
        if (getState().equals(STATE_RUNNING)) {
            saveCurrentZoneAndDuration();

            setState(STATE_PAUSED); // Body zone containerLayout disabled,

            view.pause(interactor.getAnimationsDisabled());
            chronosPresenter.pauseChronometers();

            view.disableMassageControllers(getMode());
        }
    }


    public void resumeMassage() {
        view.resume();

        //TODO: We should be able to extract this to its own method
        savePauseDuration(view.getPausedTime());

        chronosPresenter.resumeChronometers();

        if(mModeAdvanced){
            startZoneMassage(mPosition.lastPressed());
        }
        else {
            mZoneStartDateTime = new Date();
        }

        view.enableMassageControllers(getMode());
        setState(STATE_RUNNING); // Body zone containerLayout enabled, pause button enabled,
        //	chronometers running, pausedText not visible
    }

    public void endMassage() {

        if (getState().equals(STATE_NOT_STARTED)) {
            view.showEndNotSaving();
        } else {
            saveCurrentZoneAndDuration();

            long duration = interactor.finishMassage(mIdMassage);

            view.showEndSaving();

            view.logEndMassage(duration, mMassage.getModeAbbr());
        }
    }

    /**
     * Common operations for changing mPosition
     *
     * @param p_codPosition To be showed
     * Called from selectPosition
     */
    public void changePosition(String p_codPosition) {
        writeLog("Massage -> ChangePosition from " + mPosition.getPositionName());

        if (getState().equals(STATE_RUNNING)) {
            saveCurrentZoneAndDuration();
        }

        if(mModeIntermediate){
            setPositionForIntermediateMode(p_codPosition, 0L);
        }

        if(mModeAdvanced){
            setPositionForAdvancedMode(false, p_codPosition, 0, "");
        }

        view.logChangePosition(mPosition.getPositionName(), mMassage.getModeAbbr());
    }


    public void changeDivision(String newCodDivision) {
        if (getState().equals(MassageSessionPresenter.STATE_RUNNING)) {
            saveCurrentZoneAndDuration();
        }

        setCurrentCodDivision(newCodDivision);

        mPosition.loadDivision(newCodDivision);

        divisionLoaded(false);
    }

    /**
     * Manages clicks on body zone button
     * <p/>
     * Save current time and pressed button, Disable new pressed button and
     * starts partial chrono
     * First run: show start time and starts full time chrono
     *
     * @param pressedBtn - Pressed body zone button
     */
    public void startZoneMassage(View pressedBtn) {

        if(getState().equals(STATE_WAITING_TO_SELECT_ZONE)
                && (!mCurrentCodDivision.equals( Divisions.CODDIVISION_SINGLE))){
            saveCurrentZoneAndDuration();
//            view.stopPause();
            chronosPresenter.setZoneChronoNormalColor();
//            setState(STATE_RUNNING);
        }
        else {
            if ((getState().equals(STATE_RUNNING))
                    && (!mCurrentCodDivision.equals( Divisions.CODDIVISION_SINGLE))) {
                saveCurrentZoneAndDuration();
            }
        }

        mPosition.pressButtonInDivision(pressedBtn);

        chronosPresenter.startZoneChrono(pressedBtn, getState());
        mZoneStartDateTime = new Date();

        setState(STATE_RUNNING);
    }



    // **************
    // BACK FROM ACTIVITIES
    // **************
    /*Back from NewMassage*/
    public void startMassage(int mode, String codPosition) {
        setMassage( interactor.createMassage(mode));
        preparePresenter(mMassage);

        if (mModeSimple) {
            prepareForSimpleMode();
        }

        if (mModeIntermediate) {
            prepareForIntermediateMode(codPosition, 0L);
        }

        if (mModeAdvanced) {
            prepareForAdvancedMode(codPosition, 0L,
                    Divisions.CODDIVISION_SINGLE);
        }

        view.logNewMassage(mIdMassage, mMassage.getModeAbbr(),
                interactor.getMassageDurationInMinutes());
    }



    public void restartMassage(Long idMassage) {

        if(idMassage == -1) {
            view.errorResumingMassage();
            return;
        }
        setMassage(interactor.getMassage(idMassage));

        if (mMassage == null) {
            view.errorResumingMassage();
            return;
        }
        preparePresenter(mMassage);

        if(mModeSimple){
            prepareForSimpleMode();
        }
        else{
            TreatedZoneRecord tz =
                    interactor.getLastTreatedZone(mIdMassage);
            if (tz != null) {
                String firstCodPosition = tz.getCodPosition();
                long durationInLastPosition =
                        interactor.getDurationInLastPosition(mIdMassage) * 1000;

                if(mModeIntermediate) {
                    prepareForIntermediateMode(firstCodPosition,
                            durationInLastPosition );
                }
                else{
                    String firstCodDivision = tz.getCodDivision();

                    prepareForAdvancedMode(firstCodPosition,
                            durationInLastPosition, firstCodDivision);
                }
            }

        }
        view.logResumeMassage(mIdMassage, mMassage.getModeAbbr());
    }



    /*Back from Options*/
    public void optionsChanged() {
        initView();
        setUpMenu();
        resumeIfPaused();
        chronosPresenter.optionsChanged();
    }


    /*Back from TreatedZones*/
    public void resumeIfPaused(){
        if (getState().equals(STATE_PAUSED)) resumeMassage();
    }




    // *****************
	// PRIVATE FUNCTIONS
	// *****************
    // ******************
    // Setup views (visibility...)
    // ******************

    private void preparePresenter(MassageRecord massage) {
        setState(STATE_NOT_STARTED);

        setIdMassage( mMassage.getId() );
        setMode(mMassage.getMode());

        chronosPresenter.prepareMode(getMode(),
                mMassage.getDurationInMilliseconds());

    }

    private void prepareForSimpleMode() {
        view.hideSummaryMenu();
        view.prepareScreenForSimpleMode();

        mZoneStartDateTime = new Date();
        setState( STATE_RUNNING );
    }


    private void prepareForIntermediateMode(String codPosition,
                                            long positionDurationInMilliseconds) {
        view.prepareScreenForIntermediateMode(codPosition);

        setPositionForIntermediateMode(codPosition,
                positionDurationInMilliseconds);
    }

    private void setPositionForIntermediateMode(String codPosition,
                                            long positionDurationInMilliseconds) {
        setPosition( view.loadIntermediatePosition(mCurrentCodDivision,
                codPosition));

        chronosPresenter.loadIntermediatePosition(codPosition,
                view.getFullBodyBtnLabel(), positionDurationInMilliseconds);
        mZoneStartDateTime = new Date();
        setState( STATE_RUNNING);
    }

    private void prepareForAdvancedMode(String codPosition,
                                        long positionDurationInMilliseconds,
                                        String codDivision) {
        view.prepareScreenForAdvancedMode(codPosition, codDivision);

        setPositionForAdvancedMode(true, codPosition,
                positionDurationInMilliseconds, codDivision);
    }

    private void setPositionForAdvancedMode(boolean firstPosition,
                                        String codPosition,
                                        long positionDurationInMilliseconds,
                                        String codDivision) {
        setPosition( Position.getPosition(
                (MassageSessionActivity) view,
                codPosition));

        if(firstPosition) {
            mCurrentCodDivision = codDivision;
            chronosPresenter.startFullChronoIn(mMassage.getDurationInMilliseconds());
        }
        else{
            codDivisionForNewPosition(codDivision);
        }

        startPositionChronoForAdvancedMode( positionDurationInMilliseconds);

        mPosition.loadDivision(mCurrentCodDivision);

        chronosPresenter.changePositionChronoText(mPosition.getCodPosition());

        divisionLoaded(true);
    }


    private void startPositionChronoForAdvancedMode(
            long positionDurationInMilliseconds) {
        chronosPresenter.startPositionChronoIn(positionDurationInMilliseconds);

        if(!mCurrentCodDivision.equals( Divisions.CODDIVISION_SINGLE ) ) {
            chronosPresenter.blinkZoneChrono();

            view.enablePause(false);

            setState( STATE_WAITING_TO_SELECT_ZONE);
        }
    }


    private void codDivisionForNewPosition(String codDivision) {
        if (codDivision.equals("")) {
            if (mCurrentCodDivision.equals("")) {
                mCurrentCodDivision = Divisions.CODDIVISION_SINGLE;
            } else {
                if (!mPosition.hasDivision(mCurrentCodDivision)) {
                    mCurrentCodDivision = Divisions.CODDIVISION_SINGLE;
                }
            }
        } else {
            if (mPosition.hasDivision(codDivision)) {
                mCurrentCodDivision = codDivision;
            } else {
                mCurrentCodDivision = Divisions.CODDIVISION_SINGLE;
            }
        }
    }


    private void divisionLoaded( boolean newPosition ) {
        writeLog("divisionLoaded(). mCurrentCodDivision: " +
                mCurrentCodDivision + " )");

        view.changeDisabledDivision(mPosition.getCodPosition(), mCurrentCodDivision);
        chronosPresenter.divisionChangedTo(mCurrentCodDivision);


        if (mCurrentCodDivision.equals( Divisions.CODDIVISION_SINGLE)) {
            startZoneMassage(view.getFullBodyBtn());
        } else {
            if(( !newPosition && !getState().equals( STATE_NOT_STARTED ))
                    || getMode() == MassageRecord.MODE_ADVANCED) {
                //view.startPause();

                chronosPresenter.blinkZoneChrono();

                setState( STATE_WAITING_TO_SELECT_ZONE );
            }
        }
    }

    private void discardZone(){
        String discarded_zone = "";

        try {
            discarded_zone = mPosition.codZone(
                    mPosition.lastPressed());
        } catch (Exception e) {
            discarded_zone = "No";
        }

        view.logDiscardZone(mIdMassage, discarded_zone, mMassage.getModeAbbr());
    }

    /* We are discarding pauses of less than 10 seconds, to avoid "noise" */
    private void savePauseDuration(long pausedTime){
        int pausedTimeInSeconds = (int) (pausedTime / 1000);
        int secondsToDiscardZone = interactor.getMinimalDurationInSeconds();

        if(pausedTimeInSeconds > MINIMAL_HARDCODED_PAUSE_DURATION) {
            if (pausedTimeInSeconds > secondsToDiscardZone) {
                String codPosition, codDivision;
                if(mModeSimple){
                    codPosition = DEFAULT_SIMPLE_MODE_POSITION;
                    codDivision = DEFAULT_SIMPLE_MODE_DIVISION;
                }else{
                    codPosition = mPosition.getCodPosition();
                    codDivision = mPosition.getCodDivision();
                }

                writeLog("Massage -> Inserting pause " +
                        String.valueOf(pausedTime / 1000));

                try {
                    interactor.insertTreatedZone(mIdMassage, codPosition,
                            codDivision, TreatedZoneRecord.CODZONE_PAUSE,
                            mZoneStartDateTime, pausedTimeInSeconds);
                } catch (TreatedZoneRecordInvalidException e) {
                    writeLog("Massage -> Error inserting pause zone " +
                            e.toString());
                    view.sendExceptionTracker(e);

                }

                view.resetPausedTime();
            }
        }
    }

    private void saveCurrentZoneAndDuration() {
        int secondsToDiscardZone = interactor.getMinimalDurationInSeconds();

        writeLog("Massage -> Enter Save");

        Long elapsedTime = chronosPresenter.getZoneChronoElapsedTime(getState());
        long elapsedTimeInSeconds = elapsedTime / 1000;

        if( mModeSimple || (mPosition.lastPressed() != null) ) {
            if (elapsedTimeInSeconds > MINIMAL_HARDCODED_ZONE_DURATION) {
                if (secondsToDiscardZone == 0
                        || (int) elapsedTimeInSeconds > secondsToDiscardZone) {
                    if(mModeSimple){
                        saveForSimpleMode(elapsedTimeInSeconds);
                    }
                    else {
                        saveForIntermediateAdvancedMode(elapsedTimeInSeconds);
                    }
                } else {
                    discardZone();
                }
            }
        }
        else{
            if(getState().equals(STATE_WAITING_TO_SELECT_ZONE)){
                saveUnselectedZone(elapsedTimeInSeconds);
            }
        }
    }


    private void saveForSimpleMode(long elapsedTimeInSeconds) {
        try {
            interactor.insertTreatedZone(mIdMassage,
                    DEFAULT_SIMPLE_MODE_POSITION, DEFAULT_SIMPLE_MODE_DIVISION,
                    DEFAULT_SIMPLE_MODE_ZONE, mZoneStartDateTime,
                    elapsedTimeInSeconds);

            writeLog("Massage simple -> savedZone " + String.valueOf(mIdMassage)
                    + ". Zone: " + TreatedZoneRecord.CODZONE_MASSAGE);
        } catch (TreatedZoneRecordInvalidException e) {
            writeLog("Massage -> Error inserting time for simple massage. " +
                                e.toString());
            view.sendExceptionTracker(e);
        }
    }


    private void saveForIntermediateAdvancedMode(long elapsedTimeInSeconds) {
        try {
            String zone =
                    mPosition.codZone(mPosition.lastPressed());
            interactor.insertTreatedZone(mIdMassage,
                    mPosition.getCodPosition(),
                    mPosition.getCodDivision(), zone,
                    mZoneStartDateTime,
                    elapsedTimeInSeconds);
            writeLog( "Massage -> savedZone " + String.valueOf(mIdMassage) +
                    ". Zone: " + zone);
        } catch (TreatedZoneRecordInvalidException e) {
            writeLog( "Massage -> Error inserting zone " + e.toString());
            view.sendExceptionTracker(e);
        }
    }

    private void saveUnselectedZone(long elapsedTimeInSeconds) {
        try {
            String zone = TreatedZoneRecord.CODZONE_UNSELECTED;
            interactor.insertTreatedZone(mIdMassage,
                    mPosition.getCodPosition(),
                    mPosition.getCodDivision(), zone,
                    mZoneStartDateTime,
                    elapsedTimeInSeconds);
            writeLog( "Massage -> saved Undefined Zone " + String.valueOf(mIdMassage) +
                    ". Zone: " + zone);
        } catch (TreatedZoneRecordInvalidException e) {
            writeLog( "Massage -> Error inserting Undefined  zone " + e.toString());
            view.sendExceptionTracker(e);
        }
    }


    private void writeLog(String message){
        if (BuildConfig.DEBUG) {
            Log.d(IMassageSessionView.NAME, message);
        }
    }

}