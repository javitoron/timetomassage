package com.fingerhits.massageassistant.massagesession.divisions;

import android.view.View;
import android.widget.ImageView;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Javier Torón on 28/04/2016.
 */
public class MassageSessionDivisionSelector {
    private MassageSessionActivity mActivity;

    @BindView(R.id.divisionSingleBtn) ImageView mDivisionSingleBtn;
    @BindView(R.id.divisionLeftRightBtn) ImageView mDivisionLeftRightBtn;
    @BindView(R.id.divisionUpperLowerBtn) ImageView mDivisionUpperLowerBtn;
    @BindView(R.id.divisionDefaultBtn) ImageView mDivisionDefaultBtn;
    @BindView(R.id.divisionDetailedBtn) ImageView mDivisionDetailedBtn;

    public MassageSessionDivisionSelector(MassageSessionActivity activity){
        mActivity = activity;

        ButterKnife.bind(this, this.mActivity);
    }


    public void showHideAll(boolean show) {
        int visibility;

        if(show) visibility = View.VISIBLE;
        else visibility = View.GONE;

        mDivisionSingleBtn.setVisibility(visibility);
        mDivisionLeftRightBtn.setVisibility(visibility);
        mDivisionUpperLowerBtn.setVisibility(visibility);
        mDivisionDefaultBtn.setVisibility(visibility);
        mDivisionDetailedBtn.setVisibility(visibility);
    }


    public void enableAll(String codPosition) {

        mDivisionSingleBtn.setEnabled(true);
        mDivisionSingleBtn.setImageResource(R.drawable.divisions_single);

        if( (codPosition.equals(TreatedZoneRecord.CODPOSITION_LEFTSIDE))
                || (codPosition.equals(TreatedZoneRecord.CODPOSITION_RIGHTSIDE))) {
            mDivisionLeftRightBtn.setVisibility(View.GONE);
        }
        else{
            mDivisionLeftRightBtn.setVisibility(View.VISIBLE);
            mDivisionLeftRightBtn.setEnabled(true);
            mDivisionLeftRightBtn.setImageResource(R.drawable.divisions_left_right);
        }

        if( (codPosition.equals(TreatedZoneRecord.CODPOSITION_SITTINGBACK))
                || (codPosition.equals(TreatedZoneRecord.CODPOSITION_SITTINGFRONT))) {
            mDivisionUpperLowerBtn.setVisibility(View.GONE);
        }
        else{
            mDivisionUpperLowerBtn.setVisibility(View.VISIBLE);
            mDivisionUpperLowerBtn.setEnabled(true);
            mDivisionUpperLowerBtn.setImageResource(R.drawable.divisions_upper_lower);
        }

        mDivisionDefaultBtn.setEnabled(true);
        mDivisionDefaultBtn.setImageResource(R.drawable.divisions_default);
        mDivisionDetailedBtn.setEnabled(true);
        mDivisionDetailedBtn.setImageResource(R.drawable.divisions_detailed);
    }


    public void disableDivisionBtn( String codDivision) {

        switch (codDivision) {
            case Divisions.CODDIVISION_SINGLE:
                mDivisionSingleBtn.setEnabled(false);
                mDivisionSingleBtn.setImageResource(
                        R.drawable.divisions_single_pressed);
                break;

            case Divisions.CODDIVISION_LEFTRIGHT:
                mDivisionLeftRightBtn.setEnabled(false);
                mDivisionLeftRightBtn.setImageResource(
                        R.drawable.divisions_left_right_pressed);
                break;

            case Divisions.CODDIVISION_UPPERLOWER:
                mDivisionUpperLowerBtn.setEnabled(false);
                mDivisionUpperLowerBtn.setImageResource(
                        R.drawable.divisions_upper_lower_pressed);
                break;

            case Divisions.CODDIVISION_DEFAULT:
                mDivisionDefaultBtn.setEnabled(false);
                mDivisionDefaultBtn.setImageResource(
                        R.drawable.divisions_default_pressed);
                break;

            case Divisions.CODDIVISION_DETAILED:
                mDivisionDetailedBtn.setEnabled(false);
                mDivisionDetailedBtn.setImageResource(
                        R.drawable.divisions_detailed_pressed);
                break;

        }
    }

    /**
     * Changes to the previous mDivision
     *
     * @param view - Not used. It comes from ImageButton prevDivisionBtn
     */
/*    public void prevDivision(@SuppressWarnings(
            {"UnusedParameters", "SameParameterValue"}) View view) {

        if (!mState.equals(STATE_PAUSED)) {
            prepareToChangeDivision();

            mPosition.mDivision = mPosition.mDivision.prevDivision();
            currentCodDivision = mPosition.mDivision.codDivision;

        }
        loadDurationsFullOrDivisionOnly();
    }

*/
    /**
     * Changes to the next mDivision
     *
     * @param view - Not used. It comes from ImageButton nextDivisionBtn
     */
/*    public void nextDivision(@SuppressWarnings(
            {"UnusedParameters", "SameParameterValue"}) View view) {

        if (!mState.equals(STATE_PAUSED)) {
            prepareToChangeDivision();

            mPosition.mDivision = mPosition.mDivision.nextDivision();
            currentCodDivision = mPosition.mDivision.codDivision;
        }
        loadDurationsFullOrDivisionOnly();

    }
*/

}
