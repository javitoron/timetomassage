package com.fingerhits.massageassistant.massagesession.divisions.LyingRightSide;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * 
 * Controls LyingLeft Detailed Division
 * 
 * @author Javier Torón
 * 
 * It has buttons for Head, LeftPectoralGirdle, LeftBiceps, LeftForearm,
 *      LeftHand, LeftFlank, LeftHip, LeftThigh, LeftShin, LeftFoot.
 */
public class LyingRightSideDetailedDivision extends LyingRightSideDivisions {

	private Button mHead, mLeftPectoralGirdle, mLeftBiceps, mLeftForearm,
            mLeftHand, mLeftFlank, mLeftHip, mLeftThigh, mLeftShin,
            mLeftFoot;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingRightSideDetailedDivision(MassageSessionActivity act,
                                          LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_DETAILED;
	}



    /*Static methods*/
    public static String[] getCodZonesArray(){
        return new String[] { TreatedZoneRecord.CODZONE_HEAD,
                TreatedZoneRecord.CODZONE_LEFTBICEPS,
                TreatedZoneRecord.CODZONE_LEFTFLANK,
                TreatedZoneRecord.CODZONE_LEFTFOOT,
                TreatedZoneRecord.CODZONE_LEFTFOREARM,
                TreatedZoneRecord.CODZONE_LEFTHAND,
                TreatedZoneRecord.CODZONE_LEFTHIP,
                TreatedZoneRecord.CODZONE_LEFTPECTORALGIRDLE,
                TreatedZoneRecord.CODZONE_LEFTSHIN,
                TreatedZoneRecord.CODZONE_LEFTTHIGH };
    }


    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones( View v ){
        showHeadZones((LinearLayout) v);
        showPectoralGirdleLayout((LinearLayout) v);
        showFullBodyZones((LinearLayout) v);
    }


    public Divisions prevDivision(){
        clearZones();

        LyingRightSideDivisions newDivision =
                new LyingRightSideDefaultDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    public Divisions nextDivision(){

        clearZones();

        LyingRightSideDivisions newDivision =
                new LyingRightSideSingleDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }

    /* OVERRIDES */
    @Override
    public String zoneTitle( Button btn ){
        if (btn == mHead ){
            return res.getString(R.string.head_button);
        }

        if (btn == mLeftPectoralGirdle ){
            return res.getString(R.string.leftPectoralGirdle_button);
        }

        if (btn == mLeftBiceps ){
            return res.getString(R.string.leftBiceps_button);
        }

        if (btn == mLeftForearm ){
            return res.getString(R.string.leftForearm_button);
        }

        if (btn == mLeftHand ){
            return res.getString(R.string.leftHand_button);
        }

        if (btn == mLeftFlank ){
            return res.getString(R.string.leftFlank_button);
        }

        if (btn == mLeftHip ){
            return res.getString(R.string.leftHip_button);
        }

        if (btn == mLeftThigh ){
            return res.getString(R.string.leftThigh_button);
        }

        if (btn == mLeftShin ){
            return res.getString(R.string.leftShin_button);
        }

        if (btn == mLeftFoot ){
            return res.getString(R.string.leftFoot_button);
        }

        return super.zoneTitle(btn);
    }


    @Override
    public String codZone( Button btn ){
        if (btn == mHead ){
            return TreatedZoneRecord.CODZONE_HEAD;
        }

        if (btn == mLeftPectoralGirdle){
            return TreatedZoneRecord.CODZONE_LEFTPECTORALGIRDLE;
        }

        if (btn == mLeftBiceps){
            return TreatedZoneRecord.CODZONE_LEFTBICEPS;
        }

        if (btn == mLeftForearm){
            return TreatedZoneRecord.CODZONE_LEFTFOREARM;
        }

        if (btn == mLeftHand){
            return TreatedZoneRecord.CODZONE_LEFTHAND;
        }

        if (btn == mLeftFlank){
            return TreatedZoneRecord.CODZONE_LEFTFLANK;
        }

        if (btn == mLeftHip){
            return TreatedZoneRecord.CODZONE_LEFTHIP;
        }

        if (btn == mLeftThigh){
            return TreatedZoneRecord.CODZONE_LEFTTHIGH;
        }

        if (btn == mLeftShin){
            return TreatedZoneRecord.CODZONE_LEFTSHIN;
        }

        if (btn == mLeftFoot){
            return TreatedZoneRecord.CODZONE_LEFTFOOT;
        }

        return super.codZone(btn);
    }


    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_HEAD ) ){
            return mHead;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTPECTORALGIRDLE ) ){
            return mLeftPectoralGirdle;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTBICEPS ) ){
            return mLeftBiceps;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTFOREARM ) ){
            return mLeftForearm;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTHAND ) ){
            return mLeftHand;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTFLANK ) ){
            return mLeftFlank;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTHIP ) ){
            return mLeftHip;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTTHIGH ) ){
            return mLeftThigh;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTSHIN ) ){
            return mLeftShin;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTFOOT ) ){
            return mLeftFoot;
        }

        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mHead, enabled);
        ZonesUtils.enableButton(res, mLeftPectoralGirdle, enabled);
        ZonesUtils.enableButton(res, mLeftBiceps, enabled);
        ZonesUtils.enableButton(res, mLeftForearm, enabled);
        ZonesUtils.enableButton(res, mLeftHand, enabled);
        ZonesUtils.enableButton(res, mLeftFlank, enabled);
        ZonesUtils.enableButton(res, mLeftHip, enabled);
        ZonesUtils.enableButton(res, mLeftThigh, enabled);
        ZonesUtils.enableButton(res, mLeftShin, enabled);
        ZonesUtils.enableButton(res, mLeftFoot, enabled);
	}

    /* PRIVATES */
    /**
     * Shows buttons in HeadLayout
     */
    private void showHeadZones( LinearLayout mainLayout ){
        mHead = super.setHeadZones(mainLayout, 4);
    }

    /**
     * Shows buttons in PectoralGirdleLayout
     */
    private void showPectoralGirdleLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams pectoralGirdleLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 3);
        pectoralGirdleLayoutParameters.setMargins(0,
                GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout pectoralGirdleLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        pectoralGirdleLayout.setId(R.id.pectoralGirdleLayoutId);

        super.showFixViewWeight(pectoralGirdleLayout, 1);

        mLeftPectoralGirdle = setZoneBtn(
                R.id.leftPectoralGirdleMassageBtnId,
                pectoralGirdleLayout, 4,
                R.string.leftPectoralGirdle_button );

        super.showFixViewWeight(pectoralGirdleLayout, 1);

        mainLayout.addView(pectoralGirdleLayout,
                pectoralGirdleLayoutParameters);

    }


    /**
     * Shows buttons in fullBodyLayout
     */
    private void showFullBodyZones( LinearLayout mainLayout ){
        LinearLayout.LayoutParams fullBodyLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 25);
        fullBodyLayoutParameters.setMargins(0,
                GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout fullBodyLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        fullBodyLayout.setId(R.id.fullBodyLayoutId);

        super.showFixViewWeight(fullBodyLayout, 2);

        setLeftArmLayout(fullBodyLayout);
        setTrunkLegsLayout(fullBodyLayout);

        super.showFixViewWeight(fullBodyLayout, 2);

        mainLayout.addView(fullBodyLayout, fullBodyLayoutParameters);
    }


    private void setTrunkLegsLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams trunkLegsLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 7);
        trunkLegsLayoutParameters.setMargins(
                GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout trunkLegsLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical,
                        null);
        trunkLegsLayout.setId(R.id.trunkLegsLayoutId);


        mLeftBiceps = setZoneBtnVertical(R.id.leftBicepsMassageBtnId,
                trunkLegsLayout, 3, R.string.leftBiceps_button);

        mLeftFlank = setZoneBtnVertical(R.id.leftFlankMassageBtnId,
                trunkLegsLayout, 6, R.string.leftFlank_button);

        mLeftHip = setZoneBtnVertical(R.id.leftHipMassageBtnId,
                trunkLegsLayout, 6, R.string.leftHip_button);


        mLeftThigh = setZoneBtnVertical(R.id.leftThighMassageBtnId,
                trunkLegsLayout, 8, R.string.leftThigh_button);

        mLeftShin = setZoneBtnVertical(R.id.leftShinMassageBtnId,
                trunkLegsLayout, 8, R.string.leftShin_button);

        mLeftFoot = setZoneBtnVertical(R.id.leftFootMassageBtnId,
                trunkLegsLayout, 6, R.string.leftFoot_button);


        mainLayout.addView(trunkLegsLayout, trunkLegsLayoutParameters);
    }


    private void setLeftArmLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams leftArmLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 6);
        leftArmLayoutParameters.setMargins(GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout leftArmLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical, null);
        leftArmLayout.setId(R.id.leftArmLayoutId);

        setHorizontalLeftArmLayout(leftArmLayout);

        super.showFixViewWeight(leftArmLayout, 6);

        mainLayout.addView(leftArmLayout, leftArmLayoutParameters);
    }


    private void setHorizontalLeftArmLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams horizontalLeftArmLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 2);
        horizontalLeftArmLayoutParameters.setMargins(
                GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout horizontalLeftArmLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        horizontalLeftArmLayout.setId(R.id.horizontalLeftArmLayoutId);

        mLeftHand = setZoneBtn(R.id.leftHandMassageBtnId,
                horizontalLeftArmLayout, 3, R.string.leftHand_button);

        mLeftForearm = setZoneBtn(R.id.leftForearmMassageBtnId,
                horizontalLeftArmLayout, 2, R.string.leftForearm_button);

        mainLayout.addView(horizontalLeftArmLayout,
                horizontalLeftArmLayoutParameters);
    }

}
