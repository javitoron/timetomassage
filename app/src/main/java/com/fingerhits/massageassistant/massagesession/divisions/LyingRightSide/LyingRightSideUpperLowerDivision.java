package com.fingerhits.massageassistant.massagesession.divisions.LyingRightSide;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

/**
 * 
 * Controls LyingRightSide UpperLower Division
 * 
 * @author Javier Torón
 * 
 * It has buttons for Head, UpperBody and LowerBody.
 */
public class LyingRightSideUpperLowerDivision extends LyingRightSideDivisions {

	private Button mHead, mUpperBody, mLowerBody;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingRightSideUpperLowerDivision(MassageSessionActivity act,
                                            LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_UPPERLOWER;
	}


    /*Static methods*/
    public static String[] getCodZonesArray(){
        return new String[] { TreatedZoneRecord.CODZONE_HEAD,
                TreatedZoneRecord.CODZONE_LOWERBODY,
                TreatedZoneRecord.CODZONE_UPPERBODY };
    }



    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones( View v ){
        super.showFixLayout((LinearLayout) v, R.id.upperFixLayoutId, 1);
        showHeadZones((LinearLayout) v);
        showUpperBodyZones((LinearLayout) v);
        showLowerBodyZones((LinearLayout) v);
    }


    public Divisions prevDivision(){
        clearZones();

        LyingRightSideDivisions newDivision =
                new LyingRightSideSingleDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    public Divisions nextDivision(){

        clearZones();

        LyingRightSideDivisions newDivision =
                new LyingRightSideDefaultDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }

    /* OVERRIDES */
    @Override
    public String zoneTitle( Button btn ){
        if (btn == mHead ){
            return res.getString(R.string.head_button);
        }
        if (btn == mUpperBody ){
            return res.getString(R.string.upperBody_button);
        }
        if (btn == mLowerBody ){
            return res.getString(R.string.lowerBody_button);
        }

        return super.zoneTitle(btn);
    }


    @Override
    public String codZone( Button btn ){
        if (btn == mHead ){
            return TreatedZoneRecord.CODZONE_HEAD;
        }
        if (btn == mUpperBody ){
            return TreatedZoneRecord.CODZONE_UPPERBODY;
        }
        if (btn == mLowerBody ){
            return TreatedZoneRecord.CODZONE_LOWERBODY;
        }

        return super.codZone(btn);
    }


    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_HEAD ) ){
            return mHead;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_UPPERBODY ) ){
            return mUpperBody;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LOWERBODY ) ){
            return mLowerBody;
        }


        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mHead, enabled);
        ZonesUtils.enableButton(res, mUpperBody, enabled);
        ZonesUtils.enableButton(res, mLowerBody, enabled);
	}

    /* PRIVATES */
    /**
     * Shows buttons in HeadNeckLayout
     */
    private void showHeadZones( LinearLayout mainLayout ){
        mHead = super.setHeadZones(mainLayout, 8);
    }

    /**
     * Shows buttons in upperBodyLayout
     */
    private void showUpperBodyZones( LinearLayout mainLayout ){
        mUpperBody = setUpperBodyZones(mainLayout, 16);
    }

    /**
     * Shows buttons in lowerBodyLayout
     */
    private void showLowerBodyZones( LinearLayout mainLayout ){
        mLowerBody = setLowerBodyZones(mainLayout, 34, 1 );
    }
    
}
