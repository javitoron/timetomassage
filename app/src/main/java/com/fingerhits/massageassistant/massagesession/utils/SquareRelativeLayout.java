package com.fingerhits.massageassistant.massagesession.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

/**
 * Created by Javier Torón on 04/05/2016.
 */
public class SquareRelativeLayout extends RelativeLayout {
    LayoutInflater mInflater;

    public SquareRelativeLayout(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);
    }

    public SquareRelativeLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        mInflater = LayoutInflater.from(context);
    }
    public SquareRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

}
