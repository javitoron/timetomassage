package com.fingerhits.massageassistant.massagesession.divisions.LyingRightSide;

import android.view.View;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;

/**
 * 
 * Groups LyingRightSide Divisions
 * 
 * @author Javier Torón
 * 
 */
abstract public class LyingRightSideDivisions extends Divisions {

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingRightSideDivisions(MassageSessionActivity act,
                                   LinearLayout p_containerLayout) {

        super(act, p_containerLayout);

    }


    public static LyingRightSideDivisions getDivision( MassageSessionActivity act,
                                                       LinearLayout p_containerLayout,
                                                       String codDivision ){
        if( codDivision != null ) {
            if (codDivision.equals(Divisions.CODDIVISION_SINGLE)) {
                return new LyingRightSideSingleDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_UPPERLOWER)) {
                return new LyingRightSideUpperLowerDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DEFAULT)) {
                return new LyingRightSideDefaultDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DETAILED)) {
                return new LyingRightSideDetailedDivision(act, p_containerLayout);
            }
        }
        return new LyingRightSideSingleDivision(act, p_containerLayout);
    }


    public static String[] getCodZonesArray(String codDivision) {
        switch (codDivision) {
            case Divisions.CODDIVISION_DEFAULT:
                return LyingRightSideDefaultDivision.getCodZonesArray();

            case Divisions.CODDIVISION_DETAILED:
                return LyingRightSideDetailedDivision.getCodZonesArray();

            case Divisions.CODDIVISION_UPPERLOWER:
                return LyingRightSideUpperLowerDivision.getCodZonesArray();

            default:
                return new String[]{};
        }
    }



    /* ABSTRACT METHODS */
    public abstract void showZones( View v );
    public abstract Divisions prevDivision();
    public abstract Divisions nextDivision();


    /* OVERRIDES */

}
