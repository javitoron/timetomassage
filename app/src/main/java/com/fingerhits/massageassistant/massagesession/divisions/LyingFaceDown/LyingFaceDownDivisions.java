package com.fingerhits.massageassistant.massagesession.divisions.LyingFaceDown;

import android.view.View;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;

/**
 * 
 * Groups LyingFaceDown Divisions
 * 
 * @author Javier Torón
 * 
 */
abstract public class LyingFaceDownDivisions extends Divisions {

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingFaceDownDivisions(MassageSessionActivity act,
                                  LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
	}

    /*Static methods*/

    public static LyingFaceDownDivisions getDivision( MassageSessionActivity act,
                                                      LinearLayout p_containerLayout,
                                                      String codDivision ){
        if( codDivision != null ) {
            if (codDivision.equals(Divisions.CODDIVISION_SINGLE)) {
                return new LyingFaceDownSingleDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_LEFTRIGHT)) {
                return new LyingFaceDownLeftRightDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_UPPERLOWER)) {
                return new LyingFaceDownUpperLowerDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DEFAULT)) {
                return new LyingFaceDownDefaultDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DETAILED)) {
                return new LyingFaceDownDetailedDivision(act, p_containerLayout);
            }
        }
        return new LyingFaceDownSingleDivision(act, p_containerLayout);
    }

    public static String[] getCodZonesArray(String codDivision){
        switch(codDivision) {
            case Divisions.CODDIVISION_DEFAULT:
                return LyingFaceDownDefaultDivision.getCodZonesArray();

            case Divisions.CODDIVISION_DETAILED:
                return LyingFaceDownDetailedDivision.getCodZonesArray();

            case Divisions.CODDIVISION_LEFTRIGHT:
                return LyingFaceDownLeftRightDivision.getCodZonesArray();

            case Divisions.CODDIVISION_UPPERLOWER:
                return LyingFaceDownUpperLowerDivision.getCodZonesArray();

            default:
                return new String[] {};
        }

    }



    /* ABSTRACT METHODS */
    public abstract void showZones( View v );
    public abstract Divisions prevDivision();
    public abstract Divisions nextDivision();


    /* OVERRIDES */
}
