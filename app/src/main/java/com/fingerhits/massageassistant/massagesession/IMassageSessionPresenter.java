package com.fingerhits.massageassistant.massagesession;

import android.view.View;

/**
 * Created by Javier Torón on 13/03/2017.
 */

public interface IMassageSessionPresenter {
    void initView();
    void setUpMenu();

    void startMassage(int mode, String codPosition);
    void restartMassage(Long idMassage);
    void pauseMassage();
    void resumeMassage();
    void endMassage();

    void startZoneMassage(View pressedBtn);
    void changePosition(String codPosition);
    void changeDivision(String newCodDivision);

    void muteClick();
    void resumeIfPaused();

    void showMassageDetails();
    void goToOptions();
    void optionsChanged();


 }
