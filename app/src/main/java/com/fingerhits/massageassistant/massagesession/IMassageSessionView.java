package com.fingerhits.massageassistant.massagesession;

import android.view.View;

import com.fingerhits.massageassistant.massagesession.positions.Position;

/**
 * Created by Javier Torón on 13/03/2017.
 */

public interface IMassageSessionView {
    static String NAME = "MassageSession";

    void goToTreatedZones(Long idMassage);
    void manageScreenLock(boolean screenLock);
    View getFullBodyBtn();
    String getFullBodyBtnLabel();

    long getPausedTime();
    void pause(boolean animationsDisabled);
    void resume();
    void resetPausedTime();
    void enablePause(boolean enable);
    void stopPause();
    void startPause();


    void hideSummaryMenu();
    void toggleVolume(boolean isOn);
    void errorResumingMassage();
    void showEndNotSaving();
    void showEndSaving();
    void goToOptionsActivity();
    void logResumeMassage(Long idMassage, String mode);
    void logNewMassage(Long idMassage, String mode, int durationInMinutes);
    void logDiscardZone(Long idMassage, String discardedZone,
                               String mode);
    void logEndMassage(long duration, String mode);
    void logChangePosition(String positionName, String mode);
    void sendExceptionTracker(Exception e);

    void prepareScreenForSimpleMode();
    public void prepareScreenForIntermediateMode(String firstCodPosition);
    public void prepareScreenForAdvancedMode(String firstCodPosition,
                                             String firstCodDivision);

    Position loadIntermediatePosition(String currentCodDivision,
                                      String codPosition);
    void changeDisabledDivision(String codPosition, String codDivision);

    void enableMassageControllers(int mode);
    void disableMassageControllers(int mode);


    }
