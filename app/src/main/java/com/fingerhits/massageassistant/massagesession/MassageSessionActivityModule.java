package com.fingerhits.massageassistant.massagesession;

import com.fingerhits.massageassistant.di.ActivityScope;
import com.fingerhits.massageassistant.di.AppModule;
import com.fingerhits.massageassistant.massagesession.chronos.IMassageSessionChronosPresenter;
import com.fingerhits.massageassistant.massagesession.chronos.IMassageSessionChronosView;
import com.fingerhits.massageassistant.massagesession.chronos.MassageSessionChronosPresenter;
import com.fingerhits.massageassistant.massagesession.chronos.Warnings;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Javier Torón on 16/02/2017.
 */

@Module
public class MassageSessionActivityModule {
    public final IMassageSessionView view;
    public final IMassageSessionChronosView chronosFragment;

    public MassageSessionActivityModule(IMassageSessionView view,
                                        IMassageSessionChronosView chronosFragment) {
        this.view = view;
        this.chronosFragment = chronosFragment;
    }

    @Provides
    @ActivityScope
    IMassageSessionView provideIMassageSessionView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    IMassageSessionInteractor provideIMassageSessionInteractor(
            MassageSessionInteractor interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    IMassageSessionPresenter provideIMassageSessionPresenter(
            MassageSessionPresenter presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    IMassageSessionChronosView provideIMassageSessionChronosView( ) {
        return this.chronosFragment;
    }


    @Provides
    @ActivityScope
    IMassageSessionChronosPresenter provideIMassageSessionChronosPresenter(
            MassageSessionChronosPresenter chronosPresenter ) {
        return chronosPresenter;
    }

    /*
    IMassageSessionChronosPresenter provideIMassageSessionChronosPresenter() {
        return new MassageSessionChronosPresenter();
    }*/

    @Provides
    @ActivityScope
    public Warnings provideWarnings() {
        return Warnings.getInstance((MassageSessionActivity) view,
                AppModule.provideMassageSounds(),
                AppModule.provideMySharedPreferences());
    }
/*
    @Provides
    @ActivityScope
    public DataXmlImporter provideDataXmlImporter() {
        return new DataXmlImporter();
    }

    @Provides
    @ActivityScope
    public DataXmlExporter provideDataXmlExporter() {
        return new DataXmlExporter();
    }
*/

}
