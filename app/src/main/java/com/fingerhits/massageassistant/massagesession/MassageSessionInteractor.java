package com.fingerhits.massageassistant.massagesession;

import android.content.Context;
import android.database.Cursor;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecordInvalidException;
import com.fingerhits.massageassistant.utils.MiscUtils;

import java.util.Date;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 13/03/2017.
 *
 * I wanted to use private members to cache preference values but this was
 * creating problems from testing and when changed values from options activity.
 * The members didn't always get updated correctly.
 */

public class MassageSessionInteractor implements IMassageSessionInteractor {
    @Inject MassageAssistantDBHelper mDatabaseHelper;
    @Inject Context mContext;
    @Inject MySharedPreferences mPrefs;


    @Inject public MassageSessionInteractor(MassageAssistantDBHelper databaseHelper,
                                            Context context,
                                            MySharedPreferences prefs) {
        mDatabaseHelper = databaseHelper;
        mContext = context;
        mPrefs = prefs;
    }


    public boolean getScreenLock() {
        return mPrefs.getBool(
                MySharedPreferences.PREFS_DISABLESCREENLOCK_KEY);
    }


    //MINUTES
    public int getMassageDurationInMinutes(){
        return mPrefs.getInt( MySharedPreferences.PREFS_MASSAGEDURATION_KEY);
    }

    public long getMassageDurationInSeconds(){
        return mPrefs.getInt(
                MySharedPreferences.PREFS_MASSAGEDURATION_KEY) * 60;
    }

    public long getMassageDurationInMilliseconds(){
        return mPrefs.getInt(
                MySharedPreferences.PREFS_MASSAGEDURATION_KEY) * 60000;
    }

/*    public int getDingTimeInMinutes() {
        return mPrefs.getInt( MySharedPreferences.PREFS_DINGTIME_KEY );
    }

    public int getDingTimeInSeconds() {
        return mPrefs.getInt( MySharedPreferences.PREFS_DINGTIME_KEY ) * 60;
    }
*/

    public boolean getSoundWarningsEnabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_SOUNDWARNINGSENABLED_KEY);
    }

    public void setSoundWarningsEnabled(boolean enable ){
        mPrefs.saveBool(MySharedPreferences.PREFS_SOUNDWARNINGSENABLED_KEY,
                enable);
    }

    //SECONDS
    public int getMinimalDurationInSeconds() {
        return mPrefs.getInt( MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY);
    }

    public boolean getAnimationsDisabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_ANIMATIONS_DISABLED_FOR_TESTING);
    }


    public MassageRecord getMassage(Long idMassage) {
        return mDatabaseHelper.getMassageRecord(idMassage);
    }

    public Long getDurationInLastPosition(Long idMassage) {
        return mDatabaseHelper.getDurationInLastPosition(idMassage);
    }

    public TreatedZoneRecord getLastTreatedZone(Long idMassage) {
        Cursor lastTreatedZone =
                mDatabaseHelper.getLastTreatedZoneInMassage(idMassage);

        if (lastTreatedZone.moveToFirst()) {
            return new TreatedZoneRecord(lastTreatedZone);
        }
        else{
            return null;
        }
    }


    /* *****************   */
	/* Database operations */
	/* *****************   */
    public MassageRecord createMassage(int mode) {
        Long mIdMassage = mDatabaseHelper.insertMassageRecord(
                new MassageRecord( MiscUtils.currentLocalUser(), "",
                        new Date(), mode));

        return mDatabaseHelper.getMassageRecord(mIdMassage);
    }

    public void insertTreatedZone(Long idMassage, String s_codPosition,
                                  String s_codDivision, String s_codZone,
                                  Date zoneStartDateTime,
                                  long elapsedTimeInSeconds)
            throws TreatedZoneRecordInvalidException{
        mDatabaseHelper.insertTreatedZoneRecord(
                new TreatedZoneRecord(0, idMassage,
                        s_codPosition,
                        s_codDivision,
                        s_codZone, zoneStartDateTime,
                        new Date(), elapsedTimeInSeconds,
                        ""));

    }


    public Long finishMassage(Long idMassage) {
        return mDatabaseHelper.finishMassageRecord(idMassage);
    }


    /* ***************** */
	/* PRIVATE FUNCTIONS */
	/* ***************** */

}