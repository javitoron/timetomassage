package com.fingerhits.massageassistant.massagesession.positions;

import android.graphics.drawable.Drawable;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import butterknife.BindDrawable;
import butterknife.ButterKnife;

/**
 * Simple mode position
 * 
 * @author Javier Torón
 * 
 */
public class UndefinedPosition extends Position {

    @BindDrawable(R.drawable.ic_launcher) Drawable ic_launcher;

	public UndefinedPosition(MassageSessionActivity act) {
		super(act);

        ButterKnife.bind(this, act);

        setPositionName( "" );
        setCodPosition( TreatedZoneRecord.CODPOSITION_UNDEFINED );
        setPositionAbbr( "" );
    }


	public Drawable background(){
		return ic_launcher;
	}


    public boolean hasDivision(String p_codDivision){
        return false;
    }

}
