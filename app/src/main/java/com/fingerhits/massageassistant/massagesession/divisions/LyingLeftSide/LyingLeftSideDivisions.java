package com.fingerhits.massageassistant.massagesession.divisions.LyingLeftSide;

import android.view.View;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;

/**
 * 
 * Groups LyingLeftSide Divisions
 * 
 * @author Javier Torón
 * 
 */
abstract public class LyingLeftSideDivisions extends Divisions {

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingLeftSideDivisions(MassageSessionActivity act,
                                  LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
    }


    public static LyingLeftSideDivisions getDivision( MassageSessionActivity act,
                                                      LinearLayout p_containerLayout,
                                                      String codDivision ){
        if( codDivision != null ) {
            if (codDivision.equals(Divisions.CODDIVISION_SINGLE)) {
                return new LyingLeftSideSingleDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_UPPERLOWER)) {
                return new LyingLeftSideUpperLowerDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DEFAULT)) {
                return new LyingLeftSideDefaultDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DETAILED)) {
                return new LyingLeftSideDetailedDivision(act, p_containerLayout);
            }
        }

        return new LyingLeftSideSingleDivision(act, p_containerLayout);
    }

    public static String[] getCodZonesArray(String codDivision) {
        switch (codDivision) {
            case Divisions.CODDIVISION_DEFAULT:
                return LyingLeftSideDefaultDivision.getCodZonesArray();

            case Divisions.CODDIVISION_DETAILED:
                return LyingLeftSideDetailedDivision.getCodZonesArray();

            case Divisions.CODDIVISION_UPPERLOWER:
                return LyingLeftSideUpperLowerDivision.getCodZonesArray();

            default:
                return new String[]{};
        }
    }

    /* ABSTRACT METHODS */
    public abstract void showZones( View v );
    public abstract Divisions prevDivision();
    public abstract Divisions nextDivision();


    /* OVERRIDES */
}
