package com.fingerhits.massageassistant.massagesession.divisions.LyingFaceUp;

import android.view.View;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;

/**
 * 
 * Groups LyingFaceUp Divisions
 * 
 * @author Javier Torón
 * 
 */
abstract public class LyingFaceUpDivisions extends Divisions {
    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingFaceUpDivisions(MassageSessionActivity act,
                                LinearLayout p_containerLayout) {

        super(act, p_containerLayout);

    }


    public static LyingFaceUpDivisions getDivision( MassageSessionActivity act,
                                                    LinearLayout p_containerLayout,
                                                    String codDivision ){
        if( codDivision != null ) {
            if (codDivision.equals(Divisions.CODDIVISION_SINGLE)) {
                return new LyingFaceUpSingleDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_LEFTRIGHT)) {
                return new LyingFaceUpLeftRightDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_UPPERLOWER)) {
                return new LyingFaceUpUpperLowerDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DEFAULT)) {
                return new LyingFaceUpDefaultDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DETAILED)) {
                return new LyingFaceUpDetailedDivision(act, p_containerLayout);
            }
        }
        return new LyingFaceUpSingleDivision(act, p_containerLayout);
    }


    public static String[] getCodZonesArray(String codDivision){
        switch(codDivision) {
            case Divisions.CODDIVISION_DEFAULT:
                return LyingFaceUpDefaultDivision.getCodZonesArray();

            case Divisions.CODDIVISION_DETAILED:
                return LyingFaceUpDetailedDivision.getCodZonesArray();

            case Divisions.CODDIVISION_LEFTRIGHT:
                return LyingFaceUpLeftRightDivision.getCodZonesArray();

            case Divisions.CODDIVISION_UPPERLOWER:
                return LyingFaceUpUpperLowerDivision.getCodZonesArray();

            default:
                return new String[] {};
        }

    }



    /* ABSTRACT METHODS */
    public abstract void showZones( View v );
    public abstract Divisions prevDivision();
    public abstract Divisions nextDivision();



    /* OVERRIDES */

}
