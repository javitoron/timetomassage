package com.fingerhits.massageassistant.massagesession.positions;

import android.graphics.drawable.Drawable;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Shows buttons for a patient Lying Face Down
 * 
 * @author Javier Torón
 * 
 */
public class LyingFaceDownPosition extends Position {

    @BindString(R.string.lyingFaceDownPosition) String lyingFaceDownPosition;
    @BindString(R.string.lyingFaceDownPositionAbbr) String lyingFaceDownPositionAbbr;

    @BindDrawable(R.drawable.facedown) Drawable facedown;

    public LyingFaceDownPosition(MassageSessionActivity act) {
		super(act);

        ButterKnife.bind(this, act);

        setPositionName( lyingFaceDownPosition );
        setCodPosition( TreatedZoneRecord.CODPOSITION_FACEDOWN );
        setPositionAbbr(lyingFaceDownPositionAbbr );
    }

	public Drawable background(){
		return facedown;
	}


    public boolean hasDivision(String p_codDivision){
        return true;
    }

}
