package com.fingerhits.massageassistant.massagesession.positions;

import android.view.View;
import android.widget.ImageButton;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Javier Torón on 28/04/2016.
 */
public class MassageSessionPositionSelector {
    private MassageSessionActivity mActivity;

    @BindView(R.id.lyingFaceDownBtn) ImageButton mLyingFaceDownBtn;
    @BindView(R.id.lyingFaceUpBtn) ImageButton mLyingFaceUpBtn;
    @BindView(R.id.lyingRightSideBtn) ImageButton mLyingRightSideBtn;
    @BindView(R.id.lyingLeftSideBtn) ImageButton mLyingLeftSideBtn;
    @BindView(R.id.sittingBackBtn) ImageButton mSittingBackBtn;
    @BindView(R.id.sittingFrontBtn) ImageButton mSittingFrontBtn;

    public MassageSessionPositionSelector( MassageSessionActivity activity ){
        mActivity = activity;

        ButterKnife.bind(this, this.mActivity);
    }

    public void showHideAll(boolean show) {
        int visibility;

        if(show) visibility = View.VISIBLE;
        else visibility = View.GONE;

        mSittingBackBtn.setVisibility(visibility);
        mSittingFrontBtn.setVisibility(visibility);
        mLyingFaceDownBtn.setVisibility(visibility);
        mLyingFaceUpBtn.setVisibility(visibility);
        mLyingLeftSideBtn.setVisibility(visibility);
        mLyingRightSideBtn.setVisibility(visibility);
    }


    public void enableAll() {

        GraphicUtils.enableSelectionBtn(mLyingFaceDownBtn);
        GraphicUtils.enableSelectionBtn(mLyingFaceUpBtn);
        GraphicUtils.enableSelectionBtn(mLyingRightSideBtn);
        GraphicUtils.enableSelectionBtn(mLyingLeftSideBtn);
        GraphicUtils.enableSelectionBtn(mSittingBackBtn);
        GraphicUtils.enableSelectionBtn(mSittingFrontBtn);
    }


    public void disablePositionBtn(String codPosition) {

        switch (codPosition) {
            case TreatedZoneRecord.CODPOSITION_FACEDOWN:
                GraphicUtils.disableSelectionBtn(mLyingFaceDownBtn);
                break;

            case TreatedZoneRecord.CODPOSITION_FACEUP:
                GraphicUtils.disableSelectionBtn(mLyingFaceUpBtn);
                break;

            case TreatedZoneRecord.CODPOSITION_RIGHTSIDE:
                GraphicUtils.disableSelectionBtn(mLyingRightSideBtn);
                break;

            case TreatedZoneRecord.CODPOSITION_LEFTSIDE:
                GraphicUtils.disableSelectionBtn(mLyingLeftSideBtn);
                break;

            case TreatedZoneRecord.CODPOSITION_SITTINGBACK:
                GraphicUtils.disableSelectionBtn(mSittingBackBtn);
                break;

            case TreatedZoneRecord.CODPOSITION_SITTINGFRONT:
                GraphicUtils.disableSelectionBtn(mSittingFrontBtn);
                break;
        }
    }
}
