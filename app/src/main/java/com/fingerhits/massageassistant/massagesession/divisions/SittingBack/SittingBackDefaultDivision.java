package com.fingerhits.massageassistant.massagesession.divisions.SittingBack;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * 
 * Controls Sitting Back Default Division
 * 
 * @author Javier Torón
 * 
 * It has buttons for HeadNeck, Back, ArmLeft and ArmRight.
 */
public class SittingBackDefaultDivision extends SittingBackDivisions {
	private Button mHeadNeck, mBack, mArmLeft, mArmRight;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public SittingBackDefaultDivision(MassageSessionActivity act,
                                      LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_DEFAULT;
	}

    /*Static methods*/
    public static String[] getCodZonesArray(){
        return new String[] { TreatedZoneRecord.CODZONE_ARMLEFT,
                TreatedZoneRecord.CODZONE_ARMRIGHT,
                TreatedZoneRecord.CODZONE_BACK,
                TreatedZoneRecord.CODZONE_HEADNECK };
    }




    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones(View v) {
        super.showFixLayout((LinearLayout) v, R.id.upperFixLayoutId, 2);
        showHeadNeckZones((LinearLayout) v);
        showFullBodyZones((LinearLayout) v);
        super.showFixLayout((LinearLayout) v, R.id.bottomFixLayoutId, 8);
    }


    public Divisions prevDivision(){

        clearZones();

        SittingBackDivisions newDivision =
                new SittingBackLeftRightDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    public Divisions nextDivision(){
        clearZones();

        SittingBackDivisions newDivision =
                new SittingBackDetailedDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    /* OVERRIDES */
    @Override
    public String zoneTitle( Button btn ){
        if (btn == mHeadNeck ){
            return res.getString(R.string.headNeck_button);
        }
        if (btn == mBack ){
            return res.getString(R.string.back_button);
        }
        if (btn == mArmLeft ){
            return res.getString(R.string.armLeft_button);
        }
        if (btn == mArmRight ){
            return res.getString(R.string.armRight_button);
        }

        return super.zoneTitle(btn);
    }


    @Override
    public String codZone( Button btn ){
        if (btn == mHeadNeck ){
            return TreatedZoneRecord.CODZONE_HEADNECK;
        }
        if (btn == mBack ){
            return TreatedZoneRecord.CODZONE_BACK;
        }
        if (btn == mArmLeft ){
            return TreatedZoneRecord.CODZONE_ARMLEFT;
        }
        if (btn == mArmRight ){
            return TreatedZoneRecord.CODZONE_ARMRIGHT;
        }

        return super.codZone(btn);
    }


    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_HEADNECK ) ){
            return mHeadNeck;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_BACK ) ){
            return mBack;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_ARMLEFT ) ){
            return mArmLeft;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_ARMRIGHT ) ){
            return mArmRight;
        }

        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mHeadNeck, enabled);
        ZonesUtils.enableButton(res, mBack, enabled);
        ZonesUtils.enableButton(res, mArmLeft, enabled);
        ZonesUtils.enableButton(res, mArmRight, enabled);
	}

    /* PRIVATES */
    /**
     * Shows buttons in HeadNeckLayout
     */
    private void showHeadNeckZones( LinearLayout mainLayout ){
        mHeadNeck = super.setHeadNeckZones(mainLayout, 4);
    }

    /**
     * Shows buttons in fullBodyLayout
     */
    private void showFullBodyZones( LinearLayout mainLayout ){
        LinearLayout.LayoutParams fullBodyLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 12);
        fullBodyLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout fullBodyLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        fullBodyLayout.setId(R.id.fullBodyLayoutId);

        super.showFixViewWeight(fullBodyLayout, 2);

        setRightArmLayout(fullBodyLayout);
        setBackLayout(fullBodyLayout);
        setLeftArmLayout(fullBodyLayout);

        super.showFixViewWeight(fullBodyLayout, 2);

        mainLayout.addView(fullBodyLayout, fullBodyLayoutParameters);
    }


    private void setRightArmLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams rightArmLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 2);
        rightArmLayoutParameters.setMargins(GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout rightArmLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical, null);
        rightArmLayout.setId(R.id.rightArmLayoutId);

        mArmRight = setZoneBtnVertical(R.id.armRightMassageBtnId, rightArmLayout,
                2, R.string.armRight_button);


        mainLayout.addView(rightArmLayout, rightArmLayoutParameters);
    }


    private void setBackLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams backLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 4);
        backLayoutParameters.setMargins(GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout backLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical, null);
        backLayout.setId(R.id.backLayoutId);


        mBack = setZoneBtnVertical(R.id.backMassageBtnId, backLayout,
                10, R.string.back_button);

        super.showFixViewWeight(backLayout, 6);

        mainLayout.addView(backLayout, backLayoutParameters);
    }



    private void setLeftArmLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams leftArmLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 2);
        leftArmLayoutParameters.setMargins(GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout leftArmLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical, null);
        leftArmLayout.setId(R.id.rightArmLayoutId);


        mArmLeft = setZoneBtnVertical(R.id.armLeftMassageBtnId, leftArmLayout,
                2, R.string.armLeft_button);

        mainLayout.addView(leftArmLayout, leftArmLayoutParameters);
    }

}