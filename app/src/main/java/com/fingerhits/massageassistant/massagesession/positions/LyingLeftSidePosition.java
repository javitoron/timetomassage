package com.fingerhits.massageassistant.massagesession.positions;

import android.graphics.drawable.Drawable;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Shows buttons for a patient Lying on its left side
 * 
 * @author Javier Torón
 * 
 */
public class LyingLeftSidePosition extends Position {

    @BindString(R.string.lyingLeftSidePosition) String lyingLeftSidePosition;
    @BindString(R.string.lyingLeftSidePositionAbbr) String lyingLeftSidePositionAbbr;

    @BindDrawable(R.drawable.leftside) Drawable leftside;

    public LyingLeftSidePosition(MassageSessionActivity act) {
		super(act);

        ButterKnife.bind(this, act);

        setPositionName( lyingLeftSidePosition );
        setCodPosition( TreatedZoneRecord.CODPOSITION_LEFTSIDE );
        setPositionAbbr( lyingLeftSidePositionAbbr );
	}


	public Drawable background(){
		return leftside;
	}

    public boolean hasDivision(String p_codDivision){
        return (!p_codDivision.equals( Divisions.CODDIVISION_LEFTRIGHT));
    }

}
