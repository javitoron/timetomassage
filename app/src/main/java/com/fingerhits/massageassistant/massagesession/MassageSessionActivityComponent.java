package com.fingerhits.massageassistant.massagesession;

import com.fingerhits.massageassistant.di.ActivityScope;
import com.fingerhits.massageassistant.massagesession.chronos.MassageSessionChronosFragment;

import dagger.Subcomponent;

/**
 * Created by Javier Torón on 16/02/2017.
 */

@ActivityScope
@Subcomponent(modules = MassageSessionActivityModule.class)
public interface MassageSessionActivityComponent {
    void inject(MassageSessionActivity activity);
    void inject(MassageSessionChronosFragment chronosFragment);
}