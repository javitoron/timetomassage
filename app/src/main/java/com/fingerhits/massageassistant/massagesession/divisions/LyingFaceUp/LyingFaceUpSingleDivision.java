package com.fingerhits.massageassistant.massagesession.divisions.LyingFaceUp;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * 
 * Controls LyingFaceUp Single Division
 * 
 * @author Javier Torón
 * 
 * Only has 1 button: Full Body
 */
public class LyingFaceUpSingleDivision extends LyingFaceUpDivisions {

	private Button mFullBody;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingFaceUpSingleDivision(MassageSessionActivity act,
                                     LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_SINGLE;
	}


    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones( View v ){
        showFullBodyZone((LinearLayout) v);
    }


    public Divisions prevDivision(){
        clearZones();

        LyingFaceUpDivisions newDivision =
                new LyingFaceUpDetailedDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    public Divisions nextDivision(){

        clearZones();

        LyingFaceUpDivisions newDivision =
                new LyingFaceUpLeftRightDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }

    /* OVERRIDES */
    @Override
    public String zoneTitle( Button btn ){
        if (btn == mFullBody ){
            return res.getString(R.string.fullBody_button);
        }

        return super.zoneTitle(btn);
    }


    @Override
    public String codZone( Button btn ){
        if (btn == mFullBody ){
            return TreatedZoneRecord.CODZONE_FULLBODY;
        }
        return super.codZone(btn);
    }


    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_FULLBODY ) ){
            return mFullBody;
        }

        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mFullBody, enabled);
	}

    /* PRIVATES */
    /**
     * Shows buttons in FullBodyLayout
     */
    private void showFullBodyZone( LinearLayout mainLayout ){
        LinearLayout.LayoutParams fullBodyLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 1);
        fullBodyLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout fullBodyLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        fullBodyLayout.setId(R.id.fullBodyLayoutId);

        super.showFixViewWeight(fullBodyLayout, 1);

        mFullBody = setZoneBtn( R.id.fullBodyMassageBtnId, fullBodyLayout,
                6,R.string.fullBody_button );

        super.showFixViewWeight(fullBodyLayout, 1);

        mainLayout.addView(fullBodyLayout, fullBodyLayoutParameters);

    }

}
