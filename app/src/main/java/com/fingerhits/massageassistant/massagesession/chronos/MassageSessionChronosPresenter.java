package com.fingerhits.massageassistant.massagesession.chronos;

import android.view.View;
import android.widget.Chronometer;

import com.fingerhits.massageassistant.massagesession.IMassageSessionInteractor;
import com.fingerhits.massageassistant.massagesession.MassageSessionPresenter;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.storedData.MassageRecord;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 15/03/2017.
 */

public class MassageSessionChronosPresenter
        implements IMassageSessionChronosPresenter {

    //@Inject MassageSessionPresenter presenter;
    @Inject IMassageSessionChronosView view;
    @Inject IMassageSessionInteractor interactor;
    @Inject Warnings warnings;


    static final String CHRONO_ZONE = "zone";
    static final String CHRONO_POSITION = "mPosition";
    static final String CHRONO_FULL = "full";
    static final String CHRONO_REMAINING = "remaining";


    private boolean mTimeExceeded = false;
    private int mMode;

    @Inject
    public MassageSessionChronosPresenter(IMassageSessionChronosView view,
                                          IMassageSessionInteractor interactor,
                                          Warnings warnings){
        this.view = view;
        this.interactor = interactor;
        this.warnings = warnings;
    }


    // ******************
    // PROPERTIES
    // ******************
    public boolean getTimeExceeded(){return mTimeExceeded;}
    public void setTimeExceeded(boolean timeExceeded){mTimeExceeded = timeExceeded;}
    public void setTimeExceeded(long startingTimeInMilliseconds,
                                long massageDurationInMilliseconds ) {
        mTimeExceeded =
                (startingTimeInMilliseconds > massageDurationInMilliseconds);
    }


    public int getMode(){return mMode;}
    public void setMode(int mode) { mMode = mode; }

    // ******************
    // Setup
    // ******************
    public void setView(){
        view.initChronos(interactor.getMassageDurationInMilliseconds());
        warnings.reloadPrefs();
    }


    public void prepareMode(int mode, long lastMassageDurationInMilliseconds){
        mMode = mode;

        if( mMode == MassageRecord.MODE_SIMPLE ) {
            view.hideZoneChrono();
            view.hidePositionChrono();
            view.setVisibleChrono(CHRONO_FULL);
            startFullChronoIn(lastMassageDurationInMilliseconds);
            view.enableFullChrono(mTimeExceeded);
        }
        else{
            view.setVisibleChrono( CHRONO_POSITION );

            if( mMode == MassageRecord.MODE_INTERMEDIATE ) {
                view.hideZoneChrono();
                startFullChronoIn(lastMassageDurationInMilliseconds);
            }

        }
    }


    public void optionsChanged(){
        warnings.reloadPrefs();
        long elapsedTimeInSeconds =
                view.getFullElapsedTime(MassageSessionPresenter.STATE_RUNNING) /
                        1000;
        long mMassageDurationInSeconds =
                interactor.getMassageDurationInSeconds();

        setTimeExceeded(elapsedTimeInSeconds, mMassageDurationInSeconds);
        view.enableCurrentChrono(mTimeExceeded);
    }

    public void loadIntermediatePosition(String position_abbr,
                                         String fullBodyLabel,
                                         Long positionDurationInMilliseconds) {
        view.startPositionChronoIn(positionDurationInMilliseconds);
        view.changePositionChronoText(position_abbr);
        view.startZoneChronoIn(fullBodyLabel, 0L);
    }

    public void divisionChangedTo(String codDivision) {
        if (codDivision.equals( Divisions.CODDIVISION_SINGLE)) {
            view.showHideZoneChrono(false);
        } else {
            view.showHideZoneChrono(true);
            view.stopZoneChronoInZero();
        }
    }


    public void tickFull( Chronometer chrono ) {
		/* Gets chrono time */
        long elapsedTime = view.getFullElapsedTime(
                MassageSessionPresenter.STATE_RUNNING);

        view.tickFullAndRemaining( elapsedTime,
                interactor.getMassageDurationInMilliseconds() );

        long elapsedTimeInSeconds = elapsedTime / 1000;

        warnings.tick(elapsedTimeInSeconds);

        long mMassageDurationInSeconds =
                interactor.getMassageDurationInSeconds();

        if( !mTimeExceeded
                && (elapsedTimeInSeconds == mMassageDurationInSeconds) ) {
            //Change chrono style to red
            setTimeExceeded( true );
            view.enableCurrentChrono(true);
        }

    }

    /**
     * Pauses full, mPosition and current zone Chronometers
     */
    public void pauseChronometers(){
        view.pauseChronometers();
    }

    /**
     * Restart full, mPosition and current zone Chronometers
     */
    public void resumeChronometers() {
        view.resumeFullChrono();

        if( mMode != MassageRecord.MODE_ADVANCED ) {
            view.startZoneChronoIn("", 0L);
        }

        if( mMode != MassageRecord.MODE_SIMPLE ){
            view.resumePositionChrono();
        }
    }


    public void startFullChronoIn(Long startingTimeInMilliseconds) {
        setTimeExceeded(startingTimeInMilliseconds,
                interactor.getMassageDurationInMilliseconds() );

        view.startFullChronoIn(startingTimeInMilliseconds, mTimeExceeded);
    }
    public void startPositionChronoIn(Long startingTimeInMilliseconds) {
        view.startPositionChronoIn(startingTimeInMilliseconds);
    }

    public void changePositionChronoText(String codPosition) {
        view.changePositionChronoText(codPosition);
    }

    public void setZoneChronoNormalColor(){
        view.putZoneBackgroundNormalColor();
    }


    public void startZoneChronoIn(String label, Long startingTimeInMilliseconds) {
        view.startZoneChronoIn(label, startingTimeInMilliseconds);
    }
    public void startZoneChrono(View pressedBtn, String state){
        view.zoneStarted(pressedBtn, state);
    }


    public void blinkZoneChrono(){
        view.blinkZone();
    }

    public Long getZoneChronoElapsedTime(String state){
        return view.getZoneChronoElapsedTime(state);
    }

}