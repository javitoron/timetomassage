package com.fingerhits.massageassistant.massagesession.divisions.SittingFront;

import android.view.View;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;

/**
 * 
 * Groups Sitting Front Divisions
 * 
 * @author Javier Torón
 * 
 */
abstract public class SittingFrontDivisions extends Divisions {

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public SittingFrontDivisions(MassageSessionActivity act,
                                 LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
	}


    public static SittingFrontDivisions getDivision( MassageSessionActivity act,
                                                     LinearLayout p_containerLayout,
                                                     String codDivision ){
        if( codDivision != null ) {
            if (codDivision.equals(Divisions.CODDIVISION_SINGLE)) {
                return new SittingFrontSingleDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_LEFTRIGHT)) {
                return new SittingFrontLeftRightDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DEFAULT)) {
                return new SittingFrontDefaultDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DETAILED)) {
                return new SittingFrontDetailedDivision(act, p_containerLayout);
            }
        }
        return new SittingFrontSingleDivision(act, p_containerLayout);
    }


    public static String[] getCodZonesArray(String codDivision){
        switch(codDivision) {
            case Divisions.CODDIVISION_DEFAULT:
                return SittingFrontDefaultDivision.getCodZonesArray();

            case Divisions.CODDIVISION_DETAILED:
                return SittingFrontDetailedDivision.getCodZonesArray();

            case Divisions.CODDIVISION_LEFTRIGHT:
                return SittingFrontLeftRightDivision.getCodZonesArray();

            default:
                return new String[] {};
        }

    }



    /* ABSTRACT METHODS */
    public abstract void showZones( View v );
    public abstract Divisions prevDivision();
    public abstract Divisions nextDivision();


    /* OVERRIDES */

}