package com.fingerhits.massageassistant.massagesession.divisions.SittingBack;

import android.view.View;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;

/**
 * 
 * Groups Sitting Back Divisions
 * 
 * @author Javier Torón
 * 
 */
abstract public class SittingBackDivisions extends Divisions {

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public SittingBackDivisions(MassageSessionActivity act,
                                LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
	}


    public static SittingBackDivisions getDivision( MassageSessionActivity act,
                                                    LinearLayout p_containerLayout,
                                                    String codDivision ){
        if( codDivision != null ) {
            if (codDivision.equals(Divisions.CODDIVISION_SINGLE)) {
                return new SittingBackSingleDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_LEFTRIGHT)) {
                return new SittingBackLeftRightDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DEFAULT)) {
                return new SittingBackDefaultDivision(act, p_containerLayout);
            }

            if (codDivision.equals(Divisions.CODDIVISION_DETAILED)) {
                return new SittingBackDetailedDivision(act, p_containerLayout);
            }
        }
        return new SittingBackSingleDivision(act, p_containerLayout);
    }


    public static String[] getCodZonesArray(String codDivision){
        switch(codDivision) {
            case Divisions.CODDIVISION_DEFAULT:
                return SittingBackDefaultDivision.getCodZonesArray();

            case Divisions.CODDIVISION_DETAILED:
                return SittingBackDetailedDivision.getCodZonesArray();

            case Divisions.CODDIVISION_LEFTRIGHT:
                return SittingBackLeftRightDivision.getCodZonesArray();

            default:
                return new String[] {};
        }

    }



    /* ABSTRACT METHODS */
    public abstract void showZones( View v );
    public abstract Divisions prevDivision();
    public abstract Divisions nextDivision();


    /* OVERRIDES */
}