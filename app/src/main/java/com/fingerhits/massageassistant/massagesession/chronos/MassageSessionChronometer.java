package com.fingerhits.massageassistant.massagesession.chronos;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.MassageSessionPresenter;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * Chronometer
 *
 * @author javi on 26/03/2015.
 */
public class MassageSessionChronometer {
    Chronometer chrono;
    TextView chronoTxt;
    private LinearLayout mContainerLayout;
    private TextView mLabel;

    private long mTimeWhenStopped = 0L;
    private long mLastTick = 0;
    private boolean mStarted = false;
    private boolean mPaused = false;

    private boolean mOnlyChrono = false;
    private boolean mBackwards = false;

    private MassageSessionActivity mActivity;


    /**
     * MassageSessionChronometer constructor
     */
    public MassageSessionChronometer( Chronometer pChrono ) {
        chrono = pChrono;
        mOnlyChrono = true;
    }

    /**
     * MassageSessionChronometer constructor
     * @param pActivity To get Context
     * @param pChrono Chronometer which counts time
     * @param pContainer Layout containing chrono and label
     * @param pLabel Describes chrono to the user
     */
    MassageSessionChronometer(MassageSessionActivity pActivity,
                              Chronometer pChrono,
                              LinearLayout pContainer,
                              TextView pLabel) {
        chrono = pChrono;
        mContainerLayout = pContainer;
        mLabel = pLabel;
        mActivity = pActivity;
    }

    /**
     * Constructor overloaded for remaining time chrono
     * @param pActivity To get Context
     * @param pChronoTxt Shows time
     * @param pContainer Layout containing chrono and label
     * @param pLabel Describes chrono to the user
     */
    MassageSessionChronometer(MassageSessionActivity pActivity,
                              TextView pChronoTxt,
                              LinearLayout pContainer,
                              TextView pLabel) {
        mBackwards = true;
        chronoTxt = pChronoTxt;
        mContainerLayout = pContainer;
        mLabel = pLabel;
        mActivity = pActivity;
    }

    //******************
    // TIME PROPERTIES
    //******************
    /*********************
     * Returns last time we counted a second
     * @return mLastTick
     */
    long getLastTick(){
        return mLastTick;
    }


    /**
     * Saves time of tick
     * @param lastTick - In milliseconds
     */
    void setLastTick(long lastTick){
        mLastTick = lastTick;
    }



    /**
     * Gets current time
     * @param state Running or stopped
     * @return Current or when it was stopped
     */
    public long getElapsedTime(String state ){
        switch (state){
            case MassageSessionPresenter.STATE_NOT_STARTED:
                return 0L;

            case MassageSessionPresenter.STATE_RUNNING:
            case MassageSessionPresenter.STATE_WAITING_TO_SELECT_ZONE:
                return SystemClock.elapsedRealtime() - chrono.getBase();

            case MassageSessionPresenter.STATE_PAUSED:
                return mTimeWhenStopped;
        }

        return 0L;
    }


    //************
    //VISIBILITY
    //************
    /**
     * Hides the chrono
     */
    public void show() {

        if (!mOnlyChrono) {
            mContainerLayout.setVisibility(View.VISIBLE);
        }
    }

    void hide() {

        if (!mOnlyChrono) {
            mContainerLayout.setVisibility(View.GONE);
        }
    }

    void disable(){

        if(mOnlyChrono) {
            chrono.setVisibility(View.GONE);
        }
        else {
            GraphicUtils.defineTextStyle(mActivity.context, mLabel,
                    R.style.massage_disabled_chrono_texts);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0,
                    ViewGroup.LayoutParams.MATCH_PARENT, 1f);
            mContainerLayout.setLayoutParams(lp);
            mContainerLayout.setBackground(GraphicUtils.getDrawable(mActivity.res,
                    R.drawable.disabled_chrono));

            if (mBackwards) {
                chronoTxt.setVisibility(View.GONE);
            } else {
                chrono.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Shows the chrono
     */
    public void enable(boolean timeExceeded){
        if(mOnlyChrono) {
            chrono.setVisibility(View.VISIBLE);
        }
        else {
            GraphicUtils.defineTextStyle(mActivity.context, mLabel,
                    R.style.massage_enabled_chrono_texts);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0,
                    ViewGroup.LayoutParams.MATCH_PARENT, 2f);
            mContainerLayout.setLayoutParams(lp);

            setEnabledBackground(timeExceeded);

            if (mBackwards) {
                chronoTxt.setVisibility(View.VISIBLE);
            } else {
                chrono.setVisibility(View.VISIBLE);
            }
        }
    }

    //*****************
    //CHRONO OPERATIONS
    //*****************
    /**
     * Sets chrono to 0 and stops it
     */
    public void stopInZero() {
        if (chrono != null) {
            chrono.setBase(SystemClock.elapsedRealtime());
            chrono.stop();
            mStarted = true;
            mPaused = true;
            mTimeWhenStopped = 0L;
        }
    }


    /**
     * Sets chrono to 0 and start
     */
    public void startIn( long startingTime ){
        if( chrono != null ) {
            setStartingTime(startingTime);
            chrono.start();
            mStarted = true;
            mPaused = false;
        }
    }


    /**
     * Restart the chrono from the time it was stopped
     */
    void resume() {
        if (mStarted) {
            if(mPaused) {
                startIn(mTimeWhenStopped);
                mPaused = false;
            }
        }
    }


    /**
     * Pause chrono and saves time
     */
    public void pause(){

        if(mStarted) {
            if( !mPaused){
                mTimeWhenStopped =
                        SystemClock.elapsedRealtime()- chrono.getBase();
                chrono.stop();
                mPaused = true;
            }
        }
    }

    //*****************
    // GRAPHICAL OPERATIONS
    //*****************

    /******
     *
     * @param color To set as chonos background
     */
    void setBackgroundColor(int color){
        if(!mOnlyChrono) {
            mContainerLayout.setBackgroundColor(color);
        }
    }

    void setEnabledBackground(boolean timeExceeded){
        if(!mOnlyChrono) {
            int backgroundColor;
            if (timeExceeded) {
                backgroundColor = R.color.chrono_enabled_over_duration;
            } else {
                backgroundColor = R.color.chrono_enabled_under_duration;
            }
            mContainerLayout.setBackgroundColor(
                    GraphicUtils.getColor(mActivity.res, backgroundColor));

        }
    }

    public void setText(String text){
        if(!mOnlyChrono) {
            mLabel.setText(text);
        }
    }

    void setChronoText(String text){
        if(mBackwards) {
            chronoTxt.setText(text);
        }
    }


    //****************
    // PRIVATE METHODS
    //****************
    /**
     * Sets chrono start time
     * @param startingTime To set in milliseconds
     */
    private void setStartingTime(long startingTime){
        chrono.setBase(
                SystemClock.elapsedRealtime() - startingTime);
    }

     // Returns saved time when paused
    //  private long getElapsedTimeWhenPaused(){ return mTimeWhenStopped; }

}