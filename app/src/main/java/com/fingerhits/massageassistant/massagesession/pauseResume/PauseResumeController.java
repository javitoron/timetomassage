package com.fingerhits.massageassistant.massagesession.pauseResume;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Chronometer;
import android.widget.ImageButton;

import com.fingerhits.massageassistant.massagesession.MassageSessionPresenter;
import com.fingerhits.massageassistant.massagesession.chronos.MassageSessionChronometer;
import com.fingerhits.massageassistant.massagesession.utils.SquareRelativeLayout;

/**
 * Manages Pause/resume buttons and paused text
 *
 * @author javi on 21/04/2015.
 */
public class PauseResumeController {
    private final SquareRelativeLayout mPausedMassageLayout;
    private final ImageButton mPauseMassageBtn;
    private final ImageButton mResumeMassageBtn;
    private final MassageSessionChronometer mPauseChrono;

    private long mPausedTime = 0L;

    /**
     * Sets the pause related views
     * @param pausedMassageLayout Layout container
     * @param pauseMassageBtn Button to pause
     * @param resumeMassageBtn Button to resume
     * @param pauseChronometer Chrono
     */
    public PauseResumeController(
            SquareRelativeLayout pausedMassageLayout,
            ImageButton pauseMassageBtn,
            ImageButton resumeMassageBtn,
            Chronometer pauseChronometer) {

        mPauseMassageBtn = pauseMassageBtn;
        mResumeMassageBtn = resumeMassageBtn;
        mPausedMassageLayout = pausedMassageLayout;
        mPauseChrono = new MassageSessionChronometer( pauseChronometer );
    }


    public long getPausedTime(){ return mPausedTime; }
    public void setPausedTime(long pausedTime){ mPausedTime = pausedTime;}
    public void resetPausedTime(){ mPausedTime = 0L;}

    /**
     * Sets resume button and shows paused text
     */
    public void pause(boolean animationsDisabled) {
        mPausedMassageLayout.setVisibility(View.VISIBLE);


        if(animationsDisabled){
            mResumeMassageBtn.setVisibility(View.VISIBLE);
        }
        else{
            blinkResumeMassage();
        }
        mPauseMassageBtn.setEnabled(false);

        startPauseChrono();
    }


    public void startPauseChrono() {
        mPauseChrono.startIn(0L);
    }


    public void enablePauseButton(boolean enable) {
        mPauseMassageBtn.setEnabled(enable);
    }


    /**
     * Sets pause button, hides paused text and returns getElapsedTime
     */
    public void resume() {
        mPauseMassageBtn.setEnabled(true);

        stopResumeMassageBlinking();
        mPausedMassageLayout.setVisibility(View.GONE);

        stopPauseChrono();
    }


    public void stopPauseChrono() {
        long pausedTime =
                mPauseChrono.getElapsedTime(MassageSessionPresenter.STATE_RUNNING);
        mPauseChrono.stopInZero();

        mPausedTime += pausedTime;
    }


    // PRIVATE METHODS
    private void blinkResumeMassage() {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        //You can manage the time of the blink with this parameter
        anim.setDuration(500);
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        mResumeMassageBtn.startAnimation(anim);
    }


    private void stopResumeMassageBlinking() {
        mResumeMassageBtn.clearAnimation();
    }

}