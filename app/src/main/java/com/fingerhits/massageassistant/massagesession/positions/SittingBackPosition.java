package com.fingerhits.massageassistant.massagesession.positions;

import android.graphics.drawable.Drawable;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Shows buttons for a patient sittingback
 * 
 * @author Javier Torón
 * 
 */
public class SittingBackPosition extends Position {

    @BindString(R.string.sittingBackPosition) String sittingBackPosition;
    @BindString(R.string.sittingBackPositionAbbr) String sittingBackPositionAbbr;

    @BindDrawable(R.drawable.sittingback) Drawable sittingback;

	public SittingBackPosition(MassageSessionActivity act) {
		super(act);

        ButterKnife.bind(this, act);

        setPositionName( sittingBackPosition );
        setCodPosition( TreatedZoneRecord.CODPOSITION_SITTINGBACK );
        setPositionAbbr( sittingBackPositionAbbr );
    }


	public Drawable background(){
		return sittingback;
	}

    public boolean hasDivision(String p_codDivision){
        return (!p_codDivision.equals(Divisions.CODDIVISION_UPPERLOWER));
    }

}
