package com.fingerhits.massageassistant.massagesession.divisions.SittingBack;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

/**
 * 
 * Controls Sitting Back Default Division
 * 
 * @author Javier Torón
 * 
 * It has buttons for HeadNeck, LeftSide and RightSide.
 */
public class SittingBackLeftRightDivision extends SittingBackDivisions {
	private Button mHeadNeck, mLeftSide, mRightSide;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public SittingBackLeftRightDivision(MassageSessionActivity act,
                                        LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_LEFTRIGHT;
	}


    /*Static methods*/
    public static String[] getCodZonesArray(){
        return new String[] { TreatedZoneRecord.CODZONE_HEADNECK,
                TreatedZoneRecord.CODZONE_LEFTSIDE,
                TreatedZoneRecord.CODZONE_RIGHTSIDE };
    }

    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones(View v) {
        super.showFixLayout((LinearLayout) v, R.id.upperFixLayoutId, 2);
        showHeadNeckZones((LinearLayout) v);
        showLeftRightZones((LinearLayout) v);
        super.showFixLayout((LinearLayout) v, R.id.bottomFixLayoutId, 16);
    }


    public Divisions prevDivision(){

        clearZones();

        SittingBackDivisions newDivision =
                new SittingBackSingleDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    public Divisions nextDivision(){
        clearZones();

        SittingBackDivisions newDivision =
                new SittingBackDefaultDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    /* OVERRIDES */
    @Override
    public String zoneTitle( Button btn ){
        if (btn == mHeadNeck ){
            return res.getString(R.string.headNeck_button);
        }
        if (btn == mLeftSide ){
            return res.getString(R.string.leftSide_button);
        }
        if (btn == mRightSide ){
            return res.getString(R.string.rightSide_button);
        }

        return super.zoneTitle(btn);
    }


    @Override
    public String codZone( Button btn ){
        if (btn == mHeadNeck ){
            return TreatedZoneRecord.CODZONE_HEADNECK;
        }
        if (btn == mLeftSide ){
            return TreatedZoneRecord.CODZONE_LEFTSIDE;
        }
        if (btn == mRightSide ){
            return TreatedZoneRecord.CODZONE_RIGHTSIDE;
        }

        return super.codZone(btn);
    }


    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_HEADNECK ) ){
            return mHeadNeck;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTSIDE ) ){
            return mLeftSide;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTSIDE ) ){
            return mRightSide;
        }

        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mHeadNeck, enabled);
        ZonesUtils.enableButton(res, mLeftSide, enabled);
        ZonesUtils.enableButton(res, mRightSide, enabled);
	}

    /* PRIVATES */
    /**
     * Shows buttons in HeadNeckLayout
     */
    private void showHeadNeckZones( LinearLayout mainLayout ){
        mHeadNeck = super.setHeadNeckZones(mainLayout, 6);
    }

    /**
     * Shows buttons in leftRightLayout
     */
    private void showLeftRightZones( LinearLayout mainLayout ){
        LinearLayout leftRightLayout = super.setLeftRightLayout(10);

        super.showFixViewWeight(leftRightLayout, 1);

        mLeftSide = setZoneBtn( R.id.leftSideMassageBtnId, leftRightLayout,
                2,R.string.leftSide_button );

        mRightSide = setZoneBtn( R.id.rightSideMassageBtnId, leftRightLayout,
                2,R.string.rightSide_button );

        super.showFixViewWeight(leftRightLayout, 1);

        mainLayout.addView(leftRightLayout);

    }

}