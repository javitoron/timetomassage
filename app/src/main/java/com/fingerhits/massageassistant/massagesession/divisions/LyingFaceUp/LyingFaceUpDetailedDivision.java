package com.fingerhits.massageassistant.massagesession.divisions.LyingFaceUp;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * 
 * Controls LyingFaceUp Detailed Division
 * 
 * @author Javier Torón
 * 
 * It has buttons for Head, NeckShoulders, RightBiceps, RightForearm, RightHand,
 *      Thorax, Abdomen, Pelvis, ThighKnee, Shin, Feet, LeftBiceps, LeftForearm,
 *      LeftHand.
 */
public class LyingFaceUpDetailedDivision extends LyingFaceUpDivisions {

	private Button mHead, mNeckShoulders, mRightBiceps, mRightForearm,
            mRightHand, mThorax, mAbdomen, mPelvis, mThighKnee, mShin, mFeet,
            mLeftBiceps, mLeftForearm, mLeftHand;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingFaceUpDetailedDivision(MassageSessionActivity act,
                                       LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_DETAILED;
	}


    /*Static methods*/
    public static String[] getCodZonesArray(){
        return new String[] { TreatedZoneRecord.CODZONE_ABDOMEN,
            TreatedZoneRecord.CODZONE_HEAD,
            TreatedZoneRecord.CODZONE_FEET,
            TreatedZoneRecord.CODZONE_LEFTBICEPS,
            TreatedZoneRecord.CODZONE_LEFTFOREARM,
            TreatedZoneRecord.CODZONE_LEFTHAND,
            TreatedZoneRecord.CODZONE_NECKSHOULDERS,
            TreatedZoneRecord.CODZONE_PELVIS,
            TreatedZoneRecord.CODZONE_RIGHTBICEPS,
            TreatedZoneRecord.CODZONE_RIGHTFOREARM,
            TreatedZoneRecord.CODZONE_RIGHTHAND,
            TreatedZoneRecord.CODZONE_SHIN,
            TreatedZoneRecord.CODZONE_THIGHKNEE,
            TreatedZoneRecord.CODZONE_THORAX };
    }



    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones( View v ){
        showHeadZones((LinearLayout) v);
        showNeckShouldersZones((LinearLayout) v);
        showFullBodyZones((LinearLayout) v);
    }

    public Divisions prevDivision(){
        clearZones();

        LyingFaceUpDivisions newDivision =
                new LyingFaceUpDefaultDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    public Divisions nextDivision(){

        clearZones();

        LyingFaceUpDivisions newDivision =
                new LyingFaceUpSingleDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }

    /* OVERRIDES */
    @Override
    public String zoneTitle( Button btn ){
        if (btn == mHead ){
            return res.getString(R.string.head_button);
        }

        if (btn == mNeckShoulders ){
            return res.getString(R.string.neckShoulders_button);
        }

        if (btn == mRightBiceps ){
            return res.getString(R.string.rightBiceps_button);
        }

        if (btn == mRightForearm ){
            return res.getString(R.string.rightForearm_button);
        }

        if (btn == mRightHand ){
            return res.getString(R.string.rightHand_button);
        }

        if (btn == mThorax ){
            return res.getString(R.string.thorax_button);
        }

        if (btn == mAbdomen ){
            return res.getString(R.string.abdomen_button);
        }

        if (btn == mPelvis ){
            return res.getString(R.string.pelvis_button);
        }

        if (btn == mThighKnee ){
            return res.getString(R.string.thighKnee_button);
        }

        if (btn == mShin ){
            return res.getString(R.string.shin_button);
        }

        if (btn == mFeet ){
            return res.getString(R.string.feet_button);
        }

        if (btn == mLeftBiceps ){
            return res.getString(R.string.leftBiceps_button);
        }

        if (btn == mLeftForearm ){
            return res.getString(R.string.leftForearm_button);
        }

        if (btn == mLeftHand ){
            return res.getString(R.string.leftHand_button);
        }

        return super.zoneTitle(btn);
    }


    @Override
    public String codZone( Button btn ){
        if (btn == mHead ){
            return TreatedZoneRecord.CODZONE_HEAD;
        }

        if (btn == mNeckShoulders){
            return TreatedZoneRecord.CODZONE_NECKSHOULDERS;
        }

        if (btn == mRightBiceps){
            return TreatedZoneRecord.CODZONE_RIGHTBICEPS;
        }

        if (btn == mRightForearm){
            return TreatedZoneRecord.CODZONE_RIGHTFOREARM;
        }

        if (btn == mRightHand){
            return TreatedZoneRecord.CODZONE_RIGHTHAND;
        }

        if (btn == mThorax){
            return TreatedZoneRecord.CODZONE_THORAX;
        }

        if (btn == mAbdomen){
            return TreatedZoneRecord.CODZONE_ABDOMEN;
        }

        if (btn == mPelvis){
            return TreatedZoneRecord.CODZONE_PELVIS;
        }

        if (btn == mThighKnee){
            return TreatedZoneRecord.CODZONE_THIGHKNEE;
        }

        if (btn == mShin){
            return TreatedZoneRecord.CODZONE_SHIN;
        }

        if (btn == mFeet){
            return TreatedZoneRecord.CODZONE_FEET;
        }

        if (btn == mLeftBiceps){
            return TreatedZoneRecord.CODZONE_LEFTBICEPS;
        }

        if (btn == mLeftForearm){
            return TreatedZoneRecord.CODZONE_LEFTFOREARM;
        }

        if (btn == mLeftHand){
            return TreatedZoneRecord.CODZONE_LEFTHAND;
        }

        return super.codZone(btn);
    }


    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_HEAD ) ){
            return mHead;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_NECKSHOULDERS ) ){
            return mNeckShoulders;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTBICEPS ) ){
            return mRightBiceps;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTFOREARM ) ){
            return mRightForearm;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTHAND ) ){
            return mRightHand;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_THORAX ) ){
            return mThorax;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_ABDOMEN ) ){
            return mAbdomen;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_PELVIS ) ){
            return mPelvis;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_THIGHKNEE ) ){
            return mThighKnee;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_SHIN ) ){
            return mShin;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_FEET ) ){
            return mFeet;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTBICEPS ) ){
            return mLeftBiceps;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTFOREARM ) ){
            return mLeftForearm;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_LEFTHAND ) ){
            return mLeftHand;
        }

        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mHead, enabled);
        ZonesUtils.enableButton(res, mNeckShoulders, enabled);
        ZonesUtils.enableButton(res, mRightBiceps, enabled);
        ZonesUtils.enableButton(res, mRightForearm, enabled);
        ZonesUtils.enableButton(res, mRightHand, enabled);
        ZonesUtils.enableButton(res, mThorax, enabled);
        ZonesUtils.enableButton(res, mAbdomen, enabled);
        ZonesUtils.enableButton(res, mPelvis, enabled);
        ZonesUtils.enableButton(res, mThighKnee, enabled);
        ZonesUtils.enableButton(res, mShin, enabled);
        ZonesUtils.enableButton(res, mFeet, enabled);
        ZonesUtils.enableButton(res, mLeftBiceps, enabled);
        ZonesUtils.enableButton(res, mLeftForearm, enabled);
        ZonesUtils.enableButton(res, mLeftHand, enabled);
	}

    /* PRIVATES */
    /**
     * Shows buttons in HeadLayout
     */
    private void showHeadZones( LinearLayout mainLayout ){
        mHead = super.setHeadZones(mainLayout, 4);
    }

    /**
     * Shows buttons in NeckShouldersLayout
     */
    private void showNeckShouldersZones( LinearLayout mainLayout ){
        LinearLayout.LayoutParams neckShouldersLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 3);
        neckShouldersLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout neckShouldersLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        neckShouldersLayout.setId(R.id.neckShouldersLayoutId);

        super.showFixViewWeight(neckShouldersLayout, 1);

        mNeckShoulders = setZoneBtn( R.id.neckShouldersMassageBtnId, neckShouldersLayout,
                4, R.string.neckShoulders_button );

        super.showFixViewWeight(neckShouldersLayout, 1);

        mainLayout.addView(neckShouldersLayout, neckShouldersLayoutParameters);

    }


    /**
     * Shows buttons in fullBodyLayout
     */
    private void showFullBodyZones( LinearLayout mainLayout ){
        LinearLayout.LayoutParams fullBodyLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 30);
        fullBodyLayoutParameters.setMargins(0, GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout fullBodyLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        fullBodyLayout.setId(R.id.fullBodyLayoutId);

        super.showFixViewWeight(fullBodyLayout, 2);

        setRightArmLayout(fullBodyLayout);
        setTrunkLegsLayout(fullBodyLayout);
        setLeftArmLayout(fullBodyLayout);

        super.showFixViewWeight(fullBodyLayout, 2);

        mainLayout.addView(fullBodyLayout, fullBodyLayoutParameters);
    }


    private void setRightArmLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams rightArmLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 2);
        rightArmLayoutParameters.setMargins(GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout rightArmLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical, null);
        rightArmLayout.setId(R.id.rightArmLayoutId);

        mRightBiceps = setZoneBtnVertical(R.id.rightBicepsMassageBtnId,
                rightArmLayout, 2, R.string.rightBiceps_button);

        mRightForearm = setZoneBtnVertical(R.id.rightForearmMassageBtnId,
                rightArmLayout, 2, R.string.rightForearm_button);

        mRightHand = setZoneBtnVertical(R.id.rightHandMassageBtnId,
                rightArmLayout, 2, R.string.rightHand_button);

        super.showFixViewWeight(rightArmLayout, 6);

        mainLayout.addView(rightArmLayout, rightArmLayoutParameters);
    }


    private void setTrunkLegsLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams trunkLegsLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 3);
        trunkLegsLayoutParameters.setMargins(GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout trunkLegsLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical, null);
        trunkLegsLayout.setId(R.id.trunkLegsLayoutId);


        mThorax = setZoneBtnVertical(R.id.thoraxMassageBtnId,
                trunkLegsLayout, 3, R.string.thorax_button);

        mAbdomen = setZoneBtnVertical(R.id.abdomenMassageBtnId,
                trunkLegsLayout, 3, R.string.abdomen_button);

        mPelvis = setZoneBtnVertical(R.id.pelvisMassageBtnId,
                trunkLegsLayout, 3, R.string.pelvis_button);


        mThighKnee = setZoneBtnVertical(R.id.thighKneeMassageBtnId,
                trunkLegsLayout, 6, R.string.thighKnee_button);

        mShin = setZoneBtnVertical(R.id.shinMassageBtnId,
                trunkLegsLayout, 5, R.string.shin_button);

        mFeet = setZoneBtnVertical(R.id.feetMassageBtnId,
                trunkLegsLayout, 4, R.string.feet_button);


        mainLayout.addView(trunkLegsLayout, trunkLegsLayoutParameters);
    }


    private void setLeftArmLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams leftArmLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 2);
        leftArmLayoutParameters.setMargins(GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout leftArmLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical, null);
        leftArmLayout.setId(R.id.rightArmLayoutId);


        mLeftBiceps = setZoneBtnVertical(R.id.leftBicepsMassageBtnId,
                leftArmLayout, 2, R.string.leftBiceps_button);

        mLeftForearm = setZoneBtnVertical(R.id.leftForearmMassageBtnId,
                leftArmLayout, 2, R.string.leftForearm_button);

        mLeftHand = setZoneBtnVertical(R.id.leftHandMassageBtnId,
                leftArmLayout, 2, R.string.leftHand_button);

        super.showFixViewWeight(leftArmLayout, 6);

        mainLayout.addView(leftArmLayout, leftArmLayoutParameters);
    }

}
