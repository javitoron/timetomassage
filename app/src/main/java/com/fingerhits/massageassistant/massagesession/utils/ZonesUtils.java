package com.fingerhits.massageassistant.massagesession.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.widget.Button;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * Assorted functions for zone buttons
 *
 * @author javi on 22/10/2015.
 */
public class ZonesUtils {

    /**
     * Enable o disable the button
     * If enabled is true sets buttonUnselected and correct text label.
     * If enabled is false sets buttonSelected.
     * And finally calls the btn.setEnabled
     *
     * @param btn - Button to be enabled or disabled
     * @param enabled - New status for the button
     */
    public static void enableButton( Resources res, Button btn,
                                     Boolean enabled ){

        if(btn != null) {
            if (enabled) {
                Drawable buttonUnselected = ResourcesCompat.getDrawable(
                        res, R.drawable.zone_button, null);
                GraphicUtils.setButtonBorder(btn, buttonUnselected);
                //btn.setText(zoneTitle(btn));
            } else {
                Drawable buttonSelected = ResourcesCompat.getDrawable(
                        res, R.drawable.zone_button_selected, null);
                GraphicUtils.setButtonBorder(btn, buttonSelected);
            }

            btn.setEnabled(enabled);
        }
    }


    /**
     * Enable o disable the special button (transitions or full body button)
     * If enabled is true sets specialButtonUnselected and correct text label.
     * If enabled is false sets specialButtonSelected.
     * And finally calls the btn.setEnabled
     *
     * @param btn - Button to be enabled or disabled
     * @param enabled - New status for the button
     */
    public static void enableSpecialButton( Resources res, Button btn,
                                            Boolean enabled ){

        if( enabled ) {
            Drawable specialButtonUnselected = ResourcesCompat.getDrawable(
                    res, R.drawable.special_zone_button, null);
            GraphicUtils.setButtonBorder(btn, specialButtonUnselected);
        }
        else {
            Drawable specialButtonSelected = ResourcesCompat.getDrawable(
                    res, R.drawable.special_zone_button_selected, null);
            GraphicUtils.setButtonBorder(btn, specialButtonSelected);
        }

        btn.setEnabled(enabled);
    }


    /**
     * Returns full and translated name of the codZone
     * @param context From activity. In order to read the resources
     * @param codZone Code of zone to translate
     * @return Full translated name of the zone
     */
    public static String codZoneToString(Context context, String codZone ){
        switch ( codZone ) {
            case TreatedZoneRecord.CODZONE_PAUSE:
                return context.getString(R.string.pause_time);

            case TreatedZoneRecord.CODZONE_MASSAGE:
                return context.getString(R.string.simple_massage);

            case TreatedZoneRecord.CODZONE_FULLBODY:
                return context.getString(R.string.fullBody_button);

            case TreatedZoneRecord.CODZONE_TRANSITIONS:
                return context.getString(R.string.transitions_button);

            case TreatedZoneRecord.CODZONE_UNSELECTED:
                return context.getString(R.string.unselected_zone);

            case TreatedZoneRecord.CODZONE_HEAD:
                return context.getString(R.string.head_button);

            case TreatedZoneRecord.CODZONE_HEADNECK:
                return context.getString(R.string.headNeck_button);

            case TreatedZoneRecord.CODZONE_ARMLEFT:
                return context.getString(R.string.armLeft_button);

            case TreatedZoneRecord.CODZONE_ARMRIGHT:
                return context.getString(R.string.armRight_button);

            case TreatedZoneRecord.CODZONE_LEGLEFT:
                return context.getString(R.string.legLeft_button);

            case TreatedZoneRecord.CODZONE_LEGRIGHT:
                return context.getString(R.string.legRight_button);

            case TreatedZoneRecord.CODZONE_BACK:
                return context.getString(R.string.back_button);

            case TreatedZoneRecord.CODZONE_TORSO:
                return context.getString(R.string.torso_button);

            case TreatedZoneRecord.CODZONE_LEFTFLANK:
                return context.getString(R.string.leftFlank_button);

            case TreatedZoneRecord.CODZONE_RIGHTFLANK:
                return context.getString(R.string.rightFlank_button);

            case TreatedZoneRecord.CODZONE_LEFTSIDE:
                return context.getString(R.string.leftSide_button);

            case TreatedZoneRecord.CODZONE_RIGHTSIDE:
                return context.getString(R.string.rightSide_button);

            case TreatedZoneRecord.CODZONE_UPPERBODY:
                return context.getString(R.string.upperBody_button);

            case TreatedZoneRecord.CODZONE_LOWERBODY:
                return context.getString(R.string.lowerBody_button);

            case TreatedZoneRecord.CODZONE_NECKSHOULDERS:
                return context.getString(R.string.neckShoulders_button);

            case TreatedZoneRecord.CODZONE_RIGHTPECTORALGIRDLE:
                return context.getString(R.string.rightPectoralGirdle_button);

            case TreatedZoneRecord.CODZONE_LEFTPECTORALGIRDLE:
                return context.getString(R.string.leftPectoralGirdle_button);


            case TreatedZoneRecord.CODZONE_RIGHTBICEPS:
                return context.getString(R.string.rightBiceps_button);

            case TreatedZoneRecord.CODZONE_RIGHTFOREARM:
                return context.getString(R.string.rightForearm_button);

            case TreatedZoneRecord.CODZONE_RIGHTHAND:
                return context.getString(R.string.rightHand_button);

            case TreatedZoneRecord.CODZONE_THORACIC:
                return context.getString(R.string.thoracic_button);

            case TreatedZoneRecord.CODZONE_THORAX:
                return context.getString(R.string.thorax_button);

            case TreatedZoneRecord.CODZONE_LUMBAR:
                return context.getString(R.string.lumbar_button);

            case TreatedZoneRecord.CODZONE_ABDOMEN:
                return context.getString(R.string.abdomen_button);

            case TreatedZoneRecord.CODZONE_BUTTOCKS:
                return context.getString(R.string.buttocks_button);

            case TreatedZoneRecord.CODZONE_PELVIS:
                return context.getString(R.string.pelvis_button);

            case TreatedZoneRecord.CODZONE_RIGHTHIP:
                return context.getString(R.string.rightHip_button);

            case TreatedZoneRecord.CODZONE_LEFTHIP:
                return context.getString(R.string.leftHip_button);


            case TreatedZoneRecord.CODZONE_THIGH:
                return context.getString(R.string.thigh_button);

            case TreatedZoneRecord.CODZONE_RIGHTTHIGH:
                return context.getString(R.string.rightThigh_button);

            case TreatedZoneRecord.CODZONE_LEFTTHIGH:
                return context.getString(R.string.leftThigh_button);

            case TreatedZoneRecord.CODZONE_THIGHKNEE:
                return context.getString(R.string.thighKnee_button);

            case TreatedZoneRecord.CODZONE_CALF:
                return context.getString(R.string.calf_button);

            case TreatedZoneRecord.CODZONE_SHIN:
                return context.getString(R.string.shin_button);

            case TreatedZoneRecord.CODZONE_RIGHTSHIN:
                return context.getString(R.string.rightShin_button);

            case TreatedZoneRecord.CODZONE_LEFTSHIN:
                return context.getString(R.string.leftShin_button);

            case TreatedZoneRecord.CODZONE_FEET:
                return context.getString(R.string.feet_button);

            case TreatedZoneRecord.CODZONE_RIGHTFOOT:
                return context.getString(R.string.rightFoot_button);

            case TreatedZoneRecord.CODZONE_LEFTFOOT:
                return context.getString(R.string.leftFoot_button);

            case TreatedZoneRecord.CODZONE_LEFTBICEPS:
                return context.getString(R.string.leftBiceps_button);

            case TreatedZoneRecord.CODZONE_LEFTFOREARM:
                return context.getString(R.string.leftForearm_button);

            case TreatedZoneRecord.CODZONE_LEFTHAND:
                return context.getString(R.string.leftHand_button);


            default:
                return "";
        }

    }
}