package com.fingerhits.massageassistant.massagesession.chronos;

import android.view.View;

/**
 * Created by Javier Torón on 15/03/2017.
 */

public interface IMassageSessionChronosView {
    void initChronos(long massageDuration);
    void change( String newChrono, boolean timeExceeded );

    void pauseChronometers();
    void setVisibleChrono(String chronoAbbr );
    void enableCurrentChrono( boolean timeExceeded );

    void enableFullChrono(boolean timeExceeded);
    void resumeFullChrono();
    void startFullChronoIn(long startingTimeInMilliseconds, boolean timeExceeded);
    long getFullElapsedTime(String state);
    void tickFullAndRemaining( long elapsedTime, long massageDuration );

    void hidePositionChrono();
    void resumePositionChrono();
    void startPositionChronoIn(long startingTimeInMilliseconds);
    void changePositionChronoText( String codPosition );

    void startZoneChronoIn(String label, long startingTimeInMilliseconds );
    void hideZoneChrono();
    void putZoneBackgroundNormalColor();
    void zoneStarted(View view, String state);
    void blinkZone();
    long getZoneChronoElapsedTime(String state );
    void showHideZoneChrono(boolean show);
    void stopZoneChronoInZero();
}