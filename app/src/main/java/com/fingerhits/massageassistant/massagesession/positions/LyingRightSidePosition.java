package com.fingerhits.massageassistant.massagesession.positions;

import android.graphics.drawable.Drawable;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Shows buttons for a patient Lying on its right side
 * 
 * @author Javier Torón
 * 
 */
public class LyingRightSidePosition extends Position {

    @BindString(R.string.lyingRightSidePosition) String lyingRightSidePosition;
    @BindString(R.string.lyingRightSidePositionAbbr) String lyingRightSidePositionAbbr;

    @BindDrawable(R.drawable.rightside) Drawable rightside;

	public LyingRightSidePosition(MassageSessionActivity act) {
		super(act);

        ButterKnife.bind(this, act);

		setPositionName( lyingRightSidePosition );
        setCodPosition( TreatedZoneRecord.CODPOSITION_RIGHTSIDE );
        setPositionAbbr( lyingRightSidePositionAbbr );
	}


	public Drawable background(){
		return rightside;
	}


    public boolean hasDivision(String p_codDivision){
        return (!p_codDivision.equals( Divisions.CODDIVISION_LEFTRIGHT));
    }

}