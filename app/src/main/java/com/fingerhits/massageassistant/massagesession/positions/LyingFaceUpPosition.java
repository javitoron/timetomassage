package com.fingerhits.massageassistant.massagesession.positions;

import android.graphics.drawable.Drawable;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Shows buttons for a patient Lying Face Down
 * 
 * @author Javier Torón
 * 
 */
public class LyingFaceUpPosition extends Position {

    @BindString(R.string.lyingFaceUpPosition) String lyingFaceUpPosition;
    @BindString(R.string.lyingFaceUpPositionAbbr) String lyingFaceUpPositionAbbr;

    @BindDrawable(R.drawable.faceup) Drawable faceup;

	public LyingFaceUpPosition(MassageSessionActivity act) {
		super(act);

        ButterKnife.bind(this, act);

        setPositionName( lyingFaceUpPosition );
        setCodPosition( TreatedZoneRecord.CODPOSITION_FACEUP );
        setPositionAbbr( lyingFaceUpPositionAbbr );

	}


	public Drawable background(){
		return faceup;
	}

    public boolean hasDivision(String p_codDivision){
        return true;
    }
}
