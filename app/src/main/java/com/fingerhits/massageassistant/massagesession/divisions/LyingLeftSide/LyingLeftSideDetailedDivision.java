package com.fingerhits.massageassistant.massagesession.divisions.LyingLeftSide;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

/**
 * 
 * Controls LyingLeft Detailed Division
 * 
 * @author Javier Torón
 * 
 * It has buttons for Head, RightPectoralGirdle, RightBiceps, RightForearm,
 *      RightHand, RightFlank, RightHip, RightThigh, RightShin, RightFoot.
 */
public class LyingLeftSideDetailedDivision extends LyingLeftSideDivisions {

	private Button mHead, mRightPectoralGirdle, mRightBiceps, mRightForearm,
            mRightHand, mRightFlank, mRightHip, mRightThigh, mRightShin,
            mRightFoot;

    /**
     * Constructor: Call Divisions constructor
     * @param act Caller activity
     * @param p_containerLayout Layout to place the mDivision in
     */
	public LyingLeftSideDetailedDivision(MassageSessionActivity act,
                                         LinearLayout p_containerLayout) {

        super(act, p_containerLayout);
        codDivision = Divisions.CODDIVISION_DETAILED;
	}


    /*Static methods*/
    public static String[] getCodZonesArray(){
        return new String[] { TreatedZoneRecord.CODZONE_HEAD,
                TreatedZoneRecord.CODZONE_RIGHTBICEPS,
                TreatedZoneRecord.CODZONE_RIGHTFLANK,
                TreatedZoneRecord.CODZONE_RIGHTFOOT,
                TreatedZoneRecord.CODZONE_RIGHTFOREARM,
                TreatedZoneRecord.CODZONE_RIGHTHAND,
                TreatedZoneRecord.CODZONE_RIGHTHIP,
                TreatedZoneRecord.CODZONE_RIGHTPECTORALGIRDLE,
                TreatedZoneRecord.CODZONE_RIGHTSHIN,
                TreatedZoneRecord.CODZONE_RIGHTTHIGH };
    }



    /* IMPLEMENTATION OF ABSTRACT METHODS */
    /**
     * Shows button for each zone
     */
    public void showZones( View v ){
        showHeadZones((LinearLayout) v);
        showPectoralGirdleLayout((LinearLayout) v);
        showFullBodyZones((LinearLayout) v);
    }


    public Divisions prevDivision(){
        clearZones();

        LyingLeftSideDivisions newDivision =
                new LyingLeftSideDefaultDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }


    public Divisions nextDivision(){

        clearZones();

        LyingLeftSideDivisions newDivision =
                new LyingLeftSideSingleDivision(activity, containerLayout);
        newDivision.showZones(activity.findViewById(R.id.positionLayout));
        return newDivision;
    }

    /* OVERRIDES */
    @Override
    public String zoneTitle( Button btn ){
        if (btn == mHead ){
            return res.getString(R.string.head_button);
        }

        if (btn == mRightPectoralGirdle ){
            return res.getString(R.string.rightPectoralGirdle_button);
        }

        if (btn == mRightBiceps ){
            return res.getString(R.string.rightBiceps_button);
        }

        if (btn == mRightForearm ){
            return res.getString(R.string.rightForearm_button);
        }

        if (btn == mRightHand ){
            return res.getString(R.string.rightHand_button);
        }

        if (btn == mRightFlank ){
            return res.getString(R.string.rightFlank_button);
        }

        if (btn == mRightHip ){
            return res.getString(R.string.rightHip_button);
        }

        if (btn == mRightThigh ){
            return res.getString(R.string.rightThigh_button);
        }

        if (btn == mRightShin ){
            return res.getString(R.string.rightShin_button);
        }

        if (btn == mRightFoot ){
            return res.getString(R.string.rightFoot_button);
        }

        return super.zoneTitle(btn);
    }


    @Override
    public String codZone( Button btn ){
        if (btn == mHead ){
            return TreatedZoneRecord.CODZONE_HEAD;
        }

        if (btn == mRightPectoralGirdle){
            return TreatedZoneRecord.CODZONE_RIGHTPECTORALGIRDLE;
        }

        if (btn == mRightBiceps){
            return TreatedZoneRecord.CODZONE_RIGHTBICEPS;
        }

        if (btn == mRightForearm){
            return TreatedZoneRecord.CODZONE_RIGHTFOREARM;
        }

        if (btn == mRightHand){
            return TreatedZoneRecord.CODZONE_RIGHTHAND;
        }

        if (btn == mRightFlank){
            return TreatedZoneRecord.CODZONE_RIGHTFLANK;
        }

        if (btn == mRightHip){
            return TreatedZoneRecord.CODZONE_RIGHTHIP;
        }

        if (btn == mRightThigh){
            return TreatedZoneRecord.CODZONE_RIGHTTHIGH;
        }

        if (btn == mRightShin){
            return TreatedZoneRecord.CODZONE_RIGHTSHIN;
        }

        if (btn == mRightFoot){
            return TreatedZoneRecord.CODZONE_RIGHTFOOT;
        }

        return super.codZone(btn);
    }


    @Override
    public Button buttonForCodZone( String codZone ){
        if ( codZone.equals( TreatedZoneRecord.CODZONE_HEAD ) ){
            return mHead;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTPECTORALGIRDLE ) ){
            return mRightPectoralGirdle;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTBICEPS ) ){
            return mRightBiceps;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTFOREARM ) ){
            return mRightForearm;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTHAND ) ){
            return mRightHand;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTFLANK ) ){
            return mRightFlank;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTHIP ) ){
            return mRightHip;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTTHIGH ) ){
            return mRightThigh;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTSHIN ) ){
            return mRightShin;
        }

        if ( codZone.equals( TreatedZoneRecord.CODZONE_RIGHTFOOT ) ){
            return mRightFoot;
        }

        return super.buttonForCodZone(codZone);
    }


    @Override
	public void enableAll( Boolean enabled ){
        super.enableAll(enabled);

        ZonesUtils.enableButton(res, mHead, enabled);
        ZonesUtils.enableButton(res, mRightPectoralGirdle, enabled);
        ZonesUtils.enableButton(res, mRightBiceps, enabled);
        ZonesUtils.enableButton(res, mRightForearm, enabled);
        ZonesUtils.enableButton(res, mRightHand, enabled);
        ZonesUtils.enableButton(res, mRightFlank, enabled);
        ZonesUtils.enableButton(res, mRightHip, enabled);
        ZonesUtils.enableButton(res, mRightThigh, enabled);
        ZonesUtils.enableButton(res, mRightShin, enabled);
        ZonesUtils.enableButton(res, mRightFoot, enabled);
	}

    /* PRIVATES */
    /**
     * Shows buttons in HeadLayout
     */
    private void showHeadZones( LinearLayout mainLayout ){
        mHead = super.setHeadZones(mainLayout, 4);
    }

    /**
     * Shows buttons in PectoralGirdleLayout
     */
    private void showPectoralGirdleLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams pectoralGirdleLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 3);
        pectoralGirdleLayoutParameters.setMargins(0,
                GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout pectoralGirdleLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        pectoralGirdleLayout.setId(R.id.pectoralGirdleLayoutId);

        super.showFixViewWeight(pectoralGirdleLayout, 1);

        mRightPectoralGirdle = setZoneBtn(
                R.id.rightPectoralGirdleMassageBtnId,
                pectoralGirdleLayout, 4,
                R.string.rightPectoralGirdle_button );

        super.showFixViewWeight(pectoralGirdleLayout, 1);

        mainLayout.addView(pectoralGirdleLayout,
                pectoralGirdleLayoutParameters);

    }


    /**
     * Shows buttons in fullBodyLayout
     */
    private void showFullBodyZones( LinearLayout mainLayout ){
        LinearLayout.LayoutParams fullBodyLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 25);
        fullBodyLayoutParameters.setMargins(0,
                GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1));

        LinearLayout fullBodyLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        fullBodyLayout.setId(R.id.fullBodyLayoutId);

        super.showFixViewWeight(fullBodyLayout, 2);

        setTrunkLegsLayout(fullBodyLayout);
        setRightArmLayout(fullBodyLayout);

        super.showFixViewWeight(fullBodyLayout, 2);

        mainLayout.addView(fullBodyLayout, fullBodyLayoutParameters);
    }


    private void setTrunkLegsLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams trunkLegsLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 7);
        trunkLegsLayoutParameters.setMargins(
                GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout trunkLegsLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical,
                        null);
        trunkLegsLayout.setId(R.id.trunkLegsLayoutId);


        mRightBiceps = setZoneBtnVertical(R.id.rightBicepsMassageBtnId,
                trunkLegsLayout, 3, R.string.rightBiceps_button);

        mRightFlank = setZoneBtnVertical(R.id.rightFlankMassageBtnId,
                trunkLegsLayout, 6, R.string.rightFlank_button);

        mRightHip = setZoneBtnVertical(R.id.rightHipMassageBtnId,
                trunkLegsLayout, 6, R.string.rightHip_button);


        mRightThigh = setZoneBtnVertical(R.id.rightThighMassageBtnId,
                trunkLegsLayout, 8, R.string.rightThigh_button);

        mRightShin = setZoneBtnVertical(R.id.rightShinMassageBtnId,
                trunkLegsLayout, 8, R.string.rightShin_button);

        mRightFoot = setZoneBtnVertical(R.id.rightFootMassageBtnId,
                trunkLegsLayout, 6, R.string.rightFoot_button);


        mainLayout.addView(trunkLegsLayout, trunkLegsLayoutParameters);
    }


    private void setRightArmLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams rightArmLayoutParameters =
                new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 6);
        rightArmLayoutParameters.setMargins(GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout rightArmLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group_vertical, null);
        rightArmLayout.setId(R.id.rightArmLayoutId);

        setHorizontalRightArmLayout(rightArmLayout);

        super.showFixViewWeight(rightArmLayout, 6);

        mainLayout.addView(rightArmLayout, rightArmLayoutParameters);
    }


    private void setHorizontalRightArmLayout( LinearLayout mainLayout ){
        LinearLayout.LayoutParams horizontalRightArmLayoutParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, 0, 2);
        horizontalRightArmLayoutParameters.setMargins(
                GraphicUtils.dpToPixels(res, 1), 0,
                GraphicUtils.dpToPixels(res, 1), 0);

        LinearLayout horizontalRightArmLayout =
                (LinearLayout) activity.getLayoutInflater().inflate(
                        R.layout.template_layout_zone_group, null);
        horizontalRightArmLayout.setId(R.id.horizontalRightArmLayoutId);

        mRightForearm = setZoneBtn(R.id.rightForearmMassageBtnId,
                horizontalRightArmLayout, 2, R.string.rightForearm_button);

        mRightHand = setZoneBtn(R.id.rightHandMassageBtnId,
                horizontalRightArmLayout, 3, R.string.rightHand_button);

        mainLayout.addView(horizontalRightArmLayout,
                horizontalRightArmLayoutParameters);
    }

}
