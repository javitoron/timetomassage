package com.fingerhits.massageassistant.massagesession.positions;

import android.graphics.drawable.Drawable;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Shows buttons for a patient sitting front
 * 
 * @author Javier Torón
 * 
 */
public class SittingFrontPosition extends Position {

    @BindString(R.string.sittingFrontPosition) String sittingFrontPosition;
    @BindString(R.string.sittingFrontPositionAbbr) String sittingFrontPositionAbbr;

    @BindDrawable(R.drawable.sittingfront) Drawable sittingfront;

    public SittingFrontPosition(MassageSessionActivity act) {
		super(act);

        ButterKnife.bind(this, act);

        setPositionName( sittingFrontPosition );
        setCodPosition( TreatedZoneRecord.CODPOSITION_SITTINGFRONT );
        setPositionAbbr( sittingFrontPositionAbbr );
    }


	public Drawable background(){
		return sittingfront;
	}


    public boolean hasDivision(String p_codDivision){
        return (!p_codDivision.equals( Divisions.CODDIVISION_UPPERLOWER));
    }

}
