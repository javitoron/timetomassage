package com.fingerhits.massageassistant.syncadapter;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.utils.MiscUtils;
import com.fingerhits.massageassistant.options.MySharedPreferences;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.message.BasicHeader;

import static xdroid.toaster.Toaster.toast;


/**
 * Common functions used in communication with MySQL server
 *
 * @author javi on 12/03/2015.
 */
public class SyncDBUtils {

    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.fingerhits.massageassistant.datasync.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "fingerhits.com";

    public static String serverPath(Resources res){
        return res.getString( R.string.server_path );

        //"http://192.168.2.33/timetomassage/";
        //10.0.2.2/timetomassage
        //fingerhits.com/timetomassageDB
    }

    public static String statsPhpPath( Resources res ) {
        return serverPath(res) + "update_massage_stats.php";
    }


    /**
     * Creates an account for the sync adapter
     *
     * @param context The application context
     * @param userId To get the installationId
     */
    @SuppressWarnings("UnusedParameters")
    public static Account CreateSyncAccount(Context context, String userId) {
        // Create the account type and default account
        return new Account( userId, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
      /*  AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        Context.ACCOUNT_SERVICE);

         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        /*if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.

        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.

        }*/

    }


    public static Bundle getSyncBundle() {
        Bundle syncBundle = new Bundle();
        syncBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        syncBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        return syncBundle;
    }

    public static String[] serializeHeaders(Header[] headers) {
        if (headers == null) {
            return new String[0];
        }
        String[] rtn = new String[headers.length * 2];
        int index = -1;
        for (Header h : headers) {
            rtn[++index] = h.getName();
            rtn[++index] = h.getValue();
        }
        return rtn;
    }

    public static Header[] deserializeHeaders(String[] serialized) {
        if (serialized == null || serialized.length % 2 != 0) {
            return new Header[0];
        }
        Header[] headers = new Header[serialized.length / 2];
        for (int i = 0, h = 0; h < headers.length; i++, h++) {
            headers[h] = new BasicHeader(serialized[i], serialized[++i]);
        }
        return headers;
    }


    @SuppressWarnings("UnusedParameters")
    public static void commsErrorManager(int statusCode, Header[] headers,
                                         byte[] responseBody,
                                         Throwable error,
                                         Resources res ) {

        //      toast("Failed");

        //Only show warnings in debug mode
        if (BuildConfig.DEBUG) {
            if (statusCode == 404) {
                toast("Requested resource not found");
            } else if (statusCode == 500) {
                toast("Server error");
            } else {
                toast("Unexpected Error occurred! [Most common Error: Device might not be connected to Internet]. Status code: " +
                        statusCode + ". Error: " + error.getMessage() + ". Cause: " + error.getCause());
            }
        }
    }

}