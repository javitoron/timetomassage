package com.fingerhits.massageassistant.syncadapter;

import android.content.res.Resources;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

/**
 * Send user data to fingerhits.com DB
 * Created by javi on 08/03/2016.
 */
public class SyncUserDB {
    private MySharedPreferences mPrefs;

    public SyncUserDB(MySharedPreferences p_prefs){
        mPrefs = p_prefs;
    }

    /* USER FUNCTIONS */
    public void sendInstallationIdToMySQLDB(Resources res,
                                                   MySharedPreferences prefs) {
        //Create AsyncHttpClient object

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("op", "newInstallation" );
        params.put("installationsJSON", composeGoogleIdJSON(prefs));

        client.post( SyncDBUtils.serverPath(res) + "installations_ops.php", params,
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers,
                                          byte[] responseBody) {

                        //MiscUtils.showToast(mContext, "Success");
                        String response;
                        try {
                            response =
                                    ( responseBody == null ?
                                            null :
                                            new String(responseBody,
                                                    getCharset()) );

                        } catch (UnsupportedEncodingException e) {
                            response = null;
                        }

                        if(response != null) {

                            try {
                                JSONArray arr = new JSONArray(response);

                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject obj = (JSONObject) arr.get(i);

                                    if( obj.getLong("remoteId") != -1 ) {
                                        mPrefs.saveBool(
                                                mPrefs.PREFS_INSTALLATION_ID_SENT_KEY,
                                                true);
                                    }

                                }



                                //MiscUtils.showToast(context, "DB Sync completed!");
                            } catch (JSONException e) {
                                //MiscUtils.showToast(context,
                                //      "Error Occurred [Server's JSON response might be invalid]!");
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers,
                                          byte[] responseBody, Throwable error) {
/*                        if (statusCode == 404) {
                           /* MiscUtils.showToast(mContext,
                                    "Requested resource not found");
                        } else if (statusCode == 500) {
                            /*MiscUtils.showToast(mContext,
                                    "Something went wrong at server end");
                        } else {
                            /*MiscUtils.showToast(mContext,
                                    "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet]. Status code: " +
                                            statusCode + ". Error: " + error.getMessage() + ". Cause: " + error.getCause());
                        }*/
                    }
                });
    }

    public void sendGoogleIdToMySQLDB(Resources res) {
        //Create AsyncHttpClient object

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("op", "assignGoogleId" );
        params.put("installationsJSON", composeGoogleIdJSON(mPrefs));

        client.post(SyncDBUtils.serverPath(res) + "installations_ops.php", params,
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers,
                                          byte[] responseBody) {

                        //MiscUtils.showToast(mContext, "Success");
                        /*String response;
                        try {
                            response = responseBody == null ? null : new String(responseBody, getCharset());

                        } catch (UnsupportedEncodingException e) {
                            response = null;
                        }

                        if(response != null) {

                            try {
                                JSONArray arr = new JSONArray(response);

                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject obj = (JSONObject) arr.get(i);
                                }
                                //MiscUtils.showToast(context, "DB Sync completed!");
                            } catch (JSONException e) {
                                //MiscUtils.showToast(context,
                                //      "Error Occurred [Server's JSON response might be invalid]!");
                                e.printStackTrace();
                            }
                        }*/
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers,
                                          byte[] responseBody, Throwable error) {
/*                        if (statusCode == 404) {
                           /* MiscUtils.showToast(mContext,
                                    "Requested resource not found");
                        } else if (statusCode == 500) {
                            /*MiscUtils.showToast(mContext,
                                    "Something went wrong at server end");
                        } else {
                            /*MiscUtils.showToast(mContext,
                                    "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet]. Status code: " +
                                            statusCode + ". Error: " + error.getMessage() + ". Cause: " + error.getCause());
                        }*/
                    }
                });
    }

    /**
     * Compose JSON ( "installationURL", "googleId" )
     * @return JSON for assigning google id to the current installation
     */
    public String composeGoogleIdJSON(MySharedPreferences prefs) {
        ArrayList<HashMap<String, String>> wordList = new ArrayList<>();

        HashMap<String, String> map = new HashMap<>();

        map.put("installationURL",
                prefs.getString(prefs.PREFS_INSTALLATION_ID_KEY));
        map.put("googleId", prefs.getString(prefs.PREFS_USER_ID_KEY));

/*        map.put("latitude",
                prefs.getString(prefs.PREFS_USER_LAST_LATITUDE_KEY));
        map.put("longitude",
                prefs.getString(prefs.PREFS_USER_LAST_LONGITUDE_KEY));
*/
        wordList.add(map);

        Gson gson = new GsonBuilder().create();
        //Use GSON to serialize Array List to JSON
        return gson.toJson(wordList);
    }

}
