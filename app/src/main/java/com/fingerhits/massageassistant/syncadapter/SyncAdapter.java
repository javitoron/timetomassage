package com.fingerhits.massageassistant.syncadapter;

/**
 * Sends usage data to timetomassageDB
 *
 * Created by javi on 13/11/2015.
 */

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.options.MySharedPreferences;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    // Global variables
    // Define a variable to contain a content resolver instance
    private Context mContext;
   // private Resources mRes;
    ContentResolver mContentResolver;
    private MassageAssistantDBHelper mDatabaseHelper;
    private MySharedPreferences mPrefs;

    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContext = context;
       // mRes = mContext.getResources();
        mContentResolver = mContext.getContentResolver();
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);
        mPrefs = new MySharedPreferences(mContext);
    }


    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContext = context;
        mContentResolver = mContext.getContentResolver();
        mDatabaseHelper = new MassageAssistantDBHelper(mContext);
        mPrefs = new MySharedPreferences(mContext);
    }


    /*
     * Specify the code you want to run in the sync adapter. The entire
     * sync adapter runs in a background thread, so you don't have to set
     * up your own background processing.
     */
    @Override
    public void onPerformSync(
            Account account,
            Bundle extras,
            String authority,
            ContentProviderClient provider,
            SyncResult syncResult) {

/*        MassageStatsUpdater massageStatsUpdater =
                new MassageStatsUpdater(mContext);

        ArrayList<HashMap<String, String>> newMassagesList = new ArrayList<>();

        ArrayList<HashMap<String, String>> finishMassagesList =
                new ArrayList<>();

        ArrayList<HashMap<String, String>> deleteMassagesList =
                new ArrayList<>();

        ArrayList<HashMap<String, String>> deleteZonesList = new ArrayList<>();

        Cursor massageStats = mDatabaseHelper.getAllMassageStatsRecords();

        if( massageStats.moveToFirst() ){
            do {

                MassageStatsRecord ms = new MassageStatsRecord(massageStats);

                HashMap<String, String> map = new HashMap<>();

                map.put("installationURL",
                        mPrefs.getString(mPrefs.PREFS_INSTALLATION_ID_KEY));
                map.put("localMassageId", String.valueOf(ms.getIdMassage()));
                map.put("localMassageStatsId", String.valueOf(ms.getId()));
                map.put("operationTime",
                        MassageAssistantDBHelper.convertDateTimeForSQLLite(
                                ms.getOperationTime()));

                if( ms.isNew()){
                    map.put("started",
                            MassageAssistantDBHelper.convertDateTimeForSQLLite(
                                    ms.getStarted()));
                    map.put("longitude", ms.getLongitude());
                    map.put("latitude", ms.getLatitude());

                    newMassagesList.add(map);
                }

                if( ms.isFinishing()){
                    map.put("ended",
                            MassageAssistantDBHelper.convertDateTimeForSQLLite(
                                    ms.getEnded()));
                    map.put("restarted", String.valueOf(ms.getRestarted()));
                    map.put("duration", String.valueOf(ms.getDuration()));
                    map.put("positions", String.valueOf(ms.getPositions()));
                    map.put("divisions", String.valueOf(ms.getDivisions()));
                    map.put("zones", String.valueOf(ms.getZones()));


                    finishMassagesList.add(map);
                }

                if( ms.isDeleting()){
                    deleteMassagesList.add(map);
                }

                if( ms.isDeletingZone()){
                    map.put("ended",
                            MassageAssistantDBHelper.convertDateTimeForSQLLite(
                                    ms.getEnded()));
                    map.put("duration", String.valueOf(ms.getDuration()));
                    map.put("positions", String.valueOf(ms.getPositions()));
                    map.put("divisions", String.valueOf(ms.getDivisions()));
                    map.put("zones", String.valueOf(ms.getZones()));


                    deleteZonesList.add(map);
                }

            } while( massageStats.moveToNext() );
        }

        Gson gson = new GsonBuilder().create();

        if( !newMassagesList.isEmpty()) {
            massageStatsUpdater.newMassageToMySQLDB(
                    gson.toJson(newMassagesList));
        }

        if( !finishMassagesList.isEmpty()) {
            massageStatsUpdater.finishMassageToMySQLDB(
                    gson.toJson(finishMassagesList));
        }

        if( !deleteMassagesList.isEmpty()) {
            massageStatsUpdater.deleteMassageToMySQLDB(
                    gson.toJson(deleteMassagesList));
        }

        if( !deleteZonesList.isEmpty()) {
            massageStatsUpdater.deleteZoneToMySQLDB(
                    gson.toJson(deleteZonesList));
        }
*/
    /*
Connecting to a server

Downloading and uploading data

Handling data conflicts or determining how current the data is

Clean up.

In addition to your sync-related tasks, you should try to combine your regular
network-related tasks and add them to onPerformSync(). By concentrating all of
your network tasks in this method, you conserve the battery power that's needed
to start and stop the network interfaces. To learn more about making network
access more efficient, see the training class Transferring Data Without
Draining the Battery, which describes several network access tasks you can
include in your data transfer code.

   */



    }
}