package com.fingerhits.massageassistant.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by Javier Torón on 27/02/2017.
 */

public class EnvironmentHelper {
    public File getExternalStoragePublicDirectory(String dir) {
        return Environment.getExternalStoragePublicDirectory(dir);
    }
}
