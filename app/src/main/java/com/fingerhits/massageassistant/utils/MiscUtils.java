package com.fingerhits.massageassistant.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Assorted functions
 *
 * @author javi on 12/03/2015.
 */
public class MiscUtils {

    public static final int GLOBAL_DEFAULT_IDMASSEUR = 1;
    /**
     * For use when app is multiuser. Nowadays just returns 1
     * @return - int 1
     */
    public static int currentLocalUser(){
        return GLOBAL_DEFAULT_IDMASSEUR;
    }



    public static void prepareActionBar(Activity activity) {
        //noinspection EmptyCatchBlock
        try {
            //noinspection ConstantConditions
            activity.getActionBar().setHomeButtonEnabled(true);
            activity.getActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            //Do nothing: If we have no ActionBar, we cannot make it a back button
        }
    }

    public static void logActivityStarting(Activity activity, String name) {

        if (BuildConfig.DEBUG) {
            Log.d(name, "Entering");
        }
        MiscUtils.sendActivityTracker(activity, name);
    }


    public static void sendActivityTracker(Activity act, String name){
        Tracker mTracker;
        if (!BuildConfig.DEBUG) {
            // Obtain the shared Tracker instance.
            MainApp mainApp = (MainApp) act.getApplication();
            mTracker = mainApp.getDefaultTracker();
            mTracker.setScreenName(name);
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

    public static void sendExceptionTracker(Activity act, String name,
                                            String error, boolean isFatal){
        Tracker mTracker;
        if (!BuildConfig.DEBUG) {
            // Obtain the shared Tracker instance.
            MainApp mainApp = (MainApp) act.getApplication();
            mTracker = mainApp.getDefaultTracker();
            mTracker.setScreenName(name);
            mTracker.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(error)
                    .setFatal(isFatal)
                    .build());
        }
    }

    public static void sendEventTracker(Activity activity, String name,
                                        String category, String action,
                                        String label, Long valor) {
        sendEventTracker(activity.getApplication(), name,
                category, action,
                label, valor);
    }

    public static void sendEventTracker(Application app, String name,
                                        String category, String action,
                                        String label, Long valor){
        Tracker mTracker;
        if (!BuildConfig.DEBUG) {
            // Obtain the shared Tracker instance.
            MainApp mainApp = (MainApp) app;
            mTracker = mainApp.getDefaultTracker();
            mTracker.setScreenName(name);

            HitBuilders.EventBuilder event = new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(label);

            if( valor != -1L ) event.setValue(valor);

            Map<String, String> eventMap = event.build();
            mTracker.send(eventMap);
        }
    }


    public static void sendEmail( Activity act, String sEmailTo,
                                  String sSubject, String sMassageSummary ){

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {sEmailTo});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, sSubject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, sMassageSummary);

        Resources res = act.getResources();


        try {
            act.startActivity(Intent.createChooser(emailIntent,
                    res.getString(R.string.sendEmail_chooser)));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(act.getApplicationContext(),
                    res.getString(R.string.sendEmail_error),
                    Toast.LENGTH_SHORT).show();
        }
    }


    public static Activity getActivity( View view ) {
        Context context = view.getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    public static void playTone( int tone ){
        ToneGenerator toneG =
                new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        toneG.startTone(tone, 200);

        //Previously used in massages:
        // ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD
        //ToneGenerator.TONE_PROP_ACK
    }


    /**
     * Shows short toast
     * @param context To pass to the toast
     * @param text To show
     */
    public static void showToast( Context context, String text ) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }


    public static String extractTitle(String html){
        if(html == "" ){
            return "";
        }
        else {
            try {
                final Pattern pattern = Pattern.compile("<h1>(.+?)</h1>");
                final Matcher matcher = pattern.matcher(html);
                matcher.find();
                return matcher.group(1);
            } catch (Exception e) {
                return "";
            }
        }
    }


    public static int findInTypedArray(TypedArray array, int value) {
        for(int i=0; i<array.length(); i++)
            if(array.getResourceId(i, 0) == value)
                return i;

        return -1;
    }

    public static int findInStringArray(String[] array, String value) {
        for(int i=0; i<array.length; i++)
            if(array[i].equals(value))
                return i;

        return -1;
    }


    // Open URL in device browser
    public static void openURL(Activity callerAct, String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        callerAct.startActivity(browserIntent);
    }

}