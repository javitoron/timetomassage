package com.fingerhits.massageassistant.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import static android.content.Context.POWER_SERVICE;

/**
 * Created by Javier Torón on 03/05/2016.
 */
public class GraphicUtils {

    public static void blinkScreen(ViewGroup layout, int blinks ){
        Animation animation = new AlphaAnimation(1, 0);
        // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator());
        // do not alter animation rate
        animation.setRepeatCount(blinks);
        // Repeat animation infinitely
        animation.setRepeatMode(Animation.REVERSE);
        // Reverse animation at the end so the layout will fade back in
        layout.startAnimation(animation);
    }

    public static void blink(View view, int blinks ){
        Animation animation = new AlphaAnimation(1, 0);
        // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator());
        // do not alter animation rate
        animation.setRepeatCount(blinks);
        // Repeat animation infinitely
        animation.setRepeatMode(Animation.REVERSE);
        // Reverse animation at the end so the layout will fade back in
        view.startAnimation(animation);
    }


    public static boolean isScreenOn(Activity act) {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            // I'm counting
            // STATE_DOZE, STATE_OFF, STATE_DOZE_SUSPENDED
            // all as "OFF"

            DisplayManager dm = (DisplayManager) act.getSystemService(Context.DISPLAY_SERVICE);
            for (Display display : dm.getDisplays ()) {
                if (display.getState () == Display.STATE_ON ||
                        display.getState () == Display.STATE_UNKNOWN) {
                    return true;
                }
            }

            return false;
        }

        PowerManager powerManager = (PowerManager) act.getSystemService(POWER_SERVICE);

        return powerManager.isScreenOn();
    }
    /**
     * Converts dp in pixels based on screen metrics
     * @param res - To get the metrics
     * @param dp - Density pixels to convert
     * @return - int pixels
     */
    public static int dpToPixels(Resources res, int dp ) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                res.getDisplayMetrics());
    }

    /**
     * Returns a string with web color corresponding to android or custom color
     * @param res - To get the color
     * @param colorId - Int id of the color
     * @return - String in format #rrggbb
     */
    public static String colorToString( Resources res, int colorId ){
        return "#" + res.getString(colorId).substring(3);
    }

    @SuppressWarnings("deprecation")
    public static void defineButtonStyle(Context context, Button btn, int styleId){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            btn.setTextAppearance(styleId);
        }
        else
        {
            btn.setTextAppearance(context, styleId);
        }
    }

    @SuppressWarnings("deprecation")
    public static void defineTextStyle(Context context, TextView text,
                                       int styleId){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            text.setTextAppearance(styleId);
        }
        else
        {
            text.setTextAppearance(context, styleId);
        }
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static void setButtonBorder( Button btn, Drawable border ) {

        if(btn != null) {
            int sdk = Build.VERSION.SDK_INT;
            if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
                btn.setBackgroundDrawable(border);
            } else {
                btn.setBackground(border);
            }
        }
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getColor( Resources res, int color ){

        int sdk = Build.VERSION.SDK_INT;
        if(sdk < 23) {
            return res.getColor(color);
        }
        else {
            return res.getColor(color, null);
        }

    }

    public static String getHexColor(final int color) {
        return String.format("#%06X", (0xFFFFFF & color));
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static Drawable getDrawable( Resources res, int p_drawable ){

        int sdk = Build.VERSION.SDK_INT;
        if(sdk < 21) {
            return res.getDrawable(p_drawable);
        }
        else {
            return res.getDrawable(p_drawable, null);
        }

    }

    public static void disableSelectionBtn(View btn) {

        btn.setEnabled(false);
        btn.setAlpha(0.2f);
    }

    public static void enableSelectionBtn(View btn) {

        btn.setEnabled(true);
        btn.setAlpha(1f);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static void setImageButtonBorder(ImageButton btn, Drawable border ){

        int sdk = Build.VERSION.SDK_INT;
        if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
            btn.setBackgroundDrawable(border);
        }
        else {
            btn.setBackground(border);
        }

    }

    public static Point screenSize(Activity act) {
        Display display = act.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size;
    }

    /**
     * Bitmap functions
     */

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both height and width larger than the requested height and
            // width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res,
                                                         int resId,
                                                         int reqWidth,
                                                         int reqHeight) {
        Bitmap bm = null;
        Boolean bError;

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        do{
            bError = true;
            try {
                // First decode with inJustDecodeBounds=true to check dimensions

                // Calculate inSampleSize
                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

                // Decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeResource(res, resId, options);
                bError = false;
            }catch (OutOfMemoryError e){
                reqWidth = reqWidth/2;
                reqHeight = reqHeight/2;
            }
        }while (bError);

        return bm;
    }
}
