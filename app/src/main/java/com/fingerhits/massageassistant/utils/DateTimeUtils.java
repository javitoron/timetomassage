package com.fingerhits.massageassistant.utils;


import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Assorted date and time functions
 *
 * @author javi on 16/11/2015.
 */
public class DateTimeUtils {

    /******************************************
     * CONVERSIONS DURATION <--> STRING
     *****************************************/
    /**
     * Converts duration to mm:ss format or hh:mm:ss if it's greater than 59:59
     * @param i_duration In milliseconds
     * @return mm:ss string
     */
    public static String durationToString(int i_duration) {
        return durationToString((long) i_duration );
    }
        /**
         * Converts duration to mm:ss format or hh:mm:ss if it's greater than 59:59
         * @param l_duration In milliseconds
         * @return mm:ss string
         */
        public static String durationToString(long l_duration){
        String negativeSign = "";
        if(l_duration < 0){
            negativeSign = "-";
            l_duration = -l_duration;
        }

        int seconds = (int) (l_duration / 1000) % 60 ;
        int minutes = (int) ((l_duration / (1000*60)) % 60);
        int hours   = (int) ((l_duration / (1000*60*60)) % 24);

        String seconds_str = String.valueOf( seconds );
        if( seconds <= 9 ) seconds_str = "0" + seconds_str;

        String minutes_str = String.valueOf( minutes );
        if( minutes <= 9 ) minutes_str = "0" + minutes_str;

        String time_str = minutes_str + ":" + seconds_str;

        if( hours == 0 ) {
            return negativeSign + time_str;
                    //new SimpleDateFormat("mm:ss",
                    //java.util.Locale.getDefault()).format(l_time);
        }
        else{
            String hours_str   = String.valueOf( hours );
            if( hours <= 9 ) hours_str = "0" + hours_str;

            return negativeSign + hours_str + ":" + time_str;
//            return new SimpleDateFormat("HH:mm:ss",
  //                  java.util.Locale.getDefault()).format(l_time);
        }
    }


    /**
     * Converts duration to mm ss format or hh mm ss
     * @param l_duration In milliseconds
     * @return mm:ss string
     */
    public static String durationToZoneString(long l_duration){
        return durationToString(l_duration).replace(":", " ");

    }


    /**
     * Converts mm:ss or hh:mm:ss string to duration in milliseconds
     * @param s_duration String to convert
     * @return long milliseconds
     */
    public static long durationStringToMilliseconds(String s_duration){
        int hours, minutes, seconds;
        int sign = 1;
        try {
            if(s_duration.contains("-")){
                sign = -1;
                s_duration= s_duration.replace("-", "");
            }
            String[] tokens = s_duration.split(":");

            if(tokens.length == 2) {
                minutes = Integer.parseInt(tokens[0]);
                seconds = Integer.parseInt(tokens[1]);
                return sign * ((60 * minutes) + seconds) * 1000;
            }
            else{
                hours = Integer.parseInt(tokens[0]);
                minutes = Integer.parseInt(tokens[1]);
                seconds = Integer.parseInt(tokens[2]);
                return sign * ((3600 * hours) + (60 * minutes) + seconds) * 1000;
            }
        }
        catch(Exception e){
            return 0;
        }
    }


    /**
     * Converts duration in mm ss or hh mm ss format to milliseconds
     * @param s_zoneDuration String to convert
     * @return long milliseconds
     */
    public static long durationZoneStringToMilliseconds(String s_zoneDuration){
        return durationStringToMilliseconds(s_zoneDuration.replace(" ", ":"));
    }

    /******************************************
     * DATE FUNCTIONS
     *****************************************/
    public static Date now() {
        Calendar c = Calendar.getInstance();
        return c.getTime();
    }

    /**
     * Compares two dates to see if they are in the same day (despite the time)
     * @param date1 To compare
     * @param date2 To compare
     * @return is the same day or not?
     */
    public static boolean sameDay( Date date1, Date date2){
        if(date1 != null && date2 != null ) {
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();

            cal1.setTime(date1);
            cal2.setTime(date2);

            return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
        }
        else
        {
            //There is a null date
            return false;
        }
    }

    /**
     * Date to Formatted String
     *
     * @param dateToConvert`Date to store in SQLLite
     * @param sdf Desired format of the date string
     * @return a string ready to save in SQLLite
     */
    public static String convertDateTimeToFormattedString( Date dateToConvert,
                                                    SimpleDateFormat sdf ) {
        if( dateToConvert == null){
            return "";
        }
        else {
            return sdf.format(dateToConvert);
        }
    }

    /**
     * String to Date
     *
     * @param dateToConvert A string retrieved from SQLLite
     * @param sdf Format of the date string to convert
     * @return a Date converted from string
     */
    public static Date FormattedStringToDateTime( String dateToConvert,
                                                  SimpleDateFormat sdf ) {
        try {
            return sdf.parse( dateToConvert );
        }
        catch (ParseException e) {
            return null;
        }
    }

    /**
     * SQLLite date format
     *
     * @return SimpleDateFormat for SQLLite
     */
    @SuppressLint("SimpleDateFormat")
    public static SimpleDateFormat SQLLiteFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    /**
     * DateTime format for file name
     *
     * @return SimpleDateFormat
     */
    @SuppressLint("SimpleDateFormat")
    public static SimpleDateFormat filenameDateTimeFormat() {
        return new SimpleDateFormat("yyyyMMdd_HHmmss");
    }

    /**
     * Only Date with month abbreviation
     *
     * @return SimpleDateFormat
     */
    @SuppressLint("SimpleDateFormat")
    public static SimpleDateFormat onlyDateMonthAbbDateTimeFormat() {
        return new SimpleDateFormat("dd MMM yyyy");
    }

    /**
     * Only Time (without seconds)
     *
     * @return SimpleDateFormat
     */
    @SuppressLint("SimpleDateFormat")
    public static SimpleDateFormat onlyTimeDateTimeFormat() {
        return new SimpleDateFormat("hh:mm");
    }



}