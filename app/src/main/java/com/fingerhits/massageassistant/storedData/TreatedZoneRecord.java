package com.fingerhits.massageassistant.storedData;

import android.content.Context;
import android.database.Cursor;

import com.fingerhits.massageassistant.massagesession.positions.Position;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.utils.DateTimeUtils;

import java.util.Date;

/**
 * Represents each part of the massage. Fields: id, idMassage, codPosition,
 *      codDivision, codZone, startDateTime, endDateTime, duration, comments
 *
 * @author javi on 01/03/2015.
 *
 */
public class TreatedZoneRecord {
    private long mId;
    private long mIdMassage;
    private String mCodPosition, mCodDivision, mCodZone;
    private Date mStartDateTime, mEndDateTime;
    private long mDuration;
    private String mComments;

    public static final String XML_OPEN = "<treatedZone id='%1$d' idMassage='%2$d' codPosition='%3$s' codDivision='%4$s' codZone='%5$s' startDateTime='%6$s' endDateTime='%7$s' duration='%8$d'>";
    public static final String XML_CLOSE = "</treatedZone>";
    public static final String XML_COMMENTS = "<comments>%1$s</comments>";

    public static final String CODPOSITION_UNDEFINED = "undefined";
    public static final String CODPOSITION_FACEDOWN = "lyingFaceDown";
    public static final String CODPOSITION_FACEUP = "lyingFaceUp";
    public static final String CODPOSITION_LEFTSIDE = "lyingLeftSide";
    public static final String CODPOSITION_RIGHTSIDE = "lyingRightSide";
    public static final String CODPOSITION_SITTINGBACK = "sittingBack";
    public static final String CODPOSITION_SITTINGFRONT = "sittingFront";

    public static final String CODZONE_MASSAGE = "massage";
    public static final String CODZONE_PAUSE = "pause";
    public static final String CODZONE_FULLBODY = "fullBody";
    public static final String CODZONE_TRANSITIONS = "transitions";
    public static final String CODZONE_UNSELECTED = "unselected";
    public static final String CODZONE_HEAD = "head";
    public static final String CODZONE_HEADNECK = "headNeck";
    public static final String CODZONE_ARMLEFT = "armLeft";
    public static final String CODZONE_ARMRIGHT = "armRight";
    public static final String CODZONE_LEGLEFT = "legLeft";
    public static final String CODZONE_LEGRIGHT = "legRight";
    public static final String CODZONE_BACK = "back";
    public static final String CODZONE_TORSO = "torso";
    public static final String CODZONE_LEFTFLANK = "leftFlank";
    public static final String CODZONE_RIGHTFLANK = "rightFlank";
    public static final String CODZONE_LEFTSIDE = "leftSide";
    public static final String CODZONE_RIGHTSIDE = "rightSide";
    public static final String CODZONE_UPPERBODY = "upperBody";
    public static final String CODZONE_LOWERBODY = "lowerBody";
    public static final String CODZONE_NECKSHOULDERS = "neckShoulders";
    public static final String CODZONE_RIGHTPECTORALGIRDLE = "rightPectoralGirdle";
    public static final String CODZONE_LEFTPECTORALGIRDLE = "leftPectoralGirdle";

    public static final String CODZONE_RIGHTBICEPS = "rightBiceps";
    public static final String CODZONE_RIGHTFOREARM = "rightForearm";
    public static final String CODZONE_RIGHTHAND = "rightHand";
    public static final String CODZONE_THORACIC = "thoracic";
    public static final String CODZONE_THORAX = "thorax";
    public static final String CODZONE_LUMBAR = "lumbar";
    public static final String CODZONE_ABDOMEN = "abdomen";
    public static final String CODZONE_BUTTOCKS = "buttocks";
    public static final String CODZONE_PELVIS = "pelvis";
    public static final String CODZONE_RIGHTHIP = "rightHip";
    public static final String CODZONE_LEFTHIP = "leftHip";

    public static final String CODZONE_THIGH = "thigh";
    public static final String CODZONE_RIGHTTHIGH = "rightThigh";
    public static final String CODZONE_LEFTTHIGH = "leftThigh";
    public static final String CODZONE_THIGHKNEE = "thighKnee";
    public static final String CODZONE_CALF = "calf";
    public static final String CODZONE_SHIN = "shin";
    public static final String CODZONE_RIGHTSHIN = "rightShin";
    public static final String CODZONE_LEFTSHIN = "leftShin";
    public static final String CODZONE_FEET = "feet";
    public static final String CODZONE_RIGHTFOOT = "rightFoot";
    public static final String CODZONE_LEFTFOOT = "leftFoot";
    public static final String CODZONE_LEFTBICEPS = "leftBiceps";
    public static final String CODZONE_LEFTFOREARM = "leftForearm";
    public static final String CODZONE_LEFTHAND = "leftHand";


    /**
     * TreatedZoneRecord constructor
     *
     * @param id Identifier
     * @param idMassage Parent massage
     * @param codPosition Position of patient
     * @param codDivision How mPosition is divided in zones
     * @param codZone Massaged zone
     * @param startDateTime Start time of manipulation
     * @param endDateTime End time of manipulation
     * @param duration Total duration in seconds
     * @param comments Added by user
     */
    public TreatedZoneRecord( long id, long idMassage, String codPosition,
                              String codDivision, String codZone, Date startDateTime,
                              Date endDateTime, long duration, String comments ) {

        this.mId = id;
        this.mIdMassage = idMassage;
        this.mCodPosition = codPosition;
        this.mCodDivision = codDivision;
        this.mCodZone = codZone;
        this.mStartDateTime = startDateTime;
        this.mEndDateTime = endDateTime;
        this.mDuration = duration;
        this.mComments = comments;

    }


    public TreatedZoneRecord( long idMassage ) {
        this.mIdMassage = idMassage;
    }


    public TreatedZoneRecord() {
    }


    /**
     * Reads a treatedZoneRecord from a cursor
     *
     * @param cursor To build the TreatedZone
     */
    public TreatedZoneRecord( Cursor cursor ) {

        this.mId = cursor.getLong(cursor.getColumnIndex(
                MassageAssistantDBHelper.TREATEDZONES_COLUMN_ID));
        this.mIdMassage = cursor.getLong(cursor.getColumnIndex(
                MassageAssistantDBHelper.TREATEDZONES_COLUMN_IDMASSAGE));
        this.mCodPosition = cursor.getString(cursor.getColumnIndex(
                MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODPOSITION));
        this.mCodDivision = cursor.getString(cursor.getColumnIndex(
                MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODDIVISION));
        this.mCodZone = cursor.getString(cursor.getColumnIndex(
                MassageAssistantDBHelper.TREATEDZONES_COLUMN_CODZONE));

        String s_startDateTime =
                cursor.getString(cursor.getColumnIndex(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_STARTDATETIME));
        this.mStartDateTime = MassageAssistantDBHelper.getDateTimeFromSQLLite(
                s_startDateTime);

        String s_endDateTime =
                cursor.getString(cursor.getColumnIndex(
                        MassageAssistantDBHelper.TREATEDZONES_COLUMN_ENDDATETIME));
        this.mEndDateTime = MassageAssistantDBHelper.getDateTimeFromSQLLite(
                s_endDateTime);

        this.mDuration = cursor.getLong(cursor.getColumnIndex(
                MassageAssistantDBHelper.TREATEDZONES_COLUMN_DURATION));

        this.mComments = cursor.getString(cursor.getColumnIndex(
                MassageAssistantDBHelper.TREATEDZONES_COLUMN_COMMENTS));
    }

    /* Attributes get/set */
    public long getId() { return mId; }
    public void setId( int id ) { this.mId = id; }

    public long getIdMassage() { return mIdMassage; }
    public void setIdMassage( int idMassage ) { setIdMassage( (long) idMassage );}
    public void setIdMassage( long idMassage ) { this.mIdMassage = idMassage; }

    public String getCodPosition() { return mCodPosition; }
    public void setCodPosition( String codPosition ) { this.mCodPosition = codPosition; }

    public String getCodDivision() { return mCodDivision; }
    public void setCodDivision( String codDivision ) { this.mCodDivision = codDivision; }

    public String getCodZone() { return mCodZone; }
    public void setCodZone( String codZone ) { this.mCodZone = codZone; }

    public Date getStartDateTime() { return mStartDateTime; }
    public void setStartDateTime( Date startDateTime ) { this.mStartDateTime = startDateTime; }
    public void setStartDateTime( String startDateTime ) {
        this.mStartDateTime =
                DateTimeUtils.FormattedStringToDateTime( startDateTime,
                        DateTimeUtils.SQLLiteFormat());

    }

    public Date getEndDateTime() { return mEndDateTime; }
    public void setEndDateTime( Date endDateTime ) { this.mEndDateTime = endDateTime; }
    public void setEndDateTime( String endDateTime ) {
        this.mEndDateTime =
                DateTimeUtils.FormattedStringToDateTime( endDateTime,
                        DateTimeUtils.SQLLiteFormat()); }

    public long getDuration() { return mDuration; }
    public long getDurationInMilliseconds() { return mDuration * 1000; }
    public void setDuration(long duration) { this.mDuration = duration; }

    public String getComments() { return mComments; }
    public void setComments( String comments ) { this.mComments = comments; }


    /**
     * Shows Treated zone info and mPosition
     * @param context - To get the strings
     * @return - mPosition, zone and duration
     */
    public String fullSummary( Context context ){
        return Position.codPositionToString(context, mCodPosition) + ". " +
                ZonesUtils.codZoneToString(context, mCodZone) + ": " +
                DateTimeUtils.durationToString(getDurationInMilliseconds()) +
                System.getProperty( "line.separator" );

//        mCodDivision + ". " +

    }


    /**
     * Shows Treated zone info for email
     * @param context - To get the strings
     * @return - zone and duration
     *
     * Called from massageRecord.summary
     */
    public String summary( Context context ){
        return ZonesUtils.codZoneToString(context, mCodZone) + ": " +
                DateTimeUtils.durationToString(getDurationInMilliseconds()) +
                System.getProperty( "line.separator" );

        //mCodDivision + ": " +

    }


    public boolean isValid(){
        return (mIdMassage > 0) && (!mCodPosition.equals(""))
                && (!mCodDivision.equals("")) && (!mCodZone.equals(""))
                && (mStartDateTime != null) && (mEndDateTime != null)
                && (mDuration > 0L);
        }
    }