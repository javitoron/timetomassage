package com.fingerhits.massageassistant.storedData;

/**
 * Created by javi on 13/03/2016.
 */
public class TreatedZoneRecordInvalidException extends Exception {
    private TreatedZoneRecord mTzr;
    public TreatedZoneRecordInvalidException(TreatedZoneRecord tzr){
        mTzr = tzr;

    }

    public String toString(){
        String causes = "";

        if( mTzr.getIdMassage() <= 0 ){
            causes += "Has no idMassage. ";
        }

        if( mTzr.getCodPosition().equals("") ){
            causes += "Has no codPosition. ";
        }

        if( mTzr.getCodDivision().equals("") ){
            causes += "Has no codDivision. ";
        }

        if( mTzr.getCodZone().equals("") ){
            causes += "Has no codZone. ";
        }

        if( mTzr.getStartDateTime() == null ){
            causes += "Has no StartDateTime. ";
        }

        if( mTzr.getEndDateTime() == null ){
            causes += "Has no EndDateTime. ";
        }

        if( mTzr.getDuration() <= 0L ){
            causes += "Has no Duration. ";
        }

        return "Invalid Treated Zone: " + causes;
    }
}
