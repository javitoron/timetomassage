package com.fingerhits.massageassistant.storedData;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import com.fingerhits.massageassistant.utils.DateTimeUtils;
import com.fingerhits.massageassistant.utils.MiscUtils;

import java.util.Date;

/**
 * Database
 *
 * @author javi on 01/03/2015.
 */
public class MassageAssistantDBHelper {
    private static final int DATABASE_VERSION = 10;
    public static final String DATABASE_NAME = "massageassistant.db";
    private static final String MASSAGES_TABLE_NAME = "massages";

    public static final String MASSAGES_COLUMN_ID = "_id";
    public static final String MASSAGES_COLUMN_PATIENT = "patient";
    public static final String MASSAGES_COLUMN_IDMASSEUR = "idMasseur";
    public static final String MASSAGES_COLUMN_STARTDATETIME = "startDateTime";
    public static final String MASSAGES_COLUMN_ENDDATETIME = "endDateTime";
    public static final String MASSAGES_COLUMN_DURATION = "duration";
    public static final String MASSAGES_COLUMN_MODE = "mode";
    public static final String MASSAGES_COLUMN_TYPE = "type";
    public static final String MASSAGES_COLUMN_OBJECTIVE = "objective";
    public static final String MASSAGES_COLUMN_USEDMATERIALS = "usedMaterials";
    public static final String MASSAGES_COLUMN_COMMENTS = "comments";

    private static final String TREATEDZONES_TABLE_NAME = "treatedZones";

    public static final String TREATEDZONES_COLUMN_ID = "_id";
    public static final String TREATEDZONES_COLUMN_IDMASSAGE = "idMassage";
    public static final String TREATEDZONES_COLUMN_CODPOSITION = "codPosition";
    public static final String TREATEDZONES_COLUMN_CODDIVISION = "codDivision";
    public static final String TREATEDZONES_COLUMN_CODZONE = "codZone";
    public static final String TREATEDZONES_COLUMN_STARTDATETIME = "startDateTime";
    public static final String TREATEDZONES_COLUMN_ENDDATETIME = "endDateTime";
    public static final String TREATEDZONES_COLUMN_DURATION = "duration";
    public static final String TREATEDZONES_COLUMN_COMMENTS = "comments";

    public static final String SORT_LAST_TO_FIRST = " DESC";
    public static final String SORT_FIRST_TO_LAST = " ASC";


    private static final String MASSAGE_STATS_TABLE_NAME = "massagePendingStats";

    private final SQLiteDatabase mDatabase;

    private final int mCurrentUser;

    /**
     * MassageAssistantDBHelper constructor
     *
     * @param context Needed to pass it to the Open Helper
     */
    public MassageAssistantDBHelper(Context context) {
        MassageAssistantOpenHelper openHelper =
                new MassageAssistantOpenHelper(context);
        mCurrentUser = MiscUtils.currentLocalUser();
        mDatabase = openHelper.getWritableDatabase();
    }

    public SQLiteDatabase getDatabase(){
        return mDatabase;
    }

    public void close() { mDatabase.close(); }

    /**
     * Date to String in SQLLite format
     *
     * @param dateToConvert`Date to store in SQLLite
     * @return a string ready to save in SQLLite
     */
    public static String convertDateTimeForSQLLite( Date dateToConvert ) {
        return DateTimeUtils.convertDateTimeToFormattedString(dateToConvert,
                DateTimeUtils.SQLLiteFormat());
    }

    /**
     * String in SQLLite format to Date
     *
     * @param dateToConvert A string retrieved from SQLLite
     * @return a Date converted from string
     */
    public static Date getDateTimeFromSQLLite( String dateToConvert ) {
        return DateTimeUtils.FormattedStringToDateTime(dateToConvert,
                DateTimeUtils.SQLLiteFormat());
    }


    /**********************************
     * MASSAGES FUNCTIONS
     *********************************/

    /**
     * Number of massages made by the current user
     *
     * @return Massages count
     */
    public int numberOfMassages(){
        Cursor massages = getAllMassageRecords(SORT_FIRST_TO_LAST);
        return massages.getCount();
    }

    /**
     * Reads massages table
     *
     * @return Cursor with all the massages
     */
    public Cursor getAllMassageRecords( String sorting ) {

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(MASSAGES_TABLE_NAME);
        return qb.query(mDatabase,
                new String[]{"*"},
                MASSAGES_COLUMN_IDMASSEUR + " = ?",
                new String[]{String.valueOf(mCurrentUser)},
                null,
                null,
                MASSAGES_COLUMN_STARTDATETIME + sorting,
                null);

    }


    /**
     * Reads massage id
     * @param id Massage id
     * @return Cursor with all the massages
     */
    public Cursor getMassageCursor(long id) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        qb.setTables(MASSAGES_TABLE_NAME);

        return qb.query(mDatabase,
                new String[]{"*"},
                MASSAGES_COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null,
                null);
    }


    /**
     * Reads massage id and returns a massageRecord
     * @param id Massage id
     * @return massageRecord
     */
    public MassageRecord getMassageRecord(long id) {
        Cursor massageCursor = getMassageCursor(id);

        if( massageCursor.moveToFirst()){
            return new MassageRecord(massageCursor);
        }
        else{
            return null;
        }
    }


    /**
     * Used after insert for use in treatedZones subsequent inserts
     *
     * @return last id
     */
    public long getLastInsertedMassageId() {
        long id;
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(MASSAGES_TABLE_NAME);

        Cursor c = qb.query(mDatabase,
                new String[]{MASSAGES_COLUMN_ID},
                MASSAGES_COLUMN_IDMASSEUR + " = ?",
                new String[]{String.valueOf(mCurrentUser)},
                null,
                null,
                MASSAGES_COLUMN_STARTDATETIME + " DESC",
                "1");

        if( c.moveToFirst() ) {
            id = c.getLong(c.getColumnIndex(MASSAGES_COLUMN_ID));
        }
        else{
            id = -1;
        }

        c.close();

        return id;
    }


    /**
     * Insert a massageRecord into massages table
     * @param massageRecord To store in massages
     */
    public long insertMassageRecord(MassageRecord massageRecord) {
        ContentValues contentValues = new ContentValues();

        String s_startDateTime =
                convertDateTimeForSQLLite(massageRecord.getStartDateTime());

        contentValues.put(MASSAGES_COLUMN_PATIENT,
                massageRecord.getPatient());
        contentValues.put(MASSAGES_COLUMN_IDMASSEUR,
                massageRecord.getIdMasseur());
        contentValues.put(MASSAGES_COLUMN_STARTDATETIME, s_startDateTime);
        contentValues.put(MASSAGES_COLUMN_ENDDATETIME,
                convertDateTimeForSQLLite(massageRecord.getEndDateTime()));
        contentValues.put(MASSAGES_COLUMN_DURATION, massageRecord.getDuration());
        contentValues.put(MASSAGES_COLUMN_MODE, massageRecord.getMode());
        contentValues.put(MASSAGES_COLUMN_TYPE, massageRecord.getType());
        contentValues.put(MASSAGES_COLUMN_OBJECTIVE,
                massageRecord.getObjective());
        contentValues.put(MASSAGES_COLUMN_USEDMATERIALS,
                massageRecord.getUsedMaterials());
        contentValues.put(MASSAGES_COLUMN_COMMENTS, massageRecord.getComments());

        return mDatabase.insert(MASSAGES_TABLE_NAME, null, contentValues);
    }


    /**
     * Updates massageRecord id with endTime and duration final values
     * @param id Massage id
     */
    public long finishMassageRecord(long id ) {
        ContentValues contentValues = new ContentValues();

        Cursor lastTreatedZone = getLastTreatedZoneInMassage(id);

        if( lastTreatedZone.moveToFirst() ){
            TreatedZoneRecord tz =
                    new TreatedZoneRecord( lastTreatedZone );

            String s_endDateTime =
                    convertDateTimeForSQLLite( tz.getEndDateTime() );

            contentValues.put(MASSAGES_COLUMN_ENDDATETIME, s_endDateTime );
        }

        long l_duration = getTreatedZonesDurationInMassage(id);

        contentValues.put(MASSAGES_COLUMN_DURATION, l_duration);

        mDatabase.update(MASSAGES_TABLE_NAME, contentValues,
                MASSAGES_COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)});

        return l_duration;

    }

    /**
     * Updates massageRecord id with patient
     * @param id Massage id
     * @param patient To store in the massage
     */
    public void savePatientToMassageRecord(long id, String patient ) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(MASSAGES_COLUMN_PATIENT, patient);

        mDatabase.update(MASSAGES_TABLE_NAME, contentValues,
                MASSAGES_COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)});
    }


    /**
     * Updates massageRecord id with comments
     * @param id Massage id
     * @param comments To store in the massage
     */
    public void saveCommentsToMassageRecord(long id, String comments ) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(MASSAGES_COLUMN_COMMENTS, comments);

        mDatabase.update(MASSAGES_TABLE_NAME, contentValues,
                MASSAGES_COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)});
    }


    /**
     * Deletes the massage and its treatedZones children
     * @param id Massage id
     */
    public void deleteMassageRecord(long id) {
        mDatabase.delete(MASSAGES_TABLE_NAME, MASSAGES_COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)});

        mDatabase.delete(TREATEDZONES_TABLE_NAME,
                TREATEDZONES_COLUMN_IDMASSAGE + " = ?",
                new String[]{String.valueOf(id)});
    }


    /**
     * Delete all massages
     */
    public void deleteAllMassages() {
        String SQL = "DELETE FROM " + TREATEDZONES_TABLE_NAME;

        mDatabase.execSQL(SQL);

        SQL = "DELETE FROM " + MASSAGES_TABLE_NAME;

        mDatabase.execSQL(SQL);
    }


    /**
     * Cleanup massages list
     * Called from mainActivity
     */
    public void cleanUpMassages() {
        String SQL = "DELETE FROM " + TREATEDZONES_TABLE_NAME +
                " WHERE " + TREATEDZONES_COLUMN_DURATION + " = 0 ";

        mDatabase.execSQL(SQL);

        SQL = "DELETE FROM " + MASSAGES_TABLE_NAME +
                " WHERE NOT EXISTS ( " +
                "    SELECT 1 FROM " + TREATEDZONES_TABLE_NAME +
                "    WHERE " + TREATEDZONES_TABLE_NAME + "." +
                TREATEDZONES_COLUMN_IDMASSAGE + " = " +
                MASSAGES_TABLE_NAME + "." + MASSAGES_COLUMN_ID +
                ")";

        mDatabase.execSQL(SQL);

      /*  SQL = "UPDATE " + TREATEDZONES_TABLE_NAME +
                " SET codPosition = 'sittingBack' " +
                " where codPosition = 'sitting'";

        mDatabase.execSQL(SQL);*/
    }





    /**********************************
     * TREATEDZONES FUNCTIONS
     *********************************/

    /**
     * Reads all treatedZones
     * @return Cursor with all the treatedZones
     */
    public Cursor getAllTreatedZones() {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        qb.setTables(TREATEDZONES_TABLE_NAME);

        return qb.query(mDatabase,
                new String[]{"*"},
                "",
                null,
                null,
                null,
                TREATEDZONES_COLUMN_IDMASSAGE + " ASC",
                null);
    }



    /**
     * Reads treated zone id
     * @param id Treated zone id
     * @return Cursor with the treated zone
     */
    public Cursor getTreatedZoneCursor(long id) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        qb.setTables(TREATEDZONES_TABLE_NAME);

        return qb.query(mDatabase,
                new String[]{"*"},
                TREATEDZONES_COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null,
                null);
    }


    /**
     * Reads treated zone id and returns a treatedZoneRecord
     * @param id Treated zone id
     * @return treatedZoneRecord
     */
    public TreatedZoneRecord getTreatedZoneRecord(long id) {
        Cursor treatedZoneCursor = getTreatedZoneCursor(id);

        if( treatedZoneCursor.moveToFirst()){
            return new TreatedZoneRecord(treatedZoneCursor);
        }
        else{
            return null;
        }
    }




    /**
     * Reads treatedZones with idMassage
     * @param idMassage Parent Massage id
     * @return Cursor with all the treatedZones of the massage
     */
    public Cursor getTreatedZonesInMassage( long idMassage ) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        qb.setTables(TREATEDZONES_TABLE_NAME);

        return qb.query(mDatabase,
                new String[]{"*"},
                TREATEDZONES_COLUMN_IDMASSAGE + " = ?",
                new String[]{String.valueOf(idMassage)},
                null,
                null,
                TREATEDZONES_COLUMN_STARTDATETIME + " ASC",
                null);
    }


    public Cursor getLastTreatedZoneInMassage(long idMassage) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TREATEDZONES_TABLE_NAME);

        return qb.query(mDatabase,
                new String[]{"*"},
                TREATEDZONES_COLUMN_IDMASSAGE + " = ?",
                new String[]{String.valueOf(idMassage)},
                null,
                null,
                TREATEDZONES_COLUMN_STARTDATETIME + " DESC",
                "1");
    }


    /**
     * Get added up duration for idMassage since the last change of mPosition
     * @param idMassage Parent Massage id
     * @return sum( duration )
     */
    public long getDurationInLastPosition( long idMassage) {

        Cursor c;
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        qb.setTables(TREATEDZONES_TABLE_NAME);

        c = qb.query(mDatabase,
                new String[]{TREATEDZONES_COLUMN_DURATION,
                        TREATEDZONES_COLUMN_CODPOSITION},
                TREATEDZONES_COLUMN_IDMASSAGE + " = ?",
                new String[]{String.valueOf(idMassage)},
                null,
                null,
                TREATEDZONES_COLUMN_STARTDATETIME + " DESC",
                null);
        long totalDuration = 0;


        if (c.moveToFirst()) {
            boolean changedPosition = false;
            String  codLastPosition = c.getString(1);
            do {
                if(codLastPosition.equals( c.getString(1))) {
                    totalDuration += c.getLong(0);
                }
                else{
                    changedPosition = true;
                }
            } while (c.moveToNext() && !changedPosition );
        }


        c.close();

        return totalDuration;
    }


    /**
     * Get zones and added up durations for idMassage, codPosition and
     *      codDivision
     * @param idMassage Parent Massage id
     * @param codPosition Position required
     * @param codDivision Division required
     * @return Cursor with codZone and sum( duration ) for idMassage, codPosition
     *      and codDivision
     */
    public Cursor getTreatedZonesDurationsInMassagePositionAndDivision(
            long idMassage, String codPosition, String codDivision) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        qb.setTables(TREATEDZONES_TABLE_NAME);

        return qb.query(mDatabase,
                new String[] {TREATEDZONES_COLUMN_CODZONE,
                        "SUM(" + TREATEDZONES_COLUMN_DURATION + ") AS totalDuration"},
                TREATEDZONES_COLUMN_IDMASSAGE + " = ? AND " +
                        TREATEDZONES_COLUMN_CODPOSITION + " = ? AND " +
                        TREATEDZONES_COLUMN_CODDIVISION + " = ?",
                new String[] { String.valueOf(idMassage), codPosition, codDivision },
                TREATEDZONES_COLUMN_CODZONE,
                null,
                null,
                null);
    }


    /**
     * Get total of durations for idMassage without pauses
     * @param idMassage Parent Massage id
     * @return long with sum( duration ) for idMassage
     */
    public long getTreatedZonesDurationInMassage( long idMassage ) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        qb.setTables(TREATEDZONES_TABLE_NAME);

        Cursor c = qb.query(mDatabase,
                new String[] {
                    "SUM(" + TREATEDZONES_COLUMN_DURATION + ") AS totalDuration"},
                TREATEDZONES_COLUMN_IDMASSAGE + " = ? AND " +
                        TREATEDZONES_COLUMN_CODZONE + " <> '" +
                        TreatedZoneRecord.CODZONE_PAUSE + "'",
                new String[] { String.valueOf(idMassage) },
                null, null, null, null);

        c.moveToFirst();

        long totalDuration = c.getLong(0);

        c.close();

        return totalDuration;

    }


    /**
     * Inserts a treatedZoneRecord in treatedZones table
     * @param treatedZoneRecord Treated zone to store in TreatedZones
     */
    public long insertTreatedZoneRecord(TreatedZoneRecord treatedZoneRecord)
        throws TreatedZoneRecordInvalidException {

        if(treatedZoneRecord.isValid()) {
            ContentValues contentValues = new ContentValues();

            contentValues.put(TREATEDZONES_COLUMN_IDMASSAGE,
                    treatedZoneRecord.getIdMassage());
            contentValues.put(TREATEDZONES_COLUMN_CODPOSITION,
                    treatedZoneRecord.getCodPosition());
            contentValues.put(TREATEDZONES_COLUMN_CODDIVISION,
                    treatedZoneRecord.getCodDivision());
            contentValues.put(TREATEDZONES_COLUMN_CODZONE,
                    treatedZoneRecord.getCodZone());
            contentValues.put(TREATEDZONES_COLUMN_STARTDATETIME,
                    convertDateTimeForSQLLite(treatedZoneRecord.getStartDateTime()));
            contentValues.put(TREATEDZONES_COLUMN_ENDDATETIME,
                    convertDateTimeForSQLLite(treatedZoneRecord.getEndDateTime()));
            contentValues.put(TREATEDZONES_COLUMN_DURATION,
                    treatedZoneRecord.getDuration());
            contentValues.put(TREATEDZONES_COLUMN_COMMENTS,
                    treatedZoneRecord.getComments());

            return mDatabase.insert(TREATEDZONES_TABLE_NAME, null, contentValues);
        }
        else{
            throw new TreatedZoneRecordInvalidException(treatedZoneRecord);
        }
    }


    /**
     * Updates TreatedZoneRecord id with codZone
     * @param id TreatedZone id
     * @param codZone To store in the massage
     */
    public void saveCodZoneInTreatedZoneRecord(long id, String codZone ) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(TREATEDZONES_COLUMN_CODZONE, codZone);

        mDatabase.update(TREATEDZONES_TABLE_NAME, contentValues,
                TREATEDZONES_COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)});
    }



    /**
     * Delete the treatedZone and recalculate total duration of massage
     * @param idTreatedZone TreatedZone id
     * @param idTreatedZone TreatedZone id
     */
    public void deleteTreatedZoneRecord(long idMassage, long idTreatedZone) {
        mDatabase.delete(TREATEDZONES_TABLE_NAME,
                TREATEDZONES_COLUMN_ID + " = ?",
                new String[]{String.valueOf(idTreatedZone)});

        finishMassageRecord(idMassage);
    }



    /**
     * DATABASE CREATION AND DEFINITION
     */
    public class MassageAssistantOpenHelper extends SQLiteOpenHelper {

        /**
         * MassageAssistantOpenHelper constructor
         * @param context To pass it to superclass
         */
        MassageAssistantOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        /**
         * Creates the tables
         * @param database Pointer to actual database
         */
        public void onCreate(SQLiteDatabase database) {
            createMassagesTable(database);
            createTreatedZonesTable(database);
        }


        public void createMassagesTable(SQLiteDatabase database) {
            String massages_table_create = "CREATE TABLE " +
                    MASSAGES_TABLE_NAME +
                    " (" + MASSAGES_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MASSAGES_COLUMN_PATIENT + " TEXT, " +
                    MASSAGES_COLUMN_IDMASSEUR + " INTEGER, " +
                    MASSAGES_COLUMN_STARTDATETIME + " TEXT, " +
                    MASSAGES_COLUMN_ENDDATETIME + " TEXT, " +
                    MASSAGES_COLUMN_DURATION + " INTEGER, " +
                    MASSAGES_COLUMN_MODE + " INTEGER, " +
                    MASSAGES_COLUMN_TYPE + " TEXT, " +
                    MASSAGES_COLUMN_OBJECTIVE + " TEXT, " +
                    MASSAGES_COLUMN_USEDMATERIALS + " TEXT, " +
                    MASSAGES_COLUMN_COMMENTS + " TEXT)";
            database.execSQL(massages_table_create);
        }


        public void createTreatedZonesTable(SQLiteDatabase database) {
            String treatedZones_table_create = "CREATE TABLE " +
                    TREATEDZONES_TABLE_NAME +
                    " (" + TREATEDZONES_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TREATEDZONES_COLUMN_IDMASSAGE + " INTEGER, " +
                    TREATEDZONES_COLUMN_CODPOSITION + " TEXT, " +
                    TREATEDZONES_COLUMN_CODDIVISION + " TEXT, " +
                    TREATEDZONES_COLUMN_CODZONE + " TEXT, " +
                    TREATEDZONES_COLUMN_STARTDATETIME + " TEXT, " +
                    TREATEDZONES_COLUMN_ENDDATETIME + " TEXT, " +
                    TREATEDZONES_COLUMN_DURATION + " INTEGER, " +
                    TREATEDZONES_COLUMN_COMMENTS + " TEXT, " +
                    "FOREIGN KEY(" + TREATEDZONES_COLUMN_IDMASSAGE + ") " +
                    "REFERENCES " + MASSAGES_TABLE_NAME + "(" +
                        MASSAGES_COLUMN_ID + ") ) ";
            database.execSQL(treatedZones_table_create);
        }


        /**
         * What to do when upgrading the database definition
         * @param database Pointer to actual database
         * @param oldVersion Version to delete
         * @param newVersion Version to upgrade
         */
        public void onUpgrade(SQLiteDatabase database,
                              int oldVersion, int newVersion) {
            upgradeMassagesTable(database, oldVersion, newVersion);
            upgradeTreatedZonesTable(database, oldVersion, newVersion);
            upgradeMassageStatsTable(database, oldVersion, newVersion);
        }


        @SuppressWarnings("UnusedParameters")
        public void upgradeMassagesTable(SQLiteDatabase database,
                              int oldVersion, int newVersion) {

            String alter_query1 =
                    "alter table " + MASSAGES_TABLE_NAME + " RENAME TO temp;";
            database.execSQL(alter_query1);

            createMassagesTable(database);

            String alter_query3;
            if (oldVersion < 10) {
                alter_query3 = "insert into " + MASSAGES_TABLE_NAME +
                        " ( " + MASSAGES_COLUMN_ID + ", " +
                        MASSAGES_COLUMN_PATIENT + " , " +
                        MASSAGES_COLUMN_IDMASSEUR + ", " +
                        MASSAGES_COLUMN_STARTDATETIME + ", " +
                        MASSAGES_COLUMN_ENDDATETIME + ", " +
                        MASSAGES_COLUMN_DURATION + ", " +
                        MASSAGES_COLUMN_MODE + ", " +
                        MASSAGES_COLUMN_TYPE + ", " +
                        MASSAGES_COLUMN_OBJECTIVE + ", " +
                        MASSAGES_COLUMN_USEDMATERIALS + ", " +
                        MASSAGES_COLUMN_COMMENTS + ")" +
                        " select " + MASSAGES_COLUMN_ID + ", " +
                        "'', " +
                        MASSAGES_COLUMN_IDMASSEUR + ", " +
                        MASSAGES_COLUMN_STARTDATETIME + ", " +
                        MASSAGES_COLUMN_ENDDATETIME + ", " +
                        MASSAGES_COLUMN_DURATION + ", " +
                        "3, " +
                        MASSAGES_COLUMN_TYPE + ", " +
                        MASSAGES_COLUMN_OBJECTIVE + ", " +
                        MASSAGES_COLUMN_USEDMATERIALS + ", " +
                        MASSAGES_COLUMN_COMMENTS +
                        " from temp;";
            } else {
                alter_query3 = "insert into " + MASSAGES_TABLE_NAME +
                        " select * from temp;";
            }
            database.execSQL(alter_query3);

            String alter_query4 = "DROP TABLE temp;";
            database.execSQL(alter_query4);

        }


        @SuppressWarnings("UnusedParameters")
        public void upgradeTreatedZonesTable(SQLiteDatabase database,
                                             int oldVersion, int newVersion) {
            String alter_query1 =
                    "alter table " + TREATEDZONES_TABLE_NAME + " RENAME TO temp;";
            database.execSQL(alter_query1);

            createTreatedZonesTable(database);

            String alter_query3;
            if (oldVersion == 3) {
                alter_query3 = "insert into " + TREATEDZONES_TABLE_NAME +
                        " select " + TREATEDZONES_COLUMN_ID + ", " +
                        TREATEDZONES_COLUMN_IDMASSAGE + ", " +
                        TREATEDZONES_COLUMN_CODPOSITION + ", " +
                        "'Default', " +
                        TREATEDZONES_COLUMN_CODZONE + ", " +
                        TREATEDZONES_COLUMN_STARTDATETIME + ", " +
                        TREATEDZONES_COLUMN_ENDDATETIME + ", " +
                        TREATEDZONES_COLUMN_DURATION + ", " +
                        TREATEDZONES_COLUMN_COMMENTS +
                        " from temp;";
            } else {
                alter_query3 = "insert into " + TREATEDZONES_TABLE_NAME +
                        " select * from temp;";
            }
            database.execSQL(alter_query3);

            String alter_query4 = "DROP TABLE temp;";
            database.execSQL(alter_query4);
        }


        @SuppressWarnings("UnusedParameters")
        public void upgradeMassageStatsTable(SQLiteDatabase database,
                                             int oldVersion, int newVersion) {
            database.execSQL("DROP TABLE IF EXISTS " + MASSAGE_STATS_TABLE_NAME);
        }
    }

}