package com.fingerhits.massageassistant.storedData;

import android.content.res.Resources;
import android.database.Cursor;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.utils.DateTimeUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents a massage session. Fields: id, idPatient, idMasseur,
 *      startDateTime, endDateTime, duration, type, objective, usedMaterials,
 *      comments
 *
 * @author javi on 01/03/2015.
 *
 */
public class MassageRecord {
    public static final int MODE_SIMPLE = 1;
    public static final int MODE_INTERMEDIATE = 2;
    public static final int MODE_ADVANCED = 3;

    public static final String XML_OPEN = "<massage id='%1$d' patient='%2$s' idMasseur='%3$d' startDateTime='%4$s' endDateTime='%5$s' duration='%6$d' mode='%7$d'>";
    public static final String XML_CLOSE = "</massage>";
    public static final String XML_TYPE = "<type>%1$s</type>";
    public static final String XML_OBJECTIVE = "<objective>%1$s</objective>";
    public static final String XML_USEDMATERIALS = "<usedMaterials>%1$s</usedMaterials>";
    public static final String XML_COMMENTS = "<comments>%1$s</comments>";

    private long mId, mIdMasseur;
    private Date mStartDateTime, mEndDateTime;
    private long mDuration;
    private int mMode;
    private String mPatient, mType, mObjective, mUsedMaterials, mComments;

    /**
     * MassageRecord Constructor
     *
     * @param id Identifier
     * @param patient Patient receiving the massage (Contact:URI or free text for name)
     * @param idMasseur Masseur giving the massage
     * @param startDateTime Time of massage creation
     * @param endDateTime when finished the last manipulation
     * @param duration Total time of treatedZones durations in seconds
     * @param mode Massage mode (1 - Simple, 2 - Intermediate, 3 - Advanced)
     * @param type Text describing type of massage
     * @param objective Text describing objectives of massage
     * @param usedMaterials Text describing oils, tools or whatever used for massage
     * @param comments Regarding the massage
     */
    public MassageRecord( long id, String patient, long idMasseur,
                          Date startDateTime, Date endDateTime, long duration,
                          int mode, String type, String objective,
                          String usedMaterials, String comments ) {

        this.mId = id;
        this.mPatient = patient;
        this.mIdMasseur = idMasseur;
        this.mStartDateTime = startDateTime;
        this.mEndDateTime = endDateTime;
        this.mDuration = duration;
        this.mMode = mode;
        this.mType = type;
        this.mObjective = objective;
        this.mUsedMaterials = usedMaterials;
        this.mComments = comments;

    }

    /**
     * Used by massageSessionActivity onCreate
     *
     * @param startDateTime Date and time of creation
     */
    public MassageRecord( int idMasseur, String patient, Date startDateTime,
                          int mode ) {

        this( 0, patient, idMasseur, startDateTime, startDateTime, 0, mode, "",
                "", "", "" );

    }

    /**
     * Reads a massageRecord from a cursor
     *
     * @param cursor To be read to build the object
     */
    public MassageRecord( Cursor cursor ) {
        this.mId = cursor.getLong(cursor.getColumnIndex(
                MassageAssistantDBHelper.MASSAGES_COLUMN_ID));
        this.mPatient = cursor.getString(cursor.getColumnIndex(
                MassageAssistantDBHelper.MASSAGES_COLUMN_PATIENT));
        this.mIdMasseur = cursor.getLong(cursor.getColumnIndex(
                MassageAssistantDBHelper.MASSAGES_COLUMN_IDMASSEUR));

        String s_startDateTime =
                cursor.getString(cursor.getColumnIndex(
                    MassageAssistantDBHelper.MASSAGES_COLUMN_STARTDATETIME));
        this.mStartDateTime =
                MassageAssistantDBHelper.getDateTimeFromSQLLite(
                    s_startDateTime);

        String s_endDateTime =
                cursor.getString(cursor.getColumnIndex(
                    MassageAssistantDBHelper.MASSAGES_COLUMN_ENDDATETIME));
        this.mEndDateTime =
                MassageAssistantDBHelper.getDateTimeFromSQLLite(
                    s_endDateTime);

        this.mDuration = cursor.getLong(cursor.getColumnIndex(
                MassageAssistantDBHelper.MASSAGES_COLUMN_DURATION));

        this.mMode = cursor.getInt(cursor.getColumnIndex(
                MassageAssistantDBHelper.MASSAGES_COLUMN_MODE));

        this.mType = cursor.getString(cursor.getColumnIndex(
                MassageAssistantDBHelper.MASSAGES_COLUMN_TYPE));

        this.mObjective = cursor.getString(cursor.getColumnIndex(
                MassageAssistantDBHelper.MASSAGES_COLUMN_OBJECTIVE));

        this.mUsedMaterials = cursor.getString(cursor.getColumnIndex(
                MassageAssistantDBHelper.MASSAGES_COLUMN_USEDMATERIALS));

        this.mComments = cursor.getString(cursor.getColumnIndex(
                MassageAssistantDBHelper.MASSAGES_COLUMN_COMMENTS));
    }

    /* Attributes get/set */
    public long getId() { return mId; }
    public void setId( long id ) { this.mId = id; }

    public String getPatient() { return mPatient; }
    public void setPatient( String patient ) { this.mPatient = patient; }

    public long getIdMasseur() { return mIdMasseur; }
    public void setIdMasseur( int idMasseur ) { this.mIdMasseur = idMasseur; }

    public Date getStartDateTime() { return mStartDateTime; }
    public void setStartDateTime( Date startDateTime ) {
        this.mStartDateTime = startDateTime; }
    public void setStartDateTime( String startDateTime ) {
        this.mStartDateTime =
                DateTimeUtils.FormattedStringToDateTime( startDateTime,
                        DateTimeUtils.SQLLiteFormat()); }

    public Date getEndDateTime() { return mEndDateTime; }
    public void setEndDateTime( Date endDateTime ) {
        this.mEndDateTime = endDateTime; }
    public void setEndDateTime( String endDateTime ) {
        this.mEndDateTime =
                DateTimeUtils.FormattedStringToDateTime( endDateTime,
                        DateTimeUtils.SQLLiteFormat()); }

    public long getDuration() { return mDuration; }
    public long getDurationInMilliseconds() { return mDuration * 1000; }
    public void setDuration(long duration) { this.mDuration = duration; }

    public int getMode() { return mMode; }
    public void setMode( int mode ) { this.mMode = mode; }
    public String getModeAbbr(){
        return modeToAbbr(mMode);
    }
    public String getModeLabel(Resources res){
        return modeToStr( res, mMode);
    }
    public String getModeLabel2(Resources res){
        return modeToStr2( res, mMode);
    }

    public String getType() { return mType; }
    public void setType( String type ) { this.mType = type; }

    public String getObjective() { return mObjective; }
    public void setObjective( String objective ) {
        this.mObjective = objective; }

    public String getUsedMaterials() { return mUsedMaterials; }
    public void setUsedMaterials( String usedMaterials ) {
        this.mUsedMaterials = usedMaterials; }

    public String getComments() { return mComments; }
    public void setComments( String comments ) { this.mComments = comments; }

    public String getStartToEndString(Resources res){
        String s_startOfMassage, s_endOfMassage;

        s_startOfMassage = new SimpleDateFormat("HH:mm",
                java.util.Locale.getDefault()).format(getStartDateTime() );

        if(DateTimeUtils.sameDay(getStartDateTime(),
                getEndDateTime()) ){
            s_endOfMassage = new SimpleDateFormat("HH:mm",
                    java.util.Locale.getDefault()).format(getEndDateTime() );

        }
        else
        {
            s_endOfMassage =
                    new SimpleDateFormat("HH:mm",
                            java.util.Locale.getDefault()).format(
                            getEndDateTime() ) +
                            " (" +
                            new SimpleDateFormat("dd/MM/yyyy",
                                    java.util.Locale.getDefault()).format(
                                    getEndDateTime() ) +
                            ")";
        }

        return String.format(res.getString(R.string.myMassages_start_end_text),
                s_startOfMassage, s_endOfMassage);

    }


    public static String modeToAbbr(int mode){
        switch (mode){
            case MODE_SIMPLE:
                return "SIMPLE";

            case MODE_INTERMEDIATE:
                return "INTERMEDIATE";

            case MODE_ADVANCED:
                return "ADVANCED";
        }
        return "";
    }

    public static String modeToStr(Resources res, int mode){
        switch (mode){
            case MODE_SIMPLE:
                return res.getString(R.string.massageMode_simple);

            case MODE_INTERMEDIATE:
                return res.getString(R.string.massageMode_intermediate);

            case MODE_ADVANCED:
                return res.getString(R.string.massageMode_advanced);

        }
        return "";
    }

    public static String modeToStr2(Resources res, int mode){
        switch (mode){
            case MODE_SIMPLE:
                return res.getString(R.string.massageMode_simple2);

            case MODE_INTERMEDIATE:
                return res.getString(R.string.massageMode_intermediate2);

            case MODE_ADVANCED:
                return res.getString(R.string.massageMode_advanced2);

        }
        return "";

    }

    // PRIVATE METHODS

}
