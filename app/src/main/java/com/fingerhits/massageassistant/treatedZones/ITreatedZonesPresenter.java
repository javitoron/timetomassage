package com.fingerhits.massageassistant.treatedZones;

/**
 * Created by Javier Torón on 05/03/2017.
 */

public interface ITreatedZonesPresenter {
    public void initView(long idMassageSelected, boolean massageFinished);

    public void saveNewCodZone(long idTreatedZone, int selectedZoneIndex);
    public void deleteTreatedZone(Long idTreatedZone, boolean isTitle);
    public void deleteMassage(Long idMassage);
    public void sendMassageSummary(Long idMassage);
    }
