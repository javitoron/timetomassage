package com.fingerhits.massageassistant.treatedZones;

import android.database.Cursor;

import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

/**
 * Created by Javier Torón on 05/03/2017.
 */

public interface ITreatedZonesView {
    public static String NAME = "TreatedZones";

    public void showTreatedZones(boolean mMassageFinished);

    public void hideMassage();
    public void showMassage(MassageRecord mMassage);

    public void updateList(Cursor treatedZones);

    public void removeTreatedZone(MassageRecord massage,
                                  TreatedZoneRecord treatedZone,
                                  boolean isTitle);
    public void removeMassage(String modeLabel);

    public void sendEmail( MassageRecord massage, Cursor treatedZones);
}
