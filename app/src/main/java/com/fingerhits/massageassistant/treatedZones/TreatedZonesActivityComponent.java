package com.fingerhits.massageassistant.treatedZones;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Subcomponent;

/**
 * Created by Javier Torón on 03/03/2017.
 */

@ActivityScope
@Subcomponent(modules = TreatedZonesActivityModule.class)
public interface TreatedZonesActivityComponent {
    void inject(TreatedZonesActivity activity);
}
