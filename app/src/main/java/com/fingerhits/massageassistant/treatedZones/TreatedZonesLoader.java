package com.fingerhits.massageassistant.treatedZones;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;

import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;

/**
 * TreatedZonesLoader - Used by MyMassagesActivity
 *
 * @author javi on  01/06/2015.
 */
public class TreatedZonesLoader extends CursorLoader {
    private static final String TAG = "TreatedZonesLoader";

    private final MassageAssistantDBHelper mDatabaseHelper;
    public long idMassage = 0;

    public TreatedZonesLoader(Context context ) {
        super(context);
        mDatabaseHelper = new MassageAssistantDBHelper(context);
    }

    @Override
    public Cursor loadInBackground() {
        if( idMassage == 0 ){
            return null;
        }
        else {
            return mDatabaseHelper.getTreatedZonesInMassage(idMassage);
        }
    }
}

