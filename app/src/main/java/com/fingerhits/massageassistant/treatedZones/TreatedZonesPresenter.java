package com.fingerhits.massageassistant.treatedZones;

import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 05/03/2017.
 */

public class TreatedZonesPresenter implements ITreatedZonesPresenter {
    private ITreatedZonesView view;
    private ITreatedZonesInteractor interactor;

    private long mIdMassageSelected = 0;
    private MassageRecord mMassage;


    @Inject
    public TreatedZonesPresenter(ITreatedZonesView view,
                                 ITreatedZonesInteractor interactor){
        this.view = view;
        this.interactor = interactor;
    }

    public void initView(long idMassageSelected, boolean massageFinished){
        mIdMassageSelected = idMassageSelected;

        if(massageFinished){
            mMassage = interactor.getMassage(mIdMassageSelected);
            view.showMassage(mMassage);
        }
        else{
            view.hideMassage();
        }

        view.showTreatedZones(massageFinished);

    }


    public void saveNewCodZone(long idTreatedZone, int selectedZoneIndex){
        TreatedZoneRecord tz = interactor.getTreatedZone(idTreatedZone);
        String[] codZones =
                Divisions.getCodZonesArray( tz.getCodPosition(), tz.getCodDivision());
        String newCodZone = codZones[selectedZoneIndex];

        interactor.saveNewCodZone( idTreatedZone, newCodZone);
    }


    public void deleteTreatedZone(Long idTreatedZone, boolean isTitle) {
        TreatedZoneRecord treatedZone = interactor.getTreatedZone(idTreatedZone);

        Long idMassage = treatedZone.getIdMassage();

        interactor.deleteTreatedZone( idMassage, idTreatedZone);

        view.removeTreatedZone( interactor.getMassage(idMassage), treatedZone,
                isTitle);
    }


    public void deleteMassage(Long idMassage) {
        MassageRecord massage = interactor.getMassage(idMassage);

        interactor.deleteMassage(idMassage);

        view.removeMassage(massage.getModeAbbr());
    }


    public void sendMassageSummary(Long idMassage){
        MassageRecord massage = interactor.getMassage(idMassage);

        if( massage != null ) {
            view.sendEmail( massage, interactor.getTreatedZones(massage.getId()));
        }

    }
}
