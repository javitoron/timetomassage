package com.fingerhits.massageassistant.treatedZones;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Javier Torón on 03/03/2017.
 */

@Module
public class TreatedZonesActivityModule {
    public final ITreatedZonesView view;

    public TreatedZonesActivityModule(ITreatedZonesView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    ITreatedZonesView provideITreatedZonesView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    ITreatedZonesInteractor provideITreatedZonesInteractor(
            TreatedZonesInteractor interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    ITreatedZonesPresenter provideITreatedZonesPresenter(
            TreatedZonesPresenter presenter) {
        return presenter;
    }
}
