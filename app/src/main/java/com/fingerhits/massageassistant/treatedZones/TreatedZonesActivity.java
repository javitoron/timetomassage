package com.fingerhits.massageassistant.treatedZones;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.fingerhits.massageassistant.myMassages.MassageMailer;
import com.fingerhits.massageassistant.options.OptionsActivity;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;
import com.fingerhits.massageassistant.utils.MiscUtils;

import java.text.SimpleDateFormat;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.fingerhits.massageassistant.utils.DateTimeUtils.durationToString;

public class TreatedZonesActivity extends Activity implements ITreatedZonesView,
        LoaderManager.LoaderCallbacks<Cursor>{

    public static final String PARAMETER_IDMASSAGE = "idMassage";
    public static final String PARAMETER_MASSAGE_FINISHED = "massageFinished";

    private static final int TREATEDZONES_LOADER_ID = 1;

    @Inject Context mContext;
    @Inject Resources mRes;
    @Inject public ITreatedZonesPresenter presenter;

    @BindView(R.id.treatedZonesList) ListView mTreatedZonesList;
    @BindView(R.id.massageRecordLayout) RelativeLayout massageRecordLayout;
    @BindView(R.id.patientText) TextView patientText;
    @BindView(R.id.startDateText) TextView startDateText;
    @BindView(R.id.startEndTimeText) TextView startEndTimeText;
    @BindView(R.id.durationText) TextView durationText;
    @BindView(R.id.modeText) TextView modeText;

    @BindString(R.string.error_loading) String error_loading;
    @BindString(R.string.treatedZonesActivity_title)
    String treatedZonesActivity_title;
    @BindString(R.string.app_url) String app_url;
    @BindString(R.string.myMassages_massage_deleted)
    String myMassages_massage_deleted;
    @BindString(R.string.treatedZones_delete_confirmation_title)
    String treatedZones_delete_confirmation_title;
    @BindString(R.string.treatedZones_delete_confirmation_question)
    String treatedZones_delete_confirmation_question;
    @BindString(R.string.myMassages_delete_massage_confirmation_title)
    String myMassages_delete_massage_confirmation_title;
    @BindString(R.string.myMassages_delete_massage_confirmation_question)
    String myMassages_delete_massage_confirmation_question;

    private boolean mReloadMassageRecord = false;
    private long mIdMassageSelected;
    private int mTreatedZonesNumber = 0;

    public boolean editingZone = false;

    private ViewGroup mTreatedZoneToDeleteViewGroup;

    private LoaderManager mLoaderManager;
    private LoaderManager.LoaderCallbacks<Cursor> mCallbacks;

    private TreatedZonesAdapter mTreatedZonesAdapter;

    /**
     * Set up activity
     * @param savedInstanceState Not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treated_zones);

        ButterKnife.bind(this);

        MainApp.get(this)
                .getAppComponent()
                .plus(new TreatedZonesActivityModule(this))
                .inject(this);


        Bundle b = getIntent().getExtras();

        if (b == null) {
            MiscUtils.showToast(mContext,
                    String.format( error_loading,
                            treatedZonesActivity_title));
            this.setResult(RESULT_CANCELED);
            finish();
            return;
        }

        mIdMassageSelected = b.getLong(PARAMETER_IDMASSAGE);

        presenter.initView( mIdMassageSelected, b.getBoolean(PARAMETER_MASSAGE_FINISHED));

        MiscUtils.prepareActionBar(this);
        MiscUtils.logActivityStarting(this, this.NAME);
    }


    /**
     * Creates a new CursorLoader for the lists to be shown
     * @param id - List id
     * @param args - Not used
     * @return - MyMassagesLoader or TreatedZonesLoader
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        switch (id) {
            case TREATEDZONES_LOADER_ID:
                TreatedZonesLoader tzl;
                tzl = new TreatedZonesLoader(mContext);
                tzl.idMassage = mIdMassageSelected;

                return tzl;
        }

        return null;
    }

    /**
     * Associates queried cursor with the SimpleCursorAdapter
     * @param loader - MyMassagesLoader or TreatedZonesLoader
     * @param cursor - Queried cursor
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case TREATEDZONES_LOADER_ID:
                mTreatedZonesNumber = cursor.getCount();

                if(mTreatedZonesNumber == 1){
                    mTreatedZonesAdapter.massageFinished = false;
                }

                mTreatedZonesAdapter.swapCursor(cursor);

                break;
        }
        // The listview now displays the queried data.
    }


    /**
     * When the Loader's data is unavailable set to null cursor
     * @param loader - MyMassagesLoader or TreatedZonesLoader
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // For whatever reason, the Loader's data is now unavailable.
        // Remove any references to the old data by replacing it with
        // a null Cursor.
        switch (loader.getId()) {
            case TREATEDZONES_LOADER_ID:
                mTreatedZonesAdapter.swapCursor(null);
                break;
        }
    }


    /**
     * Makes menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_my_massages_actions, menu);
        return true;
    }


    /**
     * Manages menu options
     *
     * * options_menu: Call OptionsActivity
     * * back: return to parent activity
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options_menu:
                Intent intent = new Intent( this, OptionsActivity.class );
                startActivity(intent );

                return true;

            case R.id.open_app_website_menu:
                MiscUtils.openURL(this, app_url);

                return true;

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Request sync before exit
     */
    @Override
    public void onBackPressed() {
        if(mReloadMassageRecord){
            this.setResult(RESULT_OK);
        }
        else {
            this.setResult(RESULT_CANCELED);
        }
        finish();
    }


    /**
     * Deletes the clicked treated zone
     * @param view One treated zone of the list
     */

    public void deleteTreatedZone(View view ) {
        final View passedView = view;
        new AlertDialog.Builder(this)
                .setTitle(treatedZones_delete_confirmation_title)
                .setMessage(treatedZones_delete_confirmation_question)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                Long idTreatedZone = Long.parseLong(
                                        passedView.getTag(
                                                R.id.ZonesIdTag)
                                                .toString());

                                mTreatedZoneToDeleteViewGroup =
                                        (ViewGroup) passedView.getParent();

                                presenter.deleteTreatedZone(idTreatedZone,
                                        (passedView.getTag(R.id.ZonesIsTitleTag)
                                                .equals("y")));


                            }
                        }

                ).setNegativeButton(android.R.string.no, null)
                .show();
    }


    /**
     * Save codZone to treatedZone when pressed the save button
     *
     * @param view Save button in a treatedZone item
     */
    public void saveCodZone(View view) {
        editingZone = false;

        String s_idTreatedZone = view.getTag().toString();
        long mIdTreatedZoneSelected = Long.parseLong(s_idTreatedZone);

        RelativeLayout zoneLayout =
                (RelativeLayout) view.getParent();
        zoneLayout.setBackgroundColor(GraphicUtils.getColor(mRes,
                R.color.transparent));


        // Botón
        view.setVisibility(View.GONE);

        // Botón borrar
        ImageButton treatedZonesDeleteButton = (ImageButton)
                zoneLayout.findViewById(R.id.treatedZonesDeleteButton);
        treatedZonesDeleteButton.setVisibility(View.VISIBLE);

        // Spinner
        Spinner treatedZoneSelectSpinner = (Spinner) zoneLayout.findViewById(
                R.id.treatedZoneSelectSpinner);
        treatedZoneSelectSpinner.setVisibility(View.GONE);

        EditText treatedZoneText = (EditText)  zoneLayout.findViewById(
                R.id.treatedZoneText);
        treatedZoneText.setText(treatedZoneSelectSpinner.getSelectedItem().toString());
        treatedZoneText.setVisibility(View.VISIBLE);

        presenter.saveNewCodZone(mIdTreatedZoneSelected, treatedZoneSelectSpinner.getSelectedItemPosition());

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "SaveNewCodZone " + mIdTreatedZoneSelected);
        }
    }


    public void updateList(Cursor treatedZones) {
        if (treatedZones.moveToFirst()) {
            mTreatedZonesAdapter.swapCursor(treatedZones);
        } else {
            onBackPressed();
        }
    }


    @OnClick( R.id.massagesDeleteButton)
    public void deleteMassage() {
        new AlertDialog.Builder(this)
                .setTitle(myMassages_delete_massage_confirmation_title)
                .setMessage(myMassages_delete_massage_confirmation_question)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                presenter.deleteMassage(mIdMassageSelected);
                            }
                        }

                )
                .setNegativeButton(android.R.string.no, null)
                .show();
    }


    /**
     * Prepare email with summary of the clicked massage
     */
    @OnClick( R.id.massagesSendButton)
    public void sendMassageSummary() {
        presenter.sendMassageSummary(mIdMassageSelected);
    }


    public void sendEmail( MassageRecord massage, Cursor treatedZones){

        MassageMailer mailer = new MassageMailer();
        mailer.inject(massage, treatedZones, mContext, mRes);
        MiscUtils.sendEmail( this, "", mailer.subject(), mailer.summary());

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "SendEmail " + String.valueOf(massage.getId()));
        }
        MiscUtils.sendEventTracker(this, NAME, NAME,
                "SendEmail", massage.getModeAbbr(), -1L);

    }



    public void showTreatedZones( boolean massageFinished){
        mTreatedZonesAdapter = new TreatedZonesAdapter( this, null, 0);
        mTreatedZonesAdapter.massageFinished = massageFinished;

        mTreatedZonesList.setAdapter(mTreatedZonesAdapter);

        mCallbacks = this;
        mLoaderManager = getLoaderManager();
        mLoaderManager.initLoader(TREATEDZONES_LOADER_ID, null, mCallbacks);
    }

    public void hideMassage(){
        massageRecordLayout.setVisibility(View.GONE);
    }

    public void showMassage(MassageRecord mMassage){
        patientText.setText(mMassage.getPatient());

        startDateText.setText(new SimpleDateFormat("dd/MM/yyyy",
                java.util.Locale.getDefault()).format(
                mMassage.getStartDateTime()));

        startEndTimeText.setText(mMassage.getStartToEndString(mRes));

        durationText.setText(durationToString(
                mMassage.getDurationInMilliseconds()));

        modeText.setText(mMassage.getModeLabel2(mRes));
    }


    public void removeTreatedZone(MassageRecord massage,
                                  TreatedZoneRecord treatedZone,
                                  boolean isTitle){
        mTreatedZonesNumber--;
        if (isTitle) {
            reloadTreatedZones();

        } else {
            mTreatedZoneToDeleteViewGroup.removeAllViews();
        }


        mReloadMassageRecord = true;
        showMassage(massage);

        MiscUtils.showToast(mContext,
                mRes.getString(
                        R.string.treatedZones_deleted));

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "DeleteZone " + String.valueOf(mIdMassageSelected));
        }

        MiscUtils.sendEventTracker(getApplication(),
                NAME, NAME, "DeleteZone", treatedZone.getCodZone(), -1L);

    }


    public void removeMassage(String modeLabel) {
        MiscUtils.showToast(mContext,myMassages_massage_deleted);

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "DeleteMassage " +
                    String.valueOf(mIdMassageSelected));
        }
        MiscUtils.sendEventTracker(getApplication(),
                NAME, NAME, "DeleteMassage", modeLabel, -1L);

        mReloadMassageRecord = true;
        onBackPressed();
    }

    /* ***************** */
	/* PRIVATE FUNCTIONS */
	/* ***************** */
    private void reloadTreatedZones() {
        mTreatedZonesAdapter = new TreatedZonesAdapter(this, null, 0);
        mTreatedZonesAdapter.massageFinished = ( mTreatedZonesNumber != 1 );

        if (mLoaderManager.getLoader(TREATEDZONES_LOADER_ID) == null) {
            mLoaderManager.initLoader(TREATEDZONES_LOADER_ID, null,
                    mCallbacks);
        } else {
            mLoaderManager.restartLoader(TREATEDZONES_LOADER_ID, null,
                    mCallbacks);
        }

        mTreatedZonesList.setAdapter(mTreatedZonesAdapter);
    }

}
