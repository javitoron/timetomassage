package com.fingerhits.massageassistant.treatedZones;

import android.content.Context;
import android.database.Cursor;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.divisions.Divisions;
import com.fingerhits.massageassistant.massagesession.positions.Position;
import com.fingerhits.massageassistant.massagesession.utils.ZonesUtils;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;
import com.fingerhits.massageassistant.utils.MiscUtils;

import static com.fingerhits.massageassistant.utils.DateTimeUtils.durationToString;

/**
 * Adapter TreatedZones <--> TreatedZones of a massage
 *
 * @author javi on 01/03/2015.
 */
public class TreatedZonesAdapter extends CursorAdapter {
    //private ArrayList<TreatedZoneRecord> treatedZones = new ArrayList<>();

    public boolean massageFinished = false;

    /**
     * TreatedZonesAdapter constructor
     *
     * @param context To pass to the superclass
     * @param cursor To show in the list
     */
    public TreatedZonesAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
    }


    /**
     * How is showed a treatedZoneRecord in TreatedZones of a massage
     *
     * @param view container of the record
     * @param context To get resources
     * @param cursor To show a record
     */
    public void bindView(View view, Context context, Cursor cursor) {

        TreatedZoneRecord treatedZoneRecord = new TreatedZoneRecord( cursor );

        String isTitle = "";

        RelativeLayout positionLayout =
                (RelativeLayout) view.findViewById(R.id.positionLayout);

        TextView positionText = (TextView)
                view.findViewById(R.id.positionText);

        EditText treatedZoneText = (EditText)
                view.findViewById(R.id.treatedZoneText);
        if( treatedZoneText != null ) {

            if(cursor.isFirst()){
                isTitle = "y";
                show_position( context, positionLayout, positionText,
                        treatedZoneRecord.getCodPosition() );
                showTreatedZone(context, treatedZoneText, treatedZoneRecord);
            }
            else {
                cursor.moveToPrevious();
                TreatedZoneRecord previousTreatedZoneRecord =
                        new TreatedZoneRecord(cursor);
                cursor.moveToNext();

                if (previousTreatedZoneRecord.getCodPosition().equals(
                        treatedZoneRecord.getCodPosition())) {
                    showTreatedZone(context, treatedZoneText, treatedZoneRecord);
                    showHideLayout(positionLayout, false);
                } else {
                    show_position( context, positionLayout, positionText,
                            treatedZoneRecord.getCodPosition() );
                    showTreatedZone(context, treatedZoneText, treatedZoneRecord);
                    isTitle = "y";
                }
            }
        }

        TextView durationText = (TextView)
                view.findViewById(R.id.treatedZoneDurationText);
        if( durationText != null ) {
            durationText.setText(
                    durationToString(treatedZoneRecord.getDurationInMilliseconds()));
        }

        ImageButton treatedZonesDeleteButton = (ImageButton)
                view.findViewById(R.id.treatedZonesDeleteButton);
        if (treatedZonesDeleteButton != null) {
            if (massageFinished) {
                treatedZonesDeleteButton.setTag(R.id.ZonesIdTag,
                        treatedZoneRecord.getId());
                treatedZonesDeleteButton.setTag(R.id.ZonesIsTitleTag, isTitle);
            } else {
                treatedZonesDeleteButton.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Generates the view to contain the row
     *
     * @param context Not used
     * @param cursor Not used
     * @param parent ViewGroup containing the record
     * @return view made from list_item_treated_zone or treated_zone_list_section
     */
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        return inflater.inflate(R.layout.list_item_treated_zone, parent, false);
    }


    /* *************** */
    /* PRIVATE METHODS */
    /* *************** */
    private void show_position(Context context, RelativeLayout positionLayout,
                                 TextView positionText, String codPosition ) {
        String s_position = Position.codPositionToString( context,
                codPosition);

        if(s_position.equals("")){
            showHideLayout(positionLayout, false);
        }
        else {
            showHideLayout(positionLayout, true);
            positionText.setText(s_position );
        }
    }

    private void showHideLayout(RelativeLayout layout, boolean show){
        if(show) layout.setVisibility(View.VISIBLE);
        else layout.setVisibility(View.GONE);
    }


    private void showTreatedZone(final Context context, EditText treatedZoneText,
                                 final TreatedZoneRecord treatedZoneRecord ) {
        final String codZone = treatedZoneRecord.getCodZone();
        String zone_str = ZonesUtils.codZoneToString(
                context, codZone);

        if (codZone.equals(TreatedZoneRecord.CODZONE_TRANSITIONS)
                || codZone.equals(TreatedZoneRecord.CODZONE_PAUSE) ) {
            zone_str = "<font color='" +
                    GraphicUtils.colorToString(context.getResources(),
                            R.color.accentDarker) +
                    "'>" + zone_str + "</font>";
            treatedZoneText.setText(Html.fromHtml(zone_str));
            treatedZoneText.setClickable(false);
            treatedZoneText.setFocusable(false);

        }
        else {
           treatedZoneText.setText(zone_str);

           if (codZone.equals(TreatedZoneRecord.CODZONE_FULLBODY)) {
               treatedZoneText.setClickable(false);
               treatedZoneText.setFocusable(false);
           }
            else{
                treatedZoneText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        TreatedZonesActivity act = (TreatedZonesActivity)
                                MiscUtils.getActivity(view);
                        if(!act.editingZone) {
                            act.editingZone = true;
                            RelativeLayout zoneLayout =
                                    (RelativeLayout) view.getParent();
                            Button saveTreatedZoneButton = (Button) (zoneLayout
                                    .findViewById(R.id.saveTreatedZoneButton));
                            ImageButton treatedZonesDeleteButton = (ImageButton) (
                                    zoneLayout.findViewById(
                                            R.id.treatedZonesDeleteButton));
                            Spinner treatedZoneSelectSpinner = (Spinner) (
                                    zoneLayout.findViewById(
                                            R.id.treatedZoneSelectSpinner));
                            if (hasFocus) {
                                zoneLayout.setBackgroundColor(
                                        GraphicUtils.getColor(context.getResources(),
                                                R.color.lightGreyBackground));

                                String[] codZones =
                                        Divisions.getCodZonesArray(
                                                treatedZoneRecord.getCodPosition(),
                                                treatedZoneRecord.getCodDivision());

                                String[] codZoneLabels = new String[codZones.length];
                                for (int i = 0; i < codZones.length; i++) {
                                    codZoneLabels[i] =
                                            ZonesUtils.codZoneToString(context,
                                                    codZones[i]);
                                }

                                int zoneIndex = MiscUtils.findInStringArray(codZones, codZone);
                                ArrayAdapter<String> zonesSelectAdapter =
                                        new ArrayAdapter<String>(context,
                                                R.layout.spinner_item_treatedzone,
                                                codZoneLabels);
                                //String[] zoneLabels = buidZoneLabelsArray(codZones);
                                treatedZoneSelectSpinner.setAdapter(zonesSelectAdapter);
                                treatedZoneSelectSpinner.setSelection(zoneIndex);

                                treatedZoneSelectSpinner.setVisibility(View.VISIBLE);
                                treatedZoneSelectSpinner.performClick();

                                saveTreatedZoneButton.setVisibility(View.VISIBLE);
                                saveTreatedZoneButton.setTag(treatedZoneRecord.getId());

                                view.setVisibility(View.GONE);
                                treatedZonesDeleteButton.setVisibility(View.INVISIBLE);
                            }
                        }
                    }
                });
            }
        }
    }
}
