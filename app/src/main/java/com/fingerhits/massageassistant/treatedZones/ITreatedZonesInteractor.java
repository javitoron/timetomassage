package com.fingerhits.massageassistant.treatedZones;

import android.database.Cursor;

import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

/**
 * Created by Javier Torón on 05/03/2017.
 */

public interface ITreatedZonesInteractor {
    public MassageRecord getMassage(Long mIdMassageSelected);
    public TreatedZoneRecord getTreatedZone(Long idTreatedZone);
    public void saveNewCodZone(long idTreatedZone, String newCodZone);
    public void deleteTreatedZone( Long idMassageSelected, Long idTreatedZone);
    public void deleteMassage(Long mIdMassage);
    public Cursor getTreatedZones(Long idMassage);

}
