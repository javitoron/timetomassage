package com.fingerhits.massageassistant.treatedZones;

import android.content.Context;
import android.database.Cursor;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 05/03/2017.
 */

public class TreatedZonesInteractor implements ITreatedZonesInteractor {
    @Inject
    MassageAssistantDBHelper mDatabaseHelper;
    @Inject
    Context mContext;
    @Inject
    MySharedPreferences mPrefs;

    @Inject
    public TreatedZonesInteractor(MassageAssistantDBHelper databaseHelper,
                                  Context context, MySharedPreferences prefs){
        this.mDatabaseHelper = databaseHelper;
        this.mContext = context;
        this.mPrefs = prefs;
    }


    public MassageRecord getMassage(Long idMassageSelected) {
        return mDatabaseHelper.getMassageRecord(idMassageSelected);
    }


    public TreatedZoneRecord getTreatedZone(Long idTreatedZone) {
        return mDatabaseHelper.getTreatedZoneRecord(idTreatedZone);
    }

    public Cursor getTreatedZones(Long idMassage){
        return mDatabaseHelper.getTreatedZonesInMassage(idMassage);
    }

    public void saveNewCodZone(long idTreatedZone, String newCodZone){
        mDatabaseHelper.saveCodZoneInTreatedZoneRecord(idTreatedZone, newCodZone);
    }


    public void deleteTreatedZone( Long idMassageSelected, Long idTreatedZone) {
        mDatabaseHelper.deleteTreatedZoneRecord(idMassageSelected, idTreatedZone);
    }

    public void deleteMassage(Long idMassage){
        mDatabaseHelper.deleteMassageRecord(idMassage);
    }


}
