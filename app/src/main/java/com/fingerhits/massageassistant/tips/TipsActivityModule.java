package com.fingerhits.massageassistant.tips;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by javit on 21/02/2017.
 */

@Module
public class TipsActivityModule {
    public final ITipsView view;

    public TipsActivityModule(ITipsView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    ITipsView provideITipsView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    ITipsInteractor provideITipsInteractor(TipsInteractor interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    ITipsPresenter provideITipsPresenter(TipsPresenter presenter) {
        return presenter;
    }
}
