package com.fingerhits.massageassistant.tips;

import android.view.View;

import com.fingerhits.massageassistant.R;

import butterknife.OnClick;

/**
 * Created by javit on 21/02/2017.
 */

public interface ITipsView {
    static final String NAME = "TipsInteractor";

    void prevTip(View view);
    void nextTip(View view);
    void showTip(String sAdvice);
    void showPrevBtn(boolean show);
    void showNextBtn(boolean show);
    void showTipsNumber(String tipsNumber);

}
