package com.fingerhits.massageassistant.tips;

import android.content.res.Resources;

import com.fingerhits.massageassistant.options.MySharedPreferences;

import javax.inject.Inject;

/**
 * Created by javit on 21/02/2017.
 */

public class TipsPresenter implements ITipsPresenter {

    @Inject Resources mRes;
    @Inject MySharedPreferences mPrefs;

    private ITipsView tipsView;
    private ITipsInteractor tipsInteractor;

    @Inject
    public TipsPresenter(ITipsView tipsView, ITipsInteractor tipsInteractor){
        this.tipsView = tipsView;
        this.tipsInteractor = tipsInteractor;
    }

    public void initView(int i_tip) {
        if(i_tip == 0) {
            tipsInteractor.setUp(tipsInteractor.firstUnseenTip(), true);
        }
        else{
            tipsInteractor.setUp(i_tip, true);
        }
        showTip();
    }

    /**
     * Loads previous tip
     */
    public void prevTip() {
        tipsInteractor.goPrev();
        showTip();
    }
    /**
     * Loads next tip
     */
    public void nextTip() {
        tipsInteractor.goNext();
        showTip();
    }

        /* ***************** */
	/* PRIVATE FUNCTIONS */
	/* ***************** */

    private void showTip() {

        try {
            String sAdvice = tipsInteractor.getTipString();

            tipsView.showTip(sAdvice);
        }
        catch (RuntimeException e){
            tipsView.showTip("");
        }

        showHidePrevNextTip();
        showTipsNumber();
    }

    private void showHidePrevNextTip(){
        tipsView.showPrevBtn(!tipsInteractor.first());
        tipsView.showNextBtn(!tipsInteractor.last());
    }


    private void showTipsNumber(){
        String tipsNumber = String.valueOf( tipsInteractor.getCurrentTip() + 1 )
                + "/" + String.valueOf(tipsInteractor.getCount());

        tipsView.showTipsNumber(tipsNumber);
    }

}
