package com.fingerhits.massageassistant.tips;

/**
 * Created by javit on 21/02/2017.
 */

public interface ITipsPresenter {

    void initView(int i_tip);
    void prevTip();
    void nextTip();

}
