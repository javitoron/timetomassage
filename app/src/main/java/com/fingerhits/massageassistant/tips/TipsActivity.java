package com.fingerhits.massageassistant.tips;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.fingerhits.massageassistant.utils.MiscUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 *
 * This is the activity where tipNames can be read
 *
 * @author Javier Torón
 *
 * It contains:
 * 	* News and help Fragment
 */
public class TipsActivity extends Activity implements ITipsView {

    public static final String PARAMETER_TIPINDEX = "tipIndex";

    @BindView(R.id.tipTxt) TextView mTipTxt;
    @BindView(R.id.nextTipBtn) ImageButton mNextTipBtn;
    @BindView(R.id.prevTipBtn) ImageButton mPrevTipButton;
    @BindView(R.id.pageNumberText) TextView mPageNumberText;

    @Inject ITipsPresenter presenter;

    /**
     * Set up activity
     * @param savedInstanceState Not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);
        ButterKnife.bind(this);

        MainApp.get(this)
                .getAppComponent()
                .plus(new TipsActivityModule(this))
                .inject(this);

        Bundle b = getIntent().getExtras();
        presenter.initView(b.getInt(PARAMETER_TIPINDEX));

        MiscUtils.prepareActionBar(this);
        MiscUtils.logActivityStarting(this, this.NAME);
    }


    @OnClick( R.id.prevTipBtn)
    public void prevTip(View view) {
        presenter.prevTip();

    }


    @OnClick( R.id.nextTipBtn)
    public void nextTip(View view) {
        presenter.nextTip();
     }


    public void showTip(String sAdvice) {
         mTipTxt.setText(Html.fromHtml(sAdvice));
    }


    public void showPrevBtn(boolean show) {
        if (show) {
            mPrevTipButton.setVisibility(View.VISIBLE);
        } else {
            mPrevTipButton.setVisibility(View.INVISIBLE);
        }
    }

    public void showNextBtn(boolean show){
        if(show ) {
            mNextTipBtn.setVisibility(View.VISIBLE);
        }
        else{
            mNextTipBtn.setVisibility(View.INVISIBLE);
        }
    }


    public void showTipsNumber(String tipsNumber){
        mPageNumberText.setText( tipsNumber );
    }

}