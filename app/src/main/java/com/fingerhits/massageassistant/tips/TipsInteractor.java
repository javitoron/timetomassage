package com.fingerhits.massageassistant.tips;

import android.content.res.Resources;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.options.MySharedPreferences;

import javax.inject.Inject;

/**
 * Shows tips and marks them as read
 */
public class TipsInteractor implements ITipsInteractor{

    public static String tipNames[] = {"tip_privacy_txt",
            "tip_massage_data_txt", "tip_modes_txt", "tip_massages_list_txt",
            "tip_warning_freq_txt", "tip_warning_types_txt",
            "tip_warning_sounds_txt", "tip_display_txt", "tip_oil_txt" };

    private int mCount;
    private int mCurrentTipIndex;

    @Inject Resources mRes;
    @Inject MySharedPreferences mPrefs;

    @Inject
    public TipsInteractor(){
        mCount = getStaticCount();
    }

    public void setUp(int p_currentTipIndex, boolean fromTipsActivity){
        if(fromTipsActivity) {

            if (p_currentTipIndex < 0) {
                mCurrentTipIndex = firstUnseenTip();

                if (mCurrentTipIndex < 0) {
                    resetAllTipsShown();

                    mCurrentTipIndex = firstUnseenTip();
                }
            }
            else{
                if (p_currentTipIndex >= mCount) {
                    mCurrentTipIndex = mCount - 1;
                }
                else {
                    mCurrentTipIndex = p_currentTipIndex;
                    saveTipShown(p_currentTipIndex);
                }
            }
        }
        else{
            //From MainActivity
            //Check tipsShownInMain
            String tipsShown = getTipsShown();

            if(tipsShown.length() > getTipsShownInMain().length() ){
                mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN,
                        tipsShown );
            }
            //End check

            mCurrentTipIndex = firstUnseenInMainTip();
        }

    }

    public int getCount(){ return mCount; }
    public static int getStaticCount(){ return tipNames.length; }

    public void goPrev() {
        if(mCurrentTipIndex > 0 && mCurrentTipIndex < mCount ) {
            mCurrentTipIndex--;
        }
        else{
            if(mCurrentTipIndex <= 0) {
                mCurrentTipIndex = 0;
            }
            else {
                if (mCurrentTipIndex >= mCount) {
                    mCurrentTipIndex = mCount - 1;
                }
            }
        }
    }

    public void goNext() {
        mCurrentTipIndex = firstUnseenTipAfterCurrent();
        if(mCurrentTipIndex == -1) mCurrentTipIndex = firstUnseenTip();
    }


    public String getTipString(){ return tip(name(mCurrentTipIndex)); }

    public boolean allTipsShown(){ return (mCurrentTipIndex == -1);}
    public int getCurrentTip(){ return mCurrentTipIndex;}

    public boolean first(){ return (mCurrentTipIndex == 0);}
    public boolean last(){ return (mCurrentTipIndex == ( mCount - 1 ));}

    public int firstUnseenTip(){
        int i;

        String tipsShown = getTipsShown();
        String tipsShownInMain = getTipsShownInMain();

        for(i = 0; i < mCount; i++ ){
            if(!tipsShown.contains(tipNames[i]) ){
                if(!tipsShownInMain.contains(tipNames[i]) ) {
                    mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN,
                            tipsShownInMain + "|" + tipNames[i]);
                }
                saveTipShown(i);
                return i;
            }
        }

        return -1;
    }

    private int firstUnseenTipAfterCurrent(){
        int i;

        String tipsShown = getTipsShown();
        String tipsShownInMain = getTipsShownInMain();

        for(i = mCurrentTipIndex + 1; i < mCount; i++ ){
            if(!tipsShown.contains(tipNames[i]) ){
                if(!tipsShownInMain.contains(tipNames[i]) ) {
                    mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN,
                            tipsShownInMain + "|" + tipNames[i]);
                }
                saveTipShown(i);
                return i;
            }
        }

        return -1;
    }

    /* PRIVATE METHODS */
    private void saveTipShown(int tipIndex){
        String tipsShown = getTipsShown();
        if(!tipsShown.contains(tipNames[tipIndex]) ) {
            mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN,
                    tipsShown + "|" + tipNames[tipIndex]);
        }
    }
    private String name(int tipIndex){
        return tipNames[tipIndex];
    }

    private void resetAllTipsShown(){
        mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN, "");
    }


    private int firstUnseenInMainTip(){
        int i;

        String tipsShownInMain = getTipsShownInMain();

        for(i = 0; i < mCount; i++ ){
            if(!tipsShownInMain.contains(tipNames[i]) ){
                mPrefs.saveString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN,
                        tipsShownInMain + "|" + tipNames[i]);
                return i;
            }
        }

        i = -1;
        return i;
    }


    private String tip(String tipName ) {
        String sAdvice;

        switch( tipName ) {
            case "tip_privacy_txt":
                sAdvice = mRes.getString(R.string.tip_privacy_txt);

                break;

            case "tip_massage_data_txt":
                sAdvice = mRes.getString(R.string.tip_massage_data_txt);

                sAdvice = String.format(sAdvice,
                        mRes.getString(R.string.main_resumeLastMassage_button));
                break;

            case "tip_modes_txt":
                sAdvice = mRes.getString(R.string.tip_modes_txt);

                break;

            case "tip_massages_list_txt":
                sAdvice = mRes.getString(R.string.tip_massages_list_txt);

                sAdvice = String.format(sAdvice,
                        mRes.getString(R.string.main_myMassages_button));
                break;

            case "tip_warning_freq_txt":
                sAdvice = mRes.getString(R.string.tip_warning_freq_txt);

                sAdvice = String.format(sAdvice,
                        mRes.getString(R.string.menu_settings));
                break;

            case "tip_warning_types_txt":
                sAdvice = mRes.getString(R.string.tip_warning_types_txt);

                sAdvice = String.format(sAdvice,
                        mRes.getString(R.string.menu_settings));
                break;

            case "tip_warning_sounds_txt":
                sAdvice = mRes.getString(R.string.tip_warning_sounds_txt);

                sAdvice = String.format(sAdvice,
                        mRes.getString(R.string.menu_settings));
                break;

            case "tip_display_txt":
                sAdvice = mRes.getString(R.string.tip_display_txt);

                sAdvice = String.format(sAdvice,
                        mRes.getString(R.string.menu_settings));
                break;

            case "tip_oil_txt":
                sAdvice = mRes.getString(R.string.tip_oil_txt);
                break;

            default:
                sAdvice = "";

                break;
        }

        return sAdvice;
    }

    private String getTipsShown(){
        return mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN);
    }

    private String getTipsShownInMain() {
        return mPrefs.getString(MySharedPreferences.PREFS_TIPS_SHOWN_IN_MAIN);
    }


}
