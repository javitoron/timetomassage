package com.fingerhits.massageassistant.tips;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Subcomponent;

/**
 * Created by javit on 21/02/2017.
 */
@ActivityScope
@Subcomponent(modules = TipsActivityModule.class)
public interface TipsActivityComponent {
    void inject(TipsActivity activity);
}