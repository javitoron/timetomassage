package com.fingerhits.massageassistant.tips;

/**
 * Created by javit on 21/02/2017.
 */

public interface ITipsInteractor {
    void setUp(int p_currentTipIndex, boolean fromTipsActivity);
    int getCount();
    void goPrev();
    void goNext();

    String getTipString();
    boolean allTipsShown();
    int getCurrentTip();
    int firstUnseenTip();

    boolean first();
    boolean last();
}
