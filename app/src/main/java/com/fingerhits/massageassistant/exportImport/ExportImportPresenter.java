package com.fingerhits.massageassistant.exportImport;

import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 26/02/2017.
 */

public class ExportImportPresenter implements IExportImportPresenter {
    private IExportImportView view;
    private IExportImportInteractor interactor;


    private int mNumMassagesToExport;
    private int mNumFilesToImport;


    @Inject
    public ExportImportPresenter(IExportImportView exportImportView,
                                 IExportImportInteractor exportImportInteractor) {
        this.view = exportImportView;
        this.interactor = exportImportInteractor;
    }


    public void initView() {
        mNumMassagesToExport = interactor.numberOfMassages();
        view.showExportOptions(mNumMassagesToExport);

        interactor.updateFilesToImportList();
        view.showImportOptions( interactor.filesToImportNames() );
    }


    public void doImport(int selectedPosition){
        try{
            int numMassagesImported = interactor.doImport(selectedPosition);
            view.importFinishedOk(numMassagesImported);
        }
        catch (Exception e){
            view.importFinishedError(e);
        }
    }

    public void doExport() {
        try{
            interactor.doExport();
            interactor.updateFilesToImportList();

            view.exportFinishedOk(interactor.filesToImportNames());
        }
        catch (IOException e){
            view.exportFinishedError(e);
        }
    }
}
