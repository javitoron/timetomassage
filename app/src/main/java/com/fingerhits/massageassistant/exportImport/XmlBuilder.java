package com.fingerhits.massageassistant.exportImport;

import android.annotation.SuppressLint;

import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.DateTimeUtils;

import java.io.IOException;

/**
 * Builds the XML string from massage and treatedZone records
 * Created by javi on 10/03/2016.
 */
public class XmlBuilder {
    private static final String OPEN_XML_STANZA = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    private static final String CLOSE_WITH_TICK = "'>";
    private static final String EXPORT_OPEN = "<export exportDateTime='";
    private static final String EXPORT_CLOSE = "</export>";
    private static final String MASSAGES_OPEN = "<massages>";
    private static final String MASSAGES_CLOSE = "</massages>";
    private static final String ZONES_OPEN = "<zones>";
    private static final String ZONES_CLOSE = "</zones>";
    private static final String PREFERENCES_OPEN = "<preferences>";
    private static final String PREFERENCES_CLOSE = "</preferences>";

    private final StringBuilder sb;

    public XmlBuilder() throws IOException {
        this.sb = new StringBuilder();
    }

    void start(String exportDateTime) {
        this.sb.append(OPEN_XML_STANZA);
        this.sb.append(EXPORT_OPEN);
        this.sb.append(exportDateTime);
        this.sb.append(CLOSE_WITH_TICK);
    }

    String end() throws IOException {
        this.sb.append(EXPORT_CLOSE);
        return this.sb.toString();
    }

    void addElement(String element) {
        this.sb.append(element);
    }


    @SuppressLint("DefaultLocale")
    void openMassage(MassageRecord massage) {
        this.sb.append(String.format(MassageRecord.XML_OPEN,
                massage.getId(), massage.getPatient(), massage.getIdMasseur(),
                DateTimeUtils.convertDateTimeToFormattedString(
                        massage.getStartDateTime(),
                        DateTimeUtils.SQLLiteFormat()),
                DateTimeUtils.convertDateTimeToFormattedString(
                        massage.getEndDateTime(), DateTimeUtils.SQLLiteFormat()),
                massage.getDuration(), massage.getMode()));

        if(!massage.getType().equals("")){
            this.sb.append(
                    String.format(MassageRecord.XML_TYPE, massage.getType()));
        }

        if(!massage.getObjective().equals("")){
            this.sb.append(
                    String.format(MassageRecord.XML_OBJECTIVE,
                            massage.getObjective()));
        }

        if(!massage.getUsedMaterials().equals("")){
            this.sb.append(
                    String.format(MassageRecord.XML_USEDMATERIALS,
                            massage.getUsedMaterials()));
        }

        if(!massage.getComments().equals("")){
            this.sb.append(
                    String.format(MassageRecord.XML_COMMENTS,
                            massage.getComments()));
        }
    }


    @SuppressLint("DefaultLocale")
    void addZone(TreatedZoneRecord zone) {
        this.sb.append(String.format(TreatedZoneRecord.XML_OPEN,
                zone.getId(), zone.getIdMassage(), zone.getCodPosition(),
                zone.getCodDivision(), zone.getCodZone(),
                DateTimeUtils.convertDateTimeToFormattedString(
                        zone.getStartDateTime(),
                        DateTimeUtils.SQLLiteFormat()),
                DateTimeUtils.convertDateTimeToFormattedString(
                        zone.getEndDateTime(), DateTimeUtils.SQLLiteFormat()),
                zone.getDuration()));

        if(zone.getComments() != null) {
            if (!zone.getComments().equals("")) {
                this.sb.append(
                        String.format(TreatedZoneRecord.XML_COMMENTS,
                                zone.getComments()));
            }
        }

        close(TreatedZoneRecord.XML_CLOSE);
    }


    void openMassages() {
        this.sb.append(MASSAGES_OPEN);
    }

    void closeMassages() {
        this.sb.append(MASSAGES_CLOSE);
    }

    void openZones() {
        this.sb.append(ZONES_OPEN);
    }

    void closeZones() {
        this.sb.append(ZONES_CLOSE);
    }

    void openPreferences() {
        this.sb.append(PREFERENCES_OPEN);
    }

    void closePreferences() {
        this.sb.append(PREFERENCES_CLOSE);
    }

    void close(String closeTag) {
        this.sb.append(closeTag);
    }

}
