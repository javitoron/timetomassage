package com.fingerhits.massageassistant.exportImport;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecordInvalidException;
import com.fingerhits.massageassistant.utils.DateTimeUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Javier Torón on 30/06/2016.
 */
public class DataXmlImporter {
    private MassageAssistantDBHelper databaseHelper;
    private MySharedPreferences prefs;
    private Resources mRes;
    private Context mContext;

    public int massagesImported;


    public DataXmlImporter(   ){
    }


    public void inject(Context context){
        mContext = context;

        mRes = mContext.getResources();
        prefs = new MySharedPreferences(mContext);
    }


    public int doImport(File xmlFileToImport) {

        databaseHelper = new MassageAssistantDBHelper(mContext);

        try {
            FileInputStream fis = new FileInputStream(xmlFileToImport);
            XmlPullParserFactory xmlFactoryObject =
                    XmlPullParserFactory.newInstance();
            XmlPullParser myParser = xmlFactoryObject.newPullParser();

            myParser.setInput(fis, null);

            int event = myParser.getEventType();
            String text = "";
            MassageRecord massage = null;
            TreatedZoneRecord zone = null;
            List<TreatedZoneRecord> zones = new ArrayList<>();
            long idMassage;
            massagesImported = 0;


            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        text = "";
                        if (name.equalsIgnoreCase("massage")) {
                            zone = null;
                            zones = new ArrayList<>();

                            massage = importedMassage(myParser);
                        }

                        if (name.equalsIgnoreCase("treatedzone")) {
                            zone = importedZone(myParser);
                        }

                        break;

                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (name.equalsIgnoreCase("massage")) {
                            idMassage =
                                    databaseHelper.insertMassageRecord(massage);
                            Log.d( "importing",
                                    "massage saved " + idMassage + " has " +
                                            zones.size()  + " zones");

                            for(TreatedZoneRecord zoneRecord: zones ){
                                zoneRecord.setIdMassage(idMassage);
                                try {
                                    databaseHelper.insertTreatedZoneRecord(zoneRecord);
                                    Log.d("importing", "zone saved");
                                } catch (TreatedZoneRecordInvalidException e) {
                                    Log.e( "importing", e.toString() );
                                }
                            }

                            massagesImported++;
                        }

                        if (name.equalsIgnoreCase("treatedzone")) {
                            Log.d( "importing", "zone added to zones");
                            zones.add( zone );
                        }

                        switch (name) {
                            case MassageAssistantDBHelper
                                    .MASSAGES_COLUMN_TYPE:
                                massage.setType(text);
                                break;
                            case MassageAssistantDBHelper
                                    .MASSAGES_COLUMN_OBJECTIVE:
                                massage.setObjective(text);
                                break;
                            case MassageAssistantDBHelper
                                    .MASSAGES_COLUMN_USEDMATERIALS:
                                massage.setUsedMaterials(text);
                                break;
                            case MassageAssistantDBHelper
                                    .MASSAGES_COLUMN_COMMENTS:
                                if (zone == null) {
                                    massage.setComments(text);
                                } else {
                                    zone.setComments(text);
                                }
                                break;
                        }

                        importPreferences(name, text);

                        break;
                }
                event = myParser.next();
            }

        } catch (FileNotFoundException e) {

            Log.d( "importing", "import error: file");
            return -1;
        }
        catch (XmlPullParserException e){
            Log.d( "importing", "import error: XML parser");
            return -1;
        }
        catch (IOException e){
            Log.d( "importing", "import error: XML event");
            return -1;
        }

        databaseHelper.close();
        databaseHelper = null;

        return massagesImported;
    }

    private void importPreferences( String name, String text) {

        if (name.equals(prefs.PREFS_DISABLESCREENLOCK_KEY)
                || name.equals(prefs.PREFS_WARNINGS_SCHEMA_1_2_FIXED_KEY)
                || name.equals(prefs.PREFS_VISUALWARNINGENABLED_KEY)
                || name.equals(prefs.PREFS_VIBRATIONWARNINGENABLED_KEY)
                || name.equals(prefs.PREFS_SOUNDENABLED_KEY)
                || name.equals(prefs.PREFS_DINGTIMESOUNDENABLED_KEY)
                || name.equals(prefs.PREFS_HALFMASSAGEDURATIONSOUNDENABLED_KEY)
                || name.equals(prefs.PREFS_FULLMASSAGEDURATIONSOUNDENABLED_KEY)
                || name.equals(prefs.PREFS_VISUALWARNINGSENABLED_KEY)
                || name.equals(prefs.PREFS_VIBRATIONWARNINGSENABLED_KEY)
                || name.equals(prefs.PREFS_SOUNDWARNINGSENABLED_KEY)
                || name.equals(prefs.PREFS_DINGTIMEWARNINGSENABLED_KEY)
                || name.equals(prefs.PREFS_HALFMASSAGEDURATIONWARNINGENABLED_KEY)
                || name.equals(prefs.PREFS_FULLMASSAGEDURATIONWARNINGENABLED_KEY)
                || name.equals(prefs.PREFS_MINIMALZONEDURATION_FIXED_KEY)) {
            prefs.saveBool(name, Boolean.valueOf(text));
        }

        if (name.equals(prefs.PREFS_MODE_KEY)
                || name.equals(prefs.PREFS_MASSAGEDURATION_KEY)
                || name.equals(prefs.PREFS_DINGTIME_KEY)
                || name.equals(prefs.PREFS_DINGTIMESOUND_KEY)
                || name.equals(prefs.PREFS_HALFMASSAGEDURATIONSOUND_KEY)
                || name.equals(prefs.PREFS_FULLMASSAGEDURATIONSOUND_KEY)
                || name.equals(prefs.PREFS_MINIMALZONEDURATION_KEY)) {
            prefs.saveInt(name, Integer.valueOf(text));
        }

        if (name.equals(prefs.PREFS_MODE_KEY)){
            int mode = Integer.valueOf(text);
            if(mode < 1 || mode > 3 ) prefs.saveInt(name, 1);
        }


        if (name.equals(prefs.PREFS_TIPS_SHOWN)
                || name.equals(prefs.PREFS_TIPS_SHOWN_IN_MAIN)) {
            prefs.saveString(name, text);
        }
    }


    private MassageRecord importedMassage(XmlPullParser myParser) {
        String startDateTime, s_idMasseur;
        startDateTime = myParser.getAttributeValue(null,
                MassageAssistantDBHelper
                        .MASSAGES_COLUMN_STARTDATETIME);
        s_idMasseur = myParser.getAttributeValue(null,
                MassageAssistantDBHelper
                        .MASSAGES_COLUMN_IDMASSEUR);

        MassageRecord massage = new MassageRecord(Integer.parseInt(s_idMasseur),
                "",
                DateTimeUtils.FormattedStringToDateTime(startDateTime,
                        DateTimeUtils.SQLLiteFormat()),
                MassageRecord.MODE_ADVANCED);

        massage.setPatient(
                myParser.getAttributeValue( null,
                        MassageAssistantDBHelper
                                .MASSAGES_COLUMN_PATIENT));
        massage.setEndDateTime(myParser.getAttributeValue(
                null,
                MassageAssistantDBHelper
                        .MASSAGES_COLUMN_ENDDATETIME));

        massage.setDuration(
                Long.parseLong(myParser.getAttributeValue(
                        null,
                        MassageAssistantDBHelper
                                .MASSAGES_COLUMN_DURATION)));

        massage.setMode(
                Integer.parseInt(myParser.getAttributeValue(
                        null,
                        MassageAssistantDBHelper
                                .MASSAGES_COLUMN_MODE)));

        return massage;
    }


    private TreatedZoneRecord importedZone(XmlPullParser myParser) {
        TreatedZoneRecord zone = new TreatedZoneRecord();

        zone.setCodPosition(myParser.getAttributeValue(
                null,
                MassageAssistantDBHelper
                        .TREATEDZONES_COLUMN_CODPOSITION));

        zone.setCodDivision(myParser.getAttributeValue(
                null,
                MassageAssistantDBHelper
                        .TREATEDZONES_COLUMN_CODDIVISION));

        zone.setCodZone(myParser.getAttributeValue(
                null,
                MassageAssistantDBHelper
                        .TREATEDZONES_COLUMN_CODZONE));

        zone.setStartDateTime(myParser.getAttributeValue(
                null,
                MassageAssistantDBHelper
                        .TREATEDZONES_COLUMN_STARTDATETIME));

        zone.setEndDateTime(myParser.getAttributeValue(
                null,
                MassageAssistantDBHelper
                        .TREATEDZONES_COLUMN_ENDDATETIME));

        zone.setDuration(
                Long.parseLong(myParser.getAttributeValue(
                        null,
                        MassageAssistantDBHelper
                                .TREATEDZONES_COLUMN_DURATION)));
        return zone;
    }

}
