package com.fingerhits.massageassistant.exportImport;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Javier Torón on 16/02/2017.
 */

@Module
public class ExportImportActivityModule {
    public final IExportImportView view;

    public ExportImportActivityModule(IExportImportView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    IExportImportView provideIExportImportView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    IExportImportInteractor provideIExportImportInteractor(
            ExportImportInteractor interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    IExportImportPresenter provideIExportImportPresenter(ExportImportPresenter presenter) {
        return presenter;
    }


    @Provides
    @ActivityScope
    public ExportedFilesManager provideFileListLoader() {
        return new ExportedFilesManager();
    }

    @Provides
    @ActivityScope
    public DataXmlImporter provideDataXmlImporter() {
        return new DataXmlImporter();
    }

    @Provides
    @ActivityScope
    public DataXmlExporter provideDataXmlExporter() {
        return new DataXmlExporter();
    }
}
