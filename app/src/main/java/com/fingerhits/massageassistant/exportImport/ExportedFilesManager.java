package com.fingerhits.massageassistant.exportImport;

import android.app.DownloadManager;
import android.content.Context;
import android.os.Environment;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.utils.DateTimeUtils;
import com.fingerhits.massageassistant.utils.EnvironmentHelper;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Date;

/**
 * Created by Javier Torón on 28/02/2017.
 */

public class ExportedFilesManager {
    private EnvironmentHelper mEnvironmentHelper;
    public DownloadManager downloadManager;
    private Context mContext;

    private File[] tempFileList;

    public ExportedFilesManager(){
        mEnvironmentHelper = new EnvironmentHelper();
    }

    public void setDownloadManager(Context context){
        mContext = context;
        downloadManager = (DownloadManager)
                context.getSystemService(context.DOWNLOAD_SERVICE);

    }


    public void addFileToDownloads(DataXmlExporter exporter) {
        downloadManager.addCompletedDownload(exporter.fileName,
                exportDescription(exporter.operationDate), false, "text/xml", exporter.filePath,
                exporter.fileLength, true);
    }


    public void deleteAll(){
        loadFileList();

        for (File file : tempFileList) {
            file.delete();
        }
    }

    public void loadFileList(){
            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String filename) {
                    return filename.contains(DataXmlExporter.EXPORT_EXTENSION)
                            && filename.contains(DataXmlExporter.EXPORT_FILENAME_SUFFIX);
                }

            };
            File[] list = downloadsPath().listFiles(filter);

            if (list == null) tempFileList = new File[0];
            else tempFileList = list;
    }


    public File[] getFilesList() {
        loadFileList();

        return tempFileList;
    }


    public String[] getFileNamesList() {
        if( tempFileList == null ) {
            loadFileList();
        }

        if( tempFileList != null ) {
            String[] mFilenameList = new String[tempFileList.length];

            //iterate over tempFileList
            for (int i = 0; i < tempFileList.length; i++) {
                mFilenameList[i] = tempFileList[i].getName();
            }

            return mFilenameList;
        }
        return new String[0];
    }



    private File downloadsPath(){
        return mEnvironmentHelper.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS);
    }

    private String exportDescription( Date operationDate ){
        String s_description = mContext.getResources()
                .getString((R.string.export_XMLdataDescription));

        String s_dateWithAbbMonth =
                DateTimeUtils.convertDateTimeToFormattedString( operationDate,
                        DateTimeUtils.onlyDateMonthAbbDateTimeFormat());

        String s_time = DateTimeUtils.convertDateTimeToFormattedString(
                operationDate, DateTimeUtils.onlyTimeDateTimeFormat());

        return String.format(s_description, s_dateWithAbbMonth, s_time );
    }

}
