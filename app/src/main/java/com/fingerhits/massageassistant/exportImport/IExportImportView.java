package com.fingerhits.massageassistant.exportImport;

/**
 * Created by Javier Torón on 26/02/2017.
 */

public interface IExportImportView {
    public static String NAME = "ExportImport";

    public void showExportOptions(int mNumMassagesToExport);
    public void showImportOptions(String[] filesToImportNames);

    public void exportFinishedOk(String[] filesToImportNames);
    public void exportFinishedError(Exception e);
    public void importFinishedOk(int massagesImported);
    public void importFinishedError(Exception e);

    }
