package com.fingerhits.massageassistant.exportImport;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.fingerhits.massageassistant.utils.MiscUtils;
import com.google.android.gms.analytics.StandardExceptionParser;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Manages exporting and importing functions
 * @author Javier Torón
 * 
 * Uses DataXmlExporter to save massages and preferences to an XML file in
 *  downloads folder
 *
 * Uses DataXmlImporter to load massages and preferences from a selected XML
 *  file from downloads folder
 * 
*/
public class ExportImportActivity extends Activity implements IExportImportView {
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Inject public IExportImportPresenter presenter;
    @Inject Context mContext;
    @Inject Resources mRes;

    @BindView(R.id.exportBtn) Button mExportBtn;
    @BindView(R.id.importBtn) Button mImportBtn;
    @BindView(R.id.importFileSelectSpinner) Spinner mImportFileSelectSpinner;
    @BindView(R.id.export_noMassagesTxt) TextView mExportNoMassagesTxt;
    @BindView(R.id.import_introTxt) TextView mImportIntroTxt;
    @BindView(R.id.importLayout) RelativeLayout mImportLayout;
    @BindView(R.id.noImportFilesLabel) TextView mNoImportFilesLabel;

    @BindString(R.string.import_noFiles_label) String import_noFiles_label;
    @BindString(R.string.importBtn_description) String importBtn_description;
    @BindString(R.string.import_confirmation_question)
    String import_confirmation_question;
    @BindString(R.string.export_OK) String export_OK;
    @BindString(R.string.export_error) String export_error;
    @BindString(R.string.import_OK_noMassages) String import_OK_noMassages;
    @BindString(R.string.import_error) String import_error;

    private boolean mHaveFilesToImport;
    private boolean mHaveMassagesToImport;

    /**
     * @param savedInstanceState Not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export);
        ButterKnife.bind(this);

        MainApp.get(this)
                .getAppComponent()
                .plus(new ExportImportActivityModule(this))
                .inject(this);

        presenter.initView();

        prepareDependingOnPermissions();

        MiscUtils.prepareActionBar(this);
        MiscUtils.logActivityStarting(this, this.NAME);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    mExportBtn.setEnabled(true);
                    if(mHaveFilesToImport) mImportBtn.setEnabled(true);
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    /**
     * Saves the options and returns to the caller activity
     */
    @Override
    public void onBackPressed() {
        Intent intent = getIntent();

        this.setResult(RESULT_OK, intent);

        finish();
    }

    /**
     * Handle presses on the action bar items (home/back)
     *
     * @param item Action bar item pressed
     * @return true if action taken
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Export massages, treated zones and user preferences
     */
    @OnClick( R.id.exportBtn)
    public void export(){
        presenter.doExport();
    }


    /**
     * Import massages, treated zones and user preferences
     */
    @OnClick( R.id.importBtn)
    public void importFromXML() {
        if (mHaveMassagesToImport) {
            new AlertDialog.Builder(this)
                    .setTitle(importBtn_description)
                    .setMessage(import_confirmation_question)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    presenter.doImport(
                                            mImportFileSelectSpinner
                                                    .getSelectedItemPosition());
                                }
                            })
                    .setNegativeButton(android.R.string.no, null).show();
        } else {
            presenter.doImport(
                    mImportFileSelectSpinner.getSelectedItemPosition());

        }
    }


    public void exportFinishedOk(String[] filesToImportNames){
        logExport();
        showImportOptions(filesToImportNames);

        MiscUtils.showToast(getApplicationContext(), export_OK);
    }


    public void exportFinishedError(Exception e){
        MiscUtils.showToast(getApplicationContext(), export_error);

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "error exporting - " + e.toString());
        }
        MiscUtils.sendExceptionTracker(this, NAME,
                new StandardExceptionParser(this, null)
                        .getDescription(
                                Thread.currentThread().getName(),
                                e), false);

    }

    public void importFinishedOk(int massagesImported){
        if(massagesImported > 0){
            String s_result =
                    mRes.getQuantityString(R.plurals.import_OK,
                            massagesImported );

            MiscUtils.showToast(mContext,
                    String.format(s_result, massagesImported) );
        }
        else{
            MiscUtils.showToast(mContext, import_OK_noMassages );
        }
    }


    public void importFinishedError(Exception e){
        MiscUtils.showToast(getApplicationContext(), import_error);

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "error importing - " + e.toString());
        }
        MiscUtils.sendExceptionTracker(this, NAME,
                new StandardExceptionParser(this, null)
                        .getDescription(
                                Thread.currentThread().getName(),
                                e), false);
    }

    public void showExportOptions(int numMassagesToExport) {
        mHaveMassagesToImport = false;
        if(numMassagesToExport > 0 ) mHaveMassagesToImport = true;

        if (mHaveMassagesToImport) {
            mExportNoMassagesTxt.setVisibility(View.GONE);
        }

    }


    public void showImportOptions(String[] filesToImportNames){
        int numFilesToImport;
        if(filesToImportNames == null ) numFilesToImport = 0;
        else numFilesToImport = filesToImportNames.length;

        mHaveFilesToImport = false;
        if(numFilesToImport > 0) mHaveFilesToImport = true;

        if(mHaveFilesToImport){
            mImportLayout.setVisibility(View.VISIBLE);
            mImportIntroTxt.setVisibility(View.VISIBLE);
            mNoImportFilesLabel.setVisibility(View.GONE);

            ArrayAdapter<String> mImportFileSelectAdapter =
                    new ArrayAdapter<>(this,
                            android.R.layout.simple_spinner_item,
                            filesToImportNames);
            mImportFileSelectAdapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);

            mImportFileSelectSpinner.setAdapter(mImportFileSelectAdapter);

            mImportFileSelectSpinner.setSelection(numFilesToImport - 1);
        }
        else {
            mImportLayout.setVisibility(View.GONE);
            mImportIntroTxt.setVisibility(View.GONE);
            mNoImportFilesLabel.setVisibility(View.VISIBLE);
            mNoImportFilesLabel.setText(String.format(
                    import_noFiles_label,
                    DataXmlExporter.EXPORT_FILENAME_SUFFIX + "*" +
                            DataXmlExporter.EXPORT_EXTENSION));
        }
    }


    /* ***************** */
	/* PRIVATE FUNCTIONS */
	/* ***************** */

    private void prepareDependingOnPermissions() {

        int permission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            if (BuildConfig.DEBUG) {
                Log.d(NAME, "Request permission");
            }

            mExportBtn.setEnabled(false);
            mImportBtn.setEnabled(false);

            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(NAME, "Didn't have to request permission");
            }

            mExportBtn.setEnabled(true);
            if (mHaveFilesToImport) mImportBtn.setEnabled(true);
        }
    }


    private void logExport() {
        if (BuildConfig.DEBUG) {
            Log.d(NAME, "Export completed");
        }
        MiscUtils.sendEventTracker(this, NAME, NAME, "Export", "", -1L);
    }


}