package com.fingerhits.massageassistant.exportImport;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.DateTimeUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Date;

/**
 * Created by javi on 10/03/2016.
 */
public class DataXmlExporter {
    public static String NAME = "DataXmlExporter";

    public static final String EXPORT_EXTENSION = ".xml";
    public static final String EXPORT_FILENAME_SUFFIX="ttmassage_data_";

    private MassageAssistantDBHelper mDb;
    private ExportImportActivity mActivity;
    MySharedPreferences mPrefs;
    private Resources mRes;
    private Context mContext;
    private XmlBuilder mXmlBuilder;

    public String filePath;
    public String fileName;
    public long fileLength;
    public Date operationDate;

    public DataXmlExporter(){}

    public void inject(Context context) {
        mContext = context;

        mRes = mContext.getResources();
        this.mDb = new MassageAssistantDBHelper(mContext);
        mPrefs = new MySharedPreferences(mContext);
    }

    public void export() throws IOException {
        if (BuildConfig.DEBUG) {
            Log.d(NAME, "exporting database - " + mDb.DATABASE_NAME);
        }

        mXmlBuilder = new XmlBuilder();
        mXmlBuilder.start(mDb.DATABASE_NAME);

        exportPreferences();

        mXmlBuilder.openMassages();

        // get the tables
        Cursor massages = mDb.getAllMassageRecords(
                MassageAssistantDBHelper.SORT_FIRST_TO_LAST );
        if (BuildConfig.DEBUG) {
            Log.d(NAME,
                    "getAllMassageRecords -> " + massages.getCount() +
                            " massages");
        }
        Cursor zones;

        if (massages.moveToFirst()) {
            MassageRecord massage;
            do {
                massage = new MassageRecord(massages);

                if (BuildConfig.DEBUG) {
                    Log.d(NAME, "exporting massage - " + massage.getId());
                }
                mXmlBuilder.openMassage(massage);

                //Log.d(NAME, "massage open");

                zones = mDb.getTreatedZonesInMassage(massage.getId());

                //Log.d(NAME, "read zones");

                exportZones(zones);

                //Log.d(NAME, "zones exported");

                mXmlBuilder.close(MassageRecord.XML_CLOSE);

                //Log.d(NAME, "massage closed");

            } while (massages.moveToNext());
        }

        mXmlBuilder.closeMassages();

        String xmlString = this.mXmlBuilder.end();
        this.writeToFile(xmlString);
        if (BuildConfig.DEBUG) {
            Log.d(NAME, "exporting database complete");
        }
    }


    private void exportZones(Cursor zones) throws IOException {
        TreatedZoneRecord zone;
        mXmlBuilder.openZones();

        if (zones.moveToFirst()) {
            do {
                zone = new TreatedZoneRecord(zones);
                this.mXmlBuilder.addZone(zone);
            } while (zones.moveToNext());
        }

        mXmlBuilder.closeZones();
        zones.close();
    }

    private void exportPreferences(){
        mXmlBuilder.openPreferences();

        mXmlBuilder.addElement(
                mPrefs.getIntXML(mPrefs.PREFS_MODE_KEY));
        mXmlBuilder.addElement(
                mPrefs.getIntXML(mPrefs.PREFS_MASSAGEDURATION_KEY));
        mXmlBuilder.addElement(
                mPrefs.getBoolXML(mPrefs.PREFS_DISABLESCREENLOCK_KEY));

        mXmlBuilder.addElement(
                mPrefs.getBoolXML(mPrefs.PREFS_WARNINGS_SCHEMA_1_2_FIXED_KEY));

    /* WARNINGS SCHEMA 1.2*/
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_VISUALWARNINGENABLED_KEY));
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_VIBRATIONWARNINGENABLED_KEY));
        mXmlBuilder.addElement(mPrefs.getBoolXML(mPrefs.PREFS_SOUNDENABLED_KEY));
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_DINGTIMESOUNDENABLED_KEY));

        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_HALFMASSAGEDURATIONSOUNDENABLED_KEY));

        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_FULLMASSAGEDURATIONSOUNDENABLED_KEY));

    /* WARNINGS SCHEMA 1.3*/
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_VISUALWARNINGSENABLED_KEY));
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_VIBRATIONWARNINGSENABLED_KEY));
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_SOUNDWARNINGSENABLED_KEY));
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_DINGTIMEWARNINGSENABLED_KEY));
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_HALFMASSAGEDURATIONWARNINGENABLED_KEY));
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_FULLMASSAGEDURATIONWARNINGENABLED_KEY));

    /* END WARNING SCHEMAS */

        mXmlBuilder.addElement(mPrefs.getIntXML(mPrefs.PREFS_DINGTIME_KEY));
        mXmlBuilder.addElement(
                mPrefs.getIntXML(mPrefs.PREFS_DINGTIMESOUND_KEY));
        mXmlBuilder.addElement(
                mPrefs.getIntXML(mPrefs.PREFS_HALFMASSAGEDURATIONSOUND_KEY));
        mXmlBuilder.addElement(
                mPrefs.getIntXML(mPrefs.PREFS_FULLMASSAGEDURATIONSOUND_KEY));

        mXmlBuilder.addElement(
                mPrefs.getIntXML(mPrefs.PREFS_MINIMALZONEDURATION_KEY));
        mXmlBuilder.addElement(mPrefs.getBoolXML(
                mPrefs.PREFS_MINIMALZONEDURATION_FIXED_KEY));

        mXmlBuilder.addElement(mPrefs.getStringXML(mPrefs.PREFS_TIPS_SHOWN));
        mXmlBuilder.addElement(
                mPrefs.getStringXML(mPrefs.PREFS_TIPS_SHOWN_IN_MAIN));

        mXmlBuilder.closePreferences();

    }


    private void writeToFile(String xmlString)
            throws IOException {
        //new File(Environment.getExternalStorageDirectory(),DATASUBDIRECTORY);

        operationDate = DateTimeUtils.now();

        String s_operationDate =
                DateTimeUtils.convertDateTimeToFormattedString(
                        operationDate, DateTimeUtils.filenameDateTimeFormat());

        String exportFileName = EXPORT_FILENAME_SUFFIX +
                s_operationDate + EXPORT_EXTENSION;
        if (BuildConfig.DEBUG) {
            Log.d("exporting", "file " + exportFileName);
        }

        File dir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS);

        if(!dir.exists()){
            dir.mkdirs();
        }

        filePath = dir.getPath();

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "Downloads folder: " + filePath);
        }

        fileName = exportFileName;


        if (BuildConfig.DEBUG) {
            Log.d(NAME, "Before new file");
        }

        File file = new File(dir, exportFileName);

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "Before createNewFile");
        }

        file.createNewFile();

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "After createNewFile");
        }

        fileLength = xmlString.length();

        ByteBuffer buff = ByteBuffer.wrap(xmlString.getBytes());
        FileChannel channel = new FileOutputStream(file).getChannel();
        try {
            Log.d(NAME, "trying to write to channel");
            channel.write(buff);

        } finally {
            //noinspection ConstantConditions
            if (channel != null)
                channel.close();
        }

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "After writing");
        }
    }
}