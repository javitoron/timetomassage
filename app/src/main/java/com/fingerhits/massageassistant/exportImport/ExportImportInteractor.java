package com.fingerhits.massageassistant.exportImport;

import android.content.Context;

import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 26/02/2017.
 */

public class ExportImportInteractor implements IExportImportInteractor {

    @Inject MassageAssistantDBHelper databaseHelper;
    @Inject Context mContext;
    @Inject ExportedFilesManager mExportedFilesManager;
    @Inject DataXmlImporter importer;
    @Inject DataXmlExporter exporter;

    private File[] mFileList;
    private String[] mFilenameList;


    @Inject
    public ExportImportInteractor(MassageAssistantDBHelper databaseHelper,
                                  Context context,
                                  ExportedFilesManager exportedFilesManager,
                                  DataXmlImporter importer,
                                  DataXmlExporter exporter){
        this.databaseHelper = databaseHelper;
        this.mContext = context;
        this.mExportedFilesManager = exportedFilesManager;
        this.importer = importer;
        this.exporter = exporter;
    }


    public int numberOfMassages(){
        return databaseHelper.numberOfMassages();
    }


    public File[] filesToImport(){
        return mFileList;
    }
    public String[] filesToImportNames(){ return mFilenameList; }


    public void updateFilesToImportList() {
        mExportedFilesManager.loadFileList();

        mFileList= mExportedFilesManager.getFilesList();
        mFilenameList = mExportedFilesManager.getFileNamesList();
    }


    public int doImport(int selectedPosition) throws Exception {
        importer.inject(mContext);
        int numMassagesImported = importer.doImport(mFileList[selectedPosition]);

        return numMassagesImported;
    }


    public void doExport() throws IOException{
        exporter.inject(mContext);
        exporter.export();

        mExportedFilesManager.setDownloadManager(mContext);
        mExportedFilesManager.addFileToDownloads(exporter);
    }


    public void setFileList(File[] fileList){
        mFileList = fileList;
    }

    public void setFileNameList(String[] fileNameList){
        mFilenameList = fileNameList;
    }

    /* PRIVATE METHODS */

}
