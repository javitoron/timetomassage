package com.fingerhits.massageassistant.exportImport;

/**
 * Created by Javier Torón on 26/02/2017.
 */

public interface IExportImportPresenter {
    public void initView();

    public void doImport(int selectedPosition);
    public void doExport();

    }
