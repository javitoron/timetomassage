package com.fingerhits.massageassistant.exportImport;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Subcomponent;

/**
 * Created by Javier Torón on 16/02/2017.
 */

@ActivityScope
@Subcomponent(modules = ExportImportActivityModule.class)
public interface ExportImportActivityComponent {
    void inject(ExportImportActivity activity);
}