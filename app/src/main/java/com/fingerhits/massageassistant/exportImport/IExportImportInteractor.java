package com.fingerhits.massageassistant.exportImport;

import java.io.File;
import java.io.IOException;

/**
 * Created by Javier Torón on 26/02/2017.
 */

public interface IExportImportInteractor {
    public int numberOfMassages();
    public File[] filesToImport();
    public String[] filesToImportNames();


    public void updateFilesToImportList();

    public int doImport(int selectedPosition) throws Exception;
    public void doExport() throws IOException;
}
