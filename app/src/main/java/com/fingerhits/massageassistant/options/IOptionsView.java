package com.fingerhits.massageassistant.options;

/**
 * Created by Javier Torón on 24/02/2017.
 */

public interface IOptionsView {
    public static String NAME = "Options";

    public void prepareSoundSelectAdapter(String[] soundLabels);

    public void prepareMassageDurationOptions(int duration);
    public void writeMassageDurationLabel(int duration);

    public void prepareDisableScreenLockOptions(boolean isChecked);

    public void prepareWarningsOptions(int interval,
                                       boolean dingTimeWarningsEnabled,
                                       boolean halfMassageDurationWarningEnabled,
                                       boolean fullMassageDurationWarningEnabled);
    public void writeDingTimes(String dingTimeLabel, int minutes);

    public void prepareWarningTypesOptions(int duration, String dingTimeLabel,
                                           boolean visualWarningsEnabled,
                                           boolean vibrationWarningsEnabled,
                                           boolean soundWarningsEnabled,
                                           int dingTimeSoundIndex,
                                           int halfMassageSoundIndex,
                                           int fullMassageSoundIndex);
    public void writeDurationTexts(String halfMassageLabel,
                                   String fullMassageLabel, int duration );

    public void prepareMinimalZoneDurationOptions(int minimalZoneDuration);
    public void showVersion();


    }
