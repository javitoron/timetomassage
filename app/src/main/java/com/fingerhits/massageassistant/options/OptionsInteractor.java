package com.fingerhits.massageassistant.options;

import android.content.res.TypedArray;

import com.fingerhits.massageassistant.utils.MiscUtils;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 24/02/2017.
 */

public class OptionsInteractor implements IOptionsInteractor {
    @Inject public MySharedPreferences mPrefs;

    @Inject
    public OptionsInteractor(){ }

    /* GET PREFERENCES */
    public int massageDuration(){
        return mPrefs.getInt( MySharedPreferences.PREFS_MASSAGEDURATION_KEY);
    }

    public boolean disableScreenLock() {
        return mPrefs.getBool(MySharedPreferences.PREFS_DISABLESCREENLOCK_KEY);
    }

    public int dingTime(){
        return mPrefs.getInt(MySharedPreferences.PREFS_DINGTIME_KEY); }

    public int minimalZoneDuration(){
        return mPrefs.getInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY); }

    public boolean visualWarningsEnabled(){
        return mPrefs.getBool(MySharedPreferences.PREFS_VISUALWARNINGSENABLED_KEY); }

    public boolean vibrationWarningsEnabled(){
        return mPrefs.getBool(MySharedPreferences.PREFS_VIBRATIONWARNINGSENABLED_KEY); }

    public boolean soundWarningsEnabled(){
        return mPrefs.getBool(MySharedPreferences.PREFS_SOUNDWARNINGSENABLED_KEY); }

    public boolean dingTimeWarningsEnabled(){
        return mPrefs.getBool(MySharedPreferences.PREFS_DINGTIMEWARNINGSENABLED_KEY); }

    public boolean halfMassageDurationWarningEnabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_HALFMASSAGEDURATIONWARNINGENABLED_KEY); }

    public boolean fullMassageDurationWarningEnabled(){
        return mPrefs.getBool(
                MySharedPreferences.PREFS_FULLMASSAGEDURATIONWARNINGENABLED_KEY); }

    public int dingTimeSound() {
        return mPrefs.getInt(MySharedPreferences.PREFS_DINGTIMESOUND_KEY);
    }

    public int halfMassageSound() {
        return mPrefs.getInt(MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY);
    }

    public int fullMassageSound() {
        return mPrefs.getInt(MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY);
    }


    /* SAVE PREFERENCES */
    public void saveMassageDuration(int progress) {
        mPrefs.saveInt(MySharedPreferences.PREFS_MASSAGEDURATION_KEY, progress);
    }

    public void saveDisableScreenLock(boolean isChecked) {
        mPrefs.saveBool(MySharedPreferences.PREFS_DISABLESCREENLOCK_KEY, isChecked);
    }

    public void saveMinimalZoneDuration(int progress) {
        mPrefs.saveInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY, progress);
    }

    public void saveMassageDuration(boolean isChecked) {
        mPrefs.saveBool(MySharedPreferences.PREFS_DISABLESCREENLOCK_KEY, isChecked);
    }

    public void saveDingTimeWarnings(boolean isChecked) {
        mPrefs.saveBool(MySharedPreferences.PREFS_DINGTIMEWARNINGSENABLED_KEY,
                isChecked);
    }

    public void saveDingTime(int progress) {
        mPrefs.saveInt(MySharedPreferences.PREFS_DINGTIME_KEY, progress);
    }

    public void saveHalfDurationWarning(boolean isChecked) {
        mPrefs.saveBool(MySharedPreferences.PREFS_HALFMASSAGEDURATIONWARNINGENABLED_KEY,
                isChecked);
    }

    public void saveFullDurationWarning(boolean isChecked) {
        mPrefs.saveBool(MySharedPreferences.PREFS_FULLMASSAGEDURATIONWARNINGENABLED_KEY,
                isChecked);
    }

    public void saveVisualWarnings(boolean isChecked) {
        mPrefs.saveBool(MySharedPreferences.PREFS_VISUALWARNINGSENABLED_KEY, isChecked);
    }

    public void saveVibrationWarnings(boolean isChecked) {
        mPrefs.saveBool(MySharedPreferences.PREFS_VIBRATIONWARNINGSENABLED_KEY, isChecked);
    }

    public void saveSoundWarnings(boolean isChecked) {
        mPrefs.saveBool(MySharedPreferences.PREFS_SOUNDWARNINGSENABLED_KEY, isChecked);
    }

    public void saveDingTimeSound(int resourceId) {
        if(resourceId != 0) {
            mPrefs.saveInt(MySharedPreferences.PREFS_DINGTIMESOUND_KEY, resourceId);
        }
    }


    public void saveHalfMassageSound(int resourceId) {
        if(resourceId != 0) {
            mPrefs.saveInt(MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY,
                    resourceId);
        }
    }

    public void saveFullMassageSound(int resourceId){
        if(resourceId != 0) {
            mPrefs.saveInt(MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY,
                    resourceId);
        }
    }

    public int getSoundIndex(String prefSoundKey, TypedArray soundIds) {
        int soundIndex = MiscUtils.findInTypedArray(soundIds,
                mPrefs.getInt(prefSoundKey));

        if (soundIndex == -1) {
            mPrefs.removeValue(prefSoundKey);

            mPrefs.saveInt(prefSoundKey, mPrefs.getInt(prefSoundKey));

            soundIndex = MiscUtils.findInTypedArray(soundIds,
                    mPrefs.getInt(prefSoundKey));
        }

        return soundIndex;
    }
}
