package com.fingerhits.massageassistant.options;

import android.content.res.TypedArray;

/**
 * Created by Javier Torón on 24/02/2017.
 */

public interface IOptionsInteractor {
    public int massageDuration();
    public boolean disableScreenLock();
    public int dingTime();
    public int minimalZoneDuration();
    public boolean visualWarningsEnabled();
    public boolean vibrationWarningsEnabled();
    public boolean soundWarningsEnabled();
    public boolean dingTimeWarningsEnabled();
    public boolean halfMassageDurationWarningEnabled();
    public boolean fullMassageDurationWarningEnabled();

    public void saveMassageDuration(int progress);
    public void saveDisableScreenLock(boolean isChecked);
    public void saveMinimalZoneDuration(int progress);
    public void saveMassageDuration(boolean isChecked);
    public void saveDingTimeWarnings(boolean isChecked);
    public void saveDingTime(int progress);
    public void saveHalfDurationWarning(boolean isChecked);
    public void saveFullDurationWarning(boolean isChecked);
    public void saveVisualWarnings(boolean isChecked);
    public void saveVibrationWarnings(boolean isChecked);
    public void saveSoundWarnings(boolean isChecked);
    public void saveDingTimeSound(int resourceId);
    public void saveHalfMassageSound(int resourceId);
    public void saveFullMassageSound(int resourceId);

    public int dingTimeSound();
    public int halfMassageSound();
    public int fullMassageSound();

    public int getSoundIndex(String prefSoundKey, TypedArray soundIds);


    }
