package com.fingerhits.massageassistant.options;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Vibrator;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.storedData.MassageRecord;


/**
 * 
 * Manages the app preferences
 * 
 * @author Javier Torón
 * 
 * Allows saving and retrieving preference values
 * 
 * Values stored:
 * ******************* USER SETTINGS
 * * MassageDuration (int) MINUTES: How many MINUTES massages are expected to last
 *      (Saved in 5 minutes multiples)
 *
 * * DisableScreenLock (bool): If true screen will be always on (false to allow
 *      screen lock)
 *
 * * DingTimeWarningEnabled (bool): DingTime repeated warnings on/off
 * * HalfMassageDurationWarningEnabled (bool): Half Massage warning on/off
 * * FullMassageDurationWarningEnabled (bool): Full Massage warning on/off

 * * Visual warnings enabled (bool): Massage visual warnings on/off
 * * Vibration warnings enabled (bool): Massage vibration warnings on/off
 * * Sounds warnings enabled (bool): Massage warning sounds on/off
 *
 * * DingTime (int): MINUTES passing between sound warnings (0 for no warnings)
 *
 * * DingTimeSound (int): Raw sound resource to play every DingTime Minutes
 * * HalfMassageDurationSound (int): Raw sound resource to play in the middle
 *      of the massage
 * * FullMassageDurationSound (int): Raw sound resource to play at the end of
 *      the massage
 *
 * * MinimalZoneDuration (int) in SECONDS: If a zone manipulation lasts less
 *      seconds than this value will be discarded (0 for no discards)
 *
 *
 * ******************* GLOBAL APP VALUES
 * * TipsInteractor shown (bool): It's true when there are no more tips to show
 *
 * * User Id (String): Google sign in account id
 *
 * * Last Latitude (String): From last location on current execution
 *
 * * Last Longitude (String): From last location on current execution
 *
 * * Installation (String): Installation id
 *
 * Have 3 keys, IntTestKey, StringTestKey, BooleanTestKey just for testing purposes
 */

public class MySharedPreferences {

    private static final String PREFS_NAME = "MASSAGE_ASSISTANT_PREFERENCES";

    public static final String PREFS_MODE_KEY = "Mode";

    public static final String PREFS_MASSAGEDURATION_KEY = "MassageDuration";

    public static final String PREFS_DISABLESCREENLOCK_KEY = "DisableScreenLock";

    public static final String PREFS_WARNINGS_SCHEMA_1_2_FIXED_KEY =
            "WarningsSchema1.2_Fixed";

    /* WARNINGS SCHEMA 1.2*/
    public static final String PREFS_VISUALWARNINGENABLED_KEY = "VisualWarningEnabled";
    public static final String PREFS_VIBRATIONWARNINGENABLED_KEY =
            "VibrationWarningEnabled";
    public static final String PREFS_SOUNDENABLED_KEY = "SoundEnabled";

    public static final String PREFS_DINGTIMESOUNDENABLED_KEY = "DingTimeSoundEnabled";
    public static final String PREFS_HALFMASSAGEDURATIONSOUNDENABLED_KEY =
            "HalfMassageDurationSoundEnabled";
    public static final String PREFS_FULLMASSAGEDURATIONSOUNDENABLED_KEY =
            "FullMassageDurationSoundEnabled";

    /* WARNINGS SCHEMA 1.3*/
    public static final String PREFS_DINGTIMEWARNINGSENABLED_KEY =
            "DingTimeWarningsEnabled";
    public static final String PREFS_DINGTIME_KEY = "DingTime";
    public static final String PREFS_HALFMASSAGEDURATIONWARNINGENABLED_KEY =
            "HalfMassageDurationWarningEnabled";
    public static final String PREFS_FULLMASSAGEDURATIONWARNINGENABLED_KEY =
            "FullMassageDurationWarningEnabled";

    public static final String PREFS_VISUALWARNINGSENABLED_KEY =
            "VisualWarningsEnabled";
    public static final String PREFS_VIBRATIONWARNINGSENABLED_KEY =
            "VibrationWarningsEnabled";
    public static final String PREFS_SOUNDWARNINGSENABLED_KEY = "SoundWarningsEnabled";


    /* END WARNING SCHEMAS */


    public static final String PREFS_DINGTIMESOUND_KEY = "DingTimeSound";
    public static final String PREFS_HALFMASSAGEDURATIONSOUND_KEY =
            "HalfMassageDurationSound";
    public static final String PREFS_FULLMASSAGEDURATIONSOUND_KEY =
            "FullMassageDurationSound";

    public static final String PREFS_MINIMALZONEDURATION_KEY = "MinimalZoneDuration";
    public static final String PREFS_MINIMALZONEDURATION_FIXED_KEY =
            "MinimalZoneDuration_Fixed";


    public static final String PREFS_TIPS_SHOWN_IN_MAIN = "TipsShownInMain";
    public static final String PREFS_TIPS_SHOWN = "TipsShown";

    public static final String PREFS_ANIMATIONS_DISABLED_FOR_TESTING =
            "AnimationsDisabled";

    public static final String PREFS_USER_ID_KEY = "UserId";
    public static final String PREFS_USER_EMAIL_KEY = "UserEmail";
    public static final String PREFS_USER_LAST_LATITUDE_KEY = "LastLatitude";
    public static final String PREFS_USER_LAST_LONGITUDE_KEY = "LastLongitude";
    public static final String PREFS_INSTALLATION_ID_KEY = "Installation";
    public static final String PREFS_INSTALLATION_ID_SENT_KEY = "SentInstallationId";

    public static final String PREFS_INT_TEST_KEY = "IntTestKey";
    public static final String PREFS_STRING_TEST_KEY = "StringTestKey";
    public static final String PREFS_BOOLEAN_TEST_KEY = "BooleanTestKey";

    public static final int DEFAULT_DINGTIMESOUND_KEY = R.raw.flute_shorter;
    public static final int DEFAULT_HALFMASSAGEDURATIONSOUND_KEY = R.raw.gong_metal;
    public static final int DEFAULT_FULLMASSAGEDURATIONSOUND_KEY = R.raw.harp_dream;


    private Context mContext;
    private final SharedPreferences mSettings;
    private Editor mEditor;
    private final Resources mRes;
    private Vibrator mVibrator;


    /**
     * MySharedPreferences constructor
     * @param context To get the shared preferences
     */
    public MySharedPreferences( Context context ) {
        super();

        mContext = context;

        mSettings =
                context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        mRes = context.getResources();
    }
 

    /**
     * Stores a string value on sharedPreferences
     * @param key - Preference
     * @param text - String Value to store
     */
    public void saveString( String key, String text) {
        
    	mSettings.edit().putString(key, text).apply();
    }


    /**
     * Stores an integer value on sharedPreferences
     * @param key - Preference
     * @param value - Integer value to store
     */
    public void saveInt( String key, int value) {

        mSettings.edit().putInt(key, value).apply();
    }


    /**
     * Stores a boolean value on sharedPreferences
     * @param key - Preference
     * @param value - Boolean value to store
     */
    public void saveBool( String key, Boolean value) {
        
    	mSettings.edit().putBoolean(key, value).apply();
    }


    /**
     * Returns a string value from sharedPreferences
     * @param key - Preference
     */
    public String getString( String key ) {
        return mSettings.getString(key, defaultString( key ));
    }


    /**
     * Returns an integer value from sharedPreferences
     * @param key - Preference
     */
    public int getInt( String key ) {
        return mSettings.getInt(key, defaultInt( key ));
    }


    /**
     * Returns a boolean value from sharedPreferences
     * @param key - Preference
     */
    public Boolean getBool( String key ) {
        return mSettings.getBoolean(key, defaultBoolean( key ));
    }


    /**
     * Is the key in our sharedPreferences?
     * @param key - Preference
     */
    public Boolean contains(String key) {
        return mSettings.contains(key);
    }

    /**
     * Returns a XML string value from sharedPreferences
     * @param key - Preference
     */
    public String getStringXML( String key ) {
        return "<" + key + ">" +
                mSettings.getString(key, defaultString( key )) +
                "</" + key + ">";
    }


    /**
     * Returns a XML integer value from sharedPreferences
     * @param key - Preference
     */
    public String getIntXML( String key ) {
        return "<" + key + ">" +
                String.valueOf( mSettings.getInt(key, defaultInt( key )) ) +
                "</" + key + ">";
    }


    /**
     * Returns a XML boolean value from sharedPreferences
     * @param key - Preference
     */
    public String getBoolXML( String key ) {
        return "<" + key + ">" +
                String.valueOf( mSettings.getBoolean(key, defaultBoolean(key)) ) +
                "</" + key + ">";
    }


    /**
     * Removes a value from sharedPreferences
     * @param key - Preference
     */
	public void removeValue( String key ) {
 
    	mSettings.edit().remove(key).apply();
    }    

    
	/* ***************** */
	/* PRIVATE FUNCTIONS */
	/* ***************** */
	@SuppressWarnings({"SameReturnValue", "UnusedParameters"})
    private String defaultString( String key ){

		return "";
	}

	private int defaultInt( String key ){
        if (key.equals( PREFS_MODE_KEY ) ){
            return MassageRecord.MODE_SIMPLE;
        }

        if (key.equals( PREFS_DINGTIME_KEY ) ){
            return mRes.getInteger( R.integer.preferences_dingMinutes_default );
        }

        if (key.equals( PREFS_DINGTIMESOUND_KEY ) ){
            return DEFAULT_DINGTIMESOUND_KEY;
        }

        if (key.equals( PREFS_HALFMASSAGEDURATIONSOUND_KEY ) ){
            return DEFAULT_HALFMASSAGEDURATIONSOUND_KEY;
        }

        if (key.equals( PREFS_FULLMASSAGEDURATIONSOUND_KEY ) ){
            return DEFAULT_FULLMASSAGEDURATIONSOUND_KEY;
        }

        if (key.equals( PREFS_MINIMALZONEDURATION_KEY ) ) {
            return mRes.getInteger( R.integer.preferences_minimalZoneDuration_default );
        }
        if (key.equals( PREFS_MASSAGEDURATION_KEY ) ) {
            return mRes.getInteger( R.integer.preferences_massageDuration_default );
        }


        return 0;
	}

	private Boolean defaultBoolean( String key ){
        //noinspection SimplifiableIfStatement

        if (key.equals( PREFS_DISABLESCREENLOCK_KEY ) ){
            return mRes.getBoolean( R.bool.preferences_disableScreenLock_default );
        }

        if (key.equals( PREFS_VISUALWARNINGSENABLED_KEY )
                || key.equals( PREFS_VISUALWARNINGENABLED_KEY ) ){
            return mRes.getBoolean(
                    R.bool.preferences_visualWarningsEnabled_default );
        }

        if (key.equals( PREFS_VIBRATIONWARNINGSENABLED_KEY )
                || key.equals( PREFS_VIBRATIONWARNINGENABLED_KEY )){
            mVibrator =
                    (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
            return mVibrator.hasVibrator();
        }
        if (key.equals( PREFS_SOUNDWARNINGSENABLED_KEY )
                || key.equals( PREFS_SOUNDENABLED_KEY ) ){
            return mRes.getBoolean( R.bool.preferences_soundWarningsEnabled_default );
        }

        if (key.equals( PREFS_DINGTIMEWARNINGSENABLED_KEY )
                || key.equals( PREFS_DINGTIMESOUNDENABLED_KEY )){
            return mRes.getBoolean(
                    R.bool.preferences_dingTimeWarningsEnabled_default );
        }

        if (key.equals( PREFS_HALFMASSAGEDURATIONWARNINGENABLED_KEY )
                || key.equals( PREFS_HALFMASSAGEDURATIONSOUNDENABLED_KEY )){
            return mRes.getBoolean(
                    R.bool.preferences_halfMassageDurationWarningEnabled_default );
        }

        if (key.equals( PREFS_FULLMASSAGEDURATIONWARNINGENABLED_KEY )
                || key.equals( PREFS_FULLMASSAGEDURATIONSOUNDENABLED_KEY )){
            return mRes.getBoolean(
                    R.bool.preferences_fullMassageDurationWarningEnabled_default );
        }

        return false;
	}

}