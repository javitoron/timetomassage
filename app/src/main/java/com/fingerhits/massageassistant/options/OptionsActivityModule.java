package com.fingerhits.massageassistant.options;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Javier Torón on 24/02/2017.
 */

@Module
public class OptionsActivityModule {
    public final IOptionsView view;

    public OptionsActivityModule(IOptionsView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    IOptionsView provideIOptionsView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    IOptionsInteractor provideIOptionsInteractor(OptionsInteractor interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    IOptionsPresenter provideIOptionsPresenter(OptionsPresenter presenter) {
        return presenter;
    }

}
