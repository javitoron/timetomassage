package com.fingerhits.massageassistant.options;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.fingerhits.massageassistant.utils.DateTimeUtils;
import com.fingerhits.massageassistant.utils.GraphicUtils;
import com.fingerhits.massageassistant.utils.MiscUtils;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Gives the user a way to change app preferences
 * @author Javier Torón
 * 
 * Uses MySharedPreferences to get and save the values
 *
 * Values:
 * * MassageDuration (int): Duration of massage session. Ding in half and
 *      complete
 *
 * * DisableScreenLock (bool): If true screen will be always on (false to allow screen
 * 		lock)
 *
 * * DingTimeWarningsEnabled (bool): DingTime Warnings on/off
 * * DingTime (int): Minutes passing between warnings (0 for no warnings)
 * * HalfDurationWarningsEnabled (bool): HalfDuration Warnings on/off
 * * FullDurationWarningsEnabled (bool): FullDuration Warnings on/off
 *
 * * VisualWarningEnabled (bool): Visual warnings on/off
 * * VibrationWarningEnabled (bool): Vibration warnings on/off
 * * SoundWarningEnabled (bool): Warning sounds on/off
 *
 * * DingTimeSound (int): Raw sound resource selected to be played as ding sound
 * * HalfDurationSound (int): Raw sound resource selected to be played as half
 * *    massage warning
 * * FullDurationSound (int): Raw sound resource selected to be played as full
 * *    massage warning
 *
 * * MinimalZoneDuration (int): If a zone manipulation lasts less seconds than
 *      this value will be discarded (0 for no discards)
 *
 *
*/
public class OptionsActivity extends FragmentActivity
        implements IOptionsView {

    @BindView(R.id.massageDurationLabel) TextView mMassageDurationLabel;
    @BindView(R.id.massageDurationSeekBar) SeekBar mMassageDurationSeekBar;

    @BindView(R.id.disableScreenLockLabel) TextView mDisableScreenLockLabel;
    @BindView(R.id.disableScreenLockCheck) CheckBox mDisableScreenLockCheck;

    @BindView(R.id.dingTimeWarningsCheck) CheckBox mDingTimeWarningsCheck;
    @BindView(R.id.dingTimeWarningsText) TextView mDingTimeWarningsText;
    @BindView(R.id.dingTimeSeekBar) SeekBar mDingTimeSeekBar;

    @BindView(R.id.halfDurationWarningText) TextView mHalfDurationWarningText;
    @BindView(R.id.halfDurationWarningCheck) CheckBox mHalfDurationWarningCheck;
    @BindView(R.id.fullDurationWarningText) TextView mFullDurationWarningText;
    @BindView(R.id.fullDurationWarningCheck) CheckBox mFullDurationWarningCheck;

    @BindView(R.id.visualWarningsLabel) TextView mVisualWarningsLabel;
    @BindView(R.id.visualWarningsCheck) CheckBox mVisualWarningsCheck;
    @BindView(R.id.vibrationWarningsLabel) TextView mVibrationWarningsLabel;
    @BindView(R.id.vibrationWarningsCheck) CheckBox mVibrationWarningsCheck;
    @BindView(R.id.soundWarningsLabel) TextView mSoundWarningsLabel;
    @BindView(R.id.soundWarningsCheck) CheckBox mSoundWarningsCheck;

    @BindView(R.id.soundWarningsLayout) RelativeLayout mSoundWarningsLayout;
    @BindView(R.id.dingTimeSoundSelectSpinner)
    Spinner mDingTimeSoundSelectSpinner;
    @BindView(R.id.dingTimeSoundText) TextView mDingTimeSoundText;
    @BindView(R.id.dingTimeSoundLayout) RelativeLayout mDingTimeSoundLayout;
    @BindView(R.id.playDingTimeBtn) ImageButton mPlayDingTimeBtn;

    @BindView(R.id.halfDurationSoundText) TextView mHalfDurationSoundText;
    @BindView(R.id.halfDurationSoundSettingsLayout)
    RelativeLayout mHalfDurationSoundSettingsLayout;
    @BindView(R.id.halfDurationSoundSelectSpinner)
    Spinner mHalfDurationSoundSelectSpinner;
    @BindView(R.id.playHalfDurationBtn) ImageButton mPlayHalfDurationBtn;

    @BindView(R.id.fullDurationSoundText) TextView mFullDurationSoundText;
    @BindView(R.id.fullDurationSoundSettingsLayout)
    RelativeLayout mFullDurationSoundSettingsLayout;
    @BindView(R.id.fullDurationSoundSelectSpinner)
    Spinner mFullDurationSoundSelectSpinner;
    @BindView(R.id.playFullDurationBtn) ImageButton mPlayFullDurationBtn;

    @BindView(R.id.minimalZoneDurationLabel)
    TextView mMinimalZoneDurationLabel;
    @BindView(R.id.minimalZoneDurationSeekBar)
    SeekBar mMinimalZoneDurationSeekBar;

    @BindString(R.string.options_massageDurationLabel)
    String Options_massageDurationLabel;
    @BindString(R.string.options_noVibrator) String options_noVibrator;
    @BindString(R.string.soundOptions_halfDuration_title) String halfDuration_title;
    @BindString(R.string.options_halfDurationSound) String halfDurationSound;
    @BindString(R.string.soundOptions_fullDuration_title) String fullDuration_title;
    @BindString(R.string.options_fullDurationSound) String fullDurationSound;
    @BindString(R.string.options_noMinimalZoneDurationLabel) String noMinimalZoneDurationLabe;
    @BindString(R.string.app_version) String app_version;
    @BindString(R.string.app_name) String app_name;

    @Inject public IOptionsPresenter presenter;
    @Inject Context context;
    @Inject Resources mRes;

    int mDingTimeInterval;

    private ArrayAdapter<String> mSoundSelectAdapter;
    private String[] mSoundLabels;
    /**
     * Load options with values saved in preferences
     *
     * @param savedInstanceState Not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        ButterKnife.bind(this);

        MainApp.get(this)
                .getAppComponent()
                .plus(new OptionsActivityModule(this))
                .inject(this);

        presenter.initView();

        MiscUtils.prepareActionBar(this);
        MiscUtils.logActivityStarting(this, this.NAME);
    }

    /**
     * Saves the options and returns to the caller activity
     */
    @Override
    public void onBackPressed() {
        Intent intent = getIntent();

        this.setResult(RESULT_OK, intent);

        finish();
    }

    /**
     * Handle presses on the action bar items (home/back)
     *
     * @param item Action bar item pressed
     * @return true if action taken
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Enable/disable screen lock
     */
    @OnClick( R.id.disableScreenLockCheck)
    public void saveDisableScreenLock() {
        boolean isChecked = mDisableScreenLockCheck.isChecked();

        presenter.saveDisableScreenLock(isChecked);

        showHideLabel(mDisableScreenLockLabel, isChecked);
    }


    /**
     * Enable/disable ding time warnings
     */
    @OnClick( R.id.dingTimeWarningsCheck)
    public void saveDingTimeWarnings() {
        boolean isChecked = mDingTimeWarningsCheck.isChecked();

        presenter.saveDingTimeWarnings(isChecked);

        showHideLabel(mDingTimeWarningsText, isChecked);

        if(isChecked){
            mDingTimeSeekBar.setVisibility(View.VISIBLE);
                mDingTimeSoundText.setVisibility(View.VISIBLE);
        }
        else{
            mDingTimeSeekBar.setVisibility(View.GONE);
                mDingTimeSoundText.setVisibility(View.GONE);
                mDingTimeSoundLayout.setVisibility(View.GONE);
        }
    }


    /**
     * Enable/disable half duration warnings
     */
    @OnClick( R.id.halfDurationWarningCheck)
    public void saveHalfDurationWarning() {
        boolean isChecked = mHalfDurationWarningCheck.isChecked();

        presenter.saveHalfDurationWarning(isChecked);

        showHideLabel(mHalfDurationWarningText, isChecked);

        if (isChecked) {
            mHalfDurationSoundText.setVisibility(View.VISIBLE);
        } else {
            mHalfDurationSoundText.setVisibility(View.GONE);
        }
    }


    /**
     * Enable/disable full duration warnings
     */
    @OnClick( R.id.fullDurationWarningCheck)
    public void saveFullDurationWarning() {
        boolean isChecked = mFullDurationWarningCheck.isChecked();

        presenter.saveFullDurationWarning(isChecked);

        showHideLabel(mFullDurationWarningText, isChecked);

        if (isChecked) {
            mFullDurationSoundText.setVisibility(View.VISIBLE);
        } else {
            mFullDurationSoundText.setVisibility(View.GONE);
        }
    }

    /**
     * Enable/disable visual warnings
     */
    @OnClick( R.id.visualWarningsCheck)
    public void saveVisualWarnings() {
        boolean isChecked = mVisualWarningsCheck.isChecked();

        presenter.saveVisualWarnings(isChecked);

        showHideLabel(mVisualWarningsLabel, isChecked);
    }

    /**
     * Enable/disable vibration warnings
     */
    @OnClick( R.id.vibrationWarningsCheck)
    public void saveVibrationWarnings() {
        boolean isChecked = mVibrationWarningsCheck.isChecked();

        presenter.saveVibrationWarnings(isChecked);

        showHideLabel(mVibrationWarningsLabel, isChecked);
    }

    /**
     * Enable/disable sound warnings
     */
    @OnClick( R.id.soundWarningsCheck)
    public void saveSoundWarnings() {
        boolean isChecked = mSoundWarningsCheck.isChecked();

        presenter.saveSoundWarnings(isChecked);

        showHideLabel(mSoundWarningsLabel, isChecked);

        if(isChecked) {
            mSoundWarningsLayout.setVisibility(View.VISIBLE);
            presenter.writeDingTimesAndDurationTexts();
        }
        else{
            mSoundWarningsLayout.setVisibility(View.GONE);
        }
    }


    /**
     * Toggles Ding Time Settings visibility
     */
    @OnClick( R.id.dingTimeSoundText)
    public void showHideDingTimeSoundSettings() {
        if(mDingTimeSoundLayout.getVisibility() == View.VISIBLE ){
            mDingTimeSoundLayout.setVisibility(View.GONE);
        }
        else{
            mDingTimeSoundLayout.setVisibility(View.VISIBLE);
        }

    }

    /**
     * Toggles Half duration Settings visibility
     */
    @OnClick( R.id.halfDurationSoundText)
    public void showHideHalfDurationSoundSettings() {
        if(mHalfDurationSoundSettingsLayout.getVisibility() == View.VISIBLE ){
            mHalfDurationSoundSettingsLayout.setVisibility(View.GONE);
        }
        else{
            mHalfDurationSoundSettingsLayout.setVisibility(View.VISIBLE);
        }

    }


    /**
     * Toggles Full duration Settings visibility
     */
    @OnClick( R.id.fullDurationSoundText)
    public void showHideFullDurationSoundSettings() {
        if(mFullDurationSoundSettingsLayout.getVisibility() == View.VISIBLE ){
            mFullDurationSoundSettingsLayout.setVisibility(View.GONE);
        }
        else{
            mFullDurationSoundSettingsLayout.setVisibility(View.VISIBLE);
        }

    }

    /**
     * Plays warning sound (ding time)
     */
    @OnClick( R.id.playDingTimeBtn)
    public void playDingTime() { presenter.playDingTime(); }

    /**
     * Plays half duration sound
     */
    @OnClick( R.id.playHalfDurationBtn)
    public void playHalfDuration() {
        presenter.playHalfMassage();
    }

    /**
     * Plays full duration sound
     */
    @OnClick( R.id.playFullDurationBtn)
    public void playFullDuration() {
        presenter.playFullMassage();
    }


    public void prepareSoundSelectAdapter(String[] soundLabels) {
        mSoundLabels = soundLabels;
        mSoundSelectAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, mSoundLabels);
        mSoundSelectAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
    }


    public void prepareMassageDurationOptions(int duration) {
        mMassageDurationSeekBar.setMax(23);
        mMassageDurationSeekBar.setProgress(( duration / 5 ) - 1);

        writeMassageDurationLabel(duration);

        mMassageDurationSeekBar.setOnSeekBarChangeListener(
                new OnSeekBarChangeListener() {
                    int progress = 0;

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progressValue,
                                                  boolean fromUser) {
                        progress = ( progressValue + 1 ) * 5;
                        presenter.saveMassageDuration(progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                }
        );
    }


    public void writeMassageDurationLabel(int duration) {
        String s = Options_massageDurationLabel;

        s = String.format(s, duration);

        mMassageDurationLabel.setText(s);
    }


    public void prepareDisableScreenLockOptions(boolean isChecked) {
        mDisableScreenLockCheck.setChecked(isChecked);

        showHideLabel(mDisableScreenLockLabel, isChecked);
    }


    public void prepareWarningsOptions(int interval,
                                       boolean dingTimeWarningsEnabled,
                                       boolean halfMassageDurationWarningEnabled,
                                       boolean fullMassageDurationWarningEnabled) {

        mDingTimeInterval = interval;
        prepareDingTimeWarningsOptions(mDingTimeInterval, dingTimeWarningsEnabled);
        prepareHalfDurationWarningOptions(halfMassageDurationWarningEnabled);
        prepareFullDurationWarningOptions(fullMassageDurationWarningEnabled);
    }

    public void writeDingTimes(String dingTimeLabel, int minutes) {
        writeDingTimeWarningText(minutes);
        writeDingTimeSoundText(dingTimeLabel, minutes);
    }


    public void prepareWarningTypesOptions(int duration, String dingTimeLabel,
                                           boolean visualWarningsEnabled,
                                           boolean vibrationWarningsEnabled,
                                           boolean soundWarningsEnabled,
                                           int dingTimeSoundIndex,
                                           int halfMassageSoundIndex,
                                           int fullMassageSoundIndex) {

        prepareVisualWarningsOptions(visualWarningsEnabled);
        prepareVibrationWarningsOptions(vibrationWarningsEnabled);
        prepareSoundWarningsOptions(duration, dingTimeLabel,
                soundWarningsEnabled, dingTimeSoundIndex, halfMassageSoundIndex,
                fullMassageSoundIndex);
    }


    public void writeDurationTexts(String halfMassageLabel,
                                   String fullMassageLabel, int duration  ) {

        writeHalfDurationTexts(halfMassageLabel, duration);
        writeFullDurationTexts(fullMassageLabel, duration);
    }


    public void prepareMinimalZoneDurationOptions(int minimalZoneDuration) {
        mMinimalZoneDurationSeekBar.setMax(60);
        mMinimalZoneDurationSeekBar.setProgress(minimalZoneDuration/5);
        writeMinimalZoneDurationLabel(minimalZoneDuration);

        mMinimalZoneDurationSeekBar.setOnSeekBarChangeListener(
                new

                        OnSeekBarChangeListener() {
                            int progress = 0;

                            @Override
                            public void onProgressChanged(SeekBar seekBar,
                                                          int progressValue,
                                                          boolean fromUser) {
                                progress = progressValue * 5;
                                presenter.saveMinimalZoneDuration(progress);

                                writeMinimalZoneDurationLabel(progress);
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {
                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {
                            }

                        }

        );

    }


    public void showVersion() {
        String version;

        try {
            PackageInfo pInfo =
                    getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version = "Error getting version";
        }

        TextView mVersionText = (TextView) findViewById(R.id.version);
        String s = app_version;
        s = String.format(s, app_name, "v" + version);
        mVersionText.setText(s);

    }


    private void prepareDingTimeWarningsOptions(int interval, boolean isChecked) {
        prepareDingTimeSeekbar(interval);

        mDingTimeWarningsCheck.setChecked(isChecked);

        showHideLabel(mDingTimeWarningsText, isChecked);
        if(isChecked){
            mDingTimeSeekBar.setVisibility(View.VISIBLE);
        }
        else{
            mDingTimeSeekBar.setVisibility(View.GONE);
        }
    }


    private void prepareDingTimeSeekbar(int interval) {
        //SeekBar
        mDingTimeSeekBar.setMax(29);
        mDingTimeSeekBar.setProgress(interval - 1);

        mDingTimeSeekBar.setOnSeekBarChangeListener(
                new OnSeekBarChangeListener() {
                    int progress = 0;

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progressValue,
                                                  boolean fromUser) {
                        progress = progressValue + 1;

                        presenter.saveDingTime(progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                }
        );

    }


    private void writeDingTimeWarningText(int minutes) {
        String s_dingTimeWarning =
                mRes.getQuantityString(
                        R.plurals.options_dingTimeWarning,
                        minutes);
        s_dingTimeWarning = String.format(s_dingTimeWarning, minutes);
        mDingTimeWarningsText.setText(s_dingTimeWarning);
        mDingTimeWarningsText.setVisibility(View.VISIBLE);
    }


    private void prepareHalfDurationWarningOptions(boolean isChecked) {

        mHalfDurationWarningCheck =
                (CheckBox) findViewById(R.id.halfDurationWarningCheck);

        mHalfDurationWarningText =
                (TextView) findViewById(R.id.halfDurationWarningText);

        mHalfDurationWarningCheck.setChecked(isChecked);

        showHideLabel(mHalfDurationWarningText, isChecked);
    }

    private void prepareFullDurationWarningOptions(boolean isChecked) {

        mFullDurationWarningCheck =
                (CheckBox) findViewById(R.id.fullDurationWarningCheck);

        mFullDurationWarningText =
                (TextView) findViewById(R.id.fullDurationWarningText);

        mFullDurationWarningCheck.setChecked(isChecked);

        showHideLabel(mFullDurationWarningText, isChecked);
    }


    private void prepareVisualWarningsOptions(boolean isChecked) {
        mVisualWarningsCheck.setChecked(isChecked);

        showHideLabel(mVisualWarningsLabel, isChecked);
    }


    private void prepareVibrationWarningsOptions(boolean isChecked) {
        Vibrator mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if(mVibrator.hasVibrator()) {
            mVibrationWarningsCheck.setChecked(isChecked);

            showHideLabel(mVibrationWarningsLabel, isChecked);
        }
        else{
            mVibrationWarningsCheck.setChecked(false);
            mVibrationWarningsCheck.setEnabled(false);
            mVibrationWarningsLabel.setEnabled(false);
            mVibrationWarningsLabel.setText(options_noVibrator);
            presenter.saveVibrationWarnings(false);

        }
    }


    private void prepareSoundWarningsOptions(int duration, String dingTimeLabel,
                                             boolean isChecked,
                                             int dingTimeSoundIndex,
                                             int halfMassageSoundIndex,
                                             int fullMassageSoundIndex) {
        prepareDingTimeSoundOptions(dingTimeLabel, mDingTimeInterval,
                dingTimeSoundIndex);

        prepareHalfDurationSoundOptions(duration, halfMassageSoundIndex);

        prepareFullDurationSoundOptions(duration, fullMassageSoundIndex);

        if(isChecked) {
            mSoundWarningsCheck.setChecked(true);
            mSoundWarningsLayout.setVisibility(View.VISIBLE);
        }
        else{
            mSoundWarningsCheck.setChecked(false);
            presenter.saveSoundWarnings(mSoundWarningsCheck.isChecked());
            mSoundWarningsLayout.setVisibility(View.GONE);
        }
    }

    private void prepareDingTimeSoundOptions(String dingTimeLabel, int interval,
                                             int dingTimeSoundIndex) {

        writeDingTimeSoundText(dingTimeLabel, interval);

        if(!mDingTimeWarningsCheck.isChecked()) {
            mDingTimeSoundText.setVisibility(View.GONE);
        }

        prepareDingTimeSoundSettings(dingTimeSoundIndex);
    }


    private void prepareDingTimeSoundSettings(int dingTimeSoundIndex) {
        prepareSoundSelectSpinner(mDingTimeSoundSelectSpinner,
                dingTimeSoundIndex);

        mDingTimeSoundSelectSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {

                        presenter.saveDingTimeSound(position);

                        sendSoundSelectedStatistics(
                                MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY,
                                mSoundLabels[position]);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });

    }


    private void prepareHalfDurationSoundOptions(int duration,
                                                 int halfMassageSoundIndex) {
        if(!mHalfDurationWarningCheck.isChecked()) {
            mHalfDurationSoundText.setVisibility(View.GONE);
        }

        prepareHalfMassageDurationSoundSettings(duration, halfMassageSoundIndex);
    }


    private void prepareHalfMassageDurationSoundSettings(int duration,
                                                   int halfMassageSoundIndex) {
        prepareSoundSelectSpinner(mHalfDurationSoundSelectSpinner,
                halfMassageSoundIndex);

        mHalfDurationSoundSelectSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {

                        presenter.saveHalfMassageSound(position);

                        sendSoundSelectedStatistics(
                                MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY,
                                mSoundLabels[position]);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });

        enableHalfMassageDurationSoundSelector(duration);

    }


    private void enableHalfMassageDurationSoundSelector( int duration ){
        mHalfDurationSoundSelectSpinner.setEnabled( duration != 0 );
        mPlayHalfDurationBtn.setEnabled(duration != 0);
    }



    private void prepareFullDurationSoundOptions(int duration,
                                                 int fullMassageSoundIndex) {

        if(!mFullDurationWarningCheck.isChecked()) {
            mFullDurationSoundText.setVisibility(View.GONE);
        }

        prepareFullMassageDurationSoundSettings(duration, fullMassageSoundIndex);
    }


    private void prepareFullMassageDurationSoundSettings(int duration,
                                                         int fullMassageSoundIndex) {
        prepareSoundSelectSpinner(mFullDurationSoundSelectSpinner,
            fullMassageSoundIndex);

        mFullDurationSoundSelectSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {

                        presenter.saveFullMassageSound(position);

                        sendSoundSelectedStatistics(
                                MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY,
                                mSoundLabels[position]);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });

        enableFullMassageDurationSoundSelector(duration);

    }


    private void enableFullMassageDurationSoundSelector( int duration ){
        mFullDurationSoundSelectSpinner.setEnabled( duration != 0 );
        mPlayFullDurationBtn.setEnabled(duration != 0);
    }


    private void writeDingTimeSoundText(String dingTimeLabel, int minutes) {
        String s_dingTimeSound =
                mRes.getQuantityString(
                        R.plurals.options_dingTimeSound,
                        minutes);
        s_dingTimeSound = String.format(s_dingTimeSound, dingTimeLabel, minutes);
        mDingTimeSoundText.setText(s_dingTimeSound);
    }


    private void writeHalfDurationTexts(String halfMassageLabel, int duration ) {
        String s_halfDuration =
                DateTimeUtils.durationToString(duration * 30000);
        String s_halfDurationWarning = String.format(halfDuration_title,
                s_halfDuration);
        mHalfDurationWarningText.setText(s_halfDurationWarning);

        String s_halfDurationSound = String.format(halfDurationSound,
                halfMassageLabel,s_halfDuration);
        mHalfDurationSoundText.setText(s_halfDurationSound);
    }


    private void writeFullDurationTexts(String fullMassageLabel, int duration ) {
        String s_fullDuration =
                DateTimeUtils.durationToString(duration * 60000);

        String s_fullDurationWarning = String.format(fullDuration_title,
                s_fullDuration);
        mFullDurationWarningText.setText(s_fullDurationWarning);

        String s_fullDurationSound = String.format(fullDurationSound,
                fullMassageLabel, s_fullDuration);
        mFullDurationSoundText.setText(s_fullDurationSound);
    }


    private void writeMinimalZoneDurationLabel(int duration) {
        String s;
        if(duration == 0){
            s = noMinimalZoneDurationLabe;
        }
        else {
            s = mRes.getQuantityString(
                    R.plurals.options_minimalZoneDurationLabel,
                    duration);

            s = String.format(s, duration);
        }

        mMinimalZoneDurationLabel.setText(s);
    }


    private void showHideLabel(TextView label, boolean enable ){
        if(enable){
            GraphicUtils.defineTextStyle(context, label,
                    R.style.settings_main_labels_enabled);
        }
        else{
            GraphicUtils.defineTextStyle(context, label,
                    R.style.settings_main_labels_disabled);
        }
    }


    private void prepareSoundSelectSpinner( Spinner theSpinner,
                                            int soundIndex ){
        //Spinner
        theSpinner.setAdapter(mSoundSelectAdapter);

        theSpinner.setSelection(soundIndex);
    }


    private void sendSoundSelectedStatistics(String sound_key,
                                             String sound_label) {
        if (BuildConfig.DEBUG) {
            Log.d(NAME,
                    "SoundSelect -> " + sound_key + ": " + sound_label);
        }
        MiscUtils.sendEventTracker(this, NAME, NAME, "SoundSelect",
                sound_key + ": " + sound_label, -1L);

    }

}