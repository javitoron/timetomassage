package com.fingerhits.massageassistant.options;

import com.fingerhits.massageassistant.massageSounds.MassageSounds;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 24/02/2017.
 */

public class OptionsPresenter implements IOptionsPresenter{
    @Inject MassageSounds mSounds;

    private IOptionsView view;
    private IOptionsInteractor interactor;

    @Inject
    public OptionsPresenter(IOptionsView optionsView,
                            IOptionsInteractor optionsInteractor){
        this.view = optionsView;
        this.interactor = optionsInteractor;
    }


    public void initView() {
        view.prepareSoundSelectAdapter(mSounds.soundLabels);
        view.prepareMassageDurationOptions(interactor.massageDuration());

        view.prepareDisableScreenLockOptions(interactor.disableScreenLock());

        view.prepareWarningsOptions(interactor.dingTime(),
                interactor.dingTimeWarningsEnabled(),
                interactor.halfMassageDurationWarningEnabled(),
                interactor.fullMassageDurationWarningEnabled());
        view.prepareWarningTypesOptions(interactor.massageDuration(),
                dingTimeLabel(),
                interactor.visualWarningsEnabled(),
                interactor.vibrationWarningsEnabled(),
                interactor.soundWarningsEnabled(),
                getSoundIndex(MySharedPreferences.PREFS_DINGTIMESOUND_KEY),
                getSoundIndex(
                        MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY),
                getSoundIndex(
                        MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY));

        writeDingTimesAndDurationTexts();

        view.prepareMinimalZoneDurationOptions(interactor.minimalZoneDuration());

        view.showVersion();
    }


    public void writeDingTimesAndDurationTexts() {
        view.writeDingTimes(dingTimeLabel(), interactor.dingTime());
        view.writeDurationTexts(halfMassageLabel(), fullMassageLabel(),
                interactor.massageDuration());
    }

    /* SAVE PREFERENCES */
    public void saveMassageDuration(int progress) {
        interactor.saveMassageDuration(progress);
        view.writeMassageDurationLabel(interactor.massageDuration());
        view.writeDurationTexts(halfMassageLabel(), fullMassageLabel(),
                interactor.massageDuration());
    }

    public void saveMinimalZoneDuration(int progress) {
        interactor.saveMinimalZoneDuration(progress);
    }

    public void saveDisableScreenLock(boolean isChecked) {
        interactor.saveDisableScreenLock(isChecked);
    }

    public void saveDingTimeWarnings(boolean isChecked) {
        interactor.saveDingTimeWarnings(
                isChecked);
    }

    public void saveDingTime(int progress) {
        interactor.saveDingTime(progress);
        view.writeDingTimes(dingTimeLabel(), interactor.dingTime());
    }

    public void saveHalfDurationWarning(boolean isChecked) {
        interactor.saveHalfDurationWarning(
                isChecked);
    }

    public void saveFullDurationWarning(boolean isChecked) {
        interactor.saveFullDurationWarning(
                isChecked);
    }

    public void saveVisualWarnings(boolean isChecked) {
        interactor.saveVisualWarnings(isChecked);
    }

    public void saveVibrationWarnings(boolean isChecked) {
        interactor.saveVibrationWarnings(isChecked);
    }

    public void saveSoundWarnings(boolean isChecked) {
        interactor.saveSoundWarnings(isChecked);
    }

    public void saveDingTimeSound(int position) {
        interactor.saveDingTimeSound( mSounds.soundIds.getResourceId(
                position, interactor.dingTimeSound()));
        mSounds.reloadDingTime();
        view.writeDingTimes(dingTimeLabel(), interactor.dingTime());
    }

    public void saveDingTimeSoundById(int resourceId) {
        interactor.saveDingTimeSound( resourceId);
    }


    public void saveHalfMassageSound(int position) {
        interactor.saveHalfMassageSound( mSounds.soundIds.getResourceId(
                position, interactor.halfMassageSound()));
        mSounds.reloadHalfMassage();
        view.writeDurationTexts(halfMassageLabel(), fullMassageLabel(),
                interactor.massageDuration());
    }

    public void saveHalfMassageSoundById(int resourceId) {
        interactor.saveHalfMassageSound(resourceId);
    }

    public void saveFullMassageSound(int position){
        interactor.saveFullMassageSound(mSounds.soundIds.getResourceId(
                position, interactor.fullMassageSound()));
        mSounds.reloadFullMassage();
        view.writeDurationTexts(halfMassageLabel(), fullMassageLabel(),
                interactor.massageDuration());
    }

    public void saveFullMassageSoundById(int resourceId){
        interactor.saveFullMassageSound(resourceId);
    }

    /* SOUNDS */

    public String dingTimeLabel() {
        return mSounds.dingTimeLabel();
    }

    public String halfMassageLabel() {
        return mSounds.halfMassageLabel();
    }

    public String fullMassageLabel() {
        return mSounds.fullMassageLabel();
    }


    /**
     * Plays warning sound (ding time)
     */
    public void playDingTime() { mSounds.playDingTime();}

    /**
     * Plays half duration sound
     */
    public void playHalfMassage() {
        mSounds.playHalfMassage();
    }

    /**
     * Plays full duration sound
     */
    public void playFullMassage() {
        mSounds.playFullMassage();
    }

    public int getSoundIndex(String prefSoundKey) {
        return interactor.getSoundIndex(prefSoundKey, mSounds.soundIds);
    }

}
