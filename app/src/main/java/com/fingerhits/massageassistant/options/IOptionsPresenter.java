package com.fingerhits.massageassistant.options;

/**
 * Created by Javier Torón on 24/02/2017.
 */

public interface IOptionsPresenter {
    public void initView();

    public void writeDingTimesAndDurationTexts();

    public void saveMassageDuration(int progress);
    public void saveMinimalZoneDuration(int progress);
    public void saveDisableScreenLock(boolean isChecked);
    public void saveDingTimeWarnings(boolean isChecked);
    public void saveDingTime(int progress);
    public void saveHalfDurationWarning(boolean isChecked);
    public void saveFullDurationWarning(boolean isChecked);
    public void saveVisualWarnings(boolean isChecked);
    public void saveVibrationWarnings(boolean isChecked);
    public void saveSoundWarnings(boolean isChecked);
    public void saveDingTimeSound(int position);
    public void saveDingTimeSoundById(int resourceId);
    public void saveHalfMassageSound(int position);
    public void saveHalfMassageSoundById(int resourceId);
    public void saveFullMassageSound(int position);
    public void saveFullMassageSoundById(int resourceId);

    public void playDingTime();
    public void playHalfMassage();
    public void playFullMassage();
}
