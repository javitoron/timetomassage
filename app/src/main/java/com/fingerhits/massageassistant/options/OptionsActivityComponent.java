package com.fingerhits.massageassistant.options;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Subcomponent;

/**
 * Created by Javier Torón on 24/02/2017.
 */

@ActivityScope
@Subcomponent(modules = OptionsActivityModule.class)
public interface OptionsActivityComponent {
    void inject(OptionsActivity activity);
}
