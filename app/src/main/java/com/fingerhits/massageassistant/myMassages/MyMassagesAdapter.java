package com.fingerhits.massageassistant.myMassages;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;

import java.text.SimpleDateFormat;

import static com.fingerhits.massageassistant.utils.DateTimeUtils.durationToString;

/**
 * Adapter MassageRecords <--> MyMassages
 *
 * @author javi on 01/03/2015.
 */
public class MyMassagesAdapter extends CursorAdapter {
    //private ArrayList<MassageRecord> massages = new ArrayList<>();

    private final Resources mRes;

    /**
     * MyMassagesAdapter constructor
     * @param context To pass to superclass and get resources
     * @param cursor To show in a list
     */
    public MyMassagesAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);

        mRes = context.getResources();
    }

    /**
     * How is showed a massageRecord in MyMassages
     *
     * @param view Layout of a record
     * @param context To get resources
     * @param cursor To show current record
     */
    @SuppressLint("SetTextI18n")
    public void bindView(View view, Context context, Cursor cursor) {

        MassageRecord massage = new MassageRecord( cursor );

        Long massageId = massage.getId();


        Button savePatientButton = (Button)
                view.findViewById(R.id.savePatientButton);

        if( savePatientButton != null ) {
            savePatientButton.setTag(massageId);

            EditText patientText = (EditText)
                    view.findViewById(R.id.patientText);
            patientText.setText(massage.getPatient());

            patientText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    Button savePatientButton = (Button) (
                            ((ViewGroup) view.getParent()).findViewById(
                                    R.id.savePatientButton));
                    if (hasFocus) {
                        view.setBackgroundColor(
                                GraphicUtils.getColor(
                                        mRes, R.color.lightGreyBackground));

                        savePatientButton.setVisibility(View.VISIBLE);
                    } else {
                        view.setBackgroundColor(Color.TRANSPARENT);

                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                            savePatientButton.setVisibility(View.INVISIBLE);
                        }
                        else{
                            savePatientButton.setVisibility(View.GONE);
                        }
                    }
                }
            });


   //         TextView modeText = (TextView) view.findViewById(R.id.modeText);
     //       modeText.setText(massage.getModeLabel(mRes));

            TextView startDateText = (TextView)
                    view.findViewById(R.id.startDateText);

            startDateText.setText(new SimpleDateFormat("dd/MM/yyyy",
                    java.util.Locale.getDefault()).format(
                    massage.getStartDateTime()));

            TextView startEndTimeText = (TextView)
                    view.findViewById(R.id.startEndTimeText);
            startEndTimeText.setText(massage.getStartToEndString(mRes));

        /*Integer.toString( massageRecord.getId() ) + ". "*/

            TextView durationText = (TextView)
                    view.findViewById(R.id.durationText);
            durationText.setText(durationToString(
                    massage.getDurationInMilliseconds()));


            Button saveCommentsButton = (Button)
                    view.findViewById(R.id.saveCommentsButton);
            saveCommentsButton.setTag(massageId);


            EditText commentsText = (EditText)
                    view.findViewById(R.id.commentsText);
            commentsText.setText(massage.getComments());

            commentsText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    Button saveCommentsButton = (Button) (
                            ((ViewGroup) view.getParent()).findViewById(
                                    R.id.saveCommentsButton));
                    if (hasFocus) {
                        view.setBackgroundColor(
                                GraphicUtils.getColor(mRes,
                                        R.color.lightGreyBackground));


                        saveCommentsButton.setVisibility(View.VISIBLE);
                    } else {
                        view.setBackgroundColor(Color.TRANSPARENT);
                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                            saveCommentsButton.setVisibility(View.INVISIBLE);
                        }
                        else {
                            saveCommentsButton.setVisibility(View.GONE);
                        }
                    }
                }
            });

            RelativeLayout mMassageRecordLayout = (RelativeLayout)
                    view.findViewById(R.id.massageRecordLayout);
            mMassageRecordLayout.setTag(massageId);

            ImageButton massagesViewButton = (ImageButton)
                    view.findViewById(R.id.massagesViewButton);
            if(massage.getMode() == MassageRecord.MODE_SIMPLE) {
                massagesViewButton.setVisibility(View.INVISIBLE);
            }
            else{
                massagesViewButton.setVisibility(View.VISIBLE);
                massagesViewButton.setTag(massageId);

            }

            ImageButton massagesDeleteButton = (ImageButton)
                    view.findViewById(R.id.massagesDeleteButton);
            massagesDeleteButton.setTag(massageId);

            ImageButton sendMassageSummaryButton = (ImageButton)
                    view.findViewById(R.id.massagesSendButton);
            if(massage.getMode() == MassageRecord.MODE_ADVANCED){
                sendMassageSummaryButton.setTag(massageId);
                sendMassageSummaryButton.setVisibility(View.VISIBLE);
            }
            else {
                sendMassageSummaryButton.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Generates the view to contain the row
     *
     * @param context Not used
     * @param cursor Not used
     * @param parent ViewGroup containing the record
     * @return view made from list_item_massage
     */
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return inflater.inflate(R.layout.list_item_massage, parent, false);
    }

    /****************************
     * PRIVATE METHODS
     /****************************/
}
