package com.fingerhits.massageassistant.myMassages;

import android.content.Context;
import android.database.Cursor;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 03/03/2017.
 */

public class MyMassagesInteractor implements IMyMassagesInteractor {
    @Inject MassageAssistantDBHelper mDatabaseHelper;
    @Inject Context mContext;
    @Inject MySharedPreferences mPrefs;

    @Inject
    public MyMassagesInteractor(MassageAssistantDBHelper databaseHelper,
                                Context context, MySharedPreferences prefs){
        this.mDatabaseHelper = databaseHelper;
        this.mContext = context;
        this.mPrefs = prefs;
   }


    public void deleteMassage(Long idMassage){
        mDatabaseHelper.deleteMassageRecord(idMassage);
    }


    public Cursor getAllMassageRecords_LastToFirst() {
        return mDatabaseHelper.getAllMassageRecords(
                MassageAssistantDBHelper.SORT_LAST_TO_FIRST);
    }

    public MassageRecord getMassage(Long idMassage){
        return mDatabaseHelper.getMassageRecord(idMassage);
    }

    public Cursor getMassageCursor(Long idMassage) {
        return mDatabaseHelper.getMassageCursor(idMassage);
    }


    public Cursor getTreatedZones(Long idMassage){
        return mDatabaseHelper.getTreatedZonesInMassage(idMassage);
    }


    public void savePatient(Long mIdMassage, String patient) {
        mDatabaseHelper.savePatientToMassageRecord(mIdMassage, patient);
    }

    public void saveComments(Long mIdMassage, String comments) {
        mDatabaseHelper.saveCommentsToMassageRecord(mIdMassage, comments);
    }

}
