package com.fingerhits.massageassistant.myMassages;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;

import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;

/**
 * MyMassagesLoader - Used by MyMassagesActivity
 *
 * @author javi on 01/06/2015.
 */
@SuppressWarnings("ALL")
public class MyMassagesLoader extends CursorLoader {
    //private static final String TAG = "MyMassagesLoader";
    private final MassageAssistantDBHelper mDatabaseHelper;

    /**
     * Opens DBHelper
     * @param context To open the DBHelper
     */
    public MyMassagesLoader(Context context) {
        super(context);
        mDatabaseHelper = new MassageAssistantDBHelper(context);
    }

    /**
     * Get all massages
     * @return All massages in a cursor
     */
    @Override
    public Cursor loadInBackground() {
        return mDatabaseHelper.getAllMassageRecords(
                MassageAssistantDBHelper.SORT_LAST_TO_FIRST);
    }
}
