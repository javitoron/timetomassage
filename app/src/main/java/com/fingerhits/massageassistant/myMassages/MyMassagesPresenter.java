package com.fingerhits.massageassistant.myMassages;

import android.database.Cursor;

import com.fingerhits.massageassistant.storedData.MassageRecord;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 03/03/2017.
 */

public class MyMassagesPresenter implements IMyMassagesPresenter {
    private IMyMassagesView view;
    private IMyMassagesInteractor interactor;

    @Inject
    public MyMassagesPresenter(IMyMassagesView view, IMyMassagesInteractor interactor){
        this.view = view;
        this.interactor = interactor;
    }

    public void initView(){}

    public void deleteMassage(Long idMassage) {
        MassageRecord massage = interactor.getMassage(idMassage);

        interactor.deleteMassage(idMassage);

        view.updateList(interactor.getAllMassageRecords_LastToFirst());

        view.sendDeleteTracker(massage.getModeAbbr());
    }

    public void savePatient(Long mIdMassage, String patient) {
        interactor.savePatient(mIdMassage, patient);
    }

    public void saveComments(Long mIdMassage, String comments) {
        interactor.saveComments(mIdMassage, comments);
    }


    public void updateMassageOrList(Long idMassage) {
        Cursor massageData = interactor.getMassageCursor(idMassage);

        if (massageData.moveToFirst()) {
            view.updateMassage(massageData);
        } else {
            view.updateList(interactor.getAllMassageRecords_LastToFirst());
        }
    }

    public void updateList() {
        view.updateList(interactor.getAllMassageRecords_LastToFirst());
    }


    public void sendMassageSummary(Long idMassage){
        MassageRecord massage = interactor.getMassage(idMassage);

        if( massage != null ) {
            view.sendEmail( massage, interactor.getTreatedZones(massage.getId()));
        }

    }
}
