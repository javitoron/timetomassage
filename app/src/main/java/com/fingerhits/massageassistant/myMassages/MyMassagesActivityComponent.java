package com.fingerhits.massageassistant.myMassages;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Subcomponent;
/**
 * Created by Javier Torón on 03/03/2017.
 */

@ActivityScope
@Subcomponent(modules = MyMassagesActivityModule.class)
public interface MyMassagesActivityComponent {
    void inject(MyMassagesActivity activity);
}
