package com.fingerhits.massageassistant.myMassages;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.massagesession.positions.Position;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.DateTimeUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Represents a massage session. Fields: id, idPatient, idMasseur,
 *      startDateTime, endDateTime, duration, type, objective, usedMaterials,
 *      comments
 *
 * @author javi on 01/03/2015.
 *
 */
public class MassageMailer {
    private MassageRecord mMassage;
    private Cursor mTreatedZones;
    private Context mContext;
    private Resources mRes;

    /**
     * MassageMailer Constructor
     */
    public MassageMailer() {

    }

    public void inject(MassageRecord massage, Cursor treatedZones, Context context, Resources res ){
        this.mMassage = massage;
        this.mTreatedZones = treatedZones;
        this.mContext = context;
        this.mRes = res;
    }


    public String subject(){
        String massageDateTimeString =
                DateFormat.getDateTimeInstance().format(
                        mMassage.getStartDateTime());

        return String.format(
                mRes.getString(R.string.sendEmail_subject),
                massageDateTimeString);

    }

    /**
     * Returns formatted string with treated zones corresponding to this
     * massage session
     *
     * @return - A string formatted ready for email
     */
    public String summary() {

        String startTime = new SimpleDateFormat("HH:mm",
                java.util.Locale.getDefault()).format(mMassage.getStartDateTime());

        String s_temp =
                mRes.getString( R.string.massageStartTime_label ).toUpperCase() +
                        startTime
                + System.getProperty( "line.separator" )
                + System.getProperty( "line.separator" );

        if(mMassage.getMode() != MassageRecord.MODE_SIMPLE) {

            String currentPosition = "";
            long positionDuration = 0L;

            TreatedZoneRecord treatedZone;

            if (mTreatedZones.moveToFirst()) {

                do {
                    treatedZone = new TreatedZoneRecord(mTreatedZones);

                    if (currentPosition.equals("")) {
                        s_temp += positionHeading(treatedZone.getCodPosition());

                        currentPosition = treatedZone.getCodPosition();
                    }

                    if (!currentPosition.equals(treatedZone.getCodPosition())) {

                        s_temp += positionSummary(positionDuration);

                        currentPosition = treatedZone.getCodPosition();

                        s_temp += positionHeading(currentPosition);

                        positionDuration = 0L;
                    }

                    positionDuration += treatedZone.getDurationInMilliseconds();

                    s_temp += treatedZone.summary(mContext);
                } while (mTreatedZones.moveToNext());

                s_temp += positionSummary(positionDuration);
            }
        }

        String endTime = new SimpleDateFormat("HH:mm",
                java.util.Locale.getDefault()).format(mMassage.getEndDateTime());

        s_temp += mRes.getString(R.string.massageEndTime_label) + endTime +
                System.getProperty( "line.separator" )
                + mRes.getString(R.string.massageTotalTime_label)
                + DateTimeUtils.durationToString(mMassage.getDurationInMilliseconds());

        if(!mMassage.getComments().equals("")) {
            s_temp += System.getProperty("line.separator")
                    + System.getProperty("line.separator")
                    + mMassage.getComments();
        }

        return s_temp;
    }



    // PRIVATE METHODS
    private String positionSummary( long positionDuration){
        return System.getProperty( "line.separator" )
                + mRes.getString(R.string.positionTotalTime_label)
                + DateTimeUtils.durationToString(positionDuration)
                + System.getProperty( "line.separator" )
                + "--------------------------"
                + System.getProperty( "line.separator" )
                + System.getProperty( "line.separator" );
    }


    private String positionHeading( String currentPosition){
        String s_temp = Position.codPositionToString(mContext,
                currentPosition);

        return s_temp.toUpperCase() + System.getProperty( "line.separator" );
    }

}
