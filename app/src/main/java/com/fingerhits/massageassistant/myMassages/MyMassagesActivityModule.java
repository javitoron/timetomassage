package com.fingerhits.massageassistant.myMassages;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Javier Torón on 03/03/2017.
 */

@Module
public class MyMassagesActivityModule {
    public final IMyMassagesView view;

    public MyMassagesActivityModule(IMyMassagesView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    IMyMassagesView provideIMyMassagesView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    IMyMassagesInteractor provideIMyMassagesInteractor(MyMassagesInteractor interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    IMyMassagesPresenter provideIMyMassagesPresenter(MyMassagesPresenter presenter) {
        return presenter;
    }
}
