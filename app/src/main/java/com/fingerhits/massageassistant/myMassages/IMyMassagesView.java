package com.fingerhits.massageassistant.myMassages;

import android.database.Cursor;

import com.fingerhits.massageassistant.storedData.MassageRecord;

/**
 * Created by Javier Torón on 03/03/2017.
 */

public interface IMyMassagesView {

    public static String NAME = "MyMassages";

    public void sendEmail(MassageRecord massage, Cursor treatedZones);

    public void updateMassage(Cursor massageData);
    public void updateList(Cursor massages);
    public void sendDeleteTracker(String label);

    }
