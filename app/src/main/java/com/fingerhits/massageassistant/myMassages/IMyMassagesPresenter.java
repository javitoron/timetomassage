package com.fingerhits.massageassistant.myMassages;

/**
 * Created by Javier Torón on 03/03/2017.
 */

public interface IMyMassagesPresenter {
    public void initView();

    public void deleteMassage(Long idMassage);
    public void updateList();

    public void savePatient(Long mIdMassage, String patient);
    public void saveComments(Long mIdMassage, String comments);

    public void updateMassageOrList(Long mIdMassageSelected);
    public void sendMassageSummary(Long idMassage);
}
