package com.fingerhits.massageassistant.myMassages;

import android.database.Cursor;

import com.fingerhits.massageassistant.storedData.MassageRecord;

/**
 * Created by Javier Torón on 03/03/2017.
 */

public interface IMyMassagesInteractor {
    public Cursor getAllMassageRecords_LastToFirst();
    public MassageRecord getMassage(Long idMassage);
    public Cursor getMassageCursor(Long idMassage);
    public Cursor getTreatedZones(Long idMassage);

    public void deleteMassage(Long mIdMassage);

    public void savePatient(Long mIdMassage, String patient);
    public void saveComments(Long mIdMassage, String comments);

    }
