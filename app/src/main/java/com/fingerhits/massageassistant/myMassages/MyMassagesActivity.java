package com.fingerhits.massageassistant.myMassages;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.fingerhits.massageassistant.options.OptionsActivity;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.treatedZones.TreatedZonesActivity;
import com.fingerhits.massageassistant.utils.MiscUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyMassagesActivity extends Activity implements IMyMassagesView,
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final int TREATED_ZONES_REQUEST_CODE = 1;

    public static final int MYMASSAGES_LOADER_ID = 1;

    @Inject Context mContext;
    @Inject Resources mRes;
    @Inject public IMyMassagesPresenter presenter;

    @BindView(R.id.myMassagesList) ListView mMyMassagesList;

    private long mIdMassageSelected = 0;
    private View mMassageSelected;

    private LoaderManager mLoaderManager;
    private LoaderManager.LoaderCallbacks<Cursor> mCallbacks;

    private MyMassagesAdapter mMyMassagesAdapter;

    /**
     * Set up activity
     *
     * @param savedInstanceState Not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_massages);

        ButterKnife.bind(this);

        MainApp.get(this)
                .getAppComponent()
                .plus(new MyMassagesActivityModule(this))
                .inject(this);

        presenter.initView();

        /* Bind massages list */
        mMyMassagesAdapter = new MyMassagesAdapter(this, null, 0);
        mMyMassagesList.setAdapter(mMyMassagesAdapter);

        mCallbacks = this;
        mLoaderManager = getLoaderManager();
        mLoaderManager.initLoader(MYMASSAGES_LOADER_ID, null, mCallbacks);

        MiscUtils.prepareActionBar(this);
        MiscUtils.logActivityStarting(this, this.NAME);
    }


    /**
     * Creates a new CursorLoader for the lists to be shown
     *
     * @param id   - List id
     * @param args - Not used
     * @return - MyMassagesLoader or TreatedZonesLoader
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        switch (id) {
            case MYMASSAGES_LOADER_ID:
                return new MyMassagesLoader(mContext);
        }

        return null;
    }

    /**
     * Associates queried cursor with the SimpleCursorAdapter
     *
     * @param loader - MyMassagesLoader or TreatedZonesLoader
     * @param cursor - Queried cursor
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case MYMASSAGES_LOADER_ID:
                // The asynchronous load is complete and the data
                // is now available for use. Only now can we associate
                // the queried Cursor with the SimpleCursorAdapter.
                mMyMassagesAdapter.swapCursor(cursor);
                break;

        }
        // The listview now displays the queried data.
    }


    /**
     * When the Loader's data is unavailable set to null cursor
     *
     * @param loader - MyMassagesLoader or TreatedZonesLoader
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // For whatever reason, the Loader's data is now unavailable.
        // Remove any references to the old data by replacing it with
        // a null Cursor.
        switch (loader.getId()) {
            case MYMASSAGES_LOADER_ID:
                mMyMassagesAdapter.swapCursor(null);
                break;
        }
    }


    /**
     * Manages return from the massage session activity
     *
     * @param requestCode To know what activity is coming from
     * @param resultCode  RESULT_OK if it's a controlled return
     * @param data        Data returned from the activity
     */
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {

/*        ContentResolver.requestSync(mAccount, SyncDBUtils.AUTHORITY,
                SyncDBUtils.getSyncBundle());
*/
        if (requestCode == TREATED_ZONES_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                presenter.updateMassageOrList(mIdMassageSelected);
            }
        }
    }


    /**
     * Makes menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_my_massages_actions, menu);
        return true;
    }


    /**
     * Manages menu options
     * <p>
     * * options_menu: Call OptionsActivity
     * * back: return to parent activity
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options_menu:
                Intent intent = new Intent(this, OptionsActivity.class);
                startActivity(intent);

                return true;

            case R.id.open_app_website_menu:
                MiscUtils.openURL(this, mRes.getString(R.string.app_url));

                return true;

            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Request sync before exit
     */
    @Override
    public void onBackPressed() {
        finish();
    }


    public void updateMassage(Cursor massageData) {

        MyMassagesAdapter adapter = new MyMassagesAdapter(mContext, null, 0);
        adapter.bindView(mMassageSelected, mContext, massageData);
    }


    public void updateList(Cursor massages) {
        if (massages.moveToFirst()) {
            mMyMassagesAdapter.swapCursor(massages);
        } else {
            onBackPressed();
        }
    }


    /**
     * Shows the treated zones of the clicked massage
     *
     * @param view One massage of the list
     */
    public void showTreatedZones(View view) {
        String s_idMassage = view.getTag().toString();

        mMassageSelected = (View) view.getParent();

        mIdMassageSelected = Long.parseLong(s_idMassage);

        Intent intent = new Intent(this, TreatedZonesActivity.class);

        Bundle b = new Bundle();
        b.putLong(TreatedZonesActivity.PARAMETER_IDMASSAGE,
                mIdMassageSelected);
        b.putBoolean(TreatedZonesActivity.PARAMETER_MASSAGE_FINISHED, true);
        intent.putExtras(b);

        startActivityForResult(intent, TREATED_ZONES_REQUEST_CODE);

    }


    /**
     * Save comments to massage when pressed the save button
     *
     * @param view Save button in a massage item
     */
    public void saveMassagePatient(View view) {
        boolean comesFromComments = false;
        if (view == null) {
            comesFromComments = true;
            view = mMassageSelected;
        } else {
            view.setVisibility(View.GONE);
            String s_idMassage = view.getTag().toString();
            mIdMassageSelected = Long.parseLong(s_idMassage);

            mMassageSelected = (View) view.getParent().getParent();
        }


        EditText patientText = (EditText)
                mMassageSelected.findViewById(R.id.patientText);

        patientText.clearFocus();

        presenter.savePatient(mIdMassageSelected,
                patientText.getText().toString());

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "SaveMassagePatient " + mIdMassageSelected);
        }

        if (!comesFromComments) saveMassageComments(null);

        presenter.updateList();
    }

    /**
     * Save comments to massage when pressed the save button
     *
     * @param view Save button in a massage item
     */
    public void saveMassageComments(View view) {
        boolean comesFromPatient = false;
        if (view == null) {
            comesFromPatient = true;
            view = mMassageSelected;
        } else {
            view.setVisibility(View.GONE);

            String s_idMassage = view.getTag().toString();
            mIdMassageSelected = Long.parseLong(s_idMassage);

            mMassageSelected = (View) view.getParent().getParent();
        }


        EditText commentsText = (EditText)
                mMassageSelected.findViewById(R.id.commentsText);

        commentsText.clearFocus();

        presenter.saveComments( mIdMassageSelected,
                commentsText.getText().toString());

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "SaveMassageComments " + mIdMassageSelected);
        }

        if (!comesFromPatient) saveMassagePatient(null);

        presenter.updateList();
    }

    /**
     * Deletes the clicked massage
     *
     * @param view One massage of the list
     */
    public void deleteMassage(View view) {
        final View passedView = view;
        new AlertDialog.Builder(this)
                .setTitle(mRes.getString(
                        R.string.myMassages_delete_massage_confirmation_title))
                .setMessage(
                        mRes.getString(
                                R.string.myMassages_delete_massage_confirmation_question))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                Long idMassage =
                                        Long.parseLong(passedView.getTag().toString());

                                presenter.deleteMassage(idMassage);

                                MiscUtils.showToast(mContext,
                                        mRes.getString(
                                                R.string.myMassages_massage_deleted));

                                if (BuildConfig.DEBUG) {
                                    Log.d(NAME,
                                            "DeleteMassage " +
                                                    String.valueOf(idMassage));
                                }



/*                                if (newCursor.getCount() == 0) {
                                    onBackPressed();
                                } else {
                                    ViewGroup parent =
                                            (ViewGroup) passedView.getParent();
                                    parent.removeAllViews();
                                    parent.setVisibility(View.GONE);
                                }
*/
                            }
                        }

                )
                .setNegativeButton(android.R.string.no, null)
                .show();
    }


    public void sendDeleteTracker(String label){
        MiscUtils.sendEventTracker(this, NAME, NAME, "DeleteMassage", label,
                -1L);
    }

    /**
     * Prepare email with summary of the clicked massage
     * @param view - Caller button
     */
    public void sendMassageSummary( View view ) {
        Long idMassage = Long.parseLong(view.getTag().toString());

        presenter.sendMassageSummary(idMassage);
    }


    public void sendEmail( MassageRecord massage, Cursor treatedZones){

        MassageMailer mailer = new MassageMailer();
        mailer.inject(massage, treatedZones, mContext, mRes);
        MiscUtils.sendEmail( this, "", mailer.subject(), mailer.summary());

        if (BuildConfig.DEBUG) {
            Log.d(NAME, "SendEmail " + String.valueOf(massage.getId()));
        }
        MiscUtils.sendEventTracker(this, NAME, NAME,
                "SendEmail", massage.getModeAbbr(), -1L);

    }



    /* ***************** */
	/* PRIVATE FUNCTIONS */
	/* ***************** */

}
