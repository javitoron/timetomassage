package com.fingerhits.massageassistant.massageSounds;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.utils.MiscUtils;

import javax.inject.Inject;

/**
 * Massage Sounds player
 *
 * @author javi on 16/11/2015.
 */
public class MassageSounds {
    private static MassageSounds instance;

    @Inject MySharedPreferences mPrefs;
    @Inject Context mContext;

    private int mIdDingTime;
    private int mIdHalfMassage;
    private int mIdFullMassage;
    private SoundPool mSoundPool;

    public String[] soundLabels;
    public TypedArray soundIds;


    @Inject
    protected MassageSounds(Context context, MySharedPreferences prefs){

        mContext = context;
        mPrefs = prefs;

        mSoundPool = loadSoundPool();

        soundLabels =
                mContext.getResources().getStringArray(R.array.sounds_array);
        soundIds =
                mContext.getResources().obtainTypedArray(R.array.soundIds_array);

        reloadAll();
    }


    //For testing purposes: to inject mock objects in a singleton like this
    public void setDependencies(Context context, MySharedPreferences prefs,
                                SoundPool soundPool) {
        mContext = context;
        mPrefs = prefs;
        mSoundPool = soundPool;
    }


    public static MassageSounds getInstance(Context context, MySharedPreferences prefs){
        if(instance == null){
            instance = new MassageSounds(context, prefs);
        }
        return instance;
    }


    public int getIdDingTime(){return mIdDingTime;}
    public void setIdDingTime(int idDingTime) { mIdDingTime = idDingTime; }

    public int getIdHalfMassage(){return mIdHalfMassage;}
    public void setIdHalfMassage(int idHalfMassage) { mIdHalfMassage = idHalfMassage; }

    public int getIdFullMassage(){return mIdFullMassage;}
    public void setIdFullMassage(int idFullMassage) { mIdFullMassage = idFullMassage; }

    public SoundPool getSoundPool() { return mSoundPool; }
    public void setSoundPool(SoundPool s) { mSoundPool = s; }


    public void reloadAll(){
        reloadDingTime();
        reloadHalfMassage();
        reloadFullMassage();
    }


    public void reloadDingTime(){
        try {
            mIdDingTime = loadSound(mSoundPool, mContext,
                    mPrefs.getInt(MySharedPreferences.PREFS_DINGTIMESOUND_KEY));
        }
        catch(Resources.NotFoundException nfe ){
            resetSoundValue(MySharedPreferences.PREFS_DINGTIMESOUND_KEY);
            reloadDingTime();
        }
        catch( Exception e){
        }
    }


    public void reloadHalfMassage(){
        try {
            mIdHalfMassage = loadSound(mSoundPool, mContext,
                    mPrefs.getInt(MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY));
        }
        catch(Resources.NotFoundException nfe ){
            resetSoundValue(MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY);
            reloadHalfMassage();
        }
        catch( Exception e){
        }
    }


    public void reloadFullMassage(){
        try {
            mIdFullMassage = loadSound(mSoundPool, mContext,
                    mPrefs.getInt(
                            MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY));
        }
        catch(Resources.NotFoundException nfe) {
            resetSoundValue(
                    MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY);
            reloadFullMassage();
        }
        catch( Exception e){
        }
    }

    public void playDingTime(){
        mSoundPool.play(mIdDingTime, 1, 1, 1, 0, 1);
    }


    public void playHalfMassage(){
        mSoundPool.play(mIdHalfMassage, 1, 1, 1, 0, 1);
    }


    public void playFullMassage(){
        mSoundPool.play(mIdFullMassage, 1, 1, 1, 0, 1);
    }


    public String dingTimeLabel(){
        return soundLabel(MySharedPreferences.PREFS_DINGTIMESOUND_KEY); //"DingTimeSound"
    }


    public String halfMassageLabel(){
        return soundLabel(MySharedPreferences.PREFS_HALFMASSAGEDURATIONSOUND_KEY);
    }


    public String fullMassageLabel(){
        return soundLabel(MySharedPreferences.PREFS_FULLMASSAGEDURATIONSOUND_KEY);
    }


    /********** PRIVATE FUNCTIONS ****/
    private String soundLabel(String soundTypeLabel){
        int soundId = mPrefs.getInt(soundTypeLabel);

        int soundIndex = MiscUtils.findInTypedArray(soundIds, soundId);

        if(soundIndex == -1){
            resetSoundValue(soundTypeLabel);
            soundIndex = MiscUtils.findInTypedArray(soundIds,
                    mPrefs.getInt(soundTypeLabel));
        }

        if(soundIndex < 0 || soundIndex >= soundLabels.length ) {
            return soundLabels[0];
        }
        else {
            return soundLabels[soundIndex];
        }
    }


    private int loadSound( SoundPool sp, Context context, int sound) {
        return sp.load(context, sound, 0);
    }


    private SoundPool loadSoundPool() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return getNewSoundPool();
        } else {
            return getOldSoundPool();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private SoundPool getNewSoundPool(){

        AudioAttributes mAudioAttributes;
        mAudioAttributes = new AudioAttributes.Builder()
                .setUsage( AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();

        return new SoundPool.Builder()
                .setAudioAttributes(mAudioAttributes)
                .build();

    }


    @SuppressWarnings("deprecation")
    private SoundPool getOldSoundPool(){
        return new SoundPool(5,AudioManager.STREAM_MUSIC,0);
    }


    private void resetSoundValue(String soundTypeLabel){
        mPrefs.removeValue(soundTypeLabel);
        mPrefs.saveInt(soundTypeLabel, mPrefs.getInt(soundTypeLabel));
    }

}