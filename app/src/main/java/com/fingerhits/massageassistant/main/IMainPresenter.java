package com.fingerhits.massageassistant.main;

/**
 * Presenter Interface for Home screen
 * Created by Javier Torón on 15/02/2017.
 */

public interface IMainPresenter {
    void initView();
    void showMassagesInfo();
    void saveMassagePatient(String s_patient);
    void saveMassageComments(String textToSave);

    }
