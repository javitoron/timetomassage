package com.fingerhits.massageassistant.main;

import com.fingerhits.massageassistant.di.ActivityScope;
import com.fingerhits.massageassistant.tips.ITipsInteractor;
import com.fingerhits.massageassistant.tips.TipsInteractor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Javier Torón on 16/02/2017.
 */

@Module
public class MainActivityModule {
    public final IMainView view;

    public MainActivityModule(IMainView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    IMainView provideIMainView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    IMainInteractor provideIMainInteractor(MainInteractor interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    IMainPresenter provideIMainPresenter(MainPresenter presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    ITipsInteractor provideITipsInteractor(TipsInteractor tipsInteractor) {
        return tipsInteractor;
    }

}
