package com.fingerhits.massageassistant.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.fingerhits.massageassistant.exportImport.ExportImportActivity;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.myMassages.MyMassagesActivity;
import com.fingerhits.massageassistant.options.OptionsActivity;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.tips.TipsActivity;
import com.fingerhits.massageassistant.utils.DateTimeUtils;
import com.fingerhits.massageassistant.utils.MiscUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 *
 * This is the launcher activity, the splash screen
 *
 * @author Javier Torón
 *
 * It contains:
 * 	* Massages area:
 *	*   * Button to start massage
 *  *	* Last massage stats
 *  *   * Button to go to the last one
 *	*   * Number of massages
 *  *   * Button My massages
 * 	* News and help Fragment
 * 	* Advertising bottom area
 *	* Menu:
 *	*	*	Options
 *  *   *   View massage stats (only for Javi)
 */
public class MainActivity extends Activity
        implements IMainView, OnConnectionFailedListener, ConnectionCallbacks  {

    private static final int MASSAGE_SESSION_REQUEST_CODE = 1;
    private static final int MY_MASSAGES_REQUEST_CODE = 2;
    private static final int OPTIONS_REQUEST_CODE = 4;
    private static final int TIPS_REQUEST_CODE = 5;
    private static final int EXPORTIMPORT_REQUEST_CODE = 5;

    @BindView(R.id.rootLayout) LinearLayout mRootLayout;

    @BindView(R.id.lastMassageLayout) LinearLayout mLastMassageLayout;
    @BindView(R.id.lastMassageText) TextView mLastMassageText;
    @BindView(R.id.resumeLastMassageBtn) Button mResumeLastMassageBtn;
    @BindView(R.id.patientText) EditText mPatientText;
    @BindView(R.id.commentsText) EditText mCommentsText;

    @BindView(R.id.myMassagesBtn) Button mMyMassagesBtn;
    @BindView(R.id.importBtn) Button mImportBtn;
    @BindView(R.id.playTutorialLayout) RelativeLayout mPlayTutorialLayout;
    @BindView(R.id.massagesCountText) TextView mMassagesCountText;

    @BindView(R.id.tipTxt) TextView mTipTxt;
    @BindView(R.id.bannerLayout) LinearLayout mBannerLayout;

    @BindString(R.string.main_last_massage_summary)
    String main_last_massage_summary;
    @BindString(R.string.tutorial_url) String tutorial_url;
    @BindString(R.string.app_url) String app_url;
    @BindString(R.string.escuelademasaje_url) String escuelademasaje_url;

    @BindString(R.string.support_email) String support_email;
    @BindString(R.string.main_massages_count_zero)
    String main_massages_count_zero;

    @BindColor(R.color.lightGreyBackground) int buttonBackgroundColor;

    @Inject public IMainPresenter presenter;
    @Inject Context context;

    private int mTipPos;
    private Long mLastMassageId;

    /**
     * Set up activity
     * @param savedInstanceState Not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        MainApp.get(this)
                .getAppComponent()
                .plus(new MainActivityModule(this))
                .inject(this);

        presenter.initView();

        MiscUtils.prepareActionBar(this);
        MiscUtils.logActivityStarting(this, this.NAME);
    }

    /**
     * Makes menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_actions, menu);


        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Manages menu options
     *
     * * options_menu: Call OptionsActivity
     * * back: finish app
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.options_menu:
                intent = new Intent(this, OptionsActivity.class);
                startActivityForResult(intent, OPTIONS_REQUEST_CODE);
                return true;

            case R.id.exportImport_menu:
                startImport();
                return true;

            case R.id.open_app_website_menu:
                openAppWebsite();
                return true;

            case R.id.open_tutorial_video_menu:
                viewTutorial();
                return true;

            case R.id.email_fingerhits:
                MiscUtils.sendEmail(this, support_email, "", "");
                return true;

            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Manages return from the massage session activity
     * @param requestCode To know what activity is coming from
     * @param resultCode RESULT_OK if it's a controlled return
     * @param data Data returned from the activity
     */
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {

/*        ContentResolver.requestSync(mAccount, SyncDBUtils.AUTHORITY,
                SyncDBUtils.getSyncBundle());
*/
        if (requestCode == MASSAGE_SESSION_REQUEST_CODE
                || requestCode == MY_MASSAGES_REQUEST_CODE
                || requestCode == OPTIONS_REQUEST_CODE
                || requestCode == EXPORTIMPORT_REQUEST_CODE) {
            presenter.showMassagesInfo();
        }

    }


    /**
     * Calls MassageSessionActivity when startMassageBtn is clicked
     *      with idMassage = null
     */
    @OnClick({ R.id.newMassageLayout, R.id.newMassageBtn, R.id.startMassageText })
    public void startMassageSession() {
        MiscUtils.showToast(context,
                getString(R.string.main_to_new_massage_msg));
        Intent intent = new Intent(this, MassageSessionActivity.class);
        startActivityForResult(intent, MASSAGE_SESSION_REQUEST_CODE);
    }


    /**
     * Calls MassageSessionActivity when startMassageBtn is clicked
     *      with idMassage != null
     */
    @OnClick( R.id.resumeLastMassageBtn)
    public void resumeLastMassageSession() {
        MiscUtils.showToast(context,
                getString(R.string.main_resume_massage_msg));
        Intent intent = new Intent(this, MassageSessionActivity.class);

        Bundle b = new Bundle();
        b.putLong(MassageSessionActivity.PARAMETER_IDMASSAGE, mLastMassageId);
        intent.putExtras(b);

        startActivityForResult(intent, MASSAGE_SESSION_REQUEST_CODE);
    }


    /**
     * Calls MyMassagesActivity when myMassagesBtn is clicked
     */
    @OnClick( R.id.myMassagesBtn)
    public void startMyMassages() {
        Intent intent = new Intent(this, MyMassagesActivity.class);
        startActivityForResult(intent, MY_MASSAGES_REQUEST_CODE);
    }


    /**
     * Opens a new intent to watch Youtube video
     */
    @OnClick( {R.id.viewTutorialBtn, R.id.playTutorialBtn })
    public void viewTutorial() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(tutorial_url)));
    }


    /**
     * Calls ExportImportActivity
     */
    @OnClick( R.id.importBtn)
    public void startImport() {
        Intent intent = new Intent(this, ExportImportActivity.class);
        startActivityForResult(intent, EXPORTIMPORT_REQUEST_CODE);
    }



    /**
     * Calls tipsActivity
     */
    @OnClick( R.id.tipLayout)
    public void viewTips() {
        Intent intent = new Intent(this, TipsActivity.class);

        Bundle b = new Bundle();
        b.putInt(TipsActivity.PARAMETER_TIPINDEX, mTipPos);
        intent.putExtras(b);

        startActivityForResult(intent, TIPS_REQUEST_CODE);
    }


    /**
     * Calls tipsActivity
     */
    @OnClick( R.id.bannerLayout)
    public void openBFWebsite() {
        MiscUtils.openURL(this, escuelademasaje_url);
    }


    /**
     * Open website in external browser
     */
    public void openAppWebsite() {
        MiscUtils.openURL(this, app_url);
    }


    public void noLastMassage() {
        mLastMassageLayout.setVisibility(View.GONE);
    }


    public void showLastMassage(MassageRecord lastMassage) {
        mLastMassageId = lastMassage.getId();

        mLastMassageLayout.setVisibility(View.VISIBLE);

        String s_patient = lastMassage.getPatient();

        mPatientText.setText(s_patient);
        setSavePatientButtonListener();

        Date startDateTime = lastMassage.getStartDateTime();

        String s_startDate = new SimpleDateFormat("dd/MM/yyyy",
                Locale.getDefault()).format(startDateTime);

        String s_startTime = new SimpleDateFormat("HH:mm",
                Locale.getDefault()).format(startDateTime);

        String s_duration = DateTimeUtils.durationToString(
                lastMassage.getDurationInMilliseconds());

        if( ( DateTimeUtils.now().getTime()
                - lastMassage.getEndDateTime().getTime() ) > 3600000 ){
            mResumeLastMassageBtn.setVisibility(View.GONE);
        }
        else{
            mResumeLastMassageBtn.setVisibility(View.VISIBLE);
        }

        mLastMassageText.setText(Html.fromHtml(String.format(
                main_last_massage_summary,
                s_startDate, s_startTime, s_duration)));

        mCommentsText.setText(lastMassage.getComments());
        setSaveCommentsButtonListener();

        mRootLayout.requestFocus();
    }



    /**
     * Save comments to massage when pressed the save button
     * @param view Save button in a massage item
     *
     */
    @SuppressWarnings("UnusedParameters")
    public void saveMassagePatient(View view) {
        presenterSavePatient(getPatient());
        mPatientText.clearFocus();

        if(view != null ) saveMassageComments(null);
    }

    public String getPatient(){
        return mPatientText.getText().toString();
    }

    public void presenterSavePatient(String patient){
        presenter.saveMassagePatient(patient);
    }


    /**
     * Save comments to massage when pressed the save button
     * @param view Save button in a massage item
     *
     */
    @SuppressWarnings("UnusedParameters")
    public void saveMassageComments(View view) {
        presenter.saveMassageComments(mCommentsText.getText().toString());
        mCommentsText.clearFocus();
        if(view != null ) saveMassagePatient(null);
    }


    public void showMassagesList( int numberOfMassages) {

        if(BuildConfig.DEBUG){
            Log.d(NAME, "mNumberOfMassages - " + numberOfMassages);
        }

        android.text.Spanned massagesCountText;
        if (numberOfMassages == 0) {
            mMyMassagesBtn.setVisibility(View.GONE);
            mImportBtn.setVisibility(View.VISIBLE);
            mPlayTutorialLayout.setVisibility(View.VISIBLE);
            massagesCountText = Html.fromHtml(main_massages_count_zero );
        } else {
            mMyMassagesBtn.setVisibility(View.VISIBLE);
            mImportBtn.setVisibility(View.GONE);
            mPlayTutorialLayout.setVisibility(View.GONE);
            massagesCountText = Html.fromHtml(
                    getResources().getQuantityString(R.plurals.main_massages_count,
                            numberOfMassages, numberOfMassages));

        }

        mMassagesCountText.setText(massagesCountText);
    }


    public void showTip(int i_tip, String s_tip){
        s_tip = MiscUtils.extractTitle(s_tip);

        mTipTxt.setText( s_tip );
        mTipPos = i_tip;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Connected to Google Play services!
        // The good stuff goes here.

        //MiscUtils.showToast(mContext, "Connected");
/*        mLastLocation = null; /*LocationServices.FusedLocationApi.getLastLocation(
                mGoogleLocationApiClient);

        if (mLastLocation == null) {
            MiscUtils.showToast(mContext, "No hay location");
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
            if(mAct==1) {
                if (((MainActivity) mContext).getmFirstTimeGps()) {
                    showGpsSettingsAlert(((MainActivity) mContext).getmLanguage());
                    ((MainActivity) mContext).setmFirstTimeGps(false);
                }
            }
        } else {
/*            MiscUtils.showToast(mContext,
                    "longitude: " +
                            String.valueOf(
                                    mLastLocation.getLongitude() )+
                            ". latitude: " +
                            String.valueOf(mLastLocation.getLatitude()));

            mPrefs.saveString(mPrefs.PREFS_USER_LAST_LATITUDE_KEY,
                    String.valueOf(mLastLocation.getLatitude()));
            mPrefs.saveString(mPrefs.PREFS_USER_LAST_LONGITUDE_KEY,
                    String.valueOf(mLastLocation.getLongitude()));
        }*/
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        // More about this in the 'Handle Connection Failures' section.
        //MiscUtils.showToast(mContext, "Not connected");
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
/*        mGoogleApiClient.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // : choose an action type.
                "Main Page", // : Define a title for the content shown.
                // : If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // : Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.fingerhits.massageassistant.main/http/host/path")
        );
        AppIndex.AppIndexApi.start(mGoogleApiClient, viewAction);*/
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        /*Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // : choose an action type.
                "Main Page", // : Define a title for the content shown.
                // : If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // : Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.fingerhits.massageassistant.main/http/host/path")
        );
        AppIndex.AppIndexApi.end(mGoogleApiClient, viewAction);
        mGoogleApiClient.disconnect();*/
    }

    /* ***************** */
	/* PRIVATE METHODS   */
	/* ***************** */

    private void setSavePatientButtonListener(){
        mPatientText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                Button mSaveButton = (Button) findViewById(
                        R.id.savePatientButton);
                if (hasFocus) {
                    showSaveButton(view, mSaveButton);
                } else {
                    hideSaveButton(view, mSaveButton);

                }
            }
        });
    }


    private void setSaveCommentsButtonListener(){
        mCommentsText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                Button mSaveButton = (Button) findViewById(
                        R.id.saveCommentsButton);
                if (hasFocus) {
                    showSaveButton(view, mSaveButton);
                } else {
                    hideSaveButton(view, mSaveButton);

                }
            }
        });
    }


    private void showSaveButton(View view, Button saveButton){
        saveButton.setVisibility(View.VISIBLE);
        view.setBackgroundColor(buttonBackgroundColor);
        mBannerLayout.setVisibility(View.GONE);

        //mScrollLayout.smoothScrollTo(0,
        //      view.getBottom() - mBannerLayout.getHeight() );
    }


    private void hideSaveButton(View view, Button saveButton){
        saveButton.setVisibility(View.GONE);
        view.setBackgroundColor(Color.TRANSPARENT);
        mBannerLayout.setVisibility(View.VISIBLE);
        InputMethodManager imm = (InputMethodManager)
                getSystemService( Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void showBanner(){
//        TextView openAppWebsiteTxt = (TextView) findViewById(R.id.openAppWebsiteTxt);

  //      openAppWebsiteTxt.setText(Html.fromHtml(mRes.getString(R.string.main_openWebsite_html)));
    }

}