package com.fingerhits.massageassistant.main;

import android.content.Context;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageAssistantDBHelper;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.utils.Installation;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 14/02/2017.
 */

public class MainInteractor implements IMainInteractor {


    @Inject MySharedPreferences mPrefs;
    @Inject MassageAssistantDBHelper mDatabaseHelper;
    @Inject Context mContext;

    @Inject
    public MainInteractor(){
    }


    public MySharedPreferences getSharedPreferences(){
        return mPrefs;
    }


    public int getNumberOfMassages(){
        mDatabaseHelper.cleanUpMassages();
        return mDatabaseHelper.numberOfMassages();
    }

    public long getLastMassageId(){
        return mDatabaseHelper.getLastInsertedMassageId();
    }

    public MassageRecord getLastMassage(){
        MassageRecord lastMassage = null;
        long lastIdMassage = getLastMassageId();

        if( lastIdMassage > 0 ) {
            lastMassage = mDatabaseHelper.getMassageRecord(lastIdMassage);
        }

        return lastMassage;
    }


    public void savePatientIntoMassage(long idMassage, String s_patient){
        mDatabaseHelper.savePatientToMassageRecord(idMassage, s_patient);
    }

    public void saveCommentsIntoMassage(long idMassage, String textToSave){
        mDatabaseHelper.saveCommentsToMassageRecord(idMassage, textToSave);
    }


    public void fixMinimalZoneDurationOldValue() {
        //Changing the default minimal zone duration from 60 to 0 for old installations
        if (!mPrefs.getBool(MySharedPreferences.PREFS_MINIMALZONEDURATION_FIXED_KEY)) {
            mPrefs.saveBool(MySharedPreferences.PREFS_MINIMALZONEDURATION_FIXED_KEY, true);

            if (mPrefs.getInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY) == 60) {
                mPrefs.saveInt(MySharedPreferences.PREFS_MINIMALZONEDURATION_KEY, 0);
            }
        }
    }


    public void fixWarningSchemaFrom1To2(){
        if(!mPrefs.getBool(mPrefs.PREFS_WARNINGS_SCHEMA_1_2_FIXED_KEY)){
            mPrefs.saveBool(mPrefs.PREFS_WARNINGS_SCHEMA_1_2_FIXED_KEY, true );

            if(mPrefs.contains(mPrefs.PREFS_SOUNDENABLED_KEY)) {
                mPrefs.saveBool(mPrefs.PREFS_VISUALWARNINGSENABLED_KEY,
                        mPrefs.getBool(mPrefs.PREFS_VISUALWARNINGENABLED_KEY));
                mPrefs.saveBool(mPrefs.PREFS_VIBRATIONWARNINGSENABLED_KEY,
                        mPrefs.getBool(mPrefs.PREFS_VIBRATIONWARNINGENABLED_KEY));
                mPrefs.saveBool(mPrefs.PREFS_SOUNDWARNINGSENABLED_KEY,
                        mPrefs.getBool(mPrefs.PREFS_SOUNDENABLED_KEY));

                mPrefs.saveBool(mPrefs.PREFS_DINGTIMEWARNINGSENABLED_KEY,
                        mPrefs.getBool(mPrefs.PREFS_DINGTIMESOUNDENABLED_KEY));
                mPrefs.saveBool(mPrefs.PREFS_HALFMASSAGEDURATIONWARNINGENABLED_KEY,
                        mPrefs.getBool(
                                mPrefs.PREFS_HALFMASSAGEDURATIONSOUNDENABLED_KEY));
                mPrefs.saveBool(mPrefs.PREFS_FULLMASSAGEDURATIONWARNINGENABLED_KEY,
                        mPrefs.getBool(
                                mPrefs.PREFS_FULLMASSAGEDURATIONSOUNDENABLED_KEY));
            }
        }
    }


    /* ***************** */
	/* PRIVATE METHODS   */
	/* ***************** */

    private void saveInstallationID() {
        //getLastLocation();
        String installationId =
                mPrefs.getString(mPrefs.PREFS_INSTALLATION_ID_KEY);

        if (installationId.equals("")) {
            mPrefs.saveString(mPrefs.PREFS_INSTALLATION_ID_KEY,
                    Installation.id(mContext));
        }

/*        if (!mPrefs.getBool(mPrefs.PREFS_INSTALLATION_ID_SENT_KEY)) {
            SyncUserDB syncUser = new SyncUserDB(mPrefs);

            syncUser.sendInstallationIdToMySQLDB(mRes, mPrefs);

            //sendGoogleAccount();
        }*/
    }

    private void getLastLocation(){
        // Create a GoogleApiClient instance for getting location
        //MiscUtils.showToast(mContext, "Creating api client for location");
/*        GoogleApiClient mGoogleLocationApiClient =
                new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
        mGoogleLocationApiClient.connect();*/
    }

    private void sendGoogleAccount() {
        /*Account mAccount =
                    SyncDBUtils.CreateSyncAccount(this, installationId);

        syncAdapter = new SyncAdapter(mContext, true);
        syncResult = new SyncResult();

        syncAdapter.onPerformSync( mAccount, SyncDBUtils.getSyncBundle(),
                SyncDBUtils.AUTHORITY, null, syncResult );

        ContentResolver.requestSync(mAccount, SyncDBUtils.AUTHORITY,
                          SyncDBUtils.getSyncBundle());*/
    }




    }
