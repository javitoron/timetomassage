package com.fingerhits.massageassistant.main;

import android.view.View;

import com.fingerhits.massageassistant.storedData.MassageRecord;

/**
 * View Interface for Home screen
 * Created by Javier Torón on 15/02/2017.
 */

public interface IMainView {
    static final String NAME = "Main";

    void startMassageSession();
    void resumeLastMassageSession();
    void startMyMassages();
    void viewTutorial();
    void startImport();
    void viewTips();

    void noLastMassage();
    void showLastMassage(MassageRecord lastMassage);
    void saveMassagePatient(View view);
    void saveMassageComments(View view);

    void showMassagesList( int numberOfMassages);

    void showTip(int i_tip, String s_tip);
}
