package com.fingerhits.massageassistant.main;

import com.fingerhits.massageassistant.options.MySharedPreferences;
import com.fingerhits.massageassistant.storedData.MassageRecord;

/**
 * Interactor Interface for Home screen
 * Created by Javier Torón on 15/02/2017.
 */

public interface IMainInteractor {
    MySharedPreferences getSharedPreferences();
    int getNumberOfMassages();
    long getLastMassageId();
    MassageRecord getLastMassage();
    void savePatientIntoMassage(long idMassage, String s_patient);
    void saveCommentsIntoMassage(long idMassage, String textToSave);
    void fixMinimalZoneDurationOldValue();
    void fixWarningSchemaFrom1To2();

}
