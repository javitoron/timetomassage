package com.fingerhits.massageassistant.main;

import com.fingerhits.massageassistant.di.ActivityScope;
import com.fingerhits.massageassistant.di.MainApp;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Subcomponent;

/**
 * Created by Javier Torón on 16/02/2017.
 */

@ActivityScope
@Subcomponent(modules = MainActivityModule.class)
public interface MainActivityComponent {
    void inject(MainActivity activity);
}