package com.fingerhits.massageassistant.main;

import android.app.Activity;
import android.util.Log;

import com.fingerhits.massageassistant.BuildConfig;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.tips.ITipsInteractor;
import com.fingerhits.massageassistant.utils.MiscUtils;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 14/02/2017.
 */

public class MainPresenter implements IMainPresenter{

    private IMainView mainView;
    private IMainInteractor mainInteractor;
    private ITipsInteractor tipsInteractor;

    private MassageRecord lastMassage;

    // Instance fields
    private int mNumberOfMassages;

    //private Location mLastLocation;

    @Inject
    public MainPresenter(IMainView mainView, IMainInteractor mainInteractor,
                         ITipsInteractor tipsInteractor){
        this.mainView = mainView;
        this.mainInteractor = mainInteractor;
        this.tipsInteractor = tipsInteractor;
    }


    public void initView() {
        //interactor.saveInstallationID();
        tipsInteractor.setUp(0, false);

        mainInteractor.fixMinimalZoneDurationOldValue();
        mainInteractor.fixWarningSchemaFrom1To2();

        showMassagesInfo();

        MiscUtils.sendEventTracker((Activity) mainView, mainView.NAME,
                mainView.NAME, "Massages", "Count",
                Long.valueOf(mNumberOfMassages));
    }


    public void showTip(){
        if( !tipsInteractor.allTipsShown() ){
            String s_tip = tipsInteractor.getTipString();
            int i_tip = tipsInteractor.getCurrentTip();
            mainView.showTip(i_tip, s_tip);
        }
    }

    public void showMassagesInfo() {

        mNumberOfMassages = mainInteractor.getNumberOfMassages();

        if( mNumberOfMassages == 0 ) {
            lastMassage = null;
        }
        else{
            lastMassage = mainInteractor.getLastMassage();

            if (lastMassage == null) {
                mNumberOfMassages = 0;
            }
        }
        mainView.showMassagesList(mNumberOfMassages);
        showLastMassage();
        showTip();
    }


    /**
     * Save Patient to massage
     * @param s_patient Patient to save in lastMassage
     *
     */
    @SuppressWarnings("UnusedParameters")
    public void saveMassagePatient(String s_patient) {
        long idMassage = lastMassage.getId();

        lastMassage.setPatient(s_patient);

        mainInteractor.savePatientIntoMassage(idMassage, s_patient);

        if (BuildConfig.DEBUG) {
            Log.d(mainView.NAME,
                    "SaveMassagePatient -> " + String.valueOf(idMassage));
        }
    }


    /**
     * Save comments to massage
     * @param textToSave Comments to save in lastMassage
     *
     */
    @SuppressWarnings("UnusedParameters")
    public void saveMassageComments(String textToSave) {
        long idMassage = lastMassage.getId();

        mainInteractor.saveCommentsIntoMassage( idMassage, textToSave);

        if (BuildConfig.DEBUG) {
            Log.d(mainView.NAME,
                    "SaveMassageComments -> " + String.valueOf(idMassage));
        }
    }


    /* ***************** */
	/* PRIVATE METHODS   */
	/* ***************** */
    private void showLastMassage() {
        if (mNumberOfMassages == 0) {
            mainView.noLastMassage();
        } else {
            mainView.showLastMassage(lastMassage);
        }
    }


}
