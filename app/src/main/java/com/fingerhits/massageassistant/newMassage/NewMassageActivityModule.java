package com.fingerhits.massageassistant.newMassage;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Javier Torón on 16/02/2017.
 */

@Module
public class NewMassageActivityModule {
    public final INewMassageView view;

    public NewMassageActivityModule(INewMassageView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    INewMassageView provideINewMassageView() {
        return this.view;
    }

    @Provides
    @ActivityScope
    INewMassageInteractor provideINewMassageInteractor(
            NewMassageInteractor interactor) {
        return interactor;
    }

    @Provides
    @ActivityScope
    INewMassagePresenter provideINewMassagePresenter(
            NewMassagePresenter presenter) {
        return presenter;
    }

}
