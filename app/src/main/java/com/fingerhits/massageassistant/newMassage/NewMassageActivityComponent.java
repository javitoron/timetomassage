package com.fingerhits.massageassistant.newMassage;

import com.fingerhits.massageassistant.di.ActivityScope;

import dagger.Subcomponent;

/**
 * Created by Javier Torón on 16/02/2017.
 */

@ActivityScope
@Subcomponent(modules = NewMassageActivityModule.class)
public interface NewMassageActivityComponent {
    void inject(NewMassageActivity activity);
}