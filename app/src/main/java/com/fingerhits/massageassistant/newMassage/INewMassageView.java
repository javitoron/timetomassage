package com.fingerhits.massageassistant.newMassage;

/**
 * Created by Javier Torón on 07/03/2017.
 */

public interface INewMassageView {
    public static String NAME = "NewMassage";

    public void setUpMassageDuration(int duration);
    public void setUpMode(int mode);

}
