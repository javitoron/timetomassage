package com.fingerhits.massageassistant.newMassage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.fingerhits.massageassistant.R;
import com.fingerhits.massageassistant.di.MainApp;
import com.fingerhits.massageassistant.massagesession.MassageSessionActivity;
import com.fingerhits.massageassistant.storedData.MassageRecord;
import com.fingerhits.massageassistant.storedData.TreatedZoneRecord;
import com.fingerhits.massageassistant.utils.GraphicUtils;
import com.fingerhits.massageassistant.utils.MiscUtils;

import javax.inject.Inject;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 
 * Shows the 5 positions as button in order to let the user to select one
 * 
 * @author Javier Torón
 * 
 * It contains 5 image buttons, one for mPosition
 * 
 * Can have 2 states:
 * * Selecting first mPosition: Coming from mainActivity (startMassageButton)
 *
 * * Changing Position: Coming from MassageSessionActivity, Remarked current
 *      mPosition.
 */
public class NewMassageActivity extends Activity implements INewMassageView{

   // private String mState; //First, ChangingPosition
    public static final String STATE_FIRST = "First";
    public static final String STATE_CHANGING = "Changing";

    public static final String PARAMETER_CURRENT_POSITION = "currentPosition";
    public static final String PARAMETER_DEFINE_MASSAGE = "defineMassage";


    @Inject
    public INewMassagePresenter presenter;
    @Inject Context context;

    private int mMode;

    @BindString(R.string.newMassageActivity_massageDurationLabel)
    String mMassageDurationLabel_text;


    @BindView(R.id.massageDurationLayout) LinearLayout mMassageDurationLayout;
    @BindView(R.id.massageDurationLabel) TextView mMassageDurationLabel;
    @BindView(R.id.massageDurationSeekBar) SeekBar mMassageDurationSeekBar;
    @BindView(R.id.massageModeRadio_simple)
    RadioButton mMassageModeRadioSimple;
    @BindView(R.id.massageModeRadio_intermediate)
    RadioButton mMassageModeRadioIntermediate;
    @BindView(R.id.massageModeRadio_advanced)
    RadioButton mMassageModeRadioAdvanced;

    @BindView(R.id.startMassageButtonLayout)
    LinearLayout mStartMassageButtonLayout;

    @BindView(R.id.positionSelectorLayout) LinearLayout mPositionSelectorLayout;
    @BindView(R.id.lyingFaceLayout) LinearLayout lyingFaceLayout;
    @BindView(R.id.lyingSideLayout) LinearLayout lyingSideLayout;
    @BindView (R.id.sittingLayout) LinearLayout sittingLayout;

    @BindView(R.id.lyingFaceDownBtn) ImageButton mLyingFaceDownBtn;
    @BindView(R.id.lyingFaceUpBtn) ImageButton mLyingFaceUpBtn;
    @BindView(R.id.lyingLeftSideBtn) ImageButton mLyingLeftSideBtn;
    @BindView(R.id.lyingRightSideBtn) ImageButton mLyingRightSideBtn;
    @BindView(R.id.sittingBackBtn) ImageButton mSittingBackBtn;
    @BindView(R.id.sittingFrontBtn) ImageButton mSittingFrontBtn;

    @BindDrawable(R.drawable.position_selector_button_selected)
    Drawable mButtonSelected_background;
    @BindDrawable(R.drawable.position_selector_button)
    Drawable mButtonBackground;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_massage);
        ButterKnife.bind(this);

        MainApp.get(this)
                .getAppComponent()
                .plus(new NewMassageActivityModule(this))
                .inject(this);

        presenter.initView();

        MiscUtils.prepareActionBar(this);
        MiscUtils.logActivityStarting(this, this.NAME);
    }


    /**
     * Handle presses on the action bar items (options or home/back)
     * @param item Action bar item pressed
     * @return True if menu processing was successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        this.setResult(RESULT_CANCELED);
        finish();
    }


    /**
     * startMassage in simple mode
     */
    @OnClick( R.id.newMassageBtn)
    public void startSimpleMassageSession(View view) {
        savePreferences();

        Intent intent = getIntent();
        intent.putExtra(MassageSessionActivity.PARAMETER_MODE,
                MassageRecord.MODE_SIMPLE);

        this.setResult(RESULT_OK, intent);
        finish();
    }


    /**
     * changeMode.
     *
     * @param view - Radio button mode selected
     */
    @OnClick( {R.id.massageModeRadio_simple, R.id.massageModeRadio_intermediate, R.id.massageModeRadio_advanced} )
    public void changeMode(View view) {
        if(view.getId() == R.id.massageModeRadio_simple ){
            mMode = MassageRecord.MODE_SIMPLE;
            prepareSimpleMode();
        }

        if(view.getId() == R.id.massageModeRadio_intermediate ){
            mMode = MassageRecord.MODE_INTERMEDIATE;
            prepareIntermediateAdvancedMode();
        }

        if(view.getId() == R.id.massageModeRadio_advanced ){
            mMode = MassageRecord.MODE_ADVANCED;
            prepareIntermediateAdvancedMode();
        }

    }

    /**
     * selectNewPosition. Pass mPosition selected to caller activity
     *
     * @param view - Position selected
     */
    @OnClick( {R.id.lyingFaceDownBtn, R.id.lyingFaceUpBtn, R.id.lyingRightSideBtn, R.id.lyingLeftSideBtn, R.id.sittingBackBtn, R.id.sittingFrontBtn} )
    public void selectPosition(View view) {

        savePreferences();

        Intent intent = getIntent();
        intent.putExtra(MassageSessionActivity.PARAMETER_MODE, mMode);

        if( view == null ) {
            intent.putExtra(MassageSessionActivity.PARAMETER_CODPOSITION, "");
        }
        else {
            GraphicUtils.setImageButtonBorder((ImageButton) view,
                    mButtonSelected_background);

            switch (view.getId()) {
                case R.id.lyingFaceDownBtn:
                    intent.putExtra(MassageSessionActivity.PARAMETER_CODPOSITION,
                            TreatedZoneRecord.CODPOSITION_FACEDOWN);
                    break;

                case R.id.lyingFaceUpBtn:
                    intent.putExtra(MassageSessionActivity.PARAMETER_CODPOSITION,
                            TreatedZoneRecord.CODPOSITION_FACEUP);
                    break;

                case R.id.lyingRightSideBtn:
                    intent.putExtra(MassageSessionActivity.PARAMETER_CODPOSITION,
                            TreatedZoneRecord.CODPOSITION_RIGHTSIDE);
                    break;

                case R.id.lyingLeftSideBtn:
                    intent.putExtra(MassageSessionActivity.PARAMETER_CODPOSITION,
                            TreatedZoneRecord.CODPOSITION_LEFTSIDE);
                    break;

                case R.id.sittingBackBtn:
                    intent.putExtra(MassageSessionActivity.PARAMETER_CODPOSITION,
                            TreatedZoneRecord.CODPOSITION_SITTINGBACK);
                    break;

                case R.id.sittingFrontBtn:
                    intent.putExtra(MassageSessionActivity.PARAMETER_CODPOSITION,
                            TreatedZoneRecord.CODPOSITION_SITTINGFRONT);
                    break;

                default:
                    intent.putExtra(MassageSessionActivity.PARAMETER_CODPOSITION, "");
                    break;
            }

        }
        this.setResult(RESULT_OK, intent);

        finish();

    //    clearPositionImages(); /* Avoid memory leaks */
    }


    public void setUpMassageDuration(int duration) {

        setMassageDurationLabel(duration);

        mMassageDurationSeekBar.setMax(23);
        mMassageDurationSeekBar.setProgress(( duration / 5 ) - 1);

        mMassageDurationSeekBar.setOnSeekBarChangeListener(
                new

                        SeekBar.OnSeekBarChangeListener() {
                            int progress = 0;

                            @Override
                            public void onProgressChanged(SeekBar seekBar,
                                                          int progressValue,
                                                          boolean fromUser) {
                                progress = progressValue + 1;
                                setMassageDurationLabel(progress * 5);
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {
                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {
                            }

                        }

        );
    }


    public void setUpMode(int mode){
        mMode = mode;
        switch (mode){
            case MassageRecord.MODE_SIMPLE:

                mMassageModeRadioSimple.setChecked(true);

                prepareSimpleMode();

                break;

            case MassageRecord.MODE_INTERMEDIATE:

                mMassageModeRadioIntermediate.setChecked(true);

                prepareIntermediateAdvancedMode();

                break;

            case MassageRecord.MODE_ADVANCED:
                mMassageModeRadioAdvanced.setChecked(true);

                prepareIntermediateAdvancedMode();

                break;
        }
    }


    /* *************** */
    /* PRIVATE METHODS */
    /* *************** */
    private void savePreferences() {
        presenter.savePreferences(
                (mMassageDurationSeekBar.getProgress() + 1) * 5,
                mMode);
    }

    private void setMassageDurationLabel(int duration) {
        mMassageDurationLabel.setText(
                String.format(mMassageDurationLabel_text, duration));
    }


    private void prepareSimpleMode(){
        mStartMassageButtonLayout.setVisibility( View.VISIBLE );
        mPositionSelectorLayout.setVisibility(View.GONE);
    }


    private void prepareIntermediateAdvancedMode(){
        mStartMassageButtonLayout.setVisibility( View.GONE);
        mPositionSelectorLayout.setVisibility(View.VISIBLE);
    }
}