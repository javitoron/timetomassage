package com.fingerhits.massageassistant.newMassage;

import android.content.Context;

import com.fingerhits.massageassistant.options.MySharedPreferences;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 07/03/2017.
 */

public class NewMassageInteractor implements INewMassageInteractor {
    @Inject public MySharedPreferences mPrefs;
    @Inject Context mContext;

    @Inject
    public NewMassageInteractor(){
    }

    public int getDuration(){
        return mPrefs.getInt(MySharedPreferences.PREFS_MASSAGEDURATION_KEY);
    }

    public void setDuration(int duration) {
        mPrefs.saveInt(MySharedPreferences.PREFS_MASSAGEDURATION_KEY, duration);
    }

    public int getMode(){
        return mPrefs.getInt(MySharedPreferences.PREFS_MODE_KEY);
    }

    public void setMode(int mode) {
        mPrefs.saveInt(MySharedPreferences.PREFS_MODE_KEY, mode);
    }


}
