package com.fingerhits.massageassistant.newMassage;

/**
 * Created by Javier Torón on 07/03/2017.
 */

public interface INewMassagePresenter {
    public void initView();
    public void savePreferences(int duration, int mode);
}
