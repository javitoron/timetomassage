package com.fingerhits.massageassistant.newMassage;

import javax.inject.Inject;

/**
 * Created by Javier Torón on 07/03/2017.
 */

public class NewMassagePresenter implements INewMassagePresenter {
    private INewMassageView view;
    private INewMassageInteractor interactor;

    @Inject
    public NewMassagePresenter(INewMassageView mainView,
                               INewMassageInteractor mainInteractor){
        this.view = mainView;
        this.interactor = mainInteractor;
    }

    public void initView() {
        view.setUpMassageDuration(interactor.getDuration());
        view.setUpMode(interactor.getMode());
    }


    public void savePreferences(int duration, int mode) {
        interactor.setDuration(duration);
        interactor.setMode(mode);
    }

}
