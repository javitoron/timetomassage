package com.fingerhits.massageassistant.newMassage;

/**
 * Created by Javier Torón on 07/03/2017.
 */

public interface INewMassageInteractor {
    public int getDuration();
    public void setDuration(int duration);
    public int getMode();
    public void setMode(int mode);
}
